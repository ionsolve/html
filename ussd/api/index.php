<?php

// requirements
require_once '../database.php';
require_once '../functions.php';
require_once '../AfricasTalkingGateway.php';

$sessionId    = $_POST["sessionId"];
$serviceCode  = $_POST["serviceCode"];
$phoneNumber  = $_POST["phoneNumber"];
$text         = $_POST["text"];
$date         = date('Y-m-d');

//3. Explode the text to get the value of the latest interaction - think 1*1
$textArray    = explode('*', $text);
$userResponse = trim(end($textArray));

$userArray    = array('phonenumber' => $phoneNumber);


$userAvail = returnExists('users', $userArray);

if($userAvail > 0)
{
    $pin            = getByValue('users','password',$userArray);
    $loanLimitValue = getByValue('users','netsalary',$userArray);
    $loanUserID     = getByValue('users','id',$userArray);

    $sumloans       = array('parent' => $loanUserID,'loanstatus' => 'active');
    $theSumOfLoans  = getSum('loans', 'loantaken', $sumloans);

    $loanLimit      = $loanLimitValue/2;
    $availableLoan  = $loanLimit - $theSumOfLoans;
}

if ($text == "") 
{   
    // check if number exists and serve menu
    if($userAvail < 1)
    {
        $response  = "END You are not registered for Emirates International Credit services. To register, call 0722553278.";
    }else{
	    $response  = "CON Hello ".getByValue('users','name',$userArray);
	    $response .= ". Please enter your PIN to continue.\n";
    }
}

else if ($text == $textArray[0]) 
{
    if($textArray[0] == $pin){
        $response  = "CON 1. Get A Loan \n";
        $response .= "2. Check Loan Limit\n";
        $response .= "3. View Loans Balance\n";
    }else{
        $response  = "END You entered an incorrect PIN.\n";
    }
}

else if ($text == $textArray[0]."*1") 
{
    $response  = "CON Enter Amount\n";
}

    else if ($text == $textArray[0]."*1*".$userResponse) 
    {
        if(ctype_digit($userResponse))
        {
            if($availableLoan >= $userResponse)
            {
                if($userResponse >=10)
                {
                    // give the loan
                    $status = giveLoan($phoneNumber,$userResponse);

                    if($status == "Queued")
                    {
                        //log loan into system
                        $loanref = uniqid();
                        mysqli_query($conn,"INSERT INTO `loans`(`parent`,`loantaken`,`loanstatus`,`datetaken`,`loanreference`)
                        VALUES('$loanUserID','$userResponse','active','$date','$loanref')");

                        $tl        = $theSumOfLoans + $userResponse;
                        $repayable = ceil(($tl * 108.5)/100);

                        $message = "You have received a loan of KES ".$userResponse." Reference ".$loanref." from Emirates Internatioal Credit. A total repayable loan of KES ".number_format($repayable)." will be deducted from your salary.";
                        opensendSMS($phoneNumber,$message);
                        $response  = "END Your loan has been processed, you will receive an SMS shortly.\n";
                    }else{
                        $response  = "END Something went wrong, please try again.\n";
                    }
                    

                }else{
                    $response  = "END The Amount should not be less than KES 10\n";
                }
            }else{
                $response  = "END The loan amount you requested is more than what you are allowed for this month\n";
            }
        }else{
            $response  = "END Amount should be a number\n";
        }
    }

else if ($text == $textArray[0]."*2") 
{
    $response = "END Your total loan limit is KES ".number_format($loanLimit)." and your available loan limit is KES ".number_format($availableLoan);
}

else if ($text == $textArray[0]."*3") 
{
    $totalRepayable = ceil(($theSumOfLoans * 108.5)/100);
    $response  = "END Your total repayable loan amount is KES ".number_format($totalRepayable);
}

else if ($text == $textArray[0]."*4") 
{
    $response  = "CON Enter your old PIN\n";
}

 
// Print the response onto the page so that our gateway can read it
header('Content-type: text/plain');
echo $response;


?>