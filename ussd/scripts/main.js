$(document).ready(function()
		{
			$('#register').click(function()
			{
				$('#register').html("<i class='fa fa-cog fa-spin'></i> processing");

				var type            = $('#type').val();
				var name            = $('#name').val();
				var idnumber        = $('#idnumber').val();
				var dob             = $('#dob').val();
				var phonenumber     = $('#phonenumber').val();
				var nextofkin       = $('#nextofkin').val();
				var address         = $('#address').val();
				var servicenumber   = $('#servicenumber').val();
				var rank            = $('#rank').val();
				var yearsinservice  = $('#yearsinservice').val();
				var previousstation = $('#previousstation').val();
				var currentstation  = $('#currentstation').val();
				var division        = $('#division').val();
				var county          = $('#county').val();
				var grosssalary     = $('#grosssalary').val();
				var netsalary       = $('#netsalary').val();

				$.post("register.php",
				{	
					type:type,
					name:name,
					idnumber:idnumber,
					dob:dob,
					phonenumber:phonenumber,
					nextofkin:nextofkin,
					address:address,
					servicenumber:servicenumber,
					rank:rank,
					yearsinservice:yearsinservice,
					previousstation:previousstation,
					currentstation:currentstation,
					division:division,
					county:county,
					grosssalary:grosssalary,
					netsalary:netsalary
				},

				function(addresponsedata)
				{
					if(addresponsedata == 1){
						location.reload();
					}else{
						$('#res').html(addresponsedata);
						$('#register').html('Save User');
					}
				});

			});


			$('#login').click(function()
			{
				$('#login').html("<i class='fa fa-cog fa-spin'></i> Processing");

				
				var email    = $('#email').val();
				var password = $('#password').val();

				$.post("login-user.php",
				{	
					email:email,
					password:password
				},

				function(loginresponse)
				{
					$('#response').html(loginresponse);
					$('#login').html('Log In');

					if (loginresponse == '<p style="color:green; font-size:20px;">Login Successful</p>') 
					{
						window.location.href = "index.php";
					};
				});

			});

			$('#logout').click(function()
			{
				load("logout.php");
			});



});