<?php
	require_once 'database.php';
	require_once 'functions.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Admin</title>
<meta content="" name="description">
<meta content="width=device-width,initial-scale=1,maximum-scale=1,minimal-ui" name="viewport">
<meta content="IE=edge" http-equiv="X-UA-Compatible">
<meta content="yes" name="apple-mobile-web-app-capable">
<meta content="black-translucent" name="apple-mobile-web-app-status-barstyle">
<link href="images/logo.png" rel="apple-touch-icon">
<meta content="" name="apple-mobile-web-app-title">
<meta content="yes" name="mobile-web-app-capable">
<link href="images/logo.png" rel="shortcut icon" sizes="196x196">
<link href="css/animate.css/animate.min.css" rel="stylesheet" type="text/css">
<link href="css/glyphicons/glyphicons.css" rel="stylesheet" type="text/css">
<link href="css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="css/material-design-icons/material-design-icons.css" rel="stylesheet" type="text/css">
<link href="css/ionicons/css/ionicons.min.css" rel="stylesheet" type="text/css">
<link href="css/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css">
<link href="css/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="css/styles/app.min.css" rel="stylesheet">
<link href="css/styles/font.css" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
<link href="codemirror/lib/codemirror.css" rel="stylesheet">
<style type="text/css">
	 ::-webkit-scrollbar { 
	   display: none; 
   }
</style>
</head>
<body>
	<div class="app" id="app">
		<div class="app-content box-shadow-z2 pjax-container" id="content" role="main">
			<div class="app-body">
				<div class="app-body-inner">
					<div class="row-col">
						<?php include_once 'top.php'; ?>
						<div class="row-row">
							<div class="row-col">
                				<?php include_once 'side.php'; ?>

								<div class="col-xs col-md-7" id="detail">
									<p class="text-md text-primary" style="margin-left: 5px;">All Loans</p>
									<div class="row-col white bg">
										<div class="row-row">
											<div class="row-body scrollable hover">
												<div class="row-inner">
													<div class="box-header text-muted"></div>
													<div style="padding:10px;">
														<a class="btn btn-primary btn-sm" href="createcsv.php">Download Excell</a>
													</div><br>
													<div class="table-responsive ">
														<table class="table table-bordered m-a-0">
															<thead>
																<tr class="text-primary">
																	<th>User </th>
																	<th>Total Loan Taken</th>
																	<th>Total Interest</th>	
                                                                    <th>Total Repayable</th>		
																	<th></th>
																</tr>
															</thead>
															<?php 
                                                                $dstl = mysqli_query($conn, "SELECT DISTINCT `parent` FROM `loans` WHERE `loanstatus`='active'");
                                                                    while($data = mysqli_fetch_array($dstl))
                                                                    {  
                                                                        $lparent        = $data['parent'];
                                                                        $userargs       = array('id' => $lparent);
                                                                        $loanargs       = array('parent' => $lparent, 'loanstatus' => 'active');
                                                                        $loansum        = getSum('loans', 'loantaken', $loanargs);
                                                                        $totalinterest  = ceil(($loansum * 8.5)/100);

                                                                        $totalRepay     = $loansum + $totalinterest;

															?>
															<tbody>
															
																<tr>
																	<td>
                                                                        <?php
                                                                            echo getByValue('users', 'name', $userargs);
                                                                        ?>
                                                                    </td>

																	<td><?php echo $loansum; ?></td>
																	<td><?php echo $totalinterest; ?></td>
																	<td><?php echo $totalRepay; ?></td>
																	
																	<td></td>
																</tr>
																
															<?php } ?>

                                                            <br><br>
                                                            
															
															</tbody>
														</table><br>

													</div>
                                                    <div class="box padding">                       
                                                            <?php
                                                                $loantotalargs       = array('loanstatus' => 'active');
                                                                $loantotalsum        = getSum('loans', 'loantaken', $loantotalargs);
                                                                $loanstotalinterest  = ceil(($loantotalsum * 8.5)/100);

                                                                echo "<h3>Total Loans Given: <b>KES ".number_format($loantotalsum)."</b></h3><br>";
                                                                echo "<h3>Total Interest:<b> KES ".number_format($loanstotalinterest)."</b></h3><br>";
                                                                echo "<h3>Total Repayable: <b>KES ".number_format($loantotalsum + $loanstotalinterest)."</b></h3><br>";
                                                            ?>
                                                    </div>
												</div>
											</div>
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> 
	<script src="scripts/app.min.js"></script>
	<script src="scripts/main.js"></script>
</body>
</html>