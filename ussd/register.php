<?php

            require_once 'database.php';
            require_once 'functions.php';
            // sanitize variables
            $type              = mysqli_real_escape_string($conn, $_REQUEST['type']);
            $name              = ucwords(mysqli_real_escape_string($conn, $_REQUEST['name']));
            $idnumber          = mysqli_real_escape_string($conn, $_REQUEST['idnumber']);
            $dob               = mysqli_real_escape_string($conn, $_REQUEST['dob']);
            $phonenumber       = mysqli_real_escape_string($conn, $_REQUEST['phonenumber']);
            $nextofkin         = ucwords(mysqli_real_escape_string($conn, $_REQUEST['nextofkin']));
            $address           = mysqli_real_escape_string($conn, $_REQUEST['address']);
            $servicenumber     = mysqli_real_escape_string($conn, $_REQUEST['servicenumber']);
            $rank              = mysqli_real_escape_string($conn, $_REQUEST['rank']);
            $yearsofservice    = mysqli_real_escape_string($conn, $_REQUEST['yearsinservice']);
            $previousstation   = ucwords(mysqli_real_escape_string($conn, $_REQUEST['previousstation']));
            $currentstation    = ucwords(mysqli_real_escape_string($conn, $_REQUEST['currentstation']));
            $division          = mysqli_real_escape_string($conn, $_REQUEST['division']);
            $county            = mysqli_real_escape_string($conn, $_REQUEST['county']);
            $grosssalary       = mysqli_real_escape_string($conn, $_REQUEST['grosssalary']);
            $netsalary         = mysqli_real_escape_string($conn, $_REQUEST['netsalary']);
            
            if(empty($type)){
                die('<font style="color:red">Please select formation</font>');
            }

            // validate name
            if(!preg_match("/^[a-zA-Z ]*$/",$name)) 
			{
			  die('<font style="color:red">The name should not contain special characters or numbers.</font>');
            }
            
            # verify username
			$verify_user = mysqli_query($conn, "SELECT * FROM `users` WHERE `servicenumber`='$servicenumber'");

			if(mysqli_num_rows($verify_user) > 0)
			{
				die('<font style="color:red">The servicenumber you gave is already registered.</font>');
            }

            $phone = "+254".substr($phonenumber,-9);
            
            # verify username
			$verify_phonenumber = mysqli_query($conn, "SELECT * FROM `users` WHERE `phonenumber`='$phone'");

			if(mysqli_num_rows($verify_phonenumber) > 0)
			{
				die('<font style="color:red">The phonenumber you gave is already registered.</font>');
            }
            
            # verify username
			$verify_idnumber = mysqli_query($conn, "SELECT * FROM `users` WHERE `idnumber`='$idnumber'");

			if(mysqli_num_rows($verify_idnumber) > 0)
			{
				die('<font style="color:red">The idnumber you gave is already registered.</font>');
			}

			$date         = date('Y-m-d');
            $password     = rand(1000,9999);

            $timestamp    = time();
            
            # insert into database
			$saveUser     =  "
            INSERT INTO `users`
            (`name`,
            `idnumber`,
            `dob`,
            `nextofkin`,
            `phonenumber`,
            `address`,
            `servicenumber`,
            `rank`,
            `previousstation`,
            `currentstation`,
            `division`,
            `county`,
            `yearsofservice`,
            `grosssalary`,
            `netsalary`,
            `type`,
            `accountnumber`,
            `password`,
            `date`) 
            
            VALUES('$name',
            '$idnumber',
            '$dob',
            '$nextofkin',
            '$phone',
            '$address',
            '$servicenumber',
            '$rank',
            '$previousstation',
            '$currentstation',
            '$division',
            '$county',
            '$yearsofservice',
            '$grosssalary',
            '$netsalary',
            '$type',
            '$timestamp',
            '$password',
            '$date')";

            if(mysqli_query($conn,$saveUser))
            {
                echo "1";

                $message      = "Dear ".$name.", You have successfully registered to Emirates international credit. Your PIN is ".$password.". To apply for a loan dial *483*20";
                
                opensendSMS($phone,$message);
                
            }else{
                die(mysqli_error($conn));
            }     


?>