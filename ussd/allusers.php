<?php
	require_once 'database.php';
	require_once 'functions.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Admin</title>
<meta content="" name="description">
<meta content="width=device-width,initial-scale=1,maximum-scale=1,minimal-ui" name="viewport">
<meta content="IE=edge" http-equiv="X-UA-Compatible">
<meta content="yes" name="apple-mobile-web-app-capable">
<meta content="black-translucent" name="apple-mobile-web-app-status-barstyle">
<link href="images/logo.png" rel="apple-touch-icon">
<meta content="" name="apple-mobile-web-app-title">
<meta content="yes" name="mobile-web-app-capable">
<link href="images/logo.png" rel="shortcut icon" sizes="196x196">
<link href="css/animate.css/animate.min.css" rel="stylesheet" type="text/css">
<link href="css/glyphicons/glyphicons.css" rel="stylesheet" type="text/css">
<link href="css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="css/material-design-icons/material-design-icons.css" rel="stylesheet" type="text/css">
<link href="css/ionicons/css/ionicons.min.css" rel="stylesheet" type="text/css">
<link href="css/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css">
<link href="css/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="css/styles/app.min.css" rel="stylesheet">
<link href="css/styles/font.css" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
<link href="codemirror/lib/codemirror.css" rel="stylesheet">
<style type="text/css">
	 ::-webkit-scrollbar { 
	   display: none; 
   }
</style>
</head>
<body>
	<div class="app" id="app">
		<div class="app-content box-shadow-z2 pjax-container" id="content" role="main">
			<div class="app-body">
				<div class="app-body-inner">
					<div class="row-col">
						<?php include_once 'top.php'; ?>
						<div class="row-row">
							<div class="row-col">
                				<?php include_once 'side.php'; ?>

								<div class="col-xs col-md-7" id="detail">
									<p class="text-md text-primary" style="margin-left: 5px;">All Users <button data-toggle="modal" data-target="#m-a-a" class="btn rounded white">Add New User</button></p>

									<div class="row-col white bg">
										<div class="row-row">
											<div class="row-body scrollable hover">
												<div class="row-inner">
													<div class="box-header text-muted"></div>
													<div style="padding:10px;">
														
													</div>
													<div class="table-responsive">
														<table class="table table-bordered m-a-0">
															<thead>
																<tr class="text-primary">
																	<th>Date </th>
																	<th>System Number</th>
																	<th>Name</th>
																	<th>Current Station</th>
																	<th>Phone Number</th>
																	<th>Service Number</th>
																	<th>Loan Limit</th>
																	
																	<th>Action</th>
																	<th></th>
																</tr>
															</thead>
															<?php 
																$allusers = returnArrayOfRequestPlain('users','id'); 
																$exusers = explode(",", $allusers);
															?>
															<tbody>
															<?php
																foreach ($exusers as $user) {
																
																	$pargs = array('id' => $user);
															?>
																<tr>
																	<td><?php echo getByValue('users', 'date', $pargs); ?></td>
																	<td><?php echo getByValue('users', 'type', $pargs).getByValue('users', 'accountnumber', $pargs); ?></td>
																	<td><a class="text-primary" href="#"><?php echo getByValue('users', 'name', $pargs); ?></a></td>
																	<td><?php echo getByValue('users', 'currentstation', $pargs); ?></td>
																	<td><?php echo getByValue('users', 'phonenumber', $pargs); ?></td>
																	<td><?php echo getByValue('users', 'servicenumber', $pargs); ?></td>
																	<td>
																		<?php 
																			$loannet = getByValue('users', 'netsalary', $pargs); 
																			$allowed = $loannet/2;

																			echo $allowed;
																		?>
																	</td>
																	
																	
																	<td>
																		<a class="text-primary" href="delete.php?id=<?php echo $user; ?>" >Remove Record</a>
																	</td>
																	
																	<td></td>
																</tr>
																
															<?php } ?>
															
															</tbody>
														</table><br>

													</div>
												</div>
											</div>
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


    			<div class="modal fade animate " data-backdrop="true" id="m-a-a">
					<div class="modal-dialog modal-lg" id="animate">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title">New User</h5>
							</div>
								<br>									
								
									<div class="row row-sm padding">

										<div class="col-sm-12">

											<select class="form-control" id="type">
												<option value="">-- Please select formation --</option>
												<option value="KPS">KPS</option>
												<option value="APS">APS</option>
												<option value="DCI">DCI</option>					
											</select>

										</div>
										<div class="col-sm-4">
											<div class="md-form-group float-label">
												<input id="name" class="md-input" required><label>Name</label>
											</div>
										</div>
										<div class="col-sm-4">
											<div class="md-form-group float-label">
												<input id="idnumber" type="number" class="md-input" required><label>ID Number</label>
											</div>
										</div>
										<div class="col-sm-4">
											<div class="md-form-group float-label">
												<input id="dob" type="date" class="md-input" required><label>Date Of Birth</label>
											</div>
										</div>
										
										<div class="col-sm-4">
											<div class="md-form-group">
												<input id="phonenumber" type="number" class="md-input" required><label>Phone Number</label>
											</div>
										</div>
										<div class="col-sm-4">
											<div class="md-form-group">
												<input id="nextofkin" class="md-input" required><label>Next Of Kin</label>
											</div>
										</div>
										<div class="col-sm-4">
											<div class="md-form-group">
												<input id="address" class="md-input" required><label>Address</label>
											</div>
										</div>

										<div class="col-sm-4">
											<div class="md-form-group">
												<input id="servicenumber" class="md-input" required><label>Service Number</label>
											</div>
										</div>
										<div class="col-sm-4">
											<div class="md-form-group">
												<input id="rank" class="md-input" required><label>Rank</label>
											</div>
										</div>
										<div class="col-sm-4">
											<div class="md-form-group">
												<input id="yearsinservice" type="number" class="md-input" required><label>Years In Service</label>
											</div>
										</div>

										<div class="col-sm-4">
											<div class="md-form-group">
												<input id="previousstation" class="md-input" required><label>Previous Station</label>
											</div>
										</div>
										<div class="col-sm-4">
											<div class="md-form-group">
												<input id="currentstation" class="md-input" required><label>Current Station</label>
											</div>
										</div>
										<div class="col-sm-4">
											<div class="md-form-group">
												<input id="division" class="md-input" required><label>Division</label>
											</div>
										</div>

										<div class="col-sm-4">
											<div class="md-form-group">
												<input id="county" class="md-input" required><label>County</label>
											</div>
										</div>
										<div class="col-sm-4">
											<div class="md-form-group">
												<input id="grosssalary" type="number" class="md-input" required><label>Gross Salary</label>
											</div>
										</div>
										<div class="col-sm-4">
											<div class="md-form-group">
												<input id="netsalary" type="number" class="md-input" required><label>Net Salary</label>
											</div>
										</div>
										
									</div>
								<div class="padding">
									<p id="res"></p>
								</div>
																	
							<div class="modal-footer">
								<button class="btn dark-white p-x-md" data-dismiss="modal" type="button">No</button> 
								<button id="register" class="btn primary p-x-md" type="button">Save User</button>
							</div>
						</div>
					</div>
				</div>




	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> 
	<script src="scripts/app.min.js"></script>
	<script src="scripts/main.js"></script>
</body>
</html>