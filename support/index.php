<?php

/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
*/

session_start();

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport">
	<meta content="" name="description">
	<meta content="" name="Ionsolve">
	<link href="../assets/img/logo_small.png" rel="icon" type="image/x-icon">
	<title>IonSolve : Support</title>
	<link href="../assets/css/app.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
</head>
<body class="loaded">
	
	<div class="paper-loading" id="app">
		
		<?php include_once '../com_header_light.php'; ?>

		<main>
            <section class="b-t text-center animatedParent white animateOnce">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-lg-2"></div>
                        <div class="col-12 col-lg-8">
                            <header class="section-heading">
                                <h2 class="styleprimary"><br>Contact our sales team</h2>
                                <h5 class="stylemuted">Our team is happy to answer your questions. Fill out <br>the form and we’ll get back as soon as possible.</h5>
                            </header>
                        </div>
                        <div class="col-12 col-lg-2"></div>
                    </div>
                   
                </div>
            </section>
            <section class="service-blocks grey lighten-5 ">
                <div class="container" >
                    <div class="row" >
                        <div class="col-12 col-lg-1"></div>

                        <div class="col-12 col-lg-5 b-r">
                            <div class="service-featured-content p-t-b-40">
                                <p class="stylemuted">
                                For direct contact, please send your query to the respective departmental contacts. 
                                </p>
                                    <div class="service-block">
                                        <div class="service-icon">
                                            <i aria-hidden="true" class="ion-ios-paper"></i>
                                        </div>
                                        <div class="service-content">
                                            <h6>Sales</h6>
                                            <p>info@ionsolve.com</p>
                                            <p>0782 810 000</p>
                                        </div>
                                    </div>

                                    <div class="service-block">
                                        <div class="service-icon">
                                            <i aria-hidden="true" class="ion-ios-gear"></i>
                                        </div>
                                        <div class="service-content">
                                            <h6>Support</h6>
                                            <p>support@ionsolve.com</p>
                                            <p>0782 810 000</p>
                                        </div>
                                    </div>
                                    <div class="service-block">
                                        <div class="service-icon">
                                            <i aria-hidden="true" class="ion-card"></i>
                                        </div>
                                        <div class="service-content">
                                            <h6>Accounts</h6>
                                            <p>accounts@ionsolve.com</p>
                                            <p>0782 810 000</p>
                                        </div>
                                    </div>
                            </div>
                        </div>

                        <div class="col-12 col-lg-6" style="">
                            <div class="service-featured-content p-t-b-40">
                            <div class="row">
                                <div class="col-md-12">
                                <form action="https://formspree.io/info@ionsolve.com" method="post">
                                    <div class="row">
                                        <div class="col-12">
                                            <label class="required" for="company">Your Name</label> 
                                            <input class="form-control form-control-lg" id="name" name="name" type="text">
                                        </div>
                                        <div class="col-12">
                                            <label class="required" for="company">Your Email</label> 
                                            <input class="form-control form-control-lg" id="email" name="email" type="email">
                                        </div>
                                        <div class="col-12">
                                            <label class="required" for="company">Your Phone Number</label> 
                                            <input class="form-control form-control-lg" id="phone" name="phone" type="text">
                                        </div>
                                        
                                        </div><label class="required" for="message">Message</label>
                                        <textarea class="form-control-lg" id="message" name="message" placeholder="Message"></textarea>
                                       
                                        <div class="text-center p-t-20">
                                            <input type="submit" class="btn btn-primary btn-lg" value="Send Message"/>
                                        </div>
                                </form>
                                </div>
                            </div>
                            </div>
                        </div>

                        
                    </div>
                </div>
                
            </section>

		</main>
		
		<?php include_once '../com_footer.php'; ?>

	</div>
	
	<script src="../assets/js/app.js"></script>
     <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-114404103-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-114404103-1');
</script>                               
</body>
</html>