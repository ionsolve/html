package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
)

const messageHost string    = "https://ionsolve.com/api/sms/send"

// EDIT ACCOUNT DETAILS
var x_username string       = "kanyi"
var x_apikey string         = "kanyiken"

func main() {
	// multiple contacts comma separated
	var phoneNumbers string = "+254790807760"
	// and of course we want our recipients to know what we really do
	var message string      = "It's song and dance until someone does rm -rf music"
	// default value is Ionsolve
	var senderId string         = ""

	// building the post params
	addparams := url.Values{}
	addparams.Set("phoneNumbers", phoneNumbers)
	addparams.Set("message", message)
	addparams.Set("senderId", senderId)

	// Values.Encode() encodes the post values into "URL encoded"
	params := addparams.Encode()

	// Perform the post
	req, err := http.NewRequest("POST", messageHost, strings.NewReader(params))
	
	if err != nil {
		fmt.Printf("Error: %v\n", err)
		return
	}

	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("Accept", "application/json")
	req.Header.Add("x-api-user",x_username)
	req.Header.Add("x-api-key",x_apikey)
	
	c := &http.Client{}
	resp, err := c.Do(req)
	
	if err != nil {
		fmt.Printf("Error: %v\n", err)
		return
	}
	
	defer resp.Body.Close()

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Printf("Error: %v\n", err)
		return
	}

	fmt.Println(string(data))
}
