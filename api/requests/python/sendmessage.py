import urllib
import urllib2
import json

# EDIT ACCOUNT DETAILS
x_username      = ""
x_apikey        = ""

phoneNumbers    = "+254711xxxxxx,+41785xxxxxx" # multiple contacts comma separated
message         = "It's song and dance until someone does rm -rf music"
senderId        = "" # default value is Ionsolve

# message endoint
messageHost     = "https://ionsolve.com/api/sms/send";

params = {
    'phoneNumbers' : phoneNumbers,
    'message' : message,
    'senderId' : senderId
}

req = urllib2.Request(messageHost, json.dumps(params), 
headers={
    'Content-type': 'application/json', 
    'Accept': 'application/json',
    'x-api-user': x_username,
    'x-api-key' : x_apikey
})

response = urllib2.urlopen(req)

resp = response.read()

print resp