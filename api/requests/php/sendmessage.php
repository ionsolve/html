<?php
/**
 * 2018 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2018 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
 *  
*/

// EDIT ACCOUNT DETAILS
$x_username      = "kanyi";
$x_apikey        = "kanyiken";

// EDIT MESSAGE PARAMS
$createMessage   = array(
    "phoneNumbers" => "+254790807760", // multiple contacts comma separated
    "message"      => "It's song and dance until someone does rm -rf music", 
    "senderId"     => "" // default value is Ionsolve
);

###################### LEAVE THIS AS IT IS ########################
##################################################################
// message endoint
$messageHost     = "https://ionsolve.com/api/sms/send";

// encoding params
$params          = json_encode($createMessage);
$req             = curl_init($messageHost);

curl_setopt($req, CURLOPT_CUSTOMREQUEST, "POST");
curl_setopt($req, CURLOPT_TIMEOUT, 60);
curl_setopt($req, CURLOPT_SSL_VERIFYPEER, FALSE);
curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
curl_setopt($req, CURLOPT_POSTFIELDS, $params);
curl_setopt($req, CURLOPT_HTTPHEADER, array(
    'Content-Type: application/json',
    'Content-Length: '.strlen($params),
    'x-api-user: '.$x_username, 
    'x-api-key: '.$x_apikey
));

// read api response
$res              = curl_exec($req);

// close curl
curl_close($req);
#################################################################
############################# UPTO HERE #########################

// print the json response
var_dump($res);

?>