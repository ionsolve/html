<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Unisport</title>
		<meta content="" name="description">
		<meta content="width=device-width,initial-scale=1,maximum-scale=1,minimal-ui" name="viewport">
		<meta content="IE=edge" http-equiv="X-UA-Compatible">
		<meta content="yes" name="apple-mobile-web-app-capable">
		<meta content="black-translucent" name="apple-mobile-web-app-status-barstyle">
		<link href="images/logo.png" rel="apple-touch-icon">
		<meta content="" name="apple-mobile-web-app-title">
		<meta content="yes" name="mobile-web-app-capable">
		<link href="images/logo.png" rel="shortcut icon" sizes="196x196">
		<link href="css/animate.css/animate.min.css" rel="stylesheet" type="text/css">
		<link href="css/glyphicons/glyphicons.css" rel="stylesheet" type="text/css">
		<link href="css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
		<link href="css/material-design-icons/material-design-icons.css" rel="stylesheet" type="text/css">
		<link href="css/ionicons/css/ionicons.min.css" rel="stylesheet" type="text/css">
		<link href="css/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css">
		<link href="css/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css">
		<link href="css/styles/app.min.css" rel="stylesheet">
		<link href="css/styles/font.css" rel="stylesheet" type="text/css">
	</head>
<body>
	<div class="app" id="app">
		<div class="padding">
			<div class="navbar">
				<div class="pull-center">
					<a class="navbar-brand" href="#">
					
					<img alt="." class="hide" src="images/logo.png"> 
					<span class="hidden-folded inline">Unisport Login</span></a>
				</div>
			</div>
		</div>
		<div class="b-t">
			<div class="center-block w-xxl w-auto-xs p-y-md text-center">
				<div class="p-a-md">
					
					<form action="log.php" method="post" id="form" name="form">
						<div class="form-group">
							<input class="form-control" name="username" placeholder="Username" required="" type="text">
						</div>
						<div class="form-group">
							<input class="form-control" name="password" placeholder="Password" required="" type="password">
						</div>
						<div class="m-b-md">
						
						<h3 class="text-warning">
							<?php 
								if(isset($_GET['error']))
									{ 
										echo $_GET['error'];
								} 
							?>
						</h3>
						</div><button class="btn btn-lg primary p-x-lg" type="submit">Sign in</button>
					</form>
				
				</div>
			</div>
		</div>
	</div>
	<script src="scripts/app.min.js">
	</script>
</body>
</html>