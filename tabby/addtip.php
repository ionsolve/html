<?php

session_start();

date_default_timezone_set('Africa/Nairobi');

if(!isset($_SESSION['superadmin']))
{
	header('location:signin.php');
}

require_once 'database.php';
require_once 'functions.php';

if(isset($_REQUEST['tipamount']) AND isset($_REQUEST['tipmessage']))
{
    $tipamount    = mysqli_real_escape_string($conn, $_REQUEST['tipamount']);
    $tipmessage   = mysqli_real_escape_string($conn, $_REQUEST['tipmessage']);


    if(empty($tipamount) OR empty($tipmessage))
    {
        die('<font style="color:red">Please fill all fields</font>');
    }

    if(!ctype_digit($tipamount))
    {
        die('<font style="color:red">The amount has to be a number.</font>');
    }

    $date = date('d-m-Y');

    $existsArray = array('message' => $tipmessage, 'date' => $date);

    if(returnExists('tips', $existsArray) == 0)
    {
        mysqli_query($conn, "INSERT INTO `tips`(`message`,`amount`,`date`,`status`) 
        VALUES('$tipmessage','$tipamount','$date','complete')");

        echo "1";
    }else{
        die('<font style="color:red">The entry is a double entry for today.</font>');
    }
}

?>