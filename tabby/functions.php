
<?php
/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
 */

// Create Search String From Array

function formSearchString($arguments)
{
    $string = "";
    foreach ($arguments as $key => $value) {
        $string .= "`" . $key . "`=" . "'" . $value . "' && ";
    }
    
    $conditions = substr($string, 0, -3);
    return $conditions;
}

function returnExists($table, $arguments)
{
    global $conn;
    $appendSearch = formSearchString($arguments);
    $formedQuery  = "SELECT * FROM $table WHERE " . $appendSearch . " ORDER BY id DESC";
    $getValues    = mysqli_num_rows(mysqli_query($conn, $formedQuery));
    return $getValues;
}

function getByValue($table, $column, $arguments)
{
    global $conn;
    $appendSearch = formSearchString($arguments);
    $formedQuery  = "SELECT * FROM $table WHERE " . $appendSearch . " ORDER BY id DESC";
    $executeQuery = mysqli_query($conn, $formedQuery);
    if (mysqli_num_rows($executeQuery) > 0) {
        $getValues = mysqli_fetch_array($executeQuery);
        return $getValues[$column];
    } else {
        return false;
    }
}


function returnArrayOfRequestPlain($table,$column)
{
    global $conn;
    $formedQuery     = "SELECT $column FROM $table ORDER BY id DESC";
    $run_Array_fetch = mysqli_query($conn, $formedQuery);
    $getValues       = mysqli_num_rows($run_Array_fetch);

    if($getValues > 0){
        $feedback = "";
        while($array_results = mysqli_fetch_array($run_Array_fetch)){
            $feedback .= $array_results[$column].",";
        }
        return substr($feedback,0, -1);
    }else{
        return "0";
    }
    
}

function returnArrayOfRequest($table,$column,$arguments)
{
    global $conn;
    $appendSearch    = formSearchString($arguments);
    $formedQuery     = "SELECT $column FROM $table WHERE " . $appendSearch." ORDER by id DESC";
    $run_Array_fetch = mysqli_query($conn, $formedQuery);
    $getValues       = mysqli_num_rows($run_Array_fetch);

    if($getValues > 0){
        $feedback = "";
        while($array_results = mysqli_fetch_array($run_Array_fetch)){
            $feedback .= $array_results[$column].",";
        }
        return substr($feedback,0, -1);
    }else{
        return "0";
    }
    
}

function returnCountWithCondition($table, $arguments)
{
    global $conn;
    $appendSearch = formSearchString($arguments);
    $result       = mysqli_query($conn, "SELECT COUNT(*) AS `id` FROM $table WHERE " . $appendSearch. " ORDER by id DESC");
    $row          = mysqli_fetch_assoc($result);
    $count        = $row['id'];
    return $count;
}

function returnSumWithCondition($table, $arguments)
{
    global $conn;
    $appendSearch = formSearchString($arguments);
    $sql          = mysqli_query($conn, "SELECT SUM(amount) as total FROM $table WHERE " . $appendSearch);
    $row          = mysqli_fetch_array($sql);
    $sum          = $row['total'];
    return $sum;
}

function returnCountNoCondition($table)
{
    global $conn;
    $appendSearch = formSearchString($arguments);
    $result       = mysqli_query($conn, "SELECT COUNT(*) AS `id` FROM $table");
    $row          = mysqli_fetch_assoc($result);
    $count        = $row['id'];
    return $count;
}

function getManyByValue($table, $column, $arguments)
{
    global $conn;
    $databack     = "";
    $appendSearch = formSearchString($arguments);
    $formedQuery  = "SELECT * FROM $table WHERE " . $appendSearch . " ORDER BY id DESC";
    $executeQuery = mysqli_query($conn, "$formedQuery");
    if (mysqli_num_rows($executeQuery) > 0) {
        while ($getValues = mysqli_fetch_array($executeQuery)) {
            $databack .= $getValues[$column] . ",";
        }
        
        $columnArray = substr($databack, 0, -1);
        return explode(",", $columnArray);
    } else {
        return "No Data Found";
    }
}

function getSum($table, $column, $arguments)
{
    global $conn;
    $databack     = 0;
    $appendSearch = formSearchString($arguments);
    $formedQuery  = "SELECT * FROM $table WHERE ".$appendSearch." ORDER BY id DESC";
    $executeQuery = mysqli_query($conn, "$formedQuery");
    if (mysqli_num_rows($executeQuery) > 0) 
    {
        while ($getValues = mysqli_fetch_array($executeQuery)) 
        {
            $databack = $databack + $getValues[$column];
        }
        return $databack;
    } else {
        return "No Data Found";
    }
}

function opensendSMS($phone,$message)
{
    // EDIT ACCOUNT DETAILS
    $x_username      = "tabitha";
    $x_apikey        = "6e65a8f47a8bc84cdaf565fb404d2f39d19aa32e";

    // EDIT MESSAGE PARAMS
    $createMessage   = array(
        "phoneNumbers" => $phone, // multiple contacts comma separated
        "message"      => $message, 
        "senderId"     => "" // default value is Ionsolve
    );

    // message endoint
    $messageHost     = "https://ionsolve.com/api/sms/send";

    // encoding params
    $params          = json_encode($createMessage);
    $req             = curl_init($messageHost);

    curl_setopt($req, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($req, CURLOPT_TIMEOUT, 60);
    curl_setopt($req, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($req, CURLOPT_POSTFIELDS, $params);
    curl_setopt($req, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: '.strlen($params),
        'x-api-user: '.$x_username, 
        'x-api-key: '.$x_apikey
    ));

    // read api response
    $res              = curl_exec($req);

    // close curl
    curl_close($req);
}

function sendMobile($amount)
{
    require_once "AfricasTalkingGateway.php";

    $use = array(
        "unisport" => array(
            "username" => "unisport",
            "apikey" => "36f5d84c96968d3c359c79f1394c06c74c2d752b8e5b4f76b4c46a96431247cb",
            "productname" => "unisport"
        )
    );

    $baseroot     = "unisport";

    $gateway      = new AfricasTalkingGateway($use[$baseroot]['username'], $use[$baseroot]['apikey']);
    $productName  = $use[$baseroot]['productname'];
    $currencyCode = "KES";

    $recipient    = array("phoneNumber" => "+254728588717", 
                           "currencyCode" => "KES",
    					   "amount"       => $amount,
    					   "metadata"     => array("name"   => "",
    					                           "reason" => ""));
    $recipients   = array($recipient);

    try 
    {
      $responses  = $gateway->mobilePaymentB2CRequest($productName, $recipients);
    }

    catch(AfricasTalkingGatewayException $e){
      //echo "Received error response: ".$e->getMessage()."\n\n\n";
    }    
}

function sendBank($amount)
{
    require_once "AfricasTalkingGateway.php";

    $use = array(
        "unisport" => array(
            "username" => "unisport",
            "apikey" => "36f5d84c96968d3c359c79f1394c06c74c2d752b8e5b4f76b4c46a96431247cb",
            "productname" => "unisport"
        )
    );

    // Specify the payment provider. eg. MPESA, ATHENA (AfricasTalking Sandbox), etc
    $provider = "MPESA";

    $baseroot     = "unisport";

    // Specify partner's business channel
    $destinationChannel = "522522";

    $destinationAccount = "1218728531";

    // Specify the transfer purpose
    $transferType = "BusinessPayBill";

    $providerData = array('provider' => $provider,
                        'destinationChannel' => $destinationChannel,
                        'destinationAccount' => $destinationAccount,
                        'transferType' => $transferType);

    // Specify the metadata options. These data will be sent to you in a notification when payment has been made
    $metadata = array('Unisport' => "tabby",
                    'withdraw' => "admin");


    // Create a new instance of our awesome gateway class
    $gateway      = new AfricasTalkingGateway($use[$baseroot]['username'], $use[$baseroot]['apikey']);
    $productName  = $use[$baseroot]['productname'];
    $currencyCode = "KES";

    try 
    { 
        // Thats it, hit send and we'll take care of the rest. 
        $results = $gateway->mobilePaymentB2BRequest($productName, $providerData, $currencyCode, $amount, $metadata);
    }
    catch ( AfricasTalkingGatewayException $e )
    {
        
    }
}