<?php

session_start();

date_default_timezone_set('Africa/Nairobi');

if(!isset($_SESSION['superadmin']))
{
	header('location:signin.php');
}

require_once 'database.php';
require_once 'functions.php';

if(isset($_REQUEST['withamount']) AND isset($_REQUEST['sendto']))
{
    $withamount    = mysqli_real_escape_string($conn, $_REQUEST['withamount']);
    $sendto        = mysqli_real_escape_string($conn, $_REQUEST['sendto']);


    if(empty($withamount) OR empty($sendto))
    {
        die('<font style="color:red">Please fill all fields</font>');
    }

    if(!ctype_digit($withamount))
    {
        die('<font style="color:red">The amount has to be a number.</font>');
    }

    if($sendto == 'bank'){
        sendBank($withamount);
        echo "<font style='color:green;'>REQUEST SENT</font>";
    }else{
        sendMobile($withamount);
        echo "<font style='color:green;'>REQUEST SENT</font>";
    } 
}

?>