<?php

session_start();

if(!isset($_SESSION['superadmin']))
{
	header('location:signin.php');
}

require_once 'database.php';
require_once 'functions.php';

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Unisport</title>
	<meta content="" name="description">
	<meta content="width=device-width,initial-scale=1,maximum-scale=1,minimal-ui" name="viewport">
	<meta content="IE=edge" http-equiv="X-UA-Compatible">
	<meta content="yes" name="apple-mobile-web-app-capable">
	<meta content="black-translucent" name="apple-mobile-web-app-status-barstyle">
	<link href="images/logo.png" rel="apple-touch-icon">
	<meta content="" name="apple-mobile-web-app-title">
	<meta content="yes" name="mobile-web-app-capable">
	<link href="images/logo.png" rel="shortcut icon" sizes="196x196">
	<link href="css/animate.css/animate.min.css" rel="stylesheet" type="text/css">
	<link href="css/glyphicons/glyphicons.css" rel="stylesheet" type="text/css">
	<link href="css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="css/material-design-icons/material-design-icons.css" rel="stylesheet" type="text/css">
	<link href="css/ionicons/css/ionicons.min.css" rel="stylesheet" type="text/css">
	<link href="css/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css">
	<link href="css/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="css/styles/app.min.css" rel="stylesheet">
	<link href="css/styles/font.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="app" id="app">
	
		<div class="app-content box-shadow-z2 bg pjax-container" id="content" role="main">
			<div class="app-header primary bg b-b">
				<div class="navbar" data-pjax="">
					<div class="navbar-item pull-left h5" id="pageTitle">
						Unisport Admin
					</div>
					<ul class="nav navbar-nav pull-right">
						<li class="nav-item dropdown pos-stc-xs">
							<a class="nav-link" data-toggle="modal" data-target="#m"><i class="ion-plus w-24"></i></a>
						</li>
						<li class="nav-item dropdown pos-stc-xs">
							<a class="nav-link" data-toggle="dropdown"><i class="ion-log-out w-24"></i></a>
						</li>
					</ul>
				</div>
			</div>
			<div class="app-footer white bg p-a b-t">
				<div class="pull-right text-sm text-muted">
          Unisport
        </div>
        <span class="text-sm text-muted">&copy; Copyright</span>
			</div>
			<div class="app-body">
				<div class="row-col">
					<div class="col-lg b-r">
						<div class="row no-gutter">
							<div class="col-xs-6 col-sm-3 b-r b-b">
								<div class="padding">
									<div>
									</div>
									<div class="text-center">
										<h3 class="text-center _600">
											<?php 
												$largs = array('status'=>'complete');
												echo "KES ".number_format(getSum('tips', 'totalearning', $largs));
											?>
										</h3>
										<p class="text-muted m-b-md">Total Earnings</p>
										
									</div>
								</div>
							</div>
							<div class="col-xs-6 col-sm-3 b-r b-b">
								<div class="padding">
									<div>
									</div>
									<div class="text-center">
										<h3 class="text-center _600">
											<?php 
												$today = date('d-m-Y');
												$targs = array('date'=> $today);
												echo "KES ".number_format(getSum('tips', 'totalearning', $targs));
											?>
										</h3>
										<p class="text-muted m-b-md">Today's Earnings</p>
									
									</div>
								</div>
							</div>
							<div class="col-xs-6 col-sm-3 b-r b-b">
								<div class="padding">
									<div>
									</div>
									<div class="text-center">
										<h2 class="text-center _600">
											<?php 
												echo returnCountNoCondition('users')
											?>
										</h2>
										<p class="text-muted m-b-md">Total Users</p>
										
									</div>
								</div>
							</div>
							<div class="col-xs-6 col-sm-3 b-b">
								<div class="padding">
									<div>
									</div>
									<div class="text-center">
										<h2 class="text-center _600">
											<?php 
												$conflict = array('type' => 'conflict');
												echo returnExists('payments',$conflict);
											?>
										</h2>
										<p class="text-muted m-b-md">Conflict Payments</p>
									
									</div>
								</div>
							</div>
						</div>
					
					</div>
					<div class="col-lg w-lg w-auto-md white bg">
						<div>
								<div class="streamline streamline-theme m-b b-b">
									<a href="#" data-toggle="modal" data-target="#with"><div class="sl-item b-danger text-primary">
										<div class="sl-content _600">
											<div>
												<i class="ion-chevron-down"></i> WITHDRAW MONEY
											</div>
											<div class="sl-date text-muted">
												SEND TO MOBILE/BANK
											</div>
										</div>
									</a>

								</div>
								<div class="streamline streamline-theme m-b b-b">
				
                                	<a href="#" data-toggle="modal" data-target="#m"><div class="sl-item b-danger text-primary">
										<div class="sl-content _600">
											<div>
												<i class="ion-plus"></i> ADD NEW MESSAGE
											</div>
											<div class="sl-date text-muted">
												PLEASE ADD A NEW MESSAGE
											</div>
										</div>
									</a>
								</div>

								<?php
									$allTipsArray  = array('status' => 'complete');
									$arrayList     = returnArrayOfRequest('tips','id',$allTipsArray);

									$explodeValues = explode(",", $arrayList);

									foreach($explodeValues as $oneValue)
									{
										$newargs = array('id' => $oneValue);
								?>

								<div class="sl-item b-success _600">
									<div class="sl-content">
										<div>
											<?php echo getByValue('tips', 'message', $newargs); ?>
										</div>
										<div class="sl-date text-muted">
											<?php echo getByValue('tips', 'date', $newargs); ?> . <?php echo "KES ".getByValue('tips', 'amount', $newargs); ?> .<a href="remove.php?id=<?php echo $oneValue; ?>" class="text-danger">Remove</a>
										</div>
									</div>
								</div>
									<?php } ?>
							</div>
						</div>
					</div>
				</div>
			
			</div>
		</div>


		<div class="modal" data-backdrop="true" id="m">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title">Add New Tip</h5>
					</div>
					<div class="modal-body text-center p-lg">
						<div class="col-sm-6">
							<div class="md-form-group float-label">
								<input id="tipamount" type="number" class="md-input" required=""><label>Amount</label>
							</div>
						</div>
						<div class="col-sm-12">
							<div class="md-form-group">
								<textarea id="tipmessage" class="md-input" rows="4"></textarea><label>Message</label>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<p id="tipresp"></p>
						<button class="btn dark-white p-x-md" data-dismiss="modal" type="button">Cancel</button> 
						<button id="tipbutton" class="btn primary p-x-md" type="button">Publish Tip</button>
					</div>
				</div>
			</div>
		</div>

		<div class="modal" data-backdrop="true" id="with">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title">Withdraw</h5>
					</div>
					<div class="modal-body text-center p-lg">
						<div class="col-sm-6">
							<div class="md-form-group float-label">
								<select id="sendto" class="form-control">
									<option value="phone">Send To Phone</option>
									<option value="bank">Send To Bank</option>
								</select>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="md-form-group float-label">
								<input id="withamount" type="number" class="md-input" required=""><label>Amount</label>
							</div>
						</div>
	
					</div>
					<div class="modal-footer">
						<p id="withresp"></p>
						<button class="btn dark-white p-x-md" data-dismiss="modal" type="button">Cancel</button> 
						<button id="withbutton" class="btn primary p-x-md" type="button">Send</button>
					</div>
				</div>
			</div>
		</div>

	</div>
	<script src="scripts/app.min.js"></script>
	<script>
		$(document).ready(function()
		{
			$('#tipbutton').click(function()
			{
				$('#tipbutton').html('<i class="fa ion-load-c fa-spin"></i>');
				$('#tipbutton').css('disabled','1');

				var tipamount  = $('#tipamount').val();
				var tipmessage = $('#tipmessage').val();

				$.post("addtip.php",
				{
					tipamount:tipamount,
					tipmessage:tipmessage
				},
				function(ajaxresponse)
				{
					if(ajaxresponse == 1)
					{
						location.reload();
					}else{
						$('#tipresp').html(ajaxresponse);
						$('#tipbutton').html('Publish Tip');
						$('#tipbutton').css('disabled','0');
					}
				});
			});

			$('#withbutton').click(function()
			{
				$('#withbutton').html('<i class="fa ion-load-c fa-spin"></i>');
				$('#withbutton').css('disabled','1');

				var withamount  = $('#withamount').val();
				var sendto      = $('#sendto').val();

				$.post("withfile.php",
				{
					withamount:withamount,
					sendto:sendto
				},
				function(wajaxresponse)
				{
					if(wajaxresponse == 1)
					{
						location.reload();
					}else{
						$('#withresp').html(wajaxresponse);
						$('#withbutton').html('Send');
						$('#withbutton').css('disabled','0');
					}
				});
			});
		});
	</script>
</body>
</html>