<?php

/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
*/

// requirements

require_once '../database.php';
require_once '../functions.php';

date_default_timezone_set('Africa/Nairobi');

$data  = json_decode(file_get_contents('php://input'), true);

$category            = $data["category"];
$phoneNumber         = $data["source"];
$value               = $data["value"];
$acc                 = strtolower($data["clientAccount"]);
$account             = trim($acc);
$providerRefId       = $data["providerRefId"];
$useraccount         = "+254".substr($account,-9);

$editamount          = explode(".", $value);
$amountvalue         = $editamount[0];
$removespaces        = str_replace(" ", "", $amountvalue);
$intamount           = (int)str_replace("KES", "", $removespaces);

$date                = date('d-m-Y');

$todaysargs          = array('date' => $date);
$tipToday            = returnArrayOfRequest('tips','amount',$todaysargs);
$expectedArray       = explode(',',$tipToday);

if ($category == "MobileC2B") 
{
    if(in_array($intamount,$expectedArray))
    {
        $messageArgs         = array('amount' => $intamount, 'date' => $date);
        $getMessageForAmount = getByValue('tips', 'message', $messageArgs);
        $getTotalForAmount   = getByValue('tips', 'totalearning', $messageArgs);
        $getIdForAmount      = getByValue('tips', 'id', $messageArgs);

        $revisedAmount       = $getTotalForAmount + $intamount;

        opensendSMS($phoneNumber,$getMessageForAmount);

        mysqli_query($conn, "UPDATE `tips` SET `totalearning`='$revisedAmount' WHERE `id`='$getIdForAmount'");

        // add customer if not exists
        $customerArray = array('phone' => $phoneNumber);

        if(returnExists('users', $customerArray) == 0)
        {
            mysqli_query($conn, "INSERT INTO `users`(`phone`,`date`) 
            VALUES('$phoneNumber','$date')");
        }
    }else{
        mysqli_query($conn, "INSERT INTO `payments`(`amount`,`phone`,`type`)
        VALUES('$intamount','$phoneNumber','conflict')");
    }
} 
