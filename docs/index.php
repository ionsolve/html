<?php

session_start();


?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport">
	<meta content="" name="description">
	<meta content="" name="Ionsolve">
	<link href="../assets/img/logo_small.png" rel="icon" type="image/x-icon">
	<title>IonSolve : Docs</title>
	<link href="../assets/css/app.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
	<link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css">
	<link href="../assets/prism.css" rel="stylesheet" type="text/css">
	<style type="text/css">
	          ::-webkit-scrollbar { 
	            display: none; 
	        }
	</style>
</head>
<body>
	<div class="paper-loading" id="app">
		<div class="container">
			<div class="row">
			
				<div class="col-md-3 sticky">
					<aside class="main-sidebar">
						<div class="">
							<section class="sidebar p-t-b-40" style="height: auto;">
								<a href="https://ionsolve.com/" target="_blank"> <img src="../assets/img/logoblue.png" style="width:60%;"></a><br>
								<br>
								<br>
								<ul class="sidebar-menu scroll">
									<li class="text-info">
										<a class="text-info" href="#introduction"><i class="ion-ios-arrow-thin-right"></i> Introduction</a>
									</li>
									<li>
										<a class="text-info" href="#curl"><i class="ion-ios-arrow-thin-right"></i>Curl</a>
									</li>
									<li>
										<a class="text-info" href="#nodejs"><i class="ion-ios-arrow-thin-right"></i>Node JS</a>
									</li>
									<li>
										<a class="text-info" href="#php"><i class="ion-ios-arrow-thin-right"></i>PHP</a>
									</li>
									<li>
										<a class="text-info" href="#python"><i class="ion-ios-arrow-thin-right"></i>Python</a>
									</li>
									<li>
										<a class="text-info" href="#golang"><i class="ion-ios-arrow-thin-right"></i>Golang</a>
									</li>
									
								</ul>
							</section>
						</div>
					</aside>
				</div>
				<article class="col-md-7 p-b-20 b-l " style="background-color:#f5fafb;">
					
					<section id="introduction">
						<div class="row">
							<div class="col-md-12 p-10 b-b text-right" style="background-color:#FFFFFF;">
								<div class="p-10">
								<a class="text-left" href="../">Home&nbsp;&nbsp;&nbsp;&nbsp;</a>
								<a class="text-right" href="../support">Support&nbsp;&nbsp;&nbsp;&nbsp;</a>	
									<?php
										if(!isset($_SESSION['alphaion'])){
									?>
								<a class="text-right " href="../account/login">Sign In <i class="ion-ios-arrow-thin-right">&nbsp;</i>&nbsp;</a>
									<?php }else{ ?>
										<a class="text-right " href="../dashboard">Dashboard <i class="ion-ios-arrow-thin-right">&nbsp;</i>&nbsp;</a>
									<?php } ?>
								</div>
							</div>
							<div class="col-md-12" style="background-color:#FFFFFF;">
								<div class="col-12 col-xl-12">
								<br><br>
									<h4 class="text-primary">Documentation</h4>
									<p class="s-18 text-muted">Welcome to Ionsolve! Explore the docs and build amazing.</p>
									<img src="../assets/img/dummy/docsapi.png"/>
									<br><br>
									<h5>Getting Started</h5>
									<p class="s-18 text-muted">
										To get started, you need to <a href="https://ionsolve.com/account/signup">create an account</a> and generate your API key from the Developers section in the dashboard. 
									</p>
									<p class="s-18 text-muted">
										Done with that? Happy RESTING!
									</p>
								</div>
							</div>
						</div>
					</section>
					<section id="curl">
						<div class="row">
							<div class="col-md-12">
								<div class="col-12 col-xl-12">
								<br><br>
									<h5 class="text-primary">Curl</h5>
									<div style="background-color:#FFFFFF;">
										<pre class="line-numbers lang-curl">
<code>curl 
	-H "Content-Type: application/json" 
	-H "x-api-user: YOUR-USERNAME" 
	-H "x-api-key: YOUR-API-KEY" 
	--data '{
		"phoneNumbers" : "+254711xxxxxx,+41785xxxx"
		"message" : "It\'s song and dance until someone does rm -rf music"
		"senderId" : ""
	}' 
	"https://ionsolve.com/api/sms/send"
</code>
</pre>
									</div>
								</div>
							</div>
						</div>
					</section>
					<section id="nodejs">
						<div class="row">
							<div class="col-md-12">
								<div class="col-12 col-xl-12">
								<br><br>
									<h5 class="text-primary">Node JS</h5>
									<div style="background-color:#FFFFFF;">
										<pre class="line-numbers lang-javascript">
<code>var request = require('request');

// EDIT ACCOUNT DETAILS
var x_username       = ""
var x_apikey         = ""

// EDIT MESSAGE PARAMS
var params = {
    'phoneNumbers' : '+254711xxxxxx,+41785xxxx', 
    'message' : 'It\'s song and dance until someone does rm -rf music',
    'senderId' : ''
}

var messageHost     = "https://ionsolve.com/api/sms/send";

// Set the headers
var headers = {
    'Content-Type' :  'application/json',
    'Content-Length' : params.length,
    'Accept' : 'application/json',
    'x-api-user' : x_username,
    'x-api-key' : x_apikey
}


// Configure the request
var options = {
    url: messageHost,
    method: 'POST',
    headers: headers,
    form: params
}

// Start the request
request(options, function (error, response, body) {
    if (!error && response.statusCode == 200) {
        // Print out the response body
        console.log(body)
    }
	console.log(body)
})
</code>
</pre>
									</div>
								</div>
							</div>
						</div>
					</section>
					<section id="php">
						<div class="row">
							<div class="col-md-12">
								<div class="col-12 col-xl-12">
								<br><br>
									<h5 class="text-primary">PHP Gateway</h5>
									<div style="background-color:#FFFFFF;">
										<pre class="line-numbers lang-php">
<code>// EDIT ACCOUNT DETAILS
$x_username      = "";
$x_apikey        = "";

// EDIT MESSAGE PARAMS
$createMessage   = array(
    "phoneNumbers" => "+254711xxxxxx,+41785xxxx", // multiple contacts comma separated
    "message"      => "It's song and dance until someone does rm -rf music", 
    "senderId"     => "" // default value is Ionsolve
);

// message endoint
$messageHost     = "https://ionsolve.com/api/sms/send";

// encoding params
$params          = json_encode($createMessage);
$req             = curl_init($messageHost);

curl_setopt($req, CURLOPT_CUSTOMREQUEST, "POST");
curl_setopt($req, CURLOPT_TIMEOUT, 60);
curl_setopt($req, CURLOPT_SSL_VERIFYPEER, FALSE);
curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
curl_setopt($req, CURLOPT_POSTFIELDS, $params);
curl_setopt($req, CURLOPT_HTTPHEADER, array(
    'Content-Type: application/json',
    'Content-Length: '.strlen($params),
    'x-api-user: '.$x_username, 
    'x-api-key: '.$x_apikey
));

// read api response
$res              = curl_exec($req);

// close curl
curl_close($req);

// print the json response
var_dump($res);
</code>
</pre>
									</div>
								</div>
							</div>
						</div>
					</section>
					<section id="python">
						<div class="row">
							<div class="col-md-12">
								<div class="col-12 col-xl-12">
									<br><br>
									<h5 class="text-primary">Python</h5>
									<div style="background-color:#FFFFFF;">
										<pre class="line-numbers lang-python">
<code>import requests
import json

x_username      = "";
x_apikey        = "";

phoneNumbers    = "+254711xxxxxx,+41785xxxx" # multiple contacts comma separated
message         = "It's song and dance until someone does rm -rf music"
senderId        = "" # default value is Ionsolve

# message endoint
messageHost     = "https://ionsolve.com/api/sms/send"

headers = {
   'Content-type': 'application/json',
   'Accept': 'application/json',
   'x-api-user': x_username,
   'x-api-key' : x_apikey
}

params = {
   'phoneNumbers' : phoneNumbers,
   'message' : message,
   'senderId' : senderId
}

req = requests.post(messageHost, data=json.dumps(params), headers=headers)

print(req.text)
</code>
</pre>
									</div>
								</div>
							</div>
						</div>
					</section>

					<section id="golang">
						<div class="row">
							<div class="col-md-12">
								<div class="col-12 col-xl-12">
								<br><br>
									<h5 class="text-primary">Golang</h5>
									<div style="background-color:#FFFFFF;">
										<pre class="line-numbers lang-go">
<code>package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
)

const messageHost string    = "https://ionsolve.com/api/sms/send"

// EDIT ACCOUNT DETAILS
var x_username string       = ""
var x_apikey string         = ""

func main() {
	// multiple contacts comma separated
	var phoneNumbers string = "+254711xxxxxx,+41785xxxx"
	// and of course we want our recipients to know what we really do
	var message string      = "It's song and dance until someone does rm -rf music"
	// default value is Ionsolve
	var senderId string         = ""

	// building the post params
	addparams := url.Values{}
	addparams.Set("phoneNumbers", phoneNumbers)
	addparams.Set("message", message)
	addparams.Set("senderId", senderId)

	// Values.Encode() encodes the post values into "URL encoded"
	params := addparams.Encode()

	// Perform the post
	req, err := http.NewRequest("POST", messageHost, strings.NewReader(params))
	
	if err != nil {
		fmt.Printf("Error: %v\n", err)
		return
	}

	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("Accept", "application/json")
	req.Header.Add("x-api-user",x_username)
	req.Header.Add("x-api-key",x_apikey)
	
	c := &http.Client{}
	resp, err := c.Do(req)
	
	if err != nil {
		fmt.Printf("Error: %v\n", err)
		return
	}
	
	defer resp.Body.Close()

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Printf("Error: %v\n", err)
		return
	}

	fmt.Println(string(data))
}

</code>
</pre>
									</div>
								</div>
							</div>
						</div>
					</section>

				</article>

				<div class="col-md-2 b-l"></div>
			</div>
		</div>
		<footer>
			<div class="container">
				<div class="row"></div>
			</div>
		</footer>
	</div>

	<script src="../assets/js/app.js">
	</script> 
	<script src="../assets/prism.js" type="text/javascript">
	</script>

</body>
</html>