<?php

/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
*/

// requirements
require_once '../database.php';
require_once '../functions.php';
require_once '../AfricasTalkingGateway.php';

$data  = json_decode(file_get_contents('php://input'), true);

$category            = $data["category"];
$phoneNumber         = $data["source"];
$value               = $data["value"];
$acc                 = strtolower($data["clientAccount"]);
$account             = trim($acc);
$providerRefId       = $data["providerRefId"];
$useraccount         = "+254".substr($account,-9);

$editamount          = explode(".", $value);
$amountvalue         = $editamount[0];
$removespaces        = str_replace(" ", "", $amountvalue);
$intamount           = (int)str_replace("KES", "", $removespaces);

$date                = date('Y-m-d H:i:s');

if ($category == "MobileC2B") 
{
    // check user account if exists
    $existsArray = array('phonenumber'=>$useraccount);
    $userExists  = returnExists('users',$existsArray);
    
    if($userExists > 0)
    {
        // get user id now
        $userId     = getByValue('users', 'id', $existsArray);
        $userName   = getByValue('users', 'firstname', $existsArray);
        $userStatus = getByValue('users', 'status', $existsArray);
        $inviter    = getByValue('users', 'ref', $existsArray);
        $excess     = getByValue('users', 'excess', $existsArray);

        if($userStatus == 'pending')
        {
            // check amount
            if($intamount == 200)
            {
                $pin = rand(1000,9999);
                mysqli_query($conn, "UPDATE `users` SET `status`='active', `level`='8', `pin`='$pin',`datecreated`='$date' WHERE `phonenumber`='$phoneNumber'");
                
                if(!empty($inviter))
                {
                    $inviteArray  = array('phonenumber' => $inviter);
                    $limit        = getByValue('users','loanlimit',$inviteArray);
                    $newLoanLimit = $limit + 100;
                    mysqli_query($conn, "UPDATE `users` SET `loanlimit`='$newLoanLimit' WHERE `phonenumber`='$inviter'");     
                }
                
                $message      = "Hello ".$userName.", we have received your payment of KES ".$intamount.". Your Mjiajiri account is now active and your PIN is ".$pin.". Please dial *483*888# to get a loan.";
                opensendSMS($phoneNumber,$message);
            }            
        }else{
            // check if account has loan and pay it. Sum up loan excess in a column in database
            $balanceArray   = array('parent'=> $userId, 'status' => 'pending');

            if(returnExists('loans',$balanceArray) > 0)
            {
                $loanAmount = getByValue('loans', 'amount', $balanceArray);
                $loanIdR    = getByValue('loans', 'id', $balanceArray);
                
                if($intamount == $loanAmount)
                {
                    mysqli_query($conn,"UPDATE `loans` SET `status`='paid' WHERE `id`='$loanIdR'");
                    $message      = "Hello ".$userName.", we have received your payment of KES ".$intamount.". Your loan is fully paid. Please dial *483*888# to get a loan.";
                    opensendSMS($phoneNumber,$message);
                }

                if($intamount > $loanAmount)
                {
                    $inexcess = $intamount - $loanAmount;
                    $newExcess  = $excess + $inexcess;
                    excess($userId, $newExcess);
                    mysqli_query($conn,"UPDATE `loans` SET `status`='paid' WHERE `id`='$loanIdR'");
                    $message      = "Hello ".$userName.", we have received your payment of KES ".$intamount.". Your loan is fully paid. Please dial *483*888# to get a loan.";
                    opensendSMS($phoneNumber,$message);
                }

                if($intamount < $loanAmount)
                {
                    $ins = $loanAmount - $intamount;
                    mysqli_query($conn,"UPDATE `loans` SET `amount`='$ins' WHERE `id`='$loanIdR'");
                    $message      = "Hello ".$userName.", we have received your payment of KES ".$intamount.". Your loan balance is KES ".number_format($ins)." Please pay your loan on time to continue enjoying our services.";
                    opensendSMS($phoneNumber,$message);
                }

            }else{
                $newExcess  = $excess + $intamount;
                excess($userId, $newExcess);
            }
        }

    }
} 
