<?php

// requirements
require_once '../database.php';
require_once '../functions.php';
require_once '../AfricasTalkingGateway.php';

$sessionId    = $_POST["sessionId"];
$serviceCode  = $_POST["serviceCode"];
$phoneNumber  = $_POST["phoneNumber"];
$text         = $_POST["text"];
$date         = date('Y-m-d');

//3. Explode the text to get the value of the latest interaction - think 1*1
$textArray    = explode('*', $text);
$userResponse = trim(end($textArray));

$userArray    = array('phonenumber' => $phoneNumber);


$userAvail = returnExists('users', $userArray);

if($userAvail > 0)
{
    $pin            = getByValue('users','pin',$userArray);
    $loanLimitValue = getByValue('users','loanlimit',$userArray);
    $loanUserID     = getByValue('users','id',$userArray);
    $firstN         = getByValue('users','firstname',$userArray);
    $loanInterest   = getByValue('users','interest',$userArray);

    $balanceArray   = array('parent'=> $loanUserID, 'status' => 'pending');
}
  
    // check if number exists and serve menu
    if($userAvail < 1)
    {
        mysqli_query($conn, "INSERT INTO `users`(`phonenumber`,`level`) VALUES('$phoneNumber','0')");
        $response  = "CON 1. Register for Mjiajiri";
    }else{

        $level = getByValue('users','level',$userArray);

        if($level == '0')
        {
            mysqli_query($conn, "UPDATE `users` SET `level`='2' WHERE `phonenumber`='$phoneNumber'");
            $response  = "CON 1. Accept terms and conditions as in http://mjiajiri.com/terms/";
        }

        if($level == '1')
        {
            if($userResponse =='1')
            {
                mysqli_query($conn, "UPDATE `users` SET `level`='2' WHERE `phonenumber`='$phoneNumber'");
                $response  = "CON 1. Accept terms and conditions as in http://mjiajiri.com/terms/";
            }else{
                mysqli_query($conn, "UPDATE `users` SET `level`='0' WHERE `phonenumber`='$phoneNumber'");
                $response  = "CON Something went wrong. Please try again\n";
                $response .= "1. Try again";
            } 
        }
        if($level == '2')
        {
            if($userResponse =='1')
            {
                mysqli_query($conn, "UPDATE `users` SET `level`='3' WHERE `phonenumber`='$phoneNumber'");
                $response  = "CON Please enter your first name";
            }else{
                mysqli_query($conn, "UPDATE `users` SET `level`='0' WHERE `phonenumber`='$phoneNumber'");
                $response  = "CON Something went wrong. Please try again\n";
                $response .= "1. Try again";
            } 
        }
        if($level == '3')
        {
            if(ctype_alpha($userResponse))
            {
                mysqli_query($conn, "UPDATE `users` SET `firstname`='$userResponse', `level`='4' WHERE `phonenumber`='$phoneNumber'");
                $response  = "CON Please enter your second name";
            }else{
                $response  = "CON Incorrect Entry. Please enter your first name";
            }
        }
        if($level == '4')
        {
            if(ctype_alpha($userResponse))
            {
                mysqli_query($conn, "UPDATE `users` SET `lastname`='$userResponse', `level`='5' WHERE `phonenumber`='$phoneNumber'");
                $response  = "CON Please enter your surname";
            }else{
                $response  = "CON Incorrect Entry. Please enter your second name";
            }
        }
        if($level == '5')
        {
            if(ctype_alpha($userResponse))
            {
                mysqli_query($conn, "UPDATE `users` SET `surname`='$userResponse', `level`='6' WHERE `phonenumber`='$phoneNumber'");
                $response  = "CON Please enter your National ID number";
            }else{
                $response  = "CON Incorrect Entry. Please enter your surname";
            }
        }
        if($level == '6')
        {
            if(ctype_digit($userResponse))
            {
                mysqli_query($conn, "UPDATE `users` SET `idnumber`='$userResponse', `level`='7',`status`='pending',`interest`='14' WHERE `phonenumber`='$phoneNumber'");
                $response  = "END Thank you for registering to mjiajiri. You will receive an SMS shortly";
                $message      = "Hello ".$firstN.", please pay KES 200 to paybill 885200 account number 0".substr($phoneNumber,-9); 
                opensendSMS($phoneNumber,$message);
            }else{
                $response  = "CON Incorrect Entry. Please enter your National ID number";
            }
        }

        if($level == '7')
        {
            $response  = "END Please activate your account. Pay KES 200 to Paybill 885200 Account Number 0".substr($phoneNumber,-9);
        }

        if($level == '8')
        {
            if($text == "")
            {
                $response  = "CON Hello ".ucfirst(getByValue('users','firstname',$userArray));
	            $response .= " Please enter your PIN to continue.\n";
            }
            else if ($text == $textArray[0]) 
            {
                if($textArray[0] == $pin){
                    $response  = "CON 1. Get A Loan \n";
                    $response .= "2. Check Loan Limit\n";
                    $response .= "3. View Loan Balance\n";
                    $response .= "4. Invite A Friend\n";
                    if($phoneNumber == "+254724605144" OR $phoneNumber == "+254790807760")
                    {
                        $response .= "-- USSD ADMIN --\n";
                        $response .= "10. Change Limit\n";
                        $response .= "20. Resend PIN\n";
                        $response .= "30. Change Loan Interest\n";
                        $response .= "40. Manual Loan Update\n";
                    }
                }else{
                    $response  = "END You entered an incorrect PIN.\n";
                }
            }

            else if ($text == $textArray[0]."*10") 
            {
                if($phoneNumber == "+254724605144" OR $phoneNumber == "+254790807760")
                {
                    $response = "CON Enter Phone Number (07xxxxxxxx)";
                }
            }
                else if ($text == $textArray[0]."*10*".$userResponse) 
                {
                    if($phoneNumber == "+254724605144" OR $phoneNumber == "+254790807760")
                    {
                        $formatphone = "+254".substr($phoneNumber,-9);

                        $phoneEx = array('phonenumber' => $formatphone);

                        if(returnExists('users',$phoneEx))
                        {
                            $response = "CON Enter new loan limit";
                        }else{
                            $response = "END The phone number is not registered";
                        } 
                    }else{
                        $response = "END Wrong selection";
                    }
                }
                else if ($text == $textArray[0]."*10*".$textArray[2]."*".$userResponse) 
                {
                    $formatphone2 = "+254".substr($textArray[2],-9);

                    if(ctype_digit($userResponse))
                    {
                        mysqli_query($conn, "UPDATE `users` SET `loanlimit`='$userResponse' WHERE `phonenumber`='$formatphone2'");
                        $message = "Your new Mjiajiri loan limit is KES ".number_format($userResponse);
                        opensendSMS($formatphone2,$message);
                        $response = "END Loan Limit Updated";
                    }else{
                        $response = "END The loan limit should be a number";
                    }
                }

                // change loan interest

                else if ($text == $textArray[0]."*30") 
                {
                    if($phoneNumber == "+254724605144" OR $phoneNumber == "+254790807760")
                    {
                        $response = "CON Enter Phone Number (07xxxxxxxx)";
                    }
                }
                    else if ($text == $textArray[0]."*30*".$userResponse) 
                    {
                        if($phoneNumber == "+254724605144" OR $phoneNumber == "+254790807760")
                        {
                            $p2          = trim($userResponse);
                            $formatphone = "+254".substr($p2,-9);

                            $phoneEx = array('phonenumber' => $formatphone);

                            if(returnExists('users',$phoneEx))
                            {
                                $response = "CON Enter new loan interest";
                            }else{
                                $response = "END The phone number is not registered";
                            } 
                        }else{
                            $response = "END Wrong selection";
                        }
                    }
                    else if ($text == $textArray[0]."*30*".$textArray[2]."*".$userResponse) 
                    {
                        $p2           = trim($textArray[2]);
                        $formatphone2 = "+254".substr($p2,-9);

                        if(ctype_digit($userResponse))
                        {
                            mysqli_query($conn, "UPDATE `users` SET `interest`='$userResponse' WHERE `phonenumber`='$formatphone2'");
                            
                            $response = "END Loan interest updated";
                        }else{
                            $response = "END The loan interest should be a number";
                        }
                    }
                

                // resend pin
                else if ($text == $textArray[0]."*20") 
                {
                    if($phoneNumber == "+254724605144" OR $phoneNumber == "+254790807760")
                    {
                        $response = "CON Enter Phone Number (07xxxxxxxx)";
                    }
                }

                else if ($text == $textArray[0]."*20*".$userResponse) 
                {
                    if($phoneNumber == "+254724605144" OR $phoneNumber == "+254790807760")
                    {
                        $formatphone = "+254".substr($userResponse,-9);

                        $phoneEx = array('phonenumber' => $formatphone);

                        if(returnExists('users',$phoneEx))
                        {
                            $pinPhone = getByValue('users', 'pin', $phoneEx);
                            $pinName  = getByValue('users', 'firstname', $phoneEx);

                            $message = "Hello ".$pinName.", your Mjiajiri PIN is ".$pinPhone.". Dial *483*888# to get a loan.";
                            opensendSMS($formatphone,$message);

                            $response = "END PIN has been resent to ".$pinName;
                        }else{
                            $response = "END The phone number is not registered";
                        } 
                    }else{
                        $response = "END Wrong selection";
                    }
                }

            // manual loan update

            else if ($text == $textArray[0]."*40") 
            {
                if($phoneNumber == "+254724605144" OR $phoneNumber == "+254790807760")
                {
                    $response = "CON Enter Phone Number (07xxxxxxxx)";
                }
            }
                else if ($text == $textArray[0]."*40*".$userResponse) 
                {
                    if($phoneNumber == "+254724605144" OR $phoneNumber == "+254790807760")
                    {
                        $p2          = trim($userResponse);
                        $formatphone = "+254".substr($p2,-9);

                        $phoneEx = array('phonenumber' => $formatphone);

                        if(returnExists('users',$phoneEx))
                        {
                            $response = "CON Enter new loan amount";
                        }else{
                            $response = "END The phone number is not registered";
                        } 
                    }else{
                        $response = "END Wrong selection";
                    }
                }
                else if ($text == $textArray[0]."*40*".$textArray[2]."*".$userResponse) 
                {
                    $p2           = trim($textArray[2]);
                    $formatphone2 = "+254".substr($p2,-9);

                    $phoneEx = array('phonenumber' => $formatphone2);
                    $userID  = getByValue('users', 'id', $phoneEx);

                    if(ctype_digit($userResponse))
                    {
                        if($userResponse < 1){
                            mysqli_query($conn, "UPDATE `loans` SET `status`='paid' WHERE `parent`='$userID' AND `status`='pending'");
                        }else{
                            mysqli_query($conn, "UPDATE `loans` SET `amount`='$userResponse' WHERE `parent`='$userID' AND `status`='pending'");                       
                        }
                        $response = "END Loan interest updated";
                    }else{
                        $response = "END The loan interest should be a number";
                    }
                }

            else if ($text == $textArray[0]."*1") 
            {
                $response = "CON Enter Amount";
            }
                else if ($text == $textArray[0]."*1*".$userResponse) 
                {
                   if(ctype_digit($userResponse))
                   {
                       if(returnExists('loans',$balanceArray) < 1)
                       {
                            if($userResponse <= $loanLimitValue)
                            {
                                $tosend = ceil(($userResponse * $loanInterest)/100);

                                $loanSending = $userResponse - $tosend;
                                
                                $status = giveLoan($phoneNumber,$loanSending);

                                if($status == "Queued")
                                {
                                    //log loan into system
                                    $loanref = uniqid();
                                    $date    = date('Y-m-d H:i');
                                    mysqli_query($conn,"INSERT INTO `loans`(`amount`,`status`,`parent`,`date`,`refcode`,`given`)
                                    VALUES('$userResponse','pending','$loanUserID','$date','$loanref','$userResponse')");
            
                                    $message = "You have received a loan of KES ".number_format($userResponse)." Reference ".$loanref." from Mjiajiri. Your total repayable loan is KES ".number_format($userResponse);
                                    opensendSMS($phoneNumber,$message);
                                    $response  = "END Your loan has been processed, you will receive an SMS shortly.\n";
                                }else{
                                    $response  = "END We cannot process your loan now, please try again later.\n";
                                }
                            }else{
                                $response  = "END The amount you have given exceeds your loan limit.\n";
                            }
                       }else{
                            $response = "END Please pay your existing loan first before applying for another one.";
                       }
                   }else{
                       $response = "END Incorrect entry.";
                   }    
                }

            // loan limit
            else if ($text == $textArray[0]."*2") 
            {
                $response = "END Your loan limit is KES ".number_format($loanLimitValue);
            }

            // loan limit
            else if ($text == $textArray[0]."*3") 
            {   
                if(returnExists('loans',$balanceArray) > 0)
                {
                    $response = "END Your loan limit is KES ".number_format(getByValue('loans','amount',$balanceArray));
                }else{
                    $response = "END You do not have a pending loan. Dial *483*888# to take a loan.";
                }
            }

            else if ($text == $textArray[0]."*4") 
            {   
                $response = "CON Enter phone number you want to invite.";
            }

             // invite
             else if ($text == $textArray[0]."*4*".$userResponse) 
             {
                 $revised    = "+254".substr($userResponse,-9);  
                 $revisedNum = trim($revised);
                 $userArray = array('phonenumber' => $revisedNum);
                 if(returnExists('users',$userArray) < 1)
                 {
                    $message = $firstN." has invited you to join Mjiajiri for personal and business loans. Please dial *483*888# to join and get a loan.";
                    opensendSMS($revisedNum,$message);
                    mysqli_query($conn, "INSERT INTO `users`(`phonenumber`,`ref`,`level`)VALUES('$revisedNum','$phoneNumber','0')");
                    $response = "END Invite sent to ".$revisedNum;
                 }else{
                     $response = "END The phone number is already registered with Mjiajiri.";
                 }
             }
            
        }

    }

 
// Print the response onto the page so that our gateway can read it
header('Content-type: text/plain');
echo $response;


?>