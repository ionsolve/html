<?php

	require_once 'database.php';
	require_once 'functions.php';
                 
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Dashboard</title>
	<meta content="" name="description">
	<meta content="width=device-width,initial-scale=1,maximum-scale=1,minimal-ui" name="viewport">
	<meta content="IE=edge" http-equiv="X-UA-Compatible">
	<meta content="yes" name="apple-mobile-web-app-capable">
	<meta content="black-translucent" name="apple-mobile-web-app-status-barstyle">
	<link href="images/logo.png" rel="apple-touch-icon">
	<meta content="Nes Capital" name="apple-mobile-web-app-title">
	<meta content="yes" name="mobile-web-app-capable">
	<link href="images/logo.png" rel="shortcut icon" sizes="196x196">
	<link href="css/animate.css/animate.min.css" rel="stylesheet" type="text/css">
	<link href="css/glyphicons/glyphicons.css" rel="stylesheet" type="text/css">
	<link href="css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="css/material-design-icons/material-design-icons.css" rel="stylesheet" type="text/css">
	<link href="css/ionicons/css/ionicons.min.css" rel="stylesheet" type="text/css">
	<link href="css/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css">
	<link href="css/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="css/styles/app.min.css" rel="stylesheet">
	<link href="css/styles/font.css" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
	<link href="codemirror/lib/codemirror.css" rel="stylesheet">
	<style type="text/css">
	     ::-webkit-scrollbar { 
	       display: none; 
	   }
	</style>
</head>
<body>
	<div class="app" id="app">
		<div class="app-content box-shadow-z2 pjax-container" id="content" role="main">
			<div class="app-body">
				<div class="app-body-inner">
					<div class="row-col">
						<?php include_once 'top.php'; ?>
						<div class="row-row">
							<div class="row-col">
                				<?php include_once 'side.php'; ?>
                

								<div class="col-xs col-md-7" id="detail">
									<p class="text-md text-primary" style="margin-left: 5px;">Dashboard</p>
									<div class="row-col">
										<div class="row-row">
											<div class="row-body scrollable hover">
												<div class="row-inner">
													
                                                    
                                                    <div class="box">
														<div class="box-header text-muted">
                                                            <p>Analytics</p>
														</div>
														<div class="row no-gutter">
															<div class="col-xs-6 col-sm-4 b-r b-b">
																<div class="padding">
																	
																	<div class="text-center">
																		<h2 class="text-center _600">
																			<?php 
																				echo returnCountNoCondition('users');
																			?>																			
																		</h2>
																		<p class="text-muted m-b-md">Total Users</p>
																		
																	</div>
																</div>
															</div>
															<div class="col-xs-6 col-sm-4 b-r b-b">
																<div class="padding">
																	
																	<div class="text-center">
																		<h2 class="text-center _600">
																			<?php 
																				echo returnCountNoCondition('loans');
																			?>
																		</h2>
																		<p class="text-muted m-b-md">Total Loans</p>
																		
																	</div>
																</div>
															</div>
															<div class="col-xs-6 col-sm-4 b-r b-b">
																<div class="padding">
																	
																	<div class="text-center">
																		<h2 class="text-center _600">
																			<?php 
																				$pargs = array('loanstatus' => 'active');
																				echo returnCountWithCondition('loans', $pargs)
																			?>
																		</h2>
																		<p class="text-muted m-b-md">Active Loans</p>
																		
																	</div>
																</div>
															</div>
															
														</div>

														
														
													</div>

													<div class="box">
														<div class="box-header text-muted">
                                                            <p>General Ledger</p>
														</div>
														<div class="row no-gutter">
															<div class="col-xs-6 col-sm-4 b-r b-b">
																<div class="padding">
																	
																	<div class="text-center">
																		<h2 class="text-center _600">
																			<?php 
																				$largs = array('track'=>0);
																				echo "KES ".number_format(getSum('loans', 'loantaken', $largs));
																			?>																			
																		</h2>
																		<p class="text-muted m-b-md">Total Loans Given</p>
																		
																	</div>
																</div>
															</div>
															<div class="col-xs-6 col-sm-4 b-r b-b">
																<div class="padding">
																	
																	<div class="text-center">
																		<h2 class="text-center _600">
																			<?php 
																				$largs2 = array('loanstatus' => 'active');
																				echo "KES ".number_format(getSum('loans', 'loantaken', $largs2));
																			?>
																		</h2>
																		<p class="text-muted m-b-md">Total Unpaid Loans</p>
																		
																	</div>
																</div>
															</div>

															<div class="col-xs-6 col-sm-4 b-r b-b">
																<div class="padding">
																	
																	<div class="text-center">
																		<h2 class="text-center _600">
																			<a href="#" data-toggle="modal" data-target="#m-a-a" class="btn btn-lg btn-primary">Reset All Loans</a>																			
																		</h2>
																		<p class="text-muted m-b-md">Reset Loans</p>
																		
																	</div>
																</div>
															</div>
															
															
														</div>

														
														
													</div>
													
												</div>
											</div>
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

				<div class="modal fade animate " data-backdrop="true" id="m-a-a">
					<div class="modal-dialog modal-sm" id="animate">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title">Are You Sure You Want To Reset Loans?</h5>
							</div>										
							<div class="modal-footer">
								<button class="btn dark-white p-x-md" data-dismiss="modal" type="button">No</button> 
								<a href="reset.php" class="btn primary p-x-md" type="button">Yes, Reset</a>
							</div>
						</div>
					</div>
				</div>


	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js">
	</script> 
	<script src="scripts/app.min.js">
	</script>


	
</body>
</html>