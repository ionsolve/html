
  <div class="col-xs w modal fade aside aside-md col-md-5" id="subnav">
    <div class="row-col light bg">
      <div class="row-row">
        <div class="row-body scrollable hover">
          <div class="row-inner">
            <div class="navside">
              <nav class="nav-border b-primary" data-ui-nav="">
                <ul class="nav">
                  <br>
                  <?php

                  if($_SESSION['superadmin'] == "admin"){
                  ?>
                  <li class="">
                    <a href="index.php"><span class="nav-label"><span class="nav-label"><i class="ion-ios-arrow-thin-right">&nbsp;</i></span></span> <span class="nav-text">Dashboard</span></a>
                  </li>
                  <li class="">
                    <a href="allusers.php"><span class="nav-label"><span class="nav-label"><i class="ion-ios-arrow-thin-right">&nbsp;</i></span></span> <span class="nav-text">All Users</span></a>
                  </li>
                  <?php } ?>
                  <li class="">
                    <a href="allloans.php"><span class="nav-label"><span class="nav-label"><i class="ion-ios-arrow-thin-right">&nbsp;</i></span></span> <span class="nav-text">All Loans</span></a>
                  </li>
                  <li class="">
                    <a href="loanreport.php"><span class="nav-label"><span class="nav-label"><i class="ion-ios-arrow-thin-right">&nbsp;</i></span></span> <span class="nav-text">Loan Report</span></a>
                  </li>

                </ul>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
