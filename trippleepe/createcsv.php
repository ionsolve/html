<?php
	require_once 'database.php';
    require_once 'functions.php';

    $random = "File ".date('d-m-Y H:i:s').".csv";

    $myfile = fopen($random, "w");

    $contents = "Customer,Total Loan Taken, Total Interest, Total Payable\n";

    $dstl = mysqli_query($conn, "SELECT DISTINCT `parent` FROM `loans` WHERE `loanstatus`='active'");
    
    while($data = mysqli_fetch_array($dstl))
    {  
        $lparent        = $data['parent'];
        $userargs       = array('id' => $lparent);
        $loanargs       = array('parent' => $lparent, 'loanstatus' => 'active');
        $loansum        = getSum('loans', 'loantaken', $loanargs);
        $totalinterest  = ceil(($loansum * 8.5)/100);

        $totalRepay     = $loansum + $totalinterest;
        getByValue('users', 'name', $userargs);
        

        $contents .= getByValue('users', 'name', $userargs).",".$loansum.",".$totalinterest.",".$totalRepay."\n";
    }

    fwrite($myfile,$contents);

    header('location:'.$random);

?>
