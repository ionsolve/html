 
        <footer>
                <div class="container">
                    <div class="row">
                        <div class=" col-lg-1 col-xl-2 col-12 responsive-phone">
                        </div>
                        <div class="col-6 col-md-4 col-lg-2">
                            <h6>Quick Links</h6>
                            <ul class="footer-links">
                                <li>
                                    <a href="https://ionsolve.com/about/">About Us</a>
                                </li> 
                            </ul>
                        </div>
                        <div class="col-6 col-md-4 col-lg-2">
                            <h6>Knowledgebase</h6>
                            <ul class="footer-links">
                                <li>
                                    <a href="https://ionsolve.com/docs/">API Documentation</a>
                                </li>
                                
                                <li>
                                    <a href="https://ionsolve.com/support/">Support</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-6 col-md-4 col-lg-2">
                            <h6>The Company</h6>
                            <ul class="footer-links">
                                
                            <li>
                                <a href="https://ionsolve.com/terms/">Privacy & Terms</a>
                            </li>
                                <li>
                                    <a href="https://ionsolve.com/account/login/">Login</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-6 col-md-12 col-12 col-xl-3 responsive-phone">
                            <h6>Connect via Social</h6>
                            <ul class="social">
                                <li class="facebook">
                                    <a href="#"><i class="ion-social-facebook"></i></a>
                                </li>
                                <li class="twitter">
                                    <a href="#"><i class="ion-social-twitter"></i></a>
                                </li>
                                
                            </ul>
                            <div class="copyrights">
                                <p>&#xA9; Ionsolve </p>
                            </div>
                        </div>
                    </div>
                </div>
        </footer>


        