<?php

/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
*/

session_start();

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport">
	<meta content="" name="description">
	<meta content="" name="Ionsolve">
	<link href="../assets/img/logo_small.png" rel="icon" type="image/x-icon">
	<title>IonSolve : Pricing</title>
	<link href="../assets/css/app.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
	<link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css">
</head>
<body class="loaded">
	<div class="paper-loading" id="app">
		<?php include_once '../com_header_light.php'; ?>
		<main>
			<section class="b-t text-left animatedParent white animateOnce">
				<div class="container">
					<div class="row">
						<div class="col-12 col-lg-1"></div>
						<div class="col-12 col-lg-10">
							<header class="section-heading">
								<h5 class="styleprimary"><br>
								<br>
								Bulk SMS Pricing</h5>
							</header>
							<div class="card">
								<div class="card-header text-primary">
									Kenya (KES)
								</div>	
								<table class="table">
									<thead>
										<tr>
											<th>Package</th>
											<th>1 - 10,000 </th>
											<th>Above 10,000 </th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td><b>Price Per SMS</b></td>
											<td>0.80</td>
											<td>0.60</td>
										</tr>
									</tbody>
								</table>
							</div>
                            <br>
                            <div class="card">
								<div class="card-header text-primary">
									Tanzania (TZS)
								</div>	
								<table class="table">
									<thead>
										<tr>
											<th>Package</th>
											<th>1 - 200,000 </th>
											<th>Above 200,000 </th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td><b>Price Per SMS</b></td>
											<td>35</td>
											<td>30</td>
										</tr>
									</tbody>
								</table>
							</div>
                            <br>
                            <div class="card">
								<div class="card-header text-primary">
									Uganda (UGX)
								</div>	
								<table class="table">
									<thead>
										<tr>
											<th>Package</th>
											<th>1 - 350,000 </th>
											<th>Above 350,000 </th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td><b>Price Per SMS</b></td>
											<td>35</td>
											<td>30</td>
										</tr>
									</tbody>
								</table>
							</div>

                            <br>
                            <div class="card">
								<div class="card-header text-primary">
									Rwanda (RWF)
								</div>	
								<table class="table">
									<thead>
										<tr>
											<th>Package</th>
											<th>1 - 80,000 </th>
											<th>Above 80,000 </th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td><b>Price Per SMS</b></td>
											<td>10</td>
											<td>8</td>
										</tr>
									</tbody>
								</table>
							</div>

                            <br>
                            <div class="card">
								<div class="card-header text-primary">
									Malawi (MWK)
								</div>	
								<table class="table">
									<thead>
										<tr>
											<th>Package</th>
											<th>1 - 70,000 </th>
											<th>Above 70,000 </th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td><b>Price Per SMS</b></td>
											<td>15</td>
											<td>10</td>
										</tr>
									</tbody>
								</table>
							</div>

                            <br>
                            <div class="card">
								<div class="card-header text-primary">
									Nigeria (NGN)
								</div>	
								<table class="table">
									<thead>
										<tr>
											<th>Package</th>
											<th>1 - 35,000 </th>
											<th>Above 35,000 </th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td><b>Price Per SMS</b></td>
											<td>2.2</td>
											<td>2.0</td>
										</tr>
									</tbody>
								</table>
                            </div>
                            
                            <br>
                            <div class="card">
								<div class="card-header text-primary">
									Others (USD)
								</div>	
								<table class="table">
									<thead>
										<tr>
											<th>Package</th>
											<th>1 - 100 </th>
											<th>Above 100 </th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td><b>Price Per SMS</b></td>
											<td>0.02</td>
											<td>0.018</td>
										</tr>
									</tbody>
								</table>
							</div>

                            <br><br>
						</div>
						<div class="col-12 col-lg-1"></div>
					</div>
				</div>
			</section>
			<section class="service-blocks lighten-5">
				<div class="container">
					<div class="row">
						<div class="col-12 col-lg-1"></div>
					</div>
				</div>
			</section>
		</main><?php include_once '../com_footer.php'; ?>
	</div>
	<script src="../assets/js/app.js"></script>
	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-114404103-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-114404103-1');
</script>
</body>
</html>