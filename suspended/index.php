<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title><?php echo $account_name; ?> Account Suspended.</title>
    <meta name="description" content="Responsive, Bootstrap, BS4">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-barstyle" content="black-translucent">
    <link rel="apple-touch-icon" href="images/logo.png">
    <meta name="apple-mobile-web-app-title" content="Flatkit">
    <meta name="mobile-web-app-capable" content="yes">
    <link rel="shortcut icon" sizes="196x196" href="images/logo.png">
    <link rel="stylesheet" href="css/animate.css/animate.min.css" type="text/css">
    <link rel="stylesheet" href="css/glyphicons/glyphicons.css" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="../assets/css/app.css">
    <link rel="stylesheet" href="../assets/css/styles/font.css" type="text/css">
    <link rel="stylesheet" href="../assets/css/styles/app.min.css" type="text/css">
</head>

<body>
    <div class="app" id="app">
        <?php include_once '../com_header_light.php'; ?>

        <div id="content" class="app-content" role="main">
            <div class="app-body">
                <div class="row-col h-v">
                    <div class="row-cell v-m">
                        <div style="margin-top: -150px;" class="text-center col-sm-6 offset-sm-3 p-y-lg">
                            <h1 class="display-3 m-y-lg">Sorry, your account has been suspended.</h1>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php include_once('../com_footer.php') ?>
    </div>
    <script src="scripts/app.min.js"></script>
</body>

</html>