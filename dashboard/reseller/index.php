<?php

/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
*/

include_once($_SERVER["DOCUMENT_ROOT"] . '/system/core/index.php');


?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title><?php echo $account_name; ?> - Profile</title>
	<meta content="Ionsolve" name="description">
	<meta content="width=device-width,initial-scale=1,maximum-scale=1,minimal-ui" name="viewport">
	<meta content="IE=edge" http-equiv="X-UA-Compatible">
	<meta content="yes" name="">
	<meta content="black-translucent" name="">
	<link href="../images/logo_small.png" rel="">
	<meta content="Ionsolve" name="">
	<meta content="yes" name="">
	<link href="../images/logo_small.png" rel="shortcut icon" sizes="196x196">
	<link href="../css/animate.css/animate.min.css" rel="stylesheet" type="text/css">
	<link href="../css/glyphicons/glyphicons.css" rel="stylesheet" type="text/css">
	<link href="../css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="../css/material-design-icons/material-design-icons.css" rel="stylesheet" type="text/css">
	<link href="../css/ionicons/css/ionicons.min.css" rel="stylesheet" type="text/css">
	<link href="../css/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css">
	<link href="../css/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="../css/styles/app.min.css" rel="stylesheet">
	<link href="../css/styles/font.css" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
	<style type="text/css">
		::-webkit-scrollbar { 
		display: none; 
	}
	</style>
</head>
<body>
	<div class="app" id="app">
		<div class="app-content box-shadow-z2 pjax-container" id="content" role="main">
			<div class="app-body">
				<div class="app-body-inner">
					<div class="row-col">
						<?php include_once '../top.php'; ?>
						<div class="row-row">
							<div class="row-col">
								<?php include_once '../side.php'; ?>
								<div class="col-xs col-md-9" id="detail">
									<p class="text-md text-primary" style="margin-left: 5px;">Reseller</p>
									<div class="row-col">
										<div class="row-row">
											<div class="row-body scrollable hover">
												<div class="row-inner">

													<div class="">
														<div class="box b-a b-t">
															<div class="box-header light lt b-t">
																<h2> <?php echo $account_currency." ".$account_income; ?></h2><small>Earned Commission</small>
															</div>
															<div class="box-body">
																<p class="m-a-0">
																	<a class="btn btn-md btn-primary text-white btn-xs" href="#">Withdraw <i class="ion-ios-arrow-thin-right">&nbsp;</i></a>
																	<a class="btn btn-md btn-primary text-white btn-xs" href="#">Buy Bulk SMS <i class="ion-ios-arrow-thin-right">&nbsp;</i></a>
																</p>
															</div>
														</div>
														
														<div style="padding:10px;">
														<p class="text-md text-primary" style="margin-left: 5px;">
															References
															<a class="btn btn-sm rounded white text-primary" data-toggle="modal" data-target="#writereferenceaccount" data-ui-toggle-class="fade-left-big" data-ui-target="#animate" style="width:170px;">
																<i class="fa fa-plus"></i> Add Reference
															</a>
															
														</p>
														</div>

														<div style="padding:10px;">
															<div class="md-form-group">
															<input id="searchref_" class="md-input" type="text" placeholder="Search References">
															</div>
														</div>
														
														<div id="refspace"></div>

													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div aria-hidden="true" class="modal fade animate" data-backdrop="true" id="writereferenceaccount" style="display: none;">
					<div class="modal-dialog fade-left-big" data-ui-class="fade-left-big" id="animate">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title text-primary">New Reference Account</h5>
								
							</div>
							<div class="modal-body">
								
								<div class="md-form-group">
									<input class="md-input" id="a_name_" placeholder="Account Name">
								</div>
								<div class="md-form-group">
									<input class="md-input" id="a_username_" placeholder ="Pick a username">
								</div>
								<div class="md-form-group">
									<input class="md-input" id="a_phone_" placeholder ="Phone number (International format)">
								</div>
								<div class="md-form-group">
									<input class="md-input" id="a_email_" placeholder ="Account email">
								</div>
								<div class="md-form-group">
									<select class="md-input" id="a_country_">
										<option class="form-control no-border" value="">Select Country</option>
										<option class="form-control no-border" value="KES">Kenya</option>
										<option class="form-control no-border" value="UGX">Uganda</option>
										<option class="form-control no-border" value="TZS">Tanzania</option>
										<option class="form-control no-border" value="MWK">Malawi</option>
										<option class="form-control no-border" value="NGN">Nigeria</option>
										<option class="form-control no-border" value="USD">Other Country</option>
									</select>
                                </div>
								<div class="md-form-group">
									<input class="md-input" type="password" id="a_password_" placeholder ="Set a password">
								</div>
								<br>
								<p id="savereference"></p>
								<div class="checkbox m-b">
									By clicking on signup, you agree our <a href="https://ionsolve.com/terms" class="text-primary">terms and conditions</a> and <a href="https://ionsolve.com/terms" class="text-primary">privacy policy</a>
								</div>
							</div>
							<div class="modal-footer">
								
								<button class="btn dark-white p-x-md" id="forcecloseref" data-dismiss="modal" type="button">Cancel</button> 
								<button class="btn primary p-x-md" id="a_new_account_" type="button">Save Reference <i class="ion-ios-arrow-thin-right">&nbsp;</i></button>
							</div>
						</div>
					</div>
				</div>

		</div>
	</div>
	
	<script src="../scripts/app.min.js"></script>
	<script src="../scripts/dmain.js"></script>

</body>
</html>