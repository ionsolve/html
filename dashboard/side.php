  <head>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
  </head>
  <div class="col-xs w modal fade aside aside-md col-md-3" id="subnav">
    <div class="row-col light bg">
      <div class="row-row">
        <div class="row-body scrollable hover">
          <div class="row-inner">
            <div class="navside">
              <nav class="nav-border b-primary" data-ui-nav="">
                <ul class="nav">
                  <br>

                  <li class="">
                    <a href="<?php echo $base_path; ?>dashboard/">
                      <span class="nav-label text-primary"><i class="ion-ios-arrow-thin-right">&nbsp;</i></span> 
                      <span class="nav-text">Dashboard</span></a>
                  </li>
                  <hr>
                  <li class="">
                    <a href="<?php echo $base_path; ?>dashboard/contacts">
                      <span class="nav-label text-primary"><i class="ion-ios-arrow-thin-right">&nbsp;</i></span>
                      <span class="nav-text">Contacts</span></a>
                  </li>
                  <hr>
                  <li class="">
                    <a href="<?php echo $base_path; ?>dashboard/choosegroup">
                      <span class="nav-label text-primary"><i class="ion-ios-arrow-thin-right">&nbsp;</i></span>
                      <span class="nav-text">Quick SMS</span></a>
                  </li>
                  <hr>
                  <li>
                    <a>
                      <span class="nav-label text-primary"><i class="fa fa-caret-down">&nbsp;</i></span> 
                      <span class="nav-text">Bulk SMS</span>
                    </a>

                    <ul class="nav-sub">
                      <li class="">
                        <a href="<?php echo $base_path; ?>dashboard/templates/">
                        <span class="nav-label text-primary"><i class="ion-ios-arrow-thin-right">&nbsp;</i></span> 
                        <span class="nav-text">Message Templates</span></a>
                      </li>
                      <li class="">
                        <a href="<?php echo $base_path; ?>dashboard/groups">
                        <span class="nav-label text-primary"><i class="ion-ios-arrow-thin-right">&nbsp;</i></span> 
                        <span class="nav-text">Groups</span></a>
                      </li>
                      <li class="">
                        <a href="<?php echo $base_path; ?>dashboard/outbox">
                        <span class="nav-label text-primary"><i class="ion-ios-arrow-thin-right">&nbsp;</i></span> 
                        <span class="nav-text">Outbox</span></a>
                      </li>
                      <li class="">
                        <a href="<?php echo $base_path; ?>dashboard/schedules/">
                        <span class="nav-label text-primary"><i class="ion-ios-arrow-thin-right">&nbsp;</i></span> 
                        <span class="nav-text">Scheduled Messages</span></a>
                      </li>
                      <li class="">
                        <a href="<?php echo $base_path; ?>dashboard/senderids">
                        <span class="nav-label text-primary"><i class="ion-ios-arrow-thin-right">&nbsp;</i></span>
                        <span class="nav-text">Sender IDs</span></a>
                      </li> 
                    </ul>
                  </li>
                  <hr>
                  <li>
                    <a>
                      <span class="nav-label text-primary"><i class="fa fa-caret-down">&nbsp;</i></span> 
                      <span class="nav-text">Shortcodes</span>
                    </a>

                    <ul class="nav-sub">
                      <li class="">
                        <a href="<?php echo $base_path; ?>dashboard/shortcodes">
                        <span class="nav-label text-primary"><i class="ion-ios-arrow-thin-right">&nbsp;</i></span> 
                        <span class="nav-text">My Shortcodes</span></a>
                      </li>
                      
                    </ul>
                  </li>
                  <hr>
                  <li>
                    <a>
                      <span class="nav-label text-primary"><i class="fa fa-caret-down">&nbsp;</i></span> 
                      <span class="nav-text">Billing</span>
                    </a>

                    <ul class="nav-sub">
                    <li class="">
                      <a href="<?php echo $base_path; ?>dashboard/topup">
                      <span class="nav-label text-primary"><i class="ion-ios-arrow-thin-right">&nbsp;</i></span>
                      <span class="nav-text">Top Up Account</span></a>
                    </li>

                    <li class="">
                      <a href="<?php echo $base_path; ?>dashboard/statement">
                      <span class="nav-label text-primary"><i class="ion-ios-arrow-thin-right">&nbsp;</i></span>
                      <span class="nav-text">View Statement</span></a>
                    </li>
                      
                    </ul>
                  </li>
                  <hr>
                  <li>
                    <a>
                      <span class="nav-label text-primary"><i class="fa fa-caret-down">&nbsp;</i></span> 
                      <span class="nav-text">Developers</span>
                    </a>

                    <ul class="nav-sub">
                        <?php
                        if($account_ref ==0){
                      ?>
                      <li class="">
                        <a href="<?php echo $base_path; ?>dashboard/reseller">
                        <span class="nav-label text-primary"><i class="ion-network">&nbsp;</i></span>
                        <span class="nav-text">Reseller</span></a>
                      </li>
                        <?php } ?>
                      <li class="">
                        <a href="<?php echo $base_path; ?>dashboard/apikeys">
                        <span class="nav-label text-primary"><i class="ion-ios-gear-outline">&nbsp;</i></span>
                        <span class="nav-text">API Keys</span></a>
                      </li>
                      
                      <li class="">
                        <a href="<?php echo $base_path; ?>docs" target="_blank">
                        <span class="nav-label text-primary"><i class="ion-code">&nbsp;</i></span>
                        <span class="nav-text">API Documentation</span></a>
                      </li>
                    </ul>
                  </li>
                  <hr>
                  <li class="">
                    <a href="<?php echo $base_path; ?>dashboard/settings">
                    <span class="nav-label text-primary"><i class="ion-ios-settings-strong">&nbsp;</i></span>
                    <span class="nav-text">Account Settings</span></a>
                  </li>
                  <hr>
                  <li class="">
                    <a href="<?php echo $base_path; ?>support" target="_blank">
                    <span class="nav-label text-primary"><i class="ion-ios-help-outline">&nbsp;</i></span>
                    <span class="nav-text">Support</span></a>
                  </li>

                  <?php
                  if (isset($_SESSION['loggedin']) && isset($_SESSION['alphaion'])) {
                  ?>
                  <hr><li class="">
                    <a id="switchtoadmin">
                    <span class="nav-label text-primary"><i class="fas fa-toggle-on">&nbsp;</i></span>
                    <span class="nav-text">Switch To Admin</span></a>
                  </li>
                  <?php
                  }
                  ?>
                  <hr>
                </ul>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> 

  <script type="text/javascript">
    // unset user session after clicking switch user button

    $("#switchtoadmin").click(function(event) {
        $.post('../admin/resources/unsetsession/',
        function(data, textStatus, xhr) {
          if (data == 1) {
            window.location.href = '../admin/dashboard/';
          }
          else{
            location.reload();
          }
        });
    });
  </script>