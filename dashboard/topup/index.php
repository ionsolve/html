<?php

/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
*/

include_once($_SERVER["DOCUMENT_ROOT"] . '/system/core/index.php');
include_once($_SERVER["DOCUMENT_ROOT"] . '/configs/system.php');

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title><?php echo $account_name; ?> - Top Up</title>
	<meta content="Ionsolve" name="description">
	<meta content="width=device-width,initial-scale=1,maximum-scale=1,minimal-ui" name="viewport">
	<meta content="IE=edge" http-equiv="X-UA-Compatible">
	<meta content="yes" name="">
	<meta content="black-translucent" name="">
	<link href="../images/logo_small.png" rel="">
	<meta content="Ionsolve" name="">
	<meta content="yes" name="">
	<link href="../images/logo_small.png" rel="shortcut icon" sizes="196x196">
	<link href="../css/animate.css/animate.min.css" rel="stylesheet" type="text/css">
	<link href="../css/glyphicons/glyphicons.css" rel="stylesheet" type="text/css">
	<link href="../css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="../css/material-design-icons/material-design-icons.css" rel="stylesheet" type="text/css">
	<link href="../css/ionicons/css/ionicons.min.css" rel="stylesheet" type="text/css">
	<link href="../css/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css">
	<link href="../css/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="../css/styles/app.min.css" rel="stylesheet">
	<link href="../css/styles/font.css" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
	<style type="text/css">
		::-webkit-scrollbar { 
		display: none; 
	}
	</style>
</head>

<body>
	<div class="app" id="app">
		<div class="app-content box-shadow-z2 pjax-container" id="content" role="main">
			<div class="app-body">
				<div class="app-body-inner">
					<div class="row-col">
						<?php include_once '../top.php'; ?>
						<div class="row-row">
							<div class="row-col">
                				<?php include_once '../side.php'; ?>
                

								<div class="col-xs col-md-7" id="detail">
									<p class="text-md text-primary" style="margin-left: 5px;">Top Up Account</p>
									<div class="row-col">
										<div class="row-row">
											<div class="row-body scrollable hover">
												<div class="row-inner">
													<div class="m-b" id="accordion">
														<div class="panel box no-border m-b-xs">
															<div class="box-header p-y-sm">
																<a aria-expanded="true" class="" data-parent="#accordion" data-target="#c_1" data-toggle="collapse">Mobile Money</a>
															</div>
															<div aria-expanded="true" class="collapse in" id="c_1" style="">
																<div class="box-body">
																	<div class="streamline">
																		<div class="sl-item b-default">
																			<div class="sl-content text-muted">	
																				<div>
																					Go to MPESA
																				</div>
																			</div>
																		</div>
																		<div class="sl-item b-default">
																			<div class="sl-content text-muted">	
																				<div>
																					Select Lipa Na MPESA
																				</div>
																			</div>
																		</div>
																		<div class="sl-item b-default">
																			<div class="sl-content text-muted">	
																				<div>
																					Select Pay Bill
																				</div>
																			</div>
																		</div>
																		<div class="sl-item b-default">
																			<div class="sl-content text-muted">	
																				<div>
																					Enter Business Number <b><?php echo $paybill_number; ?></b>
																				</div>
																			</div>
																		</div>

																		<div class="sl-item b-default">
																			<div class="sl-content text-muted">	
																				<div>
																					Enter Account Number <b>
																					<?php 
																						$usrArgs = array('id' => $account_id);
																						echo getByValue('users', 'username', $usrArgs); 
																					?></b>
																				</div>
																			</div>
																		</div>

																		<div class="sl-item b-default">
																			<div class="sl-content text-muted">	
																				<div>
																					Enter Amount
																				</div>
																			</div>
																		</div>


																		<div class="sl-item b-success">
																			<div class="sl-icon">
																				<i class="fa fa-check"></i>
																			</div>
																			<div class="sl-content">
																				<div>
																					Send
																				</div>
																			</div>
																		</div>
																		
																		
																	</div>
																</div>
															</div>
														</div>
														
														<div class="panel box no-border m-b-xs">
															<div class="box-header p-y-sm">
																<a aria-expanded="false" class="collapsed text-muted" data-parent="#accordion" data-target="#c_3" data-toggle="collapse">Cheque/Bank</a>
															</div>
															<div aria-expanded="false" class="collapse" id="c_3">
																<div class="box-body">
																<div class="streamline">
																<div class="sl-item b-default">
																	<div class="sl-content text-muted">	
																		<div>
																			Family Bank
																		</div>
																	</div>
																</div>
																<div class="sl-item b-default">
																	<div class="sl-content text-muted">	
																		<div>
																			Account <b>Ionsolve Limited</b>
																		</div>
																	</div>
																</div>
																<div class="sl-item b-default">
																	<div class="sl-content text-muted">	
																		<div>
																			Account Number <b><?php echo $account_number; ?></b>
																		</div>
																	</div>
																</div>

																<div class="sl-item b-info">
																	<div class="sl-icon">
																		<i class="ion-information-circled"></i>
																	</div>
																	<div class="sl-content">
																		<div>
																			For top ups outside Kenya, Use Bank transfer <b class="text-primary">Swift code: <u>FABLKENA</u>, Branch code: 012, Bank code:070</b>
																		</div>
																	</div>
																</div>

																<div class="sl-item b-success">
																	<div class="sl-icon">
																		<i class="fa fa-check"></i>
																	</div>
																	<div class="sl-content">
																		<div>
																			Share receipt / cheque with <a class="text-primary" href="mailto:info@ionsolve.com">info@ionsolve.com</a> for top up
																		</div>
																	</div>
																</div>
						
																
															</div>
																</div>
															</div>
														</div>
														
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<script src="../scripts/app.min.js"></script>
	<script src="../scripts/dmain.js"></script>

</body>
</html>