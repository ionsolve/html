$(document).ready(function()
{
    loadcodes();
    loadcontacts();
    loadgroups();
    loadforgroup();
    loadgroupmembers();
    loadsenders();
    loadstatement();
    loadrefs();
    loadsubcode();
    loadsubscriptionmessages();
    loadoncode();
    loadTemplates();
    loadScheduledMessages();

    // new contact

    $('#save_contact').click(function()
	{
		$('#save_contact').html('<i class="fa ion-load-c fa-spin"></i>');
		$('#save_contact').css('disabled','1');

        var _contact_      = $('#_contact_').val();
        var _phone_        = $('#_phone_').val();
        var _tags_         = $('#_tags_').val();

		$.post("../../system/savecontact/",
		{
            _contact_:_contact_,
            _phone_:_phone_,
            _tags_:_tags_
		},
		function(saveresponse)
		{
			if (saveresponse == 1)
			{
                loadcontacts();

                $("#forceclose" ).trigger( "click" );
                $('#_contact_').val('');
                $('#_phone_').val('');
                $('#_tags_').val('');
                $('#save_contact').html('Save');

			}else{
				$('#saveresponse').html(saveresponse);
				$('#save_contact').css('disabled','0');
				$('#save_contact').html('Save');
			}
		})
    })

    // load contacts

    function loadcontacts()
    {
        $.post('../../system/loadcontacts/',
        function(contresp)
        {
            $('#contactspace').html(contresp);
        });
    }

    function loadScheduledMessages() {
        $.post('../../system/loadschedules/', 
        function(data, textStatus, xhr) {
            $("#scheduledmsgspace").html(data);
        });
    }

    // load message templates

    function loadTemplates() {
        $.post('../../system/loadtemplates/', 
        function(data, textStatus, xhr) {
            $("#templatespace").html(data);
        });
    }


    $(document.body).on("keyup","#search_",function()
    {
        var search_    = $('#search_').val();

        $.post('../../system/loadcontacts/',
        {
            search_:search_
        }, 
        function(returnsearch)
        {
            $('#contactspace').html(returnsearch);
        });
    })

    // new group

    $('#save_group').click(function()
	{
		$('#save_group').html('<i class="fa ion-load-c fa-spin"></i>');
		$('#save_group').css('disabled','1');

        var _groupname_      = $('#_groupname_').val();

		$.post("../../system/savegroup/",
		{
            _groupname_:_groupname_
		},
		function(savegroupresponse)
		{
			if (savegroupresponse == 1)
			{
                loadgroups();

                $("#forceclosegroup" ).trigger( "click" );
                $('#_groupname_').val('');
                $('#save_group').html('Save');

			}else{
				$('#savegroupresponse').html(savegroupresponse);
				$('#save_group').css('disabled','0');
				$('#save_group').html('Save');
			}
		})
    })

    function loadgroups(){
        $.post('../../system/loadgroups/',

        function(groupresp)
        {
            $('#groupspace').html(groupresp);
        });
    }

    $(document.body).on("keyup","#groupsearch_",function()
    {
        var groupsearch_    = $('#groupsearch_').val();

        $.post('../../system/loadgroups/',
        {
            groupsearch_:groupsearch_
        }, 
        function(returngsearch)
        {
            $('#groupspace').html(returngsearch);
        });
    })



    // load contacts for group

    function loadforgroup()
    {
        $.post('../../system/searchforgroup/',
        function(contgresp)
        {
            $('#gnewcontacts').html(contgresp);
        });
    }


    $(document.body).on("keyup","#forgroup",function()
    {
        var forgroup    = $('#forgroup').val();

        $.post('../../system/searchforgroup/',
        {
            forgroup:forgroup
        }, 
        function(returngsearch)
        {
            $('#gnewcontacts').html(returngsearch);
        });
    })


      // load shortcode subscription members

      function loadsubcode()
      {
          $.post('../../system/subscriptionusers/',
          function(scoderesponse)
          {
              $('#scodemembersspace').html(scoderesponse);
          });
      }
  
  
      $(document.body).on("keyup","#forcodemembers",function()
      {
          var forcodemembers    = $('#forcodemembers').val();
  
          $.post('../../system/subscriptionusers/',
          {
            forcodemembers:forcodemembers
          }, 
          function(returnsubscribers)
          {
              $('#scodemembersspace').html(returnsubscribers);
          });
      })

       // load shortcode ondemand members

       function loadoncode()
       {
           $.post('../../system/ondemandusers/',
           function(ondemandcoderesponse)
           {
               $('#ondemandcodemembersspace').html(ondemandcoderesponse);
           });
       }
   
   
       $(document.body).on("keyup","#forondemandmembers",function()
       {
           var forondemandmembers    = $('#forondemandmembers').val();
   
           $.post('../../system/ondemandusers/',
           {
            forondemandmembers:forondemandmembers
           }, 
           function(returnonsubscribers)
           {
               $('#ondemandcodemembersspace').html(returnonsubscribers);
           });
       })


    // load groupmembers for group

    function loadgroupmembers()
    {
        $.post('../../system/loadmembers/',
        function(gmresponse)
        {
            $('#groupmembersspace').html(gmresponse);
        });
    }


    $(document.body).on("keyup","#forgroupmembers",function()
    {
        var forgroupmembers    = $('#forgroupmembers').val();

        $.post('../../system/loadmembers/',
        {
            forgroupmembers:forgroupmembers
        }, 
        function(returnmembers)
        {
            $('#groupmembersspace').html(returnmembers);
        });
    })

    $('#addselected').click(function()
    {
        var selected_values = $("input[name='checkset[]']:checked");
        alert(selected_values);
    });


    // load sender ids
    function loadsenders()
    {
        $.post('../../system/senderids/',
        function(sresponse)
        {
            $('#sids').html(sresponse);
        });
    }

    // load shortcodes

    function loadcodes(){
        $.post('../../system/loadcodes/',

        function(coderesp)
        {
            $('#codespace').html(coderesp);
        });
    }

    $(document.body).on("keyup","#codesearch_",function()
    {
        var codesearch_    = $('#codesearch_').val();

        $.post('../../system/loadcodes/',
        {
            codesearch_:codesearch_
        }, 
        function(returncodes)
        {
            $('#codespace').html(returncodes);
        });
    })


    // load statements

    function loadstatement(){
        $.post('../../system/viewstatement/',

        function(statementresp)
        {
            $('#statementloader').html(statementresp);
        });
    }

    $(document.body).on("keyup","#statsearch_",function()
    {
        var statsearch_    = $('#statsearch_').val();

        $.post('../../system/viewstatement/',
        {
            statsearch_:statsearch_
        }, 
        function(statementsearchresp)
        {
            $('#statementloader').html(statementsearchresp);
        });
    })


    $('#populatebytag').click(function()
	{
		$('#populatebytag').html('<i class="fa ion-load-c fa-spin"></i>');
		$('#populatebytag').css('disabled','1');

        var datefrom      = $('#datefrom').val();
        var dateto        = $('#dateto').val();
        var greentags     = $('#greentags').val();
        var redtags       = $('#redtags').val();

		$.post("../../system/populatebytag/",
		{
            datefrom:datefrom,
            dateto:dateto,
            greentags:greentags,
            redtags:redtags
		},
		function(tagresponse)
		{
			if (tagresponse == 1)
			{
                loadgroupmembers();

                $("#tagclose" ).trigger("click");
                $('#populatebytag').html('Add <i class="ion-ios-arrow-thin-right">&nbsp;</i>');

			}else{
				$('#tagresponse').html(tagresponse);
				$('#populatebytag').css('disabled','0');
				$('#populatebytag').html('Add <i class="ion-ios-arrow-thin-right">&nbsp;</i>');
			}
		})
    })


    // Reference account
    $('#a_new_account_').click(function()
	{
		$('#a_new_account_').html('<i class="fa ion-load-c fa-spin"></i>');
		$('#a_new_account_').css('disabled','1');

		var a_name_        = $('#a_name_').val();
		var a_email_       = $('#a_email_').val();
		var a_phone_       = $('#a_phone_').val();
		var a_username_    = $('#a_username_').val();
		var a_country_     = $('#a_country_').val();
		var a_password_    = $('#a_password_').val();
		
		$.post("../../system/createreferral/",
			{
				a_name_:a_name_,
				a_email_:a_email_,
				a_phone_:a_phone_,
				a_username_:a_username_,
				a_country_:a_country_,
				a_password_:a_password_
			},
			function(respresponse)
			{
				if(respresponse == 1)
				{
                    loadrefs();
                    $("#forcecloseref" ).trigger("click");
				}else{
					$('#savereference').html(respresponse);
					$('#a_new_account_').html('Save Reference <i class="ion-ios-arrow-thin-right">&nbsp;</i>');
                    $('#a_new_account_').css('disabled','0');
				}
			});
    });


    // change user password


    $("#accountlock").click(function(event) {
        $(this).html("<i class='fa ion-load-c fa-spin'></i>");

        var oldpassword = $("#editoldpassword").val();
        var newpassword = $("#editnewpassword").val();
        var confirmpass = $("#editcnewpassword").val();

        $.post('../../system/changepassword/', 
        {
            oldpassword:oldpassword,
            newpassword:newpassword,
            confirmpass:confirmpass
        }, 
        function(data, textStatus, xhr) {
            if (data == 1) {
                $("#accountlock").html('Update Password<i class="ion-ios-arrow-thin-right">&nbsp;</i>');
                $("#passresponse").slideDown().html("<font class='text-success'><i class='ion-checkmark-circled'></i> Password changed successfully.</font>").delay(5000).slideUp();
                $(":password").val('');
            }
            else{
                $("#accountlock").html('Update Password<i class="ion-ios-arrow-thin-right">&nbsp;</i>');
                $("#passresponse").slideDown().html(data).delay(5000).slideUp();
            }
        });
    });

    // new shortcode

    $('#savecodebtn').click(function()
	{
        
		$('#savecodebtn').html('<i class="fa ion-load-c fa-spin"></i>');
		$('#savecodebtn').css('disabled','1');

		var scode        = $('#scode').val();
		var ds           = $('#ds').val();
		var sds          = $('#sds').val();
		var smtmo        = $('#smtmo').val();
		var skeyword     = $('#skeyword').val();
        var sprice       = $('#sprice').val();
        var sautomessage = $('#sautomessage').val();
        var susername    = $('#susername').val();
        var sapikey      = $('#sapikey').val();
		
		$.post("../../system/createcode/",
			{
				scode:scode,
				ds:ds,
				sds:sds,
				smtmo:smtmo,
				skeyword:skeyword,
                sprice:sprice,
                sautomessage:sautomessage,
                susername:susername,
                sapikey:sapikey
			},
			function(coderesponse)
			{
				if(coderesponse == 1)
				{
                    loadcodes();
                    $("#forceclosecode" ).trigger("click");
				}else{
					$('#saveshortcode').html(coderesponse);
					$('#savecodebtn').html('Save  <i class="ion-ios-arrow-thin-right">&nbsp;</i>');
                    $('#savecodebtn').css('disabled','0');
				}
			});
    });
    
    // load referals
    function loadrefs()
    {
        $.post('../../system/loadrefs/',
        function(refload)
        {
            $('#refspace').html(refload);
        });
    }


    $(document.body).on("keyup","#searchref_",function()
    {
        var searchref_    = $('#searchref_').val();

        $.post('../../system/loadrefs/',
        {
            searchref_:searchref_
        }, 
        function(refsearch)
        {
            $('#refspace').html(refsearch);
        });
    })


    // load subscription messages
    function loadsubscriptionmessages()
    {
        $.post('../../system/loadsmessages/',
        function(sload)
        {
            $('#loadscodelogs').html(sload);
        });
    }

    // edit account
    $('#editinfobutton').click(function()
	{
		$('#editinfobutton').html('<i class="fa ion-load-c fa-spin"></i>');
		$('#editinfobutton').css('disabled','1');

		var editinfoname        = $('#editinfoname').val();
		var editinfophone       = $('#editinfophone').val();
		var editinfocity        = $('#editinfocity').val();
		
		$.post("../../system/editsettings/",
			{
				editinfoname:editinfoname,
				editinfophone:editinfophone,
				editinfocity:editinfocity
			},
			function(edresponse)
			{
				if(edresponse == 1)
				{
                    $('#setreference').html("<font class='text-success'><i class='ion-checkmark-circled'></i> Information Updated</font>");
                    $('#editinfobutton').html('Update Information<i class="ion-ios-arrow-thin-right">&nbsp;</i>');
                    $('#editinfobutton').css('disabled','0');
                }else{
					$('#setreference').html(edresponse);
					$('#editinfobutton').html('Update Information<i class="ion-ios-arrow-thin-right">&nbsp;</i>');
                    $('#editinfobutton').css('disabled','0');
				}
			});
    });


    // add the message to the text area when i click on the template that i want to use

    $("#templatemsgs").change(function(event) {
        var message = $(this).val();
        $("#templatemsgs").val("");
        $("#textmessageall").html(message);
    });


    // template messages for groups

    $("#templatemsgsgroup").change(function(event) {
        var message = $(this).val();
        $("#templatemsgsgroup").val("");
        $("#textmessage").html(message);
    });


    // template messages for quick message

    $("#templategmsgs").change(function(event) {
        var message = $(this).val();
        $("#templategmsgs").val("");
        $("#textmessagegroup").html(message);
    });


    // show the schedule input date field when the switch is on

    // hide send later default on page load

    $("#sendlaterdiv").hide();
    $("#sendlatergdiv").hide();

    $("#sendlaterswitch").change(function(event) {
        if ($(this).prop('checked') == true) {
            $("#sendlaterdiv").slideDown().show();
        }
        else{
            $("#sendlaterdiv").hide('4000');
            $("#scheduleinput").val("");
        }

    });

    $("#sendlatergswitch").change(function(event) {
        if ($(this).prop('checked') == true) {
            $("#sendlatergdiv").slideDown().show();
        }
        else{
            $("#sendlatergdiv").hide('4000');
            $("#scheduleinputgroup").val("");
        }

    });


    // schedule subscription
    $('#schedulesmessage').click(function()
	{
		$('#schedulesmessage').html('<i class="fa ion-load-c fa-spin"></i>');
		$('#schedulesmessage').css('disabled','1');

		var stextmessage        = $('#stextmessage').val();
		var sdatetime           = $('#sdatetime').val();
		
		$.post("../../system/sendschedule/",
			{
				stextmessage:stextmessage,
				sdatetime:sdatetime
			},
			function(subresponse)
			{
				if(subresponse == 1)
				{
                    $('#schedulemessageresp').html("<br><font class='text-success'><i class='ion-checkmark-circled'></i> Message scheduled</font>");
                    $('#schedulesmessage').html('Schedule Message <i class="ion-ios-arrow-thin-right">&nbsp;</i>');
                    $('#schedulesmessage').css('disabled','0');
                    $('#stextmessage').val("");
                    $('#sdatetime').val("");
                    loadsubscriptionmessages();
                }else{
					$('#schedulemessageresp').html(subresponse);
					$('#schedulesmessage').html('Schedule Message <i class="ion-ios-arrow-thin-right">&nbsp;</i>');
                    $('#schedulesmessage').css('disabled','0');
				}
			});
    });


    // add a new message template

    $("#savetemplate").click(function(event) {
        $(this).html("<i class='fa ion-load-c fa-spin'></i>");
        $(this).attr('disabled', 'disabled');
        var templatemsg = $("#templatemsg").val();

        $.post('../../system/addtemplate/', 
        {
            templatemsg:templatemsg
        }, 
        function(data, textStatus, xhr) {
            if (data == '1') {
                $('#forceclosetemplate').trigger('click');
                swal('Success!', "New Template has been added successfully.", 'success');
                loadTemplates();
            }
            else{
                $("#savetemplate").html("Add Template <i class='ion-ios-arrow-thin-right'>&nbsp;</i>");
                $("#savetemplate").attr('disabled', false);
                $("#addtemplateresp_").html(data).slideDown().delay(4000).slideUp();;
            }
        });
    });

})