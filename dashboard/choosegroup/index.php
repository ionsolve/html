<?php

/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
*/

include_once($_SERVER["DOCUMENT_ROOT"] . '/system/core/index.php');



?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?php echo $account_name; ?> - Outbox</title>
	<meta content="Ionsolve" name="description">
	<meta content="width=device-width,initial-scale=1,maximum-scale=1,minimal-ui" name="viewport">
	<meta content="IE=edge" http-equiv="X-UA-Compatible">
	<meta content="yes" name="">
	<meta content="black-translucent" name="">
	<link href="../images/logo_small.png" rel="">
	<meta content="Ionsolve" name="">
	<meta content="yes" name="">
	<link href="../images/logo_small.png" rel="shortcut icon" sizes="196x196">
	<link href="../css/animate.css/animate.min.css" rel="stylesheet" type="text/css">
	<link href="../css/glyphicons/glyphicons.css" rel="stylesheet" type="text/css">
	<link href="../css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="../css/material-design-icons/material-design-icons.css" rel="stylesheet" type="text/css">
	<link href="../css/ionicons/css/ionicons.min.css" rel="stylesheet" type="text/css">
	<link href="../css/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css">
	<link href="../css/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="../css/datepicker-bootstrap.css" rel="stylesheet" type="text/css">
	<link href="../css/datepicker-dark.css" rel="stylesheet" type="text/css">
	<link href="../css/styles/app.min.css" rel="stylesheet">
	<link href="../css/styles/font.css" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
	<link href="../codemirror/lib/codemirror.css" rel="stylesheet">
	<style type="text/css">
		::-webkit-scrollbar { 
		display: none; 
	}
	</style>
</head>
<body>
	<div class="app" id="app">
		<div class="app-content box-shadow-z2 pjax-container" id="content" role="main">
			<div class="app-body">
				<div class="app-body-inner">
					<div class="row-col">
						<?php include_once '../top.php'; ?>
						<div class="row-row">
							<div class="row-col">
                				<?php include_once '../side.php'; ?>

								<div class="col-xs col-md-9" id="detail">
									<p class="text-md text-primary" style="margin-left: 5px;">Outbox</p>
									<div class="row-col white bg">
										<div class="row-row">
											<div class="row-body scrollable hover">
												<div class="row-inner">

													<div class="b-b" style="width:100%; background-color:white;">
														<div class="collapse in m-a-0" id="reply-2">

															<br><select id="senderid" class="form-control c-select" style="border:none; border-bottom:1px solid #e8e8e8;">
																<option value="">--Select Sender ID--</option>
																<option value="Ionsolve">System Default</option>
																<?php
																	$sender_args_msg  = array('parent'=> $account_id, 'status'=>'active');
																	$sender_id_values = returnArrayOfRequest('sender_ids','sender_id',$sender_args_msg);

																	if(!empty($sender_id_values))
																	{
																		$ex_sender = explode(",",$sender_id_values);
																	}
																	foreach($ex_sender as $senderid)
																	{
																		echo '<option value="'.$senderid.'">'.$senderid.'</option>';
																	}
																?>
															</select><br>


															<select id="groupmessage" class="form-control c-select" style="border:none; border-bottom:1px solid #e8e8e8;">
																<option value="">--Choose Group--</option>
																<option value="allcontacts">All Contacts</option>
																<?php
																	$group_fetch  = array('parent'=> $account_id);
																	$groupstring = returnArrayOfRequest('groups','id',$group_fetch);

																	if(!empty($group_fetch))
																	{
																		$group_array = explode(",",$groupstring);
																	}
																	foreach($group_array as $eachgroup)
																	{
																		echo '<option value="'.$eachgroup.'">'.getByValue('groups', 'group_name', $group_fetch).'</option>';
																	}
																?>
															</select><br>


															<select id="templategmsgs" class="form-control c-select" style="border:none; border-bottom:1px solid #e8e8e8;">
																<option value="">--Select Template--</option>
																<?php
																	$query = mysqli_query($conn, "SELECT * FROM `templates` WHERE `parent` = '$account_id'");

																	while($listmessages = mysqli_fetch_assoc($query))
																	{
																			echo '<option>'.$listmessages['message'].'</option>';
																	}
																?>
															</select><br>

															<textarea id="textmessagegroup" style="background-color:white;" class="form-control no-border" placeholder="Type message here..." rows="5"></textarea><br>

															<p><label class="md-switch"><input id="sendlatergswitch" type="checkbox" class="has-value"> <i class="blue"></i> <strong style="color: #27AAFF;"> Send Later</strong></label></p>

															<div id="sendlatergdiv" class="container">
															    <div class="row">
															        <div class='col-sm-6'>
															            <div class="form-group">
															                <div class='input-group date' id='datetimepicker1'>
															                    <input id="scheduleinputgroup" type='text' class="form-control" />
															                    <span class="input-group-addon">
															                        <span class="glyphicon glyphicon-calendar"></span>
															                    </span>
															                </div>
															            </div>
															        </div>
															    </div>
															</div>
															
															<div class="box-footer clearfix">
                                                                <button id="send_message_group" class="btn btn-sm white">Send Message &nbsp;<i class="ion-ios-arrow-thin-right">&nbsp;</i> </button>
																<p id="groupmessageresp"></p>
															</div>
														</div>
													</div>

												<div id="loadalllogs"></div>
												</div>
											</div>
										</div>
									</div>
								</div>
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>
	<script src="../scripts/app.min.js"></script>
	<script src="../scripts/dmain.js"></script>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<script src="../css/bootstrap/dist/js/bootstrap.min.js"></script>

	<script>

		// function loadlogs(){
		// 	$('#loadlogs').load("../../system/loadgroupmessages/");
		// }

		function loadlogs(){
			$('#loadalllogs').load("../../system/loadallmessages/");
		}

		loadlogs();

		$('#send_message_group').click(function()
		{
			$(this).html('<i class="fa ion-load-c fa-spin"></i>');
			$(this).css('disabled','1');

			var senderid     = $("#senderid").val();
			var group        = $("#groupmessage").val();
			var message      = $("#textmessagegroup").val();
			var scheduledate = $("#scheduleinputgroup").val();

			$.post("../../system/preparegroupnums/",
			{
				senderid:senderid,
				group:group,
				message:message,
				scheduledate:scheduledate
			},			
			function(messageresponse)
			{
				if (messageresponse == 1) {
					swal('Messages scheduled', 'Your messages have been scheduled on '+scheduledate, 'success')
					.then((value) => {
					  window.location.href = "../../dashboard/schedules/";
					});;
					$('#sendallmessage').html('Send Message <i class="ion-ios-arrow-thin-right">&nbsp;</i>');
					$('#sendallmessage').css('disabled','0');
				}
				else{
					$('#groupmessageresp').slideDown().html(messageresponse).delay('5000').slideUp();
					$('#send_message_group').html('Send Message <i class="ion-ios-arrow-thin-right">&nbsp;</i>');
					$('#send_message_group').css('disabled','0');
				}
			});
			
		});
		
	</script>
	<script>

		// loadlogs();

	</script>

	<script src="../scripts/moment.js"></script>
	<script src="../scripts/datepicker.js"></script>


	<!-- date and time picker -->
	<script type="text/javascript">
        $(function () {
            $('#datetimepicker1').datetimepicker({
            	format: 'YYYY-MM-DD HH:mm:ss'
            });
        });
    </script>
</body>
</html>