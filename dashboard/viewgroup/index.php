<?php

/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
*/

include_once($_SERVER["DOCUMENT_ROOT"] . '/system/core/index.php');

if(!isset($_SESSION['activegroup'])){
	header('dashboard/groups');
}

$groupowner = array('id'=>$_SESSION['activegroup'],'parent'=>$account_id);

if(returnExists('groups', $groupowner) == 0){
	header('dashboard/groups');
}

?>


<!DOCTYPE html>
<html lang="en">
<head>
	<title><?php echo $account_name; ?> - Group Members</title>
	<meta content="Ionsolve" name="description">
	<meta content="width=device-width,initial-scale=1,maximum-scale=1,minimal-ui" name="viewport">
	<meta content="IE=edge" http-equiv="X-UA-Compatible">
	<meta content="yes" name="">
	<meta content="black-translucent" name="">
	<link href="../images/logo_small.png" rel="">
	<meta content="Ionsolve" name="">
	<meta content="yes" name="">
	<link href="../images/logo_small.png" rel="shortcut icon" sizes="196x196">
	<link href="../css/animate.css/animate.min.css" rel="stylesheet" type="text/css">
	<link href="../css/glyphicons/glyphicons.css" rel="stylesheet" type="text/css">
	<link href="../css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="../css/material-design-icons/material-design-icons.css" rel="stylesheet" type="text/css">
	<link href="../css/ionicons/css/ionicons.min.css" rel="stylesheet" type="text/css">
	<link href="../css/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css">
	<link href="../css/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="../css/datepicker-bootstrap.css" rel="stylesheet" type="text/css">
	<link href="../css/datepicker-dark.css" rel="stylesheet" type="text/css">
	<link href="../css/styles/app.min.css" rel="stylesheet">
	<link href="../css/styles/font.css" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
	<style type="text/css">
		::-webkit-scrollbar { 
		display: none; 
	}
	</style>
</head>
<body>
	<div class="app" id="app">
		<div class="app-content box-shadow-z2 pjax-container" id="content" role="main">
			<div class="app-body">
				<div class="app-body-inner">
					<div class="row-col">
						<?php include_once '../top.php'; ?>
						<div class="row-row">
							<div class="row-col">
                				<?php include_once '../side.php'; ?>

								<div class="col-xs col-md-9" id="detail">
									<p class="text-md text-primary" style="margin-left: 5px;">Group
									<b>	
									<?php
										$gargs = array("id" => $_SESSION['activegroup']);
										echo getByValue('groups', 'group_name', $gargs);
									?>
                                    </b>
									
                                    <a class="btn btn-sm rounded white text-primary" href="../groupmembers" style="width:170px;">
										<i class="fa fa-eye"></i> View Members
                                    </a>
									</p>
									
									<div class="row-col white bg">
										<div class="row-row">
											<div class="row-body scrollable hover">
												<div class="row-inner">													

													<div class="b-b" style="width:100%; background-color:white;">
														<div class="collapse in m-a-0" id="reply-2">

															<br><select id="templatemsgsgroup" class="form-control c-select" style="border:none; border-bottom:1px solid #e8e8e8;">
																<option value="">--Select Template--</option>
																<?php
																	$query = mysqli_query($conn, "SELECT * FROM `templates` WHERE `parent` = '$account_id'");

																	while($listmessages = mysqli_fetch_assoc($query))
																	{
																			echo '<option>'.$listmessages['message'].'</option>';
																	}
																?>
															</select><br><br>

															<textarea id="textmessage" style="background-color:white;" class="form-control no-border" placeholder="Type message here..." rows="4"></textarea><br>
															<select id="alongsender" class="form-control c-select" style="border:none; border-bottom:1px solid #e8e8e8;">
																<option value="">--Select Sender ID--</option>
																<option value="Ionsolve">System Default</option>
																<?php
																	$sender_args_msg  = array('parent'=> $account_id, 'status'=>'active');
																	$sender_id_values = returnArrayOfRequest('sender_ids','sender_id',$sender_args_msg);

																	if(!empty($sender_id_values))
																	{
																		$ex_sender = explode(",",$sender_id_values);
																	}
																	foreach($ex_sender as $senderid)
																	{
																		echo '<option value="'.$senderid.'">'.$senderid.'</option>';
																	}
																?>
															</select><br><br>

															<p><label class="md-switch"><input id="sendlaterswitch" type="checkbox" class="has-value"> <i class="blue"></i> <strong style="color: #27AAFF;"> Send Later</strong></label></p>

															<div id="sendlaterdiv" class="container">
															    <div class="row">
															        <div class='col-sm-6'>
															            <div class="form-group">
															                <div class='input-group date' id='datetimepicker1'>
															                    <input id="scheduleinput" type='text' class="form-control" />
															                    <span class="input-group-addon">
															                        <span class="glyphicon glyphicon-calendar"></span>
															                    </span>
															                </div>
															            </div>
															        </div>
															    </div>
															</div>


															<div class="box-footer clearfix">
                                                                <button id="sendgmessage" class="btn btn-sm white">Send Message &nbsp;<i class="ion-ios-arrow-thin-right">&nbsp;</i> </button>
																<p id="gmessageresp"></p>
															</div>
														</div>
													</div>
													<div id="loadlogs"></div>
													
														</div>
                                                    </div>
													
												</div>
											</div>
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
	<script src="../scripts/app.min.js"></script>
	<script src="../scripts/dmain.js"></script>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<script>
		$(document.body).on("keyup","#textmessage",function()
		{
			var datatag             = $('#textmessage').attr('data');
			var text_length         = $('#textmessage').val().length;

			$('#gmessageresp').html('<font class="text-muted"><br>'+text_length + ' characters</font><br><small class="text-info">Note: 160 characters make up one message</small>');
		})
	</script>
	<script>
		$('#sendgmessage').click(function()
		{
			var groupmessagetosend   = $('#textmessage').val();
			var group                = 'group';
			var groupmsgid           = '<?php echo $_SESSION['activegroup']; ?>';
			var alongsender          = $('#alongsender').val();
			var scheduledate           = $("#scheduleinput").val();

			$('#sendgmessage').html('<i class="fa ion-load-c fa-spin"></i>');
			$('#sendgmessage').css('disabled','1');

			$.post("../../system/preparenums/",
			{
				groupmsgid:groupmsgid,
				groupmessagetosend:groupmessagetosend,
				alongsender:alongsender,
				group:group,
				scheduledate:scheduledate
			},
			function(gmessageresponse)
			{
				if (gmessageresponse == 1) {
					swal('Messages schedlued', 'Your messages have been scheduled on '+scheduledate, 'success')
					.then((value) => {
					  window.location.href = "../../dashboard/schedules/";
					});;
					$('#sendgmessage').html('Send Message <i class="ion-ios-arrow-thin-right">&nbsp;</i>');
					$('#sendgmessage').css('disabled','0');
				}
				else{
					$('#gmessageresp').slideDown().html(gmessageresponse).delay('5000').slideUp();
					$('#sendgmessage').html('Send Message <i class="ion-ios-arrow-thin-right">&nbsp;</i>');
					$('#sendgmessage').css('disabled','0');
				}
			});
			
		});

		function loadlogs(){
			$('#loadlogs').load("../../system/loadgroupmessages/");
		}

		loadlogs();
		
	</script>

	<script src="../scripts/moment.js"></script>
	<script src="../scripts/datepicker.js"></script>


	<!-- date and time picker -->
	<script type="text/javascript">
        $(function () {
            $('#datetimepicker1').datetimepicker({
            	format: 'YYYY-MM-DD HH:mm:ss'
            });
        });
    </script>

</body>
</html>