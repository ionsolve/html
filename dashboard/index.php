<?php

/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
*/



include_once($_SERVER["DOCUMENT_ROOT"] . '/system/core/index.php');

if($account_status == 'pending'){
    header("location:account/verify");
}

elseif ($account_status == 'suspended') {
	header("location:suspended/");
}


?>


<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?php echo $account_name; ?> - Dashboard</title>
	<meta content="Ionsolve" name="description">
	<meta content="width=device-width,initial-scale=1,maximum-scale=1,minimal-ui" name="viewport">
	<meta content="IE=edge" http-equiv="X-UA-Compatible">
	<meta content="yes" name="">
	<meta content="black-translucent" name="">
	<link href="images/logo_small.png" rel="">
	<meta content="Ionsolve" name="">
	<meta content="yes" name="">
	<link href="images/logo_small.png" rel="shortcut icon" sizes="196x196">
	<link href="css/animate.css/animate.min.css" rel="stylesheet" type="text/css">
	<link href="css/glyphicons/glyphicons.css" rel="stylesheet" type="text/css">
	<link href="css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="css/material-design-icons/material-design-icons.css" rel="stylesheet" type="text/css">
	<link href="css/ionicons/css/ionicons.min.css" rel="stylesheet" type="text/css">
	<link href="css/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css">
	<link href="css/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="css/styles/app.min.css" rel="stylesheet">
	<link href="css/styles/font.css" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
	
	<link rel="stylesheet" href="//cdn.jsdelivr.net/chartist.js/latest/chartist.min.css">
    <style type="text/css">
	     ::-webkit-scrollbar { 
	       display: none; 
	   }
	   .ct-series-a .ct-bar, .ct-series-a .ct-line, .ct-series-a .ct-point, .ct-series-a .ct-slice-donut{
			stroke-width: 10px;
	   }
	   .ct-series-a .ct-bar, .ct-series-a .ct-line, .ct-series-a .ct-point, .ct-series-a .ct-slice-donut{
		   stroke: #26B555;
	   }
	   .ct-series-b .ct-bar, .ct-series-b .ct-line, .ct-series-b .ct-point, .ct-series-b .ct-slice-donut{
			stroke-width: 10px;
	   }

	   .ct-series-b .ct-bar, .ct-series-b .ct-line, .ct-series-b .ct-point, .ct-series-b .ct-slice-donut{
			stroke: #ff9999;
	   }
	</style>
</head>
<body>
	<div class="app" id="app">
		<div class="app-content box-shadow-z2 pjax-container" id="content" role="main">
			<div class="app-body">
				<div class="app-body-inner">
					<div class="row-col">
						<?php include_once 'top.php'; ?>
						<div class="row-row">
							<div class="row-col">
                				<?php include_once 'side.php'; ?>
                
								<div class="col-xs col-md-9" id="detail">
									<p class="text-md text-primary" style="margin-left: 5px;">Dashboard</p>
									<div class="row-col">
										<div class="row-row">
											<div class="row-body scrollable hover">
												<div class="row-inner">

									
													<div class="box b-a b-t">
														<div class="box-header light lt b-t">
															<h2><?php echo $account_currency." ".number_format($total_in_accounts); ?> </h2>
															<small>Account Balance</small>
														</div>
														<div class="box-body">
															<p class="m-a-0">
															<a href="topup" class="btn btn-md btn-primary text-white btn-xs">Top Up Account <i class="ion-ios-arrow-thin-right">&nbsp;</i></a>
															</p>
														</div>
													</div>
													<?php
													if($account_ref ==0){
													?>
													<div class="box b-a">
														<div class="box-header light lt">
															<h3>Ionsolve Reseller</h3>
															<small>Want to earn? Create a revenue profile and earn by referring people to Ionsolve</small>
														</div>
														<div class="box-body">
															<p class="m-a-0">
															<a href="reseller" class="btn btn-md btn-primary text-white btn-xs">Create Reseller Profile <i class="ion-ios-arrow-thin-right">&nbsp;</i></a>
															</p>
														</div>
													</div>
													<?php } ?>

                                                    <div class="row no-gutter white">
														<div class="col-xs-6 col-sm-3 b-r b-b">
															<div class="padding">					
																<div class="text-center">
																	<h2 class="text-center _600 text-primary">
	   																	<?php
																			$contargs = array('parent' => $account_id);
																			echo returnCountWithCondition('contacts', $contargs);
																		?>
																	</h2>
																	<p class="text-muted m-b-md">Contacts</p>
																	<a href="contacts" class="text-primary">View all <i class="ion-ios-arrow-thin-right">&nbsp;</i></a>
																</div>
															</div>
														</div>
														<div class="col-xs-6 col-sm-3 b-r b-b">
															<div class="padding">
																
																<div class="text-center">
																	<h2 class="text-center _600 text-primary">
																		<?php
																			$contargs = array('parent' => $account_id);
																			echo returnCountWithCondition('groups', $contargs);
																		?>
																	</h2>
																	<p class="text-muted m-b-md">Groups</p>
																	<a href="groups" class="text-primary">View all <i class="ion-ios-arrow-thin-right">&nbsp;</i></a>
																</div>
															</div>
														</div>
														<div class="col-xs-6 col-sm-3 b-r b-b">
															<div class="padding">
																<div class="text-center">
																	<h2 class="text-center _600 text-primary">
																		<?php
																			$contargs = array('parent' => $account_id);
																			echo returnCountWithCondition('sender_ids', $contargs);
																		?>
																	</h2>
																	<p class="text-muted m-b-md">Sender IDs</p>
																	<a href="senderids" class="text-primary">View all <i class="ion-ios-arrow-thin-right">&nbsp;</i></a>
																</div>
															</div>
														</div>
														<div class="col-xs-6 col-sm-3 b-b">
															<div class="padding">
																<div class="text-center">
																	<h2 class="text-center _600 text-primary">
																		<?php
																			$contargs = array('parent' => $account_id);
																			echo returnCountWithCondition('messages', $contargs);
																		?>
																	</h2>
																	<p class="text-muted m-b-md">Outbox</p>
																	<a href="outbox" class="text-primary">View all <i class="ion-ios-arrow-thin-right">&nbsp;</i></a>
																</div>
															</div>
														</div>
													</div>
                                                   
													<br>
													<div class="ct-chart white" style="margin-left:-10px;"></div>
													
											</div>
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>



	</div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> 
	<script src="scripts/app.min.js"></script>
	<script src="//cdn.jsdelivr.net/chartist.js/latest/chartist.min.js"></script>

    <script>

		var options = {
			height: 250
		};

		new Chartist.Bar('.ct-chart', {
		
		<?php
			$arrdates = getLastNDays(7, 'Y-m-d');
		?>
		
		labels: [
			<?php
				foreach($arrdates as $datethen){
					echo "'".$datethen."',";
				}
			?>
		],
		series: [
			[
				<?php
				foreach($arrdates as $datesuccess){
					$num = returnValuesOf('Success',$datesuccess,$account_id);
					if($num = 0){
						echo ",";
					}else{
						echo returnValuesOf('Success',$datesuccess,$account_id).",";
					}
				}
				?>
			],
			[
				<?php
				foreach($arrdates as $datefailed){
					$num2 = returnValuesOf('Failed',$datefailed,$account_id);
					if($num2 = 0){
						echo ",";
					}else{
						echo returnValuesOf('Failed',$datefailed,$account_id).",";
					}
				}
				?>
			]
		]
		},options,{
		axisX: {
			// On the x-axis start means top and end means bottom
			position: 'start'
		},
		axisY: {
			// On the y-axis start means left and end means right
			position: 'end'
		}
		});


	</script>

</body>
</html>