<?php

/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
*/

include_once($_SERVER["DOCUMENT_ROOT"] . '/system/core/index.php');

if(!isset($_SESSION['activecode'])){
	header('location:https://ionsolve.com/dashboard/shortcodes');
}

$codeowner = array('id'=>$_SESSION['activecode'],'parent'=>$account_id);

if(returnExists('short_codes', $codeowner) == 0){
	header('location:https://ionsolve.com/dashboard/shortcodes');
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title><?php echo $account_name; ?> - Subscription Messages</title>
	<meta content="Ionsolve" name="description">
	<meta content="width=device-width,initial-scale=1,maximum-scale=1,minimal-ui" name="viewport">
	<meta content="IE=edge" http-equiv="X-UA-Compatible">
	<meta content="yes" name="">
	<meta content="black-translucent" name="">
	<link href="../images/logo_small.png" rel="">
	<meta content="Ionsolve" name="">
	<meta content="yes" name="">
	<link href="../images/logo_small.png" rel="shortcut icon" sizes="196x196">
	<link href="../css/animate.css/animate.min.css" rel="stylesheet" type="text/css">
	<link href="../css/glyphicons/glyphicons.css" rel="stylesheet" type="text/css">
	<link href="../css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="../css/material-design-icons/material-design-icons.css" rel="stylesheet" type="text/css">
	<link href="../css/ionicons/css/ionicons.min.css" rel="stylesheet" type="text/css">
	<link href="../css/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css">
	<link href="../css/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="../css/styles/app.min.css" rel="stylesheet">
	<link href="../css/styles/font.css" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
	<style type="text/css">
		::-webkit-scrollbar { 
		display: none; 
	}
	</style>
    <link href="../css/datetime.css" rel="stylesheet">
</head>
<body>
	<div class="app" id="app">
		<div class="app-content box-shadow-z2 pjax-container" id="content" role="main">
			<div class="app-body">
				<div class="app-body-inner">
					<div class="row-col">
						<?php include_once '../top.php'; ?>
						<div class="row-row">
							<div class="row-col">
                				<?php include_once '../side.php'; ?>

								<div class="col-xs col-md-9" id="detail">
									
                                    <p class="text-md text-primary" style="margin-left: 5px;">Shortcode Details
									<b>	
									<?php
										$gargs = array("id" => $_SESSION['activecode']);
										echo getByValue('short_codes', 'code', $gargs)." - ".getByValue('short_codes', 'keyword', $gargs);
									?>
									
									</b>
										<a class="btn btn-sm rounded white text-primary" href="https://ionsolve.com/dashboard/subscriptionusers/" >
											<i class="fa fa-users"></i> View Subscribers
										</a>				
									</p>
									
									<div class="row-col white bg">
										<div class="row-row">
											<div class="row-body scrollable hover">
												<div class="row-inner">													

													<div class="b-b" style="width:100%; background-color:white;">
														<div class="collapse in m-a-0" id="reply-2">

															<textarea id="stextmessage" style="background-color:white;" class="form-control no-border" placeholder="Type message here..." rows="3"></textarea>
                                                            <input class="form-control no-border" id="sdatetime" type="text" placeholder="Enter Date/Time"/>
                                                            <br>
															<div class="box-footer clearfix">
                                                                <button id="schedulesmessage" class="btn btn-sm white">Schedule Message &nbsp;<i class="ion-ios-arrow-thin-right">&nbsp;</i> </button>
																<br><p id="schedulemessageresp"></p>
															</div>
														</div>
													</div>
													<div id="loadscodelogs"></div>
													
													</div>
                                                    </div>
													
												</div>
											</div>
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
	<script src="../scripts/app.min.js"></script>
	<script src="../scripts/dmain.js"></script>
    <script src="../scripts/datetime.js"></script>
	<script>
		$(document.body).on("keyup","#stextmessage",function()
		{
			var datatag             = $('#stextmessage').attr('data');
			var text_length         = $('#stextmessage').val().length;

			$('#gmessageresp').html('<font class="text-muted"><br>'+text_length + ' characters</font><br><small class="text-info">Note: 160 characters make up one message</small>');
		})
	</script>
	<script>
		$('#sendgmessage').click(function()
		{
			var groupmessagetosend   = $('#textmessage').val();
			var group                = 'group';
			var groupmsgid           = '<?php echo $_SESSION['activegroup']; ?>';
			var alongsender          = $('#alongsender').val();

			$('#sendgmessage').html('<i class="fa ion-load-c fa-spin"></i>');
			$('#sendgmessage').css('disabled','1');

			$.post("../../system/preparenums/",
			{
				groupmsgid:groupmsgid,
				groupmessagetosend:groupmessagetosend,
				alongsender:alongsender,
				group:group
			},
			function(gmessageresponse)
			{
				if(alongsender !==""){
					$('#textmessage').val("");
				}
				
				$('#gmessageresp').slideDown().html(gmessageresponse).delay('5000').slideUp();
				loadcodelogs();
				$('#sendgmessage').html('Send Message <i class="ion-ios-arrow-thin-right">&nbsp;</i>');
				$('#sendgmessage').css('disabled','0');
			});
			
		});

		function loadcodelogs(){
			$('#loadcodelogs').load("../../system/loadcodemessages/");
		}

		loadcodelogs();
		
	</script>

    <script>
        $('#sdatetime').datetimepicker({
            format:'Y-m-d H:i',
            minDate:'<?php echo date('Y-m-d'); ?>'
        });
    </script>

</body>
</html>