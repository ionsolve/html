<?php

/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
*/

include_once($_SERVER["DOCUMENT_ROOT"] . '/system/core/index.php');

if(!isset($_SESSION['activegroup'])){
	header('dashboard/groups');
}

$groupowner = array('id'=>$_SESSION['activegroup'],'parent'=>$account_id);

if(returnExists('groups', $groupowner) == 0){
	header('dashboard/groups');
}

?>


<!DOCTYPE html>
<html lang="en">
<head>
	<title><?php echo $account_name; ?> - Group Members</title>
	<meta content="Ionsolve" name="description">
	<meta content="width=device-width,initial-scale=1,maximum-scale=1,minimal-ui" name="viewport">
	<meta content="IE=edge" http-equiv="X-UA-Compatible">
	<meta content="yes" name="">
	<meta content="black-translucent" name="">
	<link href="../images/logo_small.png" rel="">
	<meta content="Ionsolve" name="">
	<meta content="yes" name="">
	<link href="../images/logo_small.png" rel="shortcut icon" sizes="196x196">
	<link href="../css/animate.css/animate.min.css" rel="stylesheet" type="text/css">
	<link href="../css/glyphicons/glyphicons.css" rel="stylesheet" type="text/css">
	<link href="../css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="../css/material-design-icons/material-design-icons.css" rel="stylesheet" type="text/css">
	<link href="../css/ionicons/css/ionicons.min.css" rel="stylesheet" type="text/css">
	<link href="../css/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css">
	<link href="../css/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="../css/styles/app.min.css" rel="stylesheet">
	<link href="../css/styles/font.css" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<style type="text/css">
		::-webkit-scrollbar 
		{ 
			display: none; 
		}
		#ui-datepicker-div{
			font-family: 'Varela Round', sans-serif;
		}
	</style>
</head>
<body>
	<div class="app" id="app">
		<div class="app-content box-shadow-z2 pjax-container" id="content" role="main">
			<div class="app-body">
				<div class="app-body-inner">
					<div class="row-col">
						<?php include_once '../top.php'; ?>
						<div class="row-row">
							<div class="row-col">
                				<?php include_once '../side.php'; ?>

								<div class="col-xs col-md-9" id="detail">
									<p class="text-md text-primary" style="margin-left: 5px;">Group
									<b>	
									<?php
										$gargs = array("id" => $_SESSION['activegroup']);
										echo getByValue('groups', 'group_name', $gargs);
									?>
									
									</b>Members
										<a class="btn btn-sm rounded white text-primary" data-toggle="modal" data-target="#addmembers" data-ui-toggle-class="fade-left-big" data-ui-target="#animate">
											<i class="fa fa-plus"></i> Add Members
										</a>
										<a class="btn btn-sm rounded white text-primary" href="../viewgroup" >
											<i class="fa fa-envelope"></i> Send message
										</a>
										
														
									</p>
									<div class="row-col white bg">
										<div class="row-row">
											<div class="row-body scrollable hover">
												<div class="row-inner">
													<div style="padding:10px;">

														<div class="md-form-group">
															<input id="forgroupmembers" class="md-input" type="text" placeholder="Search Members">
														</div>

														
													</div>
													

													<div id="groupmembersspace"></div>

												</div>
											</div>
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

				<div aria-hidden="true" class="modal fade animate" data-backdrop="true" id="addmembers" style="display: none;">
					<div class="modal-dialog modal-lg fade-left-big" data-ui-class="fade-left-big" id="animate">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title text-primary" >Add Members</h5>				
							</div>
							
							<div class="modal-body">
							
									<div class="m-b" id="accordion">
										<div class="panel box no-border m-b-xs">
											<div class="box-header p-y-sm">
												<a aria-expanded="true" class="text-primary" data-parent="#accordion" data-target="#c_1" data-toggle="collapse">Polulate By Tag From Contacts <i class="ion-ios-arrow-thin-right">&nbsp;</i></a>
											</div>
											<div aria-expanded="true" class="collapse in" id="c_1"  style="">
												<div class="box-body">
												<div class="m-b-lg">
													<div class="form-group row input-group-md">
														<div class="col-sm-12">
															<div class="row">
																<div class="col-md-6">
																	<input class="form-control" id="datefrom" style="border-radius:0; border:none; border-bottom:1px solid #e8e8e8;" placeholder="From" type="text" max="<?php echo date('Y-m-d'); ?>">
																</div>
																<div class="col-md-6">
																	<input class="form-control" id="dateto" style="border-radius:0; border:none; border-bottom:1px solid #e8e8e8;" placeholder="To" type="text" max="<?php echo date('Y-m-d'); ?>">
																</div>
																
															</div>
														</div>
													</div>	
													

													<div class="form-group row input-group-md">
														<div class="col-sm-12">
															<input class="form-control" id="greentags" style="border-radius:0; border:none; border-bottom:1px solid #e8e8e8;" placeholder="Included tags e.g programmers nairobi" type="text">
														</div>
													</div>	

													<div class="form-group row input-group-md">
														<div class="col-sm-12">
															<input class="form-control" style="border-radius:0; border:none; border-bottom:1px solid #e8e8e8;" id="redtags" placeholder="Excluded tags e.g mombasa farmers" type="text">
														</div>
													</div>	
													<p id="tagresponse"></p>
													<button class="pull-right btn primary p-x-md" id="populatebytag">Add <i class="ion-ios-arrow-thin-right">&nbsp;</i></button>
													<button class="pull-right btn dark-white p-x-md" id="tagclose" data-dismiss="modal" type="button">Cancel</button>
													
												</div>
												</div>
											</div>
										</div>
										<div class="panel box no-border m-b-xs">
											<div class="box-header p-y-sm">
												<a aria-expanded="false" class="text-primary collapsed" data-parent="#accordion" data-target="#c_3" data-toggle="collapse">Select Manually From Contacts <i class="ion-ios-arrow-thin-right">&nbsp;</i></a>
											</div>
											<div aria-expanded="false" class="collapse" id="c_3" style="height: 0px;">
												<div class="box-body">
												<div class="md-form-group">
													<input class="md-input" id="forgroup" placeholder="Search Contacts" style="padding:10px;">
												</div>
												<form action="https://ionsolve.com/system/addtogroup/" method="post">
													<div id="gnewcontacts"></div>
													<button class="pull-right btn primary p-x-md" id="" type="submit">Add <i class="ion-ios-arrow-thin-right">&nbsp;</i></button>
													<button class="pull-right btn dark-white p-x-md" id="" data-dismiss="modal" type="button">Cancel</button> 
													<br><br>
												</form>
												</div>
											</div>
										</div>
										<div class="panel box no-border m-b-xs">
											<div class="box-header p-y-sm">
												<a aria-expanded="false" class="text-danger collapsed" data-parent="#accordion" data-target="#c_4" data-toggle="collapse">Remove All Contacts From Group <i class="ion-ios-arrow-thin-right">&nbsp;</i></a>
											</div>
											<div aria-expanded="false" class="collapse" id="c_4" style="height: 0px;">
												<div class="box-body">
													<h6>Are you sure you want to continue? This action cannot be reversed</h6>
													<button class="pull-right btn danger p-x-md" id="" type="submit">Remove <i class="ion-ios-arrow-thin-right">&nbsp;</i></button>
													<button class="pull-right btn dark-white p-x-md" id="" data-dismiss="modal" type="button">Cancel</button> 
												
												</div>
											</div>
										</div>
									</div>
									
								<p id=""></p>
							</div>
							<div class="modal-footer">
								
							</div>
							
						</div>
					</div>
				</div>

				<!-- Send Message-->
				<div aria-hidden="true" class="modal fade animate" data-backdrop="true" id="messagegtop" style="display: none;">
					<div class="modal-dialog modal-lg fade-left-big" data-ui-class="fade-left-big" id="animate">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title text-primary">SMS <?php $gargs = array("id" => $_SESSION['activegroup']); echo getByValue('groups', 'group_name', $gargs); ?></h5>			
							</div>
							<div class="modal-body p-lg">
								<div class="md-form-group">
									<br><textarea class="md-input" id="" placeholder="Type Message Here" rows="4"></textarea>
									<label>Message</label>
								</div>
								<p id="smscontactresponse"></p>
							</div>
							<div class="modal-footer" id="message">
								<button class="btn dark-white p-x-md" id="" data-dismiss="modal" type="button">Cancel</button> 
								<button class="btn primary p-x-md" value="" id="" type="button">Send <i class="ion-ios-arrow-thin-right">&nbsp;</i></button>
							</div>
						</div>
					</div>
				</div>

	</div>
	<script src="../scripts/app.min.js"></script>
	<script src="../scripts/dmain.js"></script>
	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	  	<script>
		$( function() {
			$( "#datefrom" ).datepicker({
				dateFormat: "yy-mm-dd",
				maxDate: "+0d"
			});
			$( "#dateto" ).datepicker({
				dateFormat: "yy-mm-dd",
				maxDate: "+0d"
			});
		});
		</script>
	
</body>
</html>