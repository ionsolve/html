<?php

/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
*/

session_start();

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport">
	<meta content="" name="description">
	<meta content="" name="Ionsolve">
	<link href="../assets/img/logo_small.png" rel="icon" type="image/x-icon">
	<title>IonSolve : Terms & Conditions</title>
	<link href="../assets/css/app.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
	<link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css">
</head>
<body class="loaded">
	<div class="paper-loading" id="app">
		<?php include_once '../com_header_light.php'; ?>
		<main class="single single-knowledgebase">

			<div class="content-wrapper">
				<div class="container">
					<div class="col-lg-9 mx-md-auto">
						<article class="post">

							<h2>IonSolve Solutions Terms of Service</h2><br>
							<h4>1. Terms</h4>
							<p>By accessing the website at <a href="https://ionsolve.com">https://ionsolve.com</a>, you are agreeing to be bound by these terms of service, all applicable laws and regulations, and agree that you are responsible for compliance with any applicable local laws. If you do not agree with any of these terms, you are prohibited from using or accessing this site. The materials contained in this website are protected by applicable copyright and trademark law.</p>
							
							<h4>2. Disclaimer</h4>
							<ol type="a">
							<li>The materials on IonSolve Solutions' website are provided on an 'as is' basis. IonSolve Solutions makes no warranties, expressed or implied, and hereby disclaims and negates all other warranties including, without limitation, implied warranties or conditions of merchantability, fitness for a particular purpose, or non-infringement of intellectual property or other violation of rights.</li>
							<li>Further, IonSolve Solutions does not warrant or make any representations concerning the accuracy, likely results, or reliability of the use of the materials on its website or otherwise relating to such materials or on any sites linked to this site.</li>
							</ol>
							<h4>3. Limitations</h4>
							<p>In no event shall IonSolve Solutions or its suppliers be liable for any damages (including, without limitation, damages for loss of data or profit, or due to business interruption) arising out of the use or inability to use the materials on IonSolve Solutions' website, even if IonSolve Solutions or a IonSolve Solutions authorized representative has been notified orally or in writing of the possibility of such damage. Because some jurisdictions do not allow limitations on implied warranties, or limitations of liability for consequential or incidental damages, these limitations may not apply to you.</p>
							<h4>4. Accuracy of materials</h4>
							<p>The materials appearing on IonSolve Solutions website could include technical, typographical, or photographic errors. IonSolve Solutions does not warrant that any of the materials on its website are accurate, complete or current. IonSolve Solutions may make changes to the materials contained on its website at any time without notice. However IonSolve Solutions does not make any commitment to update the materials.</p>
							<h4>5. Links</h4>
							<p>IonSolve Solutions has not reviewed all of the sites linked to its website and is not responsible for the contents of any such linked site. The inclusion of any link does not imply endorsement by IonSolve Solutions of the site. Use of any such linked website is at the user's own risk.</p>
							<h4>6. Modifications</h4>
							<p>IonSolve Solutions may revise these terms of service for its website at any time without notice. By using this website you are agreeing to be bound by the then current version of these terms of service.</p>
							<h4>7. Governing Law</h4>
							<p>These terms and conditions are governed by and construed in accordance with the laws of Kenya and you irrevocably submit to the exclusive jurisdiction of the courts in that State or location.</p>
						</article>
					</div>
				</div>

			</div>
		</main>
		
		<?php include_once '../com_footer.php'; ?>
	</div>
	<script src="../assets/js/app.js">
	</script>
</body>
</html>