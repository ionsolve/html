		<div class="nav-absolute nav-border nav-light">		
			<nav class="mainnav navbar navbar-default justify-content-between">
				<div style="border-bottom:1px solid rgba(255, 255, 255, .2); color:white; padding:5px; width:100%; text-align:right; padding-right:50px;">
					<a class="text-white" href="tel:0782810000"><i class="ion-android-call"></i> 0782 810 000</a> | <a class="text-white" href="mailto:info@ionsolve.com"><i class="ion-email"></i> info@ionsolve.com</a>
				</div>
				<div class="container relative">
                    <a aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation" class="offcanvas dl-trigger paper-nav-toggle" data-toggle="offcanvas" type="button"><i></i></a> 
                    <a class="navbar-brand" href="/solveitafrica">
                            <img alt="" class="d-inline-block align-top" src="assets/img/logo.png" style="width:150px;">
                    </a>
					<div class="paper_menu">
						
						<div class="xv-menuwrapper responsive-menu" id="dl-menu">
							<ul class="dl-menu align-items-center">
								<li class="active">
									<a href="/">Home</a>
								</li>
								<li class="active">
									<a href="about/">About Us</a>
								</li>
								<li class="active">
									<a href="docs">Developers</a>
								</li>
								
								<li class="active">
									<a href="pricing/">Pricing</a>
								</li>
								<?php
									if(!isset($_SESSION['alphaion'])){
								?>
								<li class="active">
									<a href="account/login/">Sign In <i class="ion-ios-arrow-thin-right">&nbsp;</i></a>
								</li>
								<li>
									<a href="account/signup/" class="btn btn-primary btn-lg nav-btn">Sign Up </a>
								</li>
								<?php }else{ ?>
								<li class="active">
									<a href="dashboard/">Dashboard <i class="ion-ios-arrow-thin-right">&nbsp;</i></a>
								</li>
								<?php } ?>
								
							</ul>
						</div>
						
					</div>
				</div>
			</nav>
        </div>
