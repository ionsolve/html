<?php

/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
*/


session_start();

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport">
        <meta content="" name="description">
        <meta content="" name="Ionsolve">
        <link href="assets/img/logo_small.png" rel="icon" type="image/x-icon">
        <title>IonSolve | Best Bulk SMS And API Provider</title>
        <link href="assets/css/app.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    </head>
<body>

	<div class="paper-loading" id="app">
		
		<?php include_once 'com_header_dark.php'; ?>
        
		<main>
			<section class="hero-header animatedParent green lighten-2" style="background: url('assets/img/12346.png');">
				<div class="table">
					<div class="header-text">
						<div class="container">
							<div class="row m-t-150">
								<div class="col-lg-6 animated bounceInLeft">
									<div class="responsive">
										<div class="p-t-b-50 text-white">
                                            <br><br><br>
											<h1><span class="s-18 font-weight-lighter"></span></h1>
											<!-- <span class="s-64 font-weight-bolder">Dashboard</span></h1> -->
											<h3 class="cd-headline clip is-full-width s-36 font-weight-bolder">
                                                <span class="cd-words-wrapper">
                                                    <b class="is-visible">An Interactive Dashboard</b> 
                                                    <b>The Best Rest API</b> <b>The Lowest Pricing</b>
                                                </span></h3>
											<p class="s-18 p-t-b-20 font-weight-lighter">
                                                Create contacts, manage groups, schedule messages, add tags; Send messages to over 150  countries
                                            </p>
											
											<div class="p-t-b-20">
												<?php
													if(!isset($_SESSION['alphaion'])){
												?>
												<a class="btn btn-success btn-lg" href="account/signup">
                                                    Create Free Account <i class="ion-ios-arrow-thin-right">&nbsp;</i>
                                                </a>
												<?php
													}else{
												?>
												<a class="btn btn-success btn-lg" href="dashboard">
                                                    My Account <i class="ion-ios-arrow-thin-right">&nbsp;</i>
                                                </a>
												<?php } ?>
                                                <a class="btn btn-default btn-lg" href="docs">
                                                    API Docs <i class="ion-ios-arrow-thin-right">&nbsp;</i>
                                                </a>
											</div>
										</div>
									</div>
								</div>

								<div class="col-lg-6">
									
								</div>
							</div>
						</div>
					</div>
					<div class="overlay" data-end="#A345C1" data-opacity=".6" data-orientation="toleft" data-start="#03A9F4"></div>
				</div>
            </section>
            
            <section class="p-t-b-80 p-t-40 animatedParent animateOnce responsive">
				<div class="container">
					<header class="section-heading">
						<h3>Comes with amazing features</h3>
						<p>You don&apos;t have to believe check out yourself.</p>
					</header>

					<br><div style="margin-top: -50px;" class="row p-b-80">
						<div class="col-md-4 col-sm-4 col-xs-4"></div>
						<div class="col-md-6 col-sm-4 col-xs-4">
							<section class="m-b-lg" style="max-width: 380px;">
								
									<div class="list-group">
										<div>
											<input class="form-control no-border" id="phone" placeholder="Enter Phone Number (International Format)" type="text">
										</div><br>

										<p id="testerror"></p>

										<button class="btn btn-lg btn-primary btn-block" id="testbtn">Test</button>
									</div>		
							</section>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-4"></div>
					</div>


					<div class="row">
						<div class="col-lg-10 mx-md-auto">
							<div class="row">
								<div class="col-12 col-lg-6">
									<div class="service-featured-content p-t-b-40">
										<h3 class="fadeInUpShort animated text-primary">Complete SMS Dashboard</h3><br>
										<p class="fadeInUpShort animated s-18">Send messages on our dashboard and enjoy the amazing feature of grouping contacts</p><br>
										<?php
											if(!isset($_SESSION['alphaion'])){
										?>
										<a class="btn btn-primary fadeInUpShort animated blue1 lighten-2" href="account/signup">
											Create Free Account <i class="ion-ios-arrow-thin-right"></i>
										</a>
											<?php }else{ ?>		
										<a class="btn btn-primary fadeInUpShort animated blue1 lighten-2" href="dashboard">
											Dashboard <i class="ion-ios-arrow-thin-right"></i>
										</a>

										<?php } ?>
									</div>
								</div>

								<div class="col-12 col-lg-6">
							        <div class="service-featured-thumbs">
							            <div class="service-thumb animated fadeInUpShort go">
							                <a class="img-link" href="#">
							                    <img alt="" class="" src="assets/img/dummy/api.png"></a>
							            </div>
							        </div>
							    </div>
							</div>
							<div class="row p-t-80">
								<div class="col-12 col-lg-6">
									<div class="service-featured-thumbs">
										<div class="service-thumb animated fadeInUpShort">
											<a class="img-link" href="#">
											<img alt="" class="img-fluid" src="assets/img/dummy/reviseddash.png"></a>
										</div>
									</div><br>
								</div>
								<div class="col-12 col-lg-6">
									<div class="service-featured-content p-t-b-40">
										<h3 class="fadeInUpShort animated text-primary">SMS REST API</h3><br>
										<p class="fadeInUpShort animated s-18">Develop your own applications using our API and synchronize data on your dashboard.</p><br>
										<a class="btn btn-primary fadeInUpShort animated blue1" href="docs">Full API Reference <i class="ion-ios-arrow-thin-right"></i></a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			
			<section class="p-t-b-50 text-center animatedParent grey lighten-5 animateOnce">
				<div class="container">
					<h3 class="styleprimary">Connect your application easily using our APIs</h3>
					<p class="s-18 p-t-20 stylemuted">Send messages from your application by connection to our APIs. Its has never been this simple!</p>
					<div class="p-t-b-20">
						<a class="btn-warning btn-lg btn blue2" href="account/signup">
							Get A Free Account <i class="ion-ios-arrow-thin-right">&nbsp;</i>
						</a>
					</div>
				</div>
			</section>
			
		</main>
		
		<?php include_once 'com_footer.php'; ?>

	</div>
	<script src="assets/js/app.js"></script>
	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-114404103-1"></script>
<script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-114404103-1');
</script>

<script>
	$("#testbtn").click(function () {
		$(this).attr('disabled', true);
		$(this).html("Please wait...");
		var phone = $("#phone").val();

		$.post('system/testsms/', 
		{
			phone:phone
		}, 
		function(data, textStatus, xhr) {
			if (data == '1') {
				$("#testbtn").attr('disabled', false);
				$("#testbtn").html("Test");
				$("#phone").val('');
				swal("Success!", "We sent an SMS your phone.", "success");
			}
			else{
				$("#testbtn").attr('disabled', false);
				$("#testbtn").html('Test');
				$("#testerror").html(data).slideDown().delay(4000).slideUp();
			}
		});
	})
</script>
</body>
</html>