<?php

/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
*/

include_once($_SERVER["DOCUMENT_ROOT"] . '/system/core/index.php');


?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?php echo $account_name; ?> - Contacts</title>
	<meta content="Ionsolve" name="description">
	<meta content="width=device-width,initial-scale=1,maximum-scale=1,minimal-ui" name="viewport">
	<meta content="IE=edge" http-equiv="X-UA-Compatible">
	<meta content="yes" name="">
	<meta content="black-translucent" name="">
	<link href="../dashboard/images/logo_small.png" rel="">
	<meta content="Ionsolve" name="">
	<meta content="yes" name="">
	<link href="../dashboard/images/logo_small.png" rel="shortcut icon" sizes="196x196">
	<link href="../dashboard/css/animate.css/animate.min.css" rel="stylesheet" type="text/css">
	<link href="../dashboard/css/glyphicons/glyphicons.css" rel="stylesheet" type="text/css">
	<link href="../dashboard/css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="../dashboard/css/material-design-icons/material-design-icons.css" rel="stylesheet" type="text/css">
	<link href="../dashboard/css/ionicons/css/ionicons.min.css" rel="stylesheet" type="text/css">
	<link href="../dashboard/css/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css">
	<link href="../dashboard/css/nice.css" rel="stylesheet" type="text/css">
	<link href="../dashboard/css/taggle.css" rel="stylesheet" type="text/css">
	<link href="../dashboard/css/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="../dashboard/css/styles/app.min.css" rel="stylesheet">
	<link href="../dashboard/css/styles/font.css" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<style type="text/css">
		::-webkit-scrollbar { 
		display: none; 
	}
	menu, ol, ul {
		padding: 0px 0px 0px 4px;
	}
	</style>
	
	
</head>
<body>

<div class="app" id="app">
		<div class="app-content box-shadow-z2 pjax-container" id="content" role="main">
			<div class="app-body">
				<div class="app-body-inner">
					<div class="row-col">
						<?php include_once '../dashboard/top.php'; ?>
						<div class="row-row">
							<div class="row-col">
                				<?php include_once '../dashboard/side.php'; ?>
                                <div class="col-xs col-md-9" id="detail">
									<p class="text-md text-primary" style="margin-left: 5px;">
										Contacts Upload Report
										<a class="btn btn-sm rounded white text-primary" style="width:170px;" href="../dashboard/contacts/">
											<i class="ion-arrow-left-c"></i> Back To Contacts
										</a>
										
                                    </p>
<div class="list-group m-b">
<?php

if(isset($_POST["submit"]))
{
    $filename = $_FILES["file"]["tmp_name"];
    $date     = date('Y-m-d');

     if($_FILES["file"]["size"] > 0)
     {
         $file = fopen($filename, "r");
         while (($emapData = fgetcsv($file, 100000, ",")) !== FALSE)
         {
            if (!empty($emapData[0]) OR !empty($emapData[1]))
			{
                if(strpos($emapData[1], '+') !== false) 
                {
                    if(!empty($emapData[2]))
                    {
                        if (strpos($emapData[2], ',') === false) 
                        {
                            $arguments = array('phone_number' => $emapData[1] ,'parent' => $account_id);
                            if(returnExists('contacts', $arguments) == 0)
                            {
                                $phonenumbers = str_replace(' ', '', $emapData[1]);
                                $sql = "INSERT INTO `contacts` (`contact_name`, `phone_number`,`tags`,`date_created`,`parent`) 
                                VALUES('$emapData[0]','$phonenumbers','$emapData[2]','$date','$account_id')";

                                $update_stage = mysqli_query($conn, "UPDATE `users` SET `stage` = 'loadcontact' WHERE `email` = '{$_SESSION['alphaion']}'");
                                
                                $result = mysqli_query($conn, $sql);
                                
                                if($result)
                                {
                                    echo '<a class="list-group-item text-success" href="#">
                                        <span class="pull-right text-success">
                                            <i class="ion-checkmark-circled text-xs"></i>
                                        </span> Contact <b>'.$emapData[0].', '.$emapData[1].'</b> added successfully
                                    </a>'; 
                                }else{
                                    echo '<a class="list-group-item text-danger" href="#">
                                        <span class="pull-right text-danger">
                                            <i class="ion-close-circled text-xs"></i>
                                        </span> Something went wrong adding contact <b>'.$emapData[0].', '.$emapData[1].'</b> 
                                    </a>'; 
                                }

                            }else{
                                echo '<a class="list-group-item text-danger" href="#">
                                        <span class="pull-right text-danger">
                                            <i class="ion-close-circled text-xs"></i>
                                        </span> The phone number <b>'.$emapData[0].', '.$emapData[1].'</b> already exists
                                </a>';
                            }
                        }else{
                            echo '<a class="list-group-item text-danger" href="#">
                                        <span class="pull-right text-danger">
                                            <i class="ion-close-circled text-xs"></i>
                                        </span> The tag <b>'.$emapData[0].', '.$emapData[1].', '.$emapData[2].'</b> contains commas
                            </a>';
                        }
                    }
                    elseif (empty($emapData[2])) {
                    	$arguments = array('phone_number' => $emapData[1] ,'parent' => $account_id);
                            if(returnExists('contacts', $arguments) == 0)
                            {
                                $emapData[2] = 'notag';
                                $phonenumbers = str_replace(' ', '', $emapData[1]);
                                $sql = "INSERT INTO `contacts` (`contact_name`, `phone_number`,`tags`,`date_created`,`parent`) 
                                VALUES('$emapData[0]','$phonenumbers','$emapData[2]','$date','$account_id')";

                                $update_stage = mysqli_query($conn, "UPDATE `users` SET `stage` = 'loadcontact' WHERE `email` = '{$_SESSION['alphaion']}'");
                                
                                $result = mysqli_query($conn, $sql);
                                
                                if($result)
                                {
                                    echo '<a class="list-group-item text-success" href="#">
                                        <span class="pull-right text-success">
                                            <i class="ion-checkmark-circled text-xs"></i>
                                        </span> Contact <b>'.$emapData[0].', '.$emapData[1].'</b> added successfully
                                    </a>'; 
                                }else{
                                    echo '<a class="list-group-item text-danger" href="#">
                                        <span class="pull-right text-danger">
                                            <i class="ion-close-circled text-xs"></i>
                                        </span> Something went wrong adding contact <b>'.$emapData[0].', '.$emapData[1].'</b> 
                                    </a>'; 
                                }

                            }else{
                                echo '<a class="list-group-item text-danger" href="#">
                                        <span class="pull-right text-danger">
                                            <i class="ion-close-circled text-xs"></i>
                                        </span> The phone number <b>'.$emapData[0].', '.$emapData[1].'</b> already exists
                                </a>';
                            }
                    }
                }else{
                    echo '<a class="list-group-item text-danger" href="#">
                                        <span class="pull-right text-danger">
                                            <i class="ion-close-circled text-xs"></i>
                                        </span> The phone number <b>'.$emapData[0].', '.$emapData[1].', '.$emapData[2].'</b> should be in international format
                    </a>';
                }
            }else{
                echo '<a class="list-group-item text-danger" href="#">
                                        <span class="pull-right text-danger">
                                            <i class="ion-close-circled text-xs"></i>
                                        </span>Please provide a name and phone number
                </a>';
            }    
         }
         fclose($file);
         
         //close of connection
        mysqli_close($conn); 
     }else{
         echo '<a class="list-group-item text-danger" href="#">
            <span class="pull-right text-danger">
                <i class="ion-close-circled text-xs"></i>
            </span> Please upload a CSV file
        </a>';
     }
}	

?>

</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>


<script src="../dashboard/scripts/app.min.js"></script>
<script src="../dashboard/scripts/dmain.js"></script>
<script src="../dashboard/scripts/dropzone.js"></script>

</body>
</html>