<?php

/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
*/



require_once $_SERVER["DOCUMENT_ROOT"] . '/admin/resources/core/index.php';

?>


<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?php echo $account_name; ?> - Dashboard</title>
	<meta content="Ionsolve" name="description">
	<meta content="width=device-width,initial-scale=1,maximum-scale=1,minimal-ui" name="viewport">
	<meta content="IE=edge" http-equiv="X-UA-Compatible">
	<meta content="yes" name="">
	<meta content="black-translucent" name="">
	<link href="images/logo_small.png" rel="">
	<meta content="Ionsolve" name="">
	<meta content="yes" name="">
	<link href="images/logo_small.png" rel="shortcut icon" sizes="196x196">
	<link href="css/animate.css/animate.min.css" rel="stylesheet" type="text/css">
	<link href="css/glyphicons/glyphicons.css" rel="stylesheet" type="text/css">
	<link href="css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="css/material-design-icons/material-design-icons.css" rel="stylesheet" type="text/css">
	<link href="css/ionicons/css/ionicons.min.css" rel="stylesheet" type="text/css">
	<link href="css/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css">
	<link href="css/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="css/styles/app.min.css" rel="stylesheet">
	<link href="css/styles/font.css" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
	
	<!-- <link rel="stylesheet" href="//cdn.jsdelivr.net/chartist.js/latest/chartist.min.css"> -->
    <style type="text/css">
	     ::-webkit-scrollbar { 
	       display: none; 
	   }
	</style>
</head>
<body class="pace-done">
	<div class="app" id="app">
		<div class="app-content box-shadow-z2 pjax-container" id="content" role="main">
			<div class="app-body">
				<div class="app-body-inner">
					<div class="row-col">
						<?php include_once 'top.php'; ?>
						<div class="row-row">
							<div class="row-col">
                				<?php include_once 'side.php'; ?>
                
								<div class="col-xs col-md-9" id="detail">
									<p class="text-md text-primary" style="margin-left: 5px;">Admin Dashboard</p>
									<div class="row-col">
										<div class="row-row">
											<div class="row-body scrollable hover">
												<div class="row-inner"><br>
                                                    <div class="row no-gutter white">
														<div class="col-xs-6 col-sm-3 b-r b-b">
															<div class="padding">					
																<div class="text-center">
																	<h2 class="text-center _600 text-primary">
	   																	<?php
																			echo returnAllNumRows('users');
																		?>
																	</h2>
																	<p class="text-muted m-b-md">Users</p>
																	<a href="users" class="text-primary">View all <i class="ion-ios-arrow-thin-right">&nbsp;</i></a>
																</div>
															</div>
														</div>
														<div class="col-xs-6 col-sm-3 b-r b-b">
															<div class="padding">
																
																<div class="text-center">
																	<h2 class="text-center _600 text-primary">
																		<?php
																			$pendingargs = array('status' => 'pending');
																			echo returnCountWithCondition('users', $pendingargs);
																		?>
																	</h2>
																	<p class="text-muted m-b-md">Pending Accounts</p>
																	<a href="pendingaccounts" class="text-primary">View all <i class="ion-ios-arrow-thin-right">&nbsp;</i></a>
																</div>
															</div>
														</div>
														<div class="col-xs-6 col-sm-3 b-r b-b">
															<div class="padding">
																<div class="text-center">
																	<h2 class="text-center _600 text-primary">
																		<?php
																			echo number_format(returnCountNoCondition('messages'));
																		?> 
																	</h2>
																	<p class="text-muted m-b-md">Sent Messages</p>
																	<a href="smsperuser" class="text-primary">View all <i class="ion-ios-arrow-thin-right">&nbsp;</i></a>
																</div>
															</div>
														</div>
														<div class="col-xs-6 col-sm-3 b-b">
															<div class="padding">
																<div class="text-center">
																	<h2 class="text-center _600 text-primary">
																		<?php
																			$salesargs = array('type' => "MPESA");
																			echo returnCountWithCondition('top_ups', $salesargs);
																		?>
																	</h2>
																	<p class="text-muted m-b-md">Top Ups</p>
																	<a href="sales" class="text-primary">View all <i class="ion-ios-arrow-thin-right">&nbsp;</i></a>
																</div>
															</div>
														</div>
													</div>
                                                   
													<br><br><hr>


													<h5 class="text-primary">Messages Analysis</h5>

													<div class="container row m-b pull-right">
													<div class="col-sm-12">
														<form method="POST" action="">
															<select name="timeinterval" style="margin-right: 20px;border-color: #27AAFF;" class="custom-select w-sm inline v-middle">
																<option value="7">One Week</option>
																<option value="14">Two Weeks</option>
																<option value="30">One Month</option>
																<option value="90">Three Months</option>
																<option value="365">One Year</option>
															</select>

															<button name="message_timebtn" class="btn btn-fw info">View</button>
														</form>
													</div>
													</div>

													<canvas id="messagesanalysis" width="800" height="250">
											
													</canvas>
													
											</div>
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script type="text/javascript" src="css/bootstrap/dist/js/bootstrap.min.js"></script>
	<script src="scripts/app.min.js"></script>
	<script src="scripts/node_modules/chart.js/dist/Chart.js"></script>
	<script>

	<?php
	if (isset($_POST['message_timebtn'])) {
		$numofdays = $_POST['timeinterval'];
	}
	else{
		$numofdays = 7;
	}

	?>

	new Chart(document.getElementById("messagesanalysis"), {
	  type: 'line',
	  data: {
	    labels: [
	    <?php
	    	$arraydates = getLastNDays($numofdays, 'Y-m-d');

	    	foreach ($arraydates as $datethen) {
	    		echo "'".$datethen."',";
	    	}

	    ?>
	    ],
	    datasets: [{ 
	        data: [
	        <?php
	        $datearray = getLastNDays($numofdays, 'Y-m-d');

	        foreach ($datearray as $datetoday) {
	        	$query2 = mysqli_query($conn, "SELECT * FROM `messages` WHERE `status` = 'Success' AND `date_sent` LIKE '$datetoday%'");

	        	$no_of_messages = mysqli_num_rows($query2);

	        	echo $no_of_messages.",";
	        }
	        ?>
	        ],
	        label: "Successful Messages",
	        borderColor: "#3e95cd",
	        fill: false
	      },{ 
	        data: [
	        <?php
	        foreach ($datearray as $datefailed) {
	        	$query2 = mysqli_query($conn, "SELECT * FROM `messages` WHERE `status` = 'Failed' AND `date_sent` LIKE '$datefailed%'");

	        	$no_of_messages = mysqli_num_rows($query2);

	        	echo $no_of_messages.",";
	        }

	        ?>
	        ],
	        label: "Failed Messages",
	        borderColor: "#c45850",
	        fill: false
	      }
	    ]
	  },
	  options: {
	    title: {
	      display: true,
	      text: 'Number of Messages Sent Last <?php echo $numofdays; ?> Days'
	    }
	  }
	});
	</script>
</body>
</html>