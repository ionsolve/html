
  <div class="col-xs w modal fade aside aside-md col-md-3" id="subnav">
    <div class="row-col light bg">
      <div class="row-row">
        <div class="row-body scrollable hover">
          <div class="row-inner">
            <div class="navside">
              <nav class="nav-border b-primary" data-ui-nav="">
                <ul class="nav">
                  <br>

                  <li class="">
                    <a href="<?php echo $base_path; ?>admin/dashboard/">
                      <span class="nav-label text-primary"><i class="ion-ios-arrow-thin-right">&nbsp;</i></span> 
                      <span class="nav-text">Dashboard</span>
                    </a>
                  </li>
                  <hr>
                  <li class="">
                    <a class="dropdown-item text-primary" href="<?php echo $base_path; ?>admin/dashboard/sendall/" >
                      <span class="nav-label text-primary"><i class="ion-ios-arrow-thin-right">&nbsp;</i></span> 
                      <span class="nav-text">Send Message</span>
                    </a>
                  </li>
                  <hr>
                  <li>
                    <a>
                      <span class="nav-label text-primary"><i class="fa fa-caret-down">&nbsp;</i></span> 
                      <span class="nav-text">Users</span>
                    </a>

                    <ul class="nav-sub">
                      <li class="">
                        <a href="<?php echo $base_path; ?>admin/dashboard/users/">
                        <span class="nav-label text-primary"><i class="ion-ios-arrow-thin-right">&nbsp;</i></span> 
                        <span class="nav-text">All Users</span></a>
                      </li>

                      <li class="">
                        <a href="<?php echo $base_path; ?>admin/dashboard/recentsignups/">
                        <span class="nav-label text-primary"><i class="ion-ios-arrow-thin-right">&nbsp;</i></span> 
                        <span class="nav-text">Recent Signups</span></a>
                      </li>

                      <li class="">
                        <a href="<?php echo $base_path; ?>admin/dashboard/pendingaccounts/">
                        <span class="nav-label text-primary"><i class="ion-ios-arrow-thin-right">&nbsp;</i></span> 
                        <span class="nav-text">Pending Accounts</span></a>
                      </li>

                      <li class="">
                        <a href="<?php echo $base_path; ?>admin/dashboard/useractivity/">
                        <span class="nav-label text-primary"><i class="ion-ios-arrow-thin-right">&nbsp;</i></span>
                        <span class="nav-text">User Activity</span></a>
                      </li> 
                    </ul>
                  </li>
                  <hr>

                  <li>
                    <a>
                      <span class="nav-label text-primary"><i class="fa fa-caret-down">&nbsp;</i></span> 
                      <span class="nav-text">Products</span>
                    </a>

                    <!-- sender ids nav -->
                    <ul class="nav-sub">
                      <a>
                        <span class="nav-label text-primary"><i class="fa fa-caret-down">&nbsp;</i></span> 
                        <span class="nav-text">Sender IDs</span>
                      </a>

                      <ul class="nav-sub">
                      <li class="">
                        <a href="<?php echo $base_path; ?>admin/dashboard/senderids/">
                        <span class="nav-label text-primary"><i class="ion-ios-arrow-thin-right">&nbsp;</i></span> 
                        <span class="nav-text">All Sender IDs</span></a>
                      </li>
                      <li class="">
                        <a href="<?php echo $base_path; ?>admin/dashboard/suspendedids/">
                        <span class="nav-label text-primary"><i class="ion-ios-arrow-thin-right">&nbsp;</i></span> 
                        <span class="nav-text">Suspended IDs</span></a>
                      </li>
                    </ul>
                      
                    </ul>


                    <!-- shorcodes nav -->

                    <ul class="nav-sub">
                      <a>
                        <span class="nav-label text-primary"><i class="fa fa-caret-down">&nbsp;</i></span> 
                        <span class="nav-text">Shortcodes</span>
                      </a>

                      <ul class="nav-sub">
                      <li class="">
                        <a href="<?php echo $base_path; ?>admin/dashboard/shortcodes/">
                        <span class="nav-label text-primary"><i class="ion-ios-arrow-thin-right">&nbsp;</i></span> 
                        <span class="nav-text">All Shortcodes</span></a>
                      </li>
                      <li class="">
                        <a href="<?php echo $base_path; ?>admin/dashboard/pendingshortcodes/">
                        <span class="nav-label text-primary"><i class="ion-ios-arrow-thin-right">&nbsp;</i></span> 
                        <span class="nav-text">Inactive Shortcodes</span></a>
                      </li>
                    </ul>
                    </ul>
                  </li>


                  <hr>
                  <li>
                    <a>
                      <span class="nav-label text-primary"><i class="fa fa-caret-down">&nbsp;</i></span> 
                      <span class="nav-text">User Top Ups</span>
                    </a>

                    <ul class="nav-sub">
                      <li class="">
                        <a href="<?php echo $base_path; ?>admin/dashboard/sales/">
                        <span class="nav-label text-primary"><i class="ion-ios-arrow-thin-right">&nbsp;</i></span> 
                        <span class="nav-text">All Top Ups</span></a>
                      </li>

                      <li class="">
                        <a href="<?php echo $base_path; ?>admin/dashboard/recentsales/">
                        <span class="nav-label text-primary"><i class="ion-ios-arrow-thin-right">&nbsp;</i></span> 
                        <span class="nav-text">Recent Top Ups</span></a>
                      </li>
                      
                    </ul>
                  </li>
                  <hr>


                  <!-- <li>
                    <a>
                      <span class="nav-label text-primary"><i class="fa fa-caret-down">&nbsp;</i></span> 
                      <span class="nav-text">CRM</span>
                    </a>

                    <ul class="nav-sub">
                    <li class="">
                      <a href="<?php echo $base_path; ?>admin/dashboard/tasks/">
                      <span class="nav-label text-primary"><i class="ion-ios-arrow-thin-right">&nbsp;</i></span>
                      <span class="nav-text">My Tasks</span></a>
                    </li>

                    <li class="">
                      <a href="<?php echo $base_path; ?>admin/dashboard/groupsperuser/">
                      <span class="nav-label text-primary"><i class="ion-ios-arrow-thin-right">&nbsp;</i></span>
                      <span class="nav-text">Leads</span></a>
                    </li>

                    <li class="">
                      <a href="<?php echo $base_path; ?>admin/dashboard/smsperuser/">
                      <span class="nav-label text-primary"><i class="ion-ios-arrow-thin-right">&nbsp;</i></span>
                      <span class="nav-text">Opportunities</span></a>
                    </li>
                      
                    </ul>
                  </li>
                  <hr> -->


                  <li>
                    <a>
                      <span class="nav-label text-primary"><i class="fa fa-caret-down">&nbsp;</i></span> 
                      <span class="nav-text">Reports</span>
                    </a>

                    <ul class="nav-sub">
                    <li class="">
                      <a href="<?php echo $base_path; ?>admin/dashboard/contactsperuser/">
                      <span class="nav-label text-primary"><i class="ion-ios-arrow-thin-right">&nbsp;</i></span>
                      <span class="nav-text">Contacts/User</span></a>
                    </li>

                    <li class="">
                      <a href="<?php echo $base_path; ?>admin/dashboard/groupsperuser/">
                      <span class="nav-label text-primary"><i class="ion-ios-arrow-thin-right">&nbsp;</i></span>
                      <span class="nav-text">Groups/User</span></a>
                    </li>

                    <li class="">
                      <a href="<?php echo $base_path; ?>admin/dashboard/smsperuser/">
                      <span class="nav-label text-primary"><i class="ion-ios-arrow-thin-right">&nbsp;</i></span>
                      <span class="nav-text">SMS Usage/User</span></a>
                    </li>
                      
                    </ul>
                  </li>
                  <hr>

                  <?php
                    if ($can_topup_account == '1'){
                  ?>
                  <li>
                    <a>
                      <span class="nav-label text-primary"><i class="fa fa-caret-down">&nbsp;</i></span> 
                      <span class="nav-text">Billing</span>
                    </a>

                    <ul class="nav-sub">

                    <li class="">
                      <a href="<?php echo $base_path; ?>admin/dashboard/topup">
                      <span class="nav-label text-primary"><i class="ion-ios-arrow-thin-right">&nbsp;</i></span>
                      <span class="nav-text">Top Up Account</span></a>
                    </li>
                    
                    </ul>
                  </li>
                  <hr>

                  <?php
                  }
                  ?>

                  <?php
                  if($permission_level == 2){
                  ?>
                  <li class="">
                    <a href="<?php echo $base_path; ?>admin/dashboard/manageadmins/">
                      <span class="nav-label text-primary"><i class="ion-ios-arrow-thin-right">&nbsp;</i></span> 
                      <span class="nav-text">Manage Admins</span>
                    </a>
                  </li>
                  <?php
                  }
                  ?>

                  <li class="">
                    <a href="<?php echo $base_path; ?>admin/dashboard/settings">
                    <span class="nav-label text-primary"><i class="ion-ios-settings-strong">&nbsp;</i></span>
                    <span class="nav-text">Account Settings</span></a>
                  </li>
                  <hr>
                  <li class="">
                    <a href="<?php echo $base_path; ?>admin/dashboard/signout">
                    <span class="nav-label text-primary"><i class="fa fa-power-off">&nbsp;</i></span>
                    <span class="nav-text">Log Out</span></a>
                  </li>
                  
                  <hr>
                </ul>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>




<!-- start of the modal for sending messages to all users -->

      <div aria-hidden="true" class="modal fade animate" data-backdrop="true" id="messageallusers" style="display: none;">
      <div class="modal-dialog fade-left-big" data-ui-class="fade-left-big" id="animate">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title text-primary">SMS All Users</h5>     
          </div>
          <div class="modal-body p-lg" id="textsingle">
                        <div class="md-form-group">
                            <br>
                            <textarea class="md-input" id="admin_sendall_input" placeholder="Type Message Here" rows="4"></textarea>
                            <label>Message</label>
                        </div>
   
            <p style="color: red;" id="admin_message_response"></p>
          </div>
          <div class="modal-footer" id="messagess">
            <button class="btn dark-white p-x-md" data-dismiss="modal" type="button">Cancel</button> 
            <button class="btn primary p-x-md" id="messageallbtn" type="button">Send <i class="ion-ios-arrow-thin-right">&nbsp;</i></button>
          </div>
        </div>
      </div>
    </div>

    <!-- end of the modal for sending messages to all users  -->