
<?php
/**
 * [$baseurl description]
 * holds the url for site...
 * @var string
 */

/**
 * [resourceBreadCrumbs description]
 *
 *builds breadcrumbs - works by using a baseurl and the requesting uri... doesn't depend on folder or file structure only the URI
 *Breaks apart /cds/resources/...(folder or filename)
 *Removes .php, index.php, ... anything in var @$REMOVE
 *Ignores any additional uri arguments strings ie: /cds/resources/...(folder or filename)?v=209ada#ok -> /cds/resources/...(folder or filename)
 *@$baseurl - stores the parent url such as http://apple.com or http://support.apple.com
 * @return string of the breadcrumbs
 */
function resourceBreadCrumbs() {
    $baseurl="";
    $urlArray = parse_url( $baseurl.$_SERVER["REQUEST_URI"], PHP_URL_PATH ); //appends the http://.../ to /folder1/subfolder/file.php
    $crumbs = array_slice(explode( "/", $urlArray ), 3); //transforms /folder/folder/file.php -> array['','folder','subfolder','file.php']
    $REMOVE = array( "", "index.php" ); //an array that will hold strings to be removed -> array['folder','subfolder','file.php']
    
    // remove the elements who's values are stored in REMOVE array
    $crumbs = array_diff( $crumbs, $REMOVE );
    //used later to avoid adding a trailing >
    $totalCrumbs = count( $crumbs ); //counts the total number of items in the array
    $count = 0;
    //this is the subdirectory we are on the folder...
    $uri = "http://localhost/solveitafrica/";
    foreach ( $crumbs as $crumb ) {
        $count++;
        if ( $count!=$totalCrumbs ) { //checks if we are at the end of the urlArray
            //not at end so, lets append an > to the url, and then for the href lets add the additonal crumb to the uri string...
            echo '<a href="'.$uri.$crumb.'">'.ucwords( str_replace( array( "-", ".php", "_" ), array( " ", "", " " ), $crumb ) . '</a> > ' );
        } else {
            //at the end of the uri string, we don't need >, and for href lets use the requesting URI (works well when pass arguments and queries ?,# in URI)
            echo '<a href="'.$_SERVER["REQUEST_URI"].'">'.ucwords( str_replace( array( "-", ".php", "_" ), array( " ", "", " " ), $crumb ) . '</a>' );
        }
        //add the string to the uri and store it.
        $uri = $uri.$crumb.'/';
    }
}

resourceBreadCrumbs();
?>