<?php

/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
*/

include_once($_SERVER["DOCUMENT_ROOT"] . '/admin/includes/mother.php');

$user_fetch_args = array('username'=>$_GET['user']);

$userid = getByValue('users', 'id', $user_fetch_args);

$_SESSION['addingfollowup'] = $userid;



?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?php echo $account_name; ?> - User Followups</title>
	<meta content="Ionsolve" name="description">
	<meta content="width=device-width,initial-scale=1,maximum-scale=1,minimal-ui" name="viewport">
	<meta content="IE=edge" http-equiv="X-UA-Compatible">
	<meta content="yes" name="">
	<meta content="black-translucent" name="">
	<link href="../images/logo_small.png" rel="">
	<meta content="Ionsolve" name="">
	<meta content="yes" name="">
	<link href="../images/logo_small.png" rel="shortcut icon" sizes="196x196">
	<link href="../css/animate.css/animate.min.css" rel="stylesheet" type="text/css">
	<link href="../css/glyphicons/glyphicons.css" rel="stylesheet" type="text/css">
	<link href="../css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="../css/material-design-icons/material-design-icons.css" rel="stylesheet" type="text/css">
	<link href="../css/ionicons/css/ionicons.min.css" rel="stylesheet" type="text/css">
	<link href="../css/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css">
	<link href="../css/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="../css/styles/app.min.css" rel="stylesheet">
	<link href="../css/styles/font.css" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
	<style type="text/css">
		::-webkit-scrollbar { 
		display: none; 
	}
	</style>
	
	
</head>
<body>
	<div class="app" id="app">
		<div class="app-content box-shadow-z2 pjax-container" id="content" role="main">
			<div class="app-body">
				<div class="app-body-inner">
					<div class="row-col">
						<?php include_once '../top.php'; ?>
						<div class="row-row">
							<div class="row-col">
                				<?php include_once '../side.php'; ?>

								<div class="col-xs col-md-9" id="detail">
									<p class="text-md text-primary" style="margin-left: 5px;">
										User Followups
									</p>
									<div class="row-col white bg">
										<div class="row-row">
											<div class="row-body scrollable hover">
												<div class="row-inner">
													<div style="padding:10px;">


 													<a class="btn btn-sm rounded white text-primary" data-toggle="modal" data-target="#addnewtask" data-ui-toggle-class="fade-left-big" data-ui-target="#animate" style="width:170px;">
														<i class="fa fa-plus"></i> Add New Followup
													</a>

													<div class="row p-a pull-right">
														<div class="col-sm-12">
															<select id="stage" style="margin-right: 20px;border-color: #27AAFF;" class="custom-select w-sm inline v-middle">
																<option value="0">Followup Status</option>
																<option value="open">Open</option>
																<option value="closed">Closed</option>
															</select>

															<select id="country" style="margin-right: 20px;border-color: #27AAFF;" class="custom-select w-sm inline v-middle">
																<option value="0">Filter By Date</option>
																<option value="">This Week</option>
																<option value="">Two Weeks Ago</option>
																<option>One Month Ago</option>
																<option>More than a month</option>
															</select>
															<button id="search_user_category" class="btn btn-fw info">Search</button>
														</div>
													</div>

													</div>

													<div id="userfollowups"></div>


													<!-- modal for adding a new followup -->

												    <div aria-hidden="true" class="modal fade animate" data-backdrop="true" id="addnewtask" style="display: none;">
												        <div class="modal-dialog fade-left-big" data-ui-class="fade-left-big" id="animate">
												            <div class="modal-content">
												                <div class="modal-header">
												                    <h5 class="modal-title text-primary">New Followup</h5>         
												                </div>
												                <div class="modal-body p-lg">

												                <div class="md-form-group">
									                                <input class="md-input" id="followuptitle" placeholder="Enter task title"><label></label>
									                            </div>

									                            <div class="md-form-group">
									                            	<textarea class="md-input" placeholder="Type Comment Here..." rows="4" id="followupcomment"></textarea>
									                            </div>

												                    <p style="color: red;" id="followupresponse"></p>
												                </div>

												                <div class="modal-footer" id="ad_resendcode_footer">
												                    <button class="btn dark-white p-x-md" id="forceclosefollowup" data-dismiss="modal" type="button">Cancel</button> 
												                    <button class="btn primary p-x-md" value="<?php echo $userid; ?>" id="addnewfollowupbtn" type="button">Add Followup <i class="ion-ios-arrow-thin-right">&nbsp;</i></button>
												                </div>
												            </div>
												        </div>
												    </div>
													
												</div>
											</div>
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script src="../scripts/app.min.js"></script>
	<script src="../scripts/dmain.js"></script>
	<script src="../scripts/dropzone.js"></script>
	<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

</body>
</html>