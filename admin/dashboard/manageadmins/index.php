<?php

/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
*/

include_once($_SERVER["DOCUMENT_ROOT"] . '/admin/resources/core/index.php');

if ($permission_level == 1) {
	die('<h1>Access Denied!</h1>');
}



?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?php echo $account_name; ?> - Manage Admins</title>
	<meta content="Ionsolve" name="description">
	<meta content="width=device-width,initial-scale=1,maximum-scale=1,minimal-ui" name="viewport">
	<meta content="IE=edge" http-equiv="X-UA-Compatible">
	<meta content="yes" name="">
	<meta content="black-translucent" name="">
	<link href="../images/logo_small.png" rel="">
	<meta content="Ionsolve" name="">
	<meta content="yes" name="">
	<link href="../images/logo_small.png" rel="shortcut icon" sizes="196x196">
	<link href="../css/animate.css/animate.min.css" rel="stylesheet" type="text/css">
	<link href="../css/glyphicons/glyphicons.css" rel="stylesheet" type="text/css">
	<link href="../css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="../css/material-design-icons/material-design-icons.css" rel="stylesheet" type="text/css">
	<link href="../css/ionicons/css/ionicons.min.css" rel="stylesheet" type="text/css">
	<link href="../css/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css">
	<link href="../css/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="../css/styles/app.min.css" rel="stylesheet">
	<link href="../css/styles/font.css" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
	<link href="../codemirror/lib/codemirror.css" rel="stylesheet">
	<style type="text/css">
		::-webkit-scrollbar { 
		display: none; 
	}
	</style>
</head>
<body>
	<div class="app" id="app">
		<div class="app-content box-shadow-z2 pjax-container" id="content" role="main">
			<div class="app-body">
				<div class="app-body-inner">
					<div class="row-col">
						<?php include_once '../top.php'; ?>
						<div class="row-row">
							<div class="row-col">
                				<?php include_once '../side.php'; ?>

								<div class="col-xs col-md-9" id="detail">
									<p class="text-md text-primary" style="margin-left: 5px;">All Admins
									<a class="btn btn-sm rounded white text-primary" data-toggle="modal" data-target="#newadmin" data-ui-toggle-class="fade-left-big" data-ui-target="#animate" style="width:170px;">
										<i class="fa fa-plus"></i> New Admin
									</a>
									</p>
									<div class="row-col white bg">
										<div class="row-row">
											<div class="row-body scrollable hover">
												<div class="row-inner">
                                                    
                                                    <div id="alladmins"></div>
													
												</div>
											</div>
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<div aria-hidden="true" class="modal fade animate" data-backdrop="true" id="newadmin" style="display: none;">
        <div class="modal-dialog fade-left-big" data-ui-class="fade-left-big" id="animate">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title text-primary">Add New Admin </h5>         
                </div>
                <div class="modal-body p-lg">
                        
                            <div style="padding-left: 20px;padding-right: 20px;" class="md-form-group">
                            	<div class="form-group"><label class="text-primary" for="name">Name</label><input type="text" class="form-control" id="adminname" placeholder="Name"></div>

	                            <div class="form-group"><label class="text-primary" for="email">Email</label><input type="email" class="form-control" id="adminemail" placeholder="Email"></div>

	                            <div class="form-group"><label class="text-primary" for="email">Password</label><input type="password" class="form-control" id="adminpassword" placeholder="Password"></div>

                                <label class="text-primary"><strong>Select Priviledges</strong></label><br>

                                <p><label class="md-check"><input id="can_topup" type="checkbox" value="1"> <i class="indigo"></i> Can Top Up Account</label></p>

                                <p><label class="md-check"><input id="can_suspend" type="checkbox" value="1"> <i class="indigo"></i> Can Suspend Account</label></p>

                                <p><label class="md-check"><input id="can_change_price" type="checkbox" value="1"> <i class="indigo"></i> Can Change Pricing</label></p>

                                <p><label class="md-check"><input id="can_map" type="checkbox" value="1"> <i class="indigo"></i> Can Map Sender IDs</label></p>

                                <div class="md-form-group">
                                <label class="text-primary"><strong>Permission Level</strong></label>
                                <select id="permission_level" class="form-control">
                                    <option value="">Select Permission Level</option>
                                    <option value="1">Admin</option>
                                    <option value="2">Super Admin</option>
                                </select>
                            </div>

                            <p id="newadminresponse"></p>
                                
                            </div>
                        </div>

                <div class="modal-footer" id="newadminfooter">
                    <button class="btn dark-white p-x-md" id="forceclosenewadmin" data-dismiss="modal" type="button">Cancel</button> 
                    <button class="btn primary p-x-md" value="" id="newadminbtn" type="button">Add Admin <i class="ion-ios-arrow-thin-right">&nbsp;</i></button>
                </div>
            </div>
        </div>
    </div>


	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> 
    <script src="../scripts/app.min.js"></script>
    <script src="../scripts/dmain.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
</body>
</html>