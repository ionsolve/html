$(document).ready(function()
{
    loadusers();
    loadrecent();
    loadPendingAccounts();
    loadAllSenderIds();
    loadInActiveSenderIds();
    loadAllShortCodes();
    loadInactiveShortcodes();
    loadAllUserTopups();
    loadOurTopups();
    loadRecentUserTopups();
    loadContactsPerUser();
    loadGroupsPerUser();
    loadSmsUsagePerUser();
    loadDormantAccounts();
    loadMyTasks();
    loadExecutiveTasks();
    loadUserFollowups();
    loadAllAdmins();

    // new contact

    $('#save_contact').click(function()
	{
		$('#save_contact').html('<i class="fa ion-load-c fa-spin"></i>');
		$('#save_contact').css('disabled','1');

        var _contact_      = $('#_contact_').val();
        var _phone_        = $('#_phone_').val();
        var _tags_         = $('#_tags_').val();

		$.post("../../system/savecontact/",
		{
            _contact_:_contact_,
            _phone_:_phone_,
            _tags_:_tags_
		},
		function(saveresponse)
		{
			if (saveresponse == 1)
			{
                loadcontacts();

                $("#forceclose" ).trigger( "click" );
                $('#_contact_').val('');
                $('#_phone_').val('');
                $('#_tags_').val('');
                $('#save_contact').html('Save');

			}else{
				$('#saveresponse').html(saveresponse);
				$('#save_contact').css('disabled','0');
				$('#save_contact').html('Save');
			}
		})
    })


    // add a new admin


    $("#newadminbtn").click(function(event) {
            var adminname = $("#adminname").val();
            var adminemail = $("#adminemail").val();
            var adminpassword = $("#adminpassword").val();
            var permission_level = $("#permission_level").val();
            if ($("#can_topup").prop('checked')) {
                var can_topup = $("#can_topup").val();
            }
            else{
                var can_topup = "0";
            }

            if ($("#can_suspend").prop('checked')) {
                var can_suspend = $("#can_suspend").val();
            }
            else{
                var can_suspend = "0";
            }

            if ($("#can_change_price").prop('checked')) {
                var can_change_price = $("#can_change_price").val();
            }
            else{
                var can_change_price = "0";
            }

            if ($("#can_map").prop('checked')) {
                var can_map = $("#can_map").val();
            }
            else{
                var can_map = "0";
            }


            $.post('../../resources/addadmin/', 
            {
                adminname:adminname,
                adminemail:adminemail,
                adminpassword:adminpassword,
                permission_level:permission_level,
                can_topup:can_topup,
                can_suspend:can_suspend,
                can_change_price:can_change_price,
                can_map:can_map
            }, 
            function(data, textStatus, xhr) {
                if (data == 1) {
                    $("#forceclosenewadmin").trigger('click');
                    swal('Success!', "Admin was added successfully", 'success');
                    loadAllAdmins();
                }
                else{
                    $("#newadminresponse").html(data).slideDown().delay(4000).slideUp();
                }
            });
    });

    // load users

    function loadusers()
    {
        $.post('../../resources/loadusers/',
        function(ajaxresponse)
        {
            $('#usersdiv').html(ajaxresponse);
        });
    }

    // load admins

    function loadAllAdmins() {
        $.post('../../resources/loadadmins/',
        function(data, textStatus, xhr) {
            $("#alladmins").html(data);
        });
    }


    // load user followups

    function loadUserFollowups(){
        $.post('../../resources/newfollowup/',
        function(data, textStatus, xhr) {
            $("#userfollowups").html(data);
        });
    }


    // load executive tasks

    function loadMyTasks() {
        $.post('../../resources/mytasks/',
        function(data, textStatus, xhr) {
            $("#mytasks").html(data);
        });
    }


    // load all the pending accounts where the user has not verified the phone number

    function loadPendingAccounts() {
        $.post('../../resources/loadpendingaccounts/',
        function(data, textStatus, xhr) {
            $("#pendingusersdiv").html(data);
        });
    }

    // load all the sender ids of the clients
    function loadAllSenderIds() {
        $.post('../../resources/loadsenderids/',
        function(data, textStatus, xhr) {
            $("#allsenderiddiv").html(data);
        });
    }

    // load all the sender ids that have not been activated

    function loadInActiveSenderIds() {
        $.post('../../resources/loadinactiveids/',
        function(data, textStatus, xhr) {
            $("#inactivesenderids").html(data);
        });
    }


    // load all the client's shortcodes

    function loadAllShortCodes() {
        $.post('../../resources/loadallshortcodes/',
        function(data, textStatus, xhr) {
            $("#allshortcodesdiv").html(data);
        });
    }


    // function to load executive tasks

    function loadExecutiveTasks() {
        $.post('../../resources/mytasks/',
        function(data, textStatus, xhr) {
            $("#mytasks").html(data);
        });
    }

    // load the dormant accounts for one week

    function loadDormantAccounts() {
        $.post('../../resources/useractivity/',
        function(data, textStatus, xhr) {
            $("#dormantaccounts").html(data);
        });
    }


    // load all the inactive shortcodes

    function loadInactiveShortcodes() {
        $.post('../../resources/loadinactiveshortcodes/',
        function(data, textStatus, xhr) {
            $("#pendingshortcodesdiv").html(data);
        });
    }

    // load all the transactions of our clients

    function loadAllUserTopups() {
        $.post('../../resources/loadusertopups/',
        function(data, textStatus, xhr) {
            $("#allusertopups").html(data);
        });
    }


    // load all the transactions to top up our account

    function loadOurTopups() {
        $.post('../../resources/loadourtopups',
        function(data, textStatus, xhr) {
            $("#statementloader").html(data);
        });
    }


    // load recent sign ups

    function loadrecent(){
        $.post('../../resources/recentusers/', 

        function(ajaxresponse) {
            $("#recentsignups").html(ajaxresponse);
        });
    }


    function loadRecentUserTopups() {
        $.post('../../resources/recenttopups/',
        function(data, textStatus, xhr) {
            $("#recentsalesdiv").html(data);
        });
    }

    // search all the users

    $(document.body).on("keyup","#_allusersearch_",function()
    {
        var search_    = $('#_allusersearch_').val();

        $.post('../../resources/loadusers/',
        {
            search_:search_
        }, 
        function(returngsearch)
        {
            $('#usersdiv').html(returngsearch);
        });
    })


    // search the recent users

    $(document.body).on("keyup","#recent_users_search_",function()
    {
        var search_    = $('#recent_users_search_').val();

        $.post('../../resources/recentusers/',
        {
            search_:search_
        }, 
        function(returngsearch)
        {
            $('#recentsignups').html(returngsearch);
        });
    })


    // search pending accounts

    $(document.body).on("keyup","#pend_acc_search_",function()
    {
        var search_    = $('#pend_acc_search_').val();

        $.post('../../resources/loadpendingaccounts/',
        {
            search_:search_
        }, 
        function(returngsearch)
        {
            $('#pendingusersdiv').html(returngsearch);
        });
    })


    // search all sender ids

    $(document.body).on("keyup","#senderid_search_",function()
    {
        var search_    = $('#senderid_search_').val();

        $.post('../../resources/loadsenderids/',
        {
            search_:search_
        }, 
        function(returngsearch)
        {
            $('#allsenderiddiv').html(returngsearch);
        });
    })

    // search inactive sender ids

    $(document.body).on("keyup","#inactive_id_search_",function()
    {
        var search_    = $('#inactive_id_search_').val();

        $.post('../../resources/loadinactiveids/',
        {
            search_:search_
        }, 
        function(returngsearch)
        {
            $('#inactivesenderids').html(returngsearch);
        });
    })


    // search all shortcodes

    $(document.body).on("keyup","#shortcode_search_",function()
    {
        var search_    = $('#shortcode_search_').val();
        $.post('../../resources/loadallshortcodes/',
        {
            search_:search_
        }, 
        function(returngsearch, status)
        {
            $('#allshortcodesdiv').html(returngsearch);
        });
    })


    // search the pending shortcodes

    $(document.body).on("keyup","#pendingshortcode_search_",function()
    {
        var search_    = $('#pendingshortcode_search_').val();
        $.post('../../resources/loadinactiveshortcodes/',
        {
            search_:search_
        }, 
        function(returngsearch, status)
        {
            $('#pendingshortcodesdiv').html(returngsearch);
        });
    })


    // search all user top ups

    $(document.body).on("keyup","#alltopups_search_",function()
    {
        var search_    = $('#alltopups_search_').val();
        $.post('../../resources/loadusertopups/',
        {
            search_:search_
        }, 
        function(returngsearch, status)
        {
            $('#allusertopups').html(returngsearch);
        });
    })


    // search the recent user top ups

    $(document.body).on("keyup","#recent_topups_search",function()
    {
        var search_    = $('#recent_topups_search').val();
        $.post('../../resources/recenttopups/',
        {
            search_:search_
        }, 
        function(returngsearch)
        {
            $('#recentsalesdiv').html(returngsearch);
        });
    })


    // search smsperuser

    $("#smsperuser_search_").keyup(function(event) {
        var search_    = $('#smsperuser_search_').val();

        $.post('../../resources/smsperuser/',
        {
            search_:search_
        }, 
        function(returngsearch)
        {
            $('#smsperuser').html(returngsearch);
        });
    });


    // search groups per user

    $("#usergroups_search_").keyup(function(event) {
        var search_    = $('#usergroups_search_').val();
        
        $.post('../../resources/groupsperuser/',
        {
            search_:search_
        }, 
        function(returngsearch)
        {
            $('#groupsperuser').html(returngsearch);
        });
    });


    // search contacts per user

    $("#usercontacts_search_").keyup(function(event) {
        var search_    = $('#usercontacts_search_').val();
        $.post('../../resources/contactsperuser/',
        {
            search_:search_
        }, 
        function(returngsearch)
        {
            $('#contactsperuser').html(returngsearch);
        });
    });

    //sort the users by category
    $("#search_user_category").click(function() {
        var category = $("#category").val();
        var status    = $("#stage").val();
        var account_currency    = $("#country").val();
        var last_sent    = $("#last_sent").val();
        var search_category = "";

        $.post('../../resources/loadusers/',
        {
            category: category,
            status:status,
            account_currency:account_currency,
            last_sent:last_sent,
            search_category:search_category

        },
        function(data, textStatus, xhr) {
            $('#usersdiv').html(data);
        });
    });


    // add a new task

    $("#addnewtaskbtn").click(function(event) {
        $("#addnewtaskbtn").html("Please wait...");

        var task_title = $("#tasktitle").val();
        var task_type  = $("#tasktype").val();
        var due_date   = $("#duedate").val();
        var description = $("#taskdescription").val();

        $.post('../../resources/addtask/', 
        {
            task_title:task_title,
            task_type:task_type,
            due_date:due_date,
            description:description
        }, 
        function(data, textStatus, xhr) {
            if (data == '1') {
                swal({
                  title: "Success!",
                  text: "Task has been successfully added.",
                  icon: "success",
                })
                $("#forceclosenewtask").trigger('click');
                loadExecutiveTasks();
            }
            else{
                $("#addnewtaskbtn").html("Add Task");
                $("#newtaskresponse").html(data);
            }
        });
    });


    // add a new followup

    $("#addnewfollowupbtn").click(function(event) {
        var userid  = $(this).val();
        var comment = $("#followupcomment").val();
        var title   = $("#followuptitle").val();

        $.post('../../resources/addnewfollowup/', 
        {
            userid:userid,
            comment:comment,
            title:title
        }, 
        function(data, textStatus, xhr) {
            if (data == 1) {
                $("#forceclosefollowup").trigger('click');
                swal({
                  title: "Success!",
                  text: "Followup has been successfully added.",
                  icon: "success",
                })
                .then((success) => {
                  if (success) {
                    location.reload();
                  }
                });
            }
            else{
                $("#followupresponse").html(data);
            }
        });
    });


    // sort the sender ids using the dropdown
    $("#filter_ids_search").click(function() {
        var status = $("#stage").val();
        var country    = $("#country").val();
        var search_category = "";

        $.post('../../resources/loadsenderids/',
        {
            status: status,
            country: country,
            search_category:search_category

        },
        function(data, textStatus, xhr) {
            $('#allsenderiddiv').html(data);
        });
    });


    // filter the shortcodes using the drop down

    $("#filter_shortcodes").click(function() {
        var type = $("#type").val();
        var category    = $("#category").val();
        var status = $("#status").val();
        var date_created = $("#date_created").val();
        var search_category = "";

        $.post('../../resources/loadallshortcodes/',
        {
            type:type,
            subscriptionOndemand:category,
            status:status,
            date_created:date_created,
            search_category:search_category

        },
        function(data, textStatus, xhr) {
            $('#allshortcodesdiv').html(data);
        });
    });


    // filter user activity

    $("#useractivity_filter_").click(function(event) {
        var last_used = $("#last_used").val();
        $.post('../../resources/useractivity/', 
        {
            last_used:last_used
        }, 
        function(data, textStatus, xhr) {
            $("#dormantaccounts").html(data);
        });
    });


    // filter recent signups

    $("#filter_recent_signups").click(function(event) {
        var date_range = $("#signup_dates").val();

        $.post('../../resources/recentusers/', 
        {
            date_range:date_range
        }, 
        function(data, textStatus, xhr) {
            $("#recentsignups").html(data);
        });
    });


    // filter pending accounts based on date

    $("#filter_pending_acc").click(function(event) {
        var date_filter = $("#sortpending_acc_").val();

        $.post('../../resources/loadpendingaccounts/', 
        {
            date_filter:date_filter
        }, 
        function(data, textStatus, xhr) {
            $("#pendingusersdiv").html(data);
        });
    });


    // filter user top ups using drop down

    $("#filter_topups").click(function() {
        var type = $("#type").val();
        var status = $("#status").val();
        var date_created = $("#date_created").val();
        var search_category = "";

        $.post('../../resources/loadusertopups/',
        {
            type:type,
            status:status,
            date_created:date_created,
            search_category:search_category

        },
        function(data, textStatus, xhr) {
            $('#allusertopups').html(data);
        });
    });


    // filter all the system's payments

    $("#filter_transactions").click(function() {
        var status = $("#status").val();
        var date_created = $("#date_created").val();
        var search_category = "";

        $.post('../../resources/loadourtopups/',
        {
            status:status,
            date_created:date_created,
            search_category:search_category

        },
        function(data, textStatus, xhr) {
            $('#statementloader').html(data);
        });
    });


    // filter messages sent based on date sent and status of the sender id

    $("#filter_messages").click(function(event) {
        event.preventDefault();
        var startdate = $("#startdate").val();
        var enddate   = $("#enddate").val();

        if (startdate.length != '0' && enddate.length != '0') {
            $.post('../../resources/smsperuser/',
            {
                startdate:startdate,
                enddate:enddate
            },
            function(data, textStatus, xhr) {
                $("#smsperuser").html(data);
            });
        }
        else{
            $.post('../resources/smsperuser/',
            function(data, textStatus, xhr) {
                $("#smsperuser").html(data);
            });
        }
    });


    // send notifs to all users

    $("#messageallbtn").click(function(event) {
        var message = $("#admin_sendall_input").val();

        $.post('../../resources/adminsendnotifs/',
        {
            message:message
        }, 
        function(data, textStatus, xhr) {
            if (data == 1) {
                alert("Messages sent");
            }
            else{
                $("#admin_message_response").html(data);
            }
        });
    });


  
    $('#addselected').click(function()
    {
        var selected_values = $("input[name='checkset[]']:checked");
        alert(selected_values);
    });

    // edit account
    $('#editinfobutton').click(function()
	{
		$('#editinfobutton').html('<i class="fa ion-load-c fa-spin"></i>');
		$('#editinfobutton').css('disabled','1');

		var editinfoname        = $('#editinfoname').val();
		var editinfophone       = $('#editinfophone').val();
		var editinfocity        = $('#editinfocity').val();
		
		$.post("../../system/editsettings/",
			{
				editinfoname:editinfoname,
				editinfophone:editinfophone,
				editinfocity:editinfocity
			},
			function(edresponse)
			{
				if(edresponse == 1)
				{
                    $('#setreference').html("<font class='text-success'><i class='ion-checkmark-circled'></i> Information Updated</font>");
                    $('#editinfobutton').html('Update Information<i class="ion-ios-arrow-thin-right">&nbsp;</i>');
                    $('#editinfobutton').css('disabled','0');
                }else{
					$('#setreference').html(edresponse);
					$('#editinfobutton').html('Update Information<i class="ion-ios-arrow-thin-right">&nbsp;</i>');
                    $('#editinfobutton').css('disabled','0');
				}
			});
    });


    // schedule subscription
    $('#schedulesmessage').click(function()
	{
		$('#schedulesmessage').html('<i class="fa ion-load-c fa-spin"></i>');
		$('#schedulesmessage').css('disabled','1');

		var stextmessage        = $('#stextmessage').val();
		var sdatetime           = $('#sdatetime').val();
		
		$.post("../../system/sendschedule/",
			{
				stextmessage:stextmessage,
				sdatetime:sdatetime
			},
			function(subresponse)
			{
				if(subresponse == 1)
				{
                    $('#schedulemessageresp').html("<br><font class='text-success'><i class='ion-checkmark-circled'></i> Message scheduled</font>");
                    $('#schedulesmessage').html('Schedule Message <i class="ion-ios-arrow-thin-right">&nbsp;</i>');
                    $('#schedulesmessage').css('disabled','0');
                    $('#stextmessage').val("");
                    $('#sdatetime').val("");
                    loadsubscriptionmessages();
                }else{
					$('#schedulemessageresp').html(subresponse);
					$('#schedulesmessage').html('Schedule Message <i class="ion-ios-arrow-thin-right">&nbsp;</i>');
                    $('#schedulesmessage').css('disabled','0');
				}
			});
    });


    // edit admin profile

    $("#editadmininfo").click(function(event) {
        $(this).html("<i class='fa ion-load-c fa-spin'></i>");

        var editadminname = $("#editadminname").val();
        var editadminphone = $("#editadminphone").val();

        $.post('../../resources/editprofile/', 
        {
            editadminname:editadminname,
            editadminphone:editadminphone
        }, 
        function(data, textStatus, xhr) {
            if (data == '1') {
                $("#editprof_response").slideDown().html("<font class='text-success'><i class='ion-checkmark-circled'></i> Information Updated</font>").delay(5000).slideUp();
                $("#editadmininfo").html("Update Information<i class='ion-ios-arrow-thin-right'>&nbsp;</i>");
            }
            else{
                $("#editprof_response").slideDown().html(data).delay(5000).slideUp();
                $("#editadmininfo").html("Update Information<i class='ion-ios-arrow-thin-right'>&nbsp;</i>");
            }
        });
    });



    // change admin password

    $("#updateadminpass").click(function(event) {
        $(this).html("<i class='fa ion-load-c fa-spin'></i>");


        var oldpass = $("#adminoldpass").val();
        var newpass = $("#adminnewpass").val();
        var newpassagain = $("#adminnewpass2").val();

        $.post('../../resources/changepassword/', 
        {
            oldpass:oldpass,
            newpass:newpass,
            newpassagain:newpassagain
        }, 
        function(data, textStatus, xhr) {
            if (data == 1) {
                $("#changepassresp").slideDown().html("<font class='text-success'><i class='ion-checkmark-circled'></i> Password changed successfully.</font>").delay(5000).slideUp();
                $("#updateadminpass").html("Update Password<i class='ion-ios-arrow-thin-right'>&nbsp;</i>");
                $(":password").val('');
            }
            else{
                $("#changepassresp").slideDown().html(data).delay(5000).slideUp();
                $("#updateadminpass").html("Update Password<i class='ion-ios-arrow-thin-right'>&nbsp;</i>");
            }
        });
    });


    // get number of contacts per user

    function loadContactsPerUser() {
        $.post('../../resources/contactsperuser/',
        function(data, textStatus, xhr) {
            $("#contactsperuser").html(data);
        });
    }


    // function to load groups per user

    function loadGroupsPerUser() {
        $.post('../../resources/groupsperuser/',
        function(data, textStatus, xhr) {
            $("#groupsperuser").html(data);
        });
    }

    // function to load groups per user

    function loadSmsUsagePerUser() {
        $.post('../../resources/smsperuser/',
        function(data, textStatus, xhr) {
            $("#smsperuser").html(data);
        });
    }
    
});