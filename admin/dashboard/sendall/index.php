<?php

/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
*/

include_once($_SERVER["DOCUMENT_ROOT"] . '/admin/includes/mother.php');



?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?php echo $account_name; ?> - Send Users Notifs</title>
	<meta content="Ionsolve" name="description">
	<meta content="width=device-width,initial-scale=1,maximum-scale=1,minimal-ui" name="viewport">
	<meta content="IE=edge" http-equiv="X-UA-Compatible">
	<meta content="yes" name="">
	<meta content="black-translucent" name="">
	<link href="../images/logo_small.png" rel="">
	<meta content="Ionsolve" name="">
	<meta content="yes" name="">
	<link href="../images/logo_small.png" rel="shortcut icon" sizes="196x196">
	<link href="../css/animate.css/animate.min.css" rel="stylesheet" type="text/css">
	<link href="../css/glyphicons/glyphicons.css" rel="stylesheet" type="text/css">
	<link href="../css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="../css/material-design-icons/material-design-icons.css" rel="stylesheet" type="text/css">
	<link href="../css/ionicons/css/ionicons.min.css" rel="stylesheet" type="text/css">
	<link href="../css/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css">
	<link href="../css/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="../css/styles/app.min.css" rel="stylesheet">
	<link href="../css/styles/font.css" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
	<link href="../codemirror/lib/codemirror.css" rel="stylesheet">
	<style type="text/css">
		::-webkit-scrollbar { 
		display: none; 
	}
	</style>
</head>
<body>
	<div class="app" id="app">
		<div class="app-content box-shadow-z2 pjax-container" id="content" role="main">
			<div class="app-body">
				<div class="app-body-inner">
					<div class="row-col">
						<?php include_once '../top.php'; ?>
						<div class="row-row">
							<div class="row-col">
                				<?php include_once '../side.php'; ?>

								<div class="col-xs col-md-9" id="detail">
									<p class="text-md text-primary" style="margin-left: 5px;">Send Notifs To All Users.</p><br>
									<div class="row-col white bg">
										<div class="row-row">
											<div class="row-body scrollable hover">
												<div class="row-inner">

													<div class="b-b" style="width:100%; background-color:white;">
														<div class="collapse in m-a-0" id="reply-2">
															<textarea id="textmessageall" style="background-color:white;" class="form-control no-border" placeholder="Type message here..." rows="5"></textarea>
															<div class="box-footer clearfix">
                                                                <button style="color: #27AAFF;border-color:#27AAFF;" id="sendallmessage" class="btn btn-sm white">Send Message To Everyone &nbsp;<i class="ion-ios-arrow-thin-right">&nbsp;</i> </button>
																<p id="allmessageresp"></p>
															</div>
														</div>
													</div>

												<div id="loadadminmessages"></div>
												</div>
											</div>
										</div>
									</div>
								</div>
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script src="../scripts/app.min.js"></script>
	<script src="../scripts/dmain.js"></script>

	<script>
		$('#sendallmessage').click(function()
		{
			var textmessageall       = $('#textmessageall').val();

			$('#sendallmessage').html('<i class="fa ion-load-c fa-spin"></i>');
			$('#sendallmessage').css('disabled','1');

			$.post("../../resources/sendall/",
			{
				textmessageall:textmessageall,
			},			
			function(allmessageresponse)
			{
				$('#allmessageresp').slideDown().html(allmessageresponse).delay('5000').slideUp();
				$('#sendallmessage').html('Send Message To Everyone <i class="ion-ios-arrow-thin-right">&nbsp;</i>');
				$('#sendallmessage').css('disabled','0');
			});
			
		});

		function loadMessages(){
			$('#loadadminmessages').load("../../resources/loadmessages/");
		}

		loadMessages();
		
	</script>
</body>
</html>