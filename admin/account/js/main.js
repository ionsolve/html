$(document).ready(function()
{
	// Admin Login

	$('#ad_loginbtn').click(function()
	{
		$('#ad_loginbtn').html('<i class="fa ion-load-c fa-spin"></i>');
		$('#ad_loginbtn').css('disabled','1');

		var ad_email_      = $('#ad_email_').val();
		var ad_password_   = $('#ad_password_').val();

		$.post("../../resources/login/",
		{
			ad_email_:ad_email_,
			ad_password_:ad_password_
		},
		function(loginresponse)
		{
			if (loginresponse == 1)
			{
				window.location.href = "../../dashboard/";
			}else{
				$('#ad_loginresponse').html(loginresponse);
				$('#ad_loginbtn').css('disabled','0');
				$('#ad_loginbtn').html('Sign In');
			}
		});
	});

});
