<?php

session_start();

if(isset($_SESSION['alphaion']))
{
    header('location:../../dashboard/');
}

?>
<!DOCTYPE html>
<html class="" lang="en">
<head>
	<meta charset="utf-8">
	<title>Signup - Ionsolve</title>
	<meta content="" name="description">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport">
	<link href="../images/logo_small.png" rel="icon" type="image/x-icon">
    <link href="../css/app.v1.css" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!--[if lt IE 9]> <script src="js/ie/html5shiv.js"></script> <script src="js/ie/respond.min.js"></script> <script src="js/ie/excanvas.js"></script> <![endif]-->
</head>
<body class="">
	<section class="m-t-lg wrapper-md animated fadeInDown" id="content">
		<div class="container aside-xl">
			<a class="block text-center" href="https://ionsolve.com"><img src="../images/logo.png" style="width:150px;"/></a>
			<section class="m-b-lg">
				<header class="wrapper text-center">
					<strong id="signresp">Sign up to find interesting things</strong>
				</header>
				
					<div class="list-group">
						<div class="list-group-item">
							<input id="_name_" type="text" class="form-control no-border" placeholder="Your name">
						</div>
						<div class="list-group-item">
							<input id="_username_" type="text" class="form-control no-border" placeholder="Pick a username">
						</div>
						<div class="list-group-item">
							<input id="_phone_" class="form-control no-border" placeholder="Phone number (International format)" type="text">
                        </div>
						<div class="list-group-item">
							<input id="_email_" class="form-control no-border" placeholder="Your email" type="email">
                        </div>
                        
						<div class="list-group-item">
							<select class="form-control no-border" id="_country_">
								<option class="form-control no-border" value="">Select Country</option>
								<option class="form-control no-border" value="KES">Kenya</option>
								<option class="form-control no-border" value="UGX">Uganda</option>
								<option class="form-control no-border" value="TZS">Tanzania</option>
								<option class="form-control no-border" value="MWK">Malawi</option>
								<option class="form-control no-border" value="NGN">Nigeria</option>
								<option class="form-control no-border" value="USD">Other Country</option>
							</select>
						</div>
						<div class="list-group-item">
							<input id="_password_" class="form-control no-border" placeholder="Set a password" type="password">
                        </div>

					</div>
					<div class="checkbox m-b">
					    By clicking on signup, you agree our <a href="https://ionsolve.com/terms" class="text-primary">terms and conditions</a> and <a href="https://ionsolve.com/terms" class="text-primary">privacy policy</a>
					</div>
					<button id="_new_account_" class="btn btn-lg btn-primary btn-block" type="submit">Sign up</button>

					<div class="line line-dashed"></div>
					<p class="text-muted text-center"><small>Already have an account?</small></p>
					<a class="btn btn-lg btn-default btn-block" href="../login/">Login</a>
				
			</section>
		</div>
	</section><!-- footer -->
	<footer id="footer">
		<div class="text-center padder clearfix">
			<p><small>Ionsolve<br>
			&copy; 2018</small></p>
		</div>
	</footer><!-- / footer --><!-- Bootstrap --><!-- App -->
	<script src="../js/app.v1.js"></script> 
	<script src="../js/app.plugin.js"></script>
	<script src="../js/main.js"></script>
	<script>
		$(document).ready(function(){
			$('#_name_').val("");
			$('#_email_').val("");
			$('#_username_').val("");
			$('#_email_').val("");
		});
	</script>
</body>
</html>