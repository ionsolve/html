<?php


/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
 *  
*/


include_once($_SERVER["DOCUMENT_ROOT"] . '/admin/resources/core/index.php');


if (isset($_POST['search_category'])) {
    $append = array();
    foreach($_POST as $key => $value) {
        if ($value != '0') {
            $append[] = "`".$key."`".' = '."'".$value."'";
        }
    }
    $appendquery = substr(implode(' AND ',$append), 0, -27);

    if (!empty($appendquery)) {
        $getUsers = "SELECT * FROM `users` WHERE ".$appendquery." ORDER BY name";
    }
    else{
        $getUsers = "SELECT * FROM `users` ORDER BY `name`";
    }
}

else{

    $getTasks = "SELECT * FROM `tasks` WHERE `task_owner`='$account_id'";
}

$run_fetch_query    = mysqli_query($conn, $getTasks);

$number_of_tasks = mysqli_num_rows($run_fetch_query);

if($number_of_tasks < 1)
{
?>

<br><br><div class="">
                                                    
    <div class="list-group m-b">
        <font class="list-group-item text-md text-primary" href="#">No Tasks to display</font> 
        <font class="list-group-item text-success" href="#"><i class="ion-information-circled"></i> 
            Please add a new task.
        </font> 
        <font class="list-group-item text-muted" href="#">
            Tasks that you have added to the system will be displayed here.
        </font>
    </div>

</div>

<?php
}else{

?>

<div class="table-responsive">
<table id="mytaskstable" class="table table-bordered m-a-0">
    <thead>
        <tr class="text-primary">
            
            <th>Task Title</th>
            <th>Task Type</th>
            <th>Task Status</th>
            <th>Task Description</th>
            <th>Due Date</th>
            <th>Date Created</th>
            <th>Action</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
<?php

while($listtasks = mysqli_fetch_array($run_fetch_query)){

    // $task_fetch_args = array('task_owner'=>$account_id);
?>
        <tr>
            <td><?php echo $listtasks['task_title']; ?></td>
            <td><?php echo $listtasks['task_type'];?></td>
            <td><?php echo $listtasks['task_status']; ?></td>
            <td><?php echo $listtasks['task_description']; ?></td>
            <td><?php echo $listtasks['due_date']; ?></td>
            <td><?php echo explode(" ",$listtasks['date_created'])[0]; ?></td>
            <td>
                <div class="btn-group dropdown">
                   
                    <button aria-expanded="false" class="btn btn-sm white dropdown-toggle" data-toggle="dropdown"></button>
                    <div class="dropdown-menu"> 
                        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#message<?php echo $userid; ?>" data-ui-toggle-class="fade-left-big" data-ui-target="#animate">Mark as Complete</a>
                        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#map_id<?php echo $userid; ?>" data-ui-toggle-class="fade-left-big" data-ui-target="#animate">Edit</a>
                    </div>
                </div>
            </td>
            <td></td>
        </tr>
<?php

}

?>
    </tbody>

</table><br><br>

</div>

<?php  
} 
?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script>   
    $(document.body).on("keyup","#textsingle textarea",function()
    {
        var datatag             = $(this).attr('data');
        var text_length         = $(this).val().length;
        $('#countfeed'+datatag).html('<font class="text-muted">'+text_length + ' characters</font><br><small class="text-info">Note: 160 characters make up one message</small>');
    })
</script>

<script>

    $(document).ready( function () {
        $('#mytaskstable').DataTable( {
            // paging:false
        })
    });
  
</script>

