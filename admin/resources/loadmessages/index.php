<?php


/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
 *  
*/


include_once($_SERVER["DOCUMENT_ROOT"] . '/admin/resources/core/index.php');
include_once($_SERVER["DOCUMENT_ROOT"] . '/admin/loader.php');

$fetch_args  =  array('parent' => '14', 'type'=>'5');

$totalsenthere = returnExists('messages',$fetch_args);

if($totalsenthere == 0){
?>

<div class="">
                                                    
    <div class="list-group m-b">
        <font class="list-group-item text-md text-primary" href="#">No messages found</font> 
        <font class="list-group-item text-success" href="#">
            <i class="ion-information-circled"></i> Please send message
        </font> 
        <font class="list-group-item text-muted" href="#">
            Update the users on the latest features in the system.
        </font>
    </div>

</div>
<?php
}else{

?>

<div class="streamline" style="">           
    <div class="p-a-md">
															
<?php

    $allMessages = "SELECT DISTINCT `refid`
    FROM `messages` 
    WHERE `parent`= '14' AND `type`='5'";

    $run_query = mysqli_query($conn, $allMessages);

    while($loadMessages = mysqli_fetch_array($run_query))
    {
        $mref           = $loadMessages['refid'];
        $typeof         = $loadMessages['type'];
        $fargs          = array('refid' => $mref);

        $successargs = array('refid' => $mref, 'status' => 'Success');
        $failedargs  = array('refid' => $mref, 'status' => 'Failed');
        
?>

<div class="m-b">
    <a class="pull-left w-40 m-r-sm" href="#">
        <span class="w-40 avatar circle b-primary text-primary b-a">
            <?php echo substr(getByValue('messages', 'message', $fargs),0,1); ?>
        </span>
    </a>
    <div class="clear">
        <div class="p-a p-y-sm b-a b-primary text-primary inline r">
            <?php echo getByValue('messages', 'message', $fargs); ?>
        </div>
        <div class="text-muted text-xs m-t-xs">
            <?php echo getByValue('messages', 'date_sent', $fargs); ?>   
            
            <a href="#" class="text-success"><?php echo returnExists('messages', $successargs) ?> Successful</a> . 
            <a href="#" class="text-warning"><?php echo returnExists('messages', $failedargs) ?> Failed</a>
        </div>
    </div>
</div>

<?php 
}
?>
</div>
</div>
<?php
}
?>
<script>
    function loadlogs(){
        $('#loadadminmessages').load("../../resources/loadmessages/");
    }
    
    $('#retrywrapper a').click(function()
    {
        $('#retrywrapper a').html('<i class="fa ion-load-c fa-spin"></i>');

        var resendmessageref = $('#retrywrapper a').attr('value');
        var resend  = "resend";

        $.post("../../system/preparenums/",
        {
            resend:resend,
            resendmessageref:resendmessageref
        },
        function(loadmessages)
        {
            if(loadmessages == 1){
                loadlogs();
            }else{
                loadlogs();
            }
        });
    });

    $('#pagination button').click(function()
    {

        var pagenumber = $(this).attr("value");

        $('#loadlogs').load("../../system/loadgroupmessages/?page="+pagenumber);

    })
</script>

