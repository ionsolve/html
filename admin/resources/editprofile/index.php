<?php
/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
 *
 */
include_once ($_SERVER["DOCUMENT_ROOT"] . '/admin/resources/core/index.php');

if (isset($_REQUEST['editadminname']) AND isset($_REQUEST['editadminphone']))
{

	// sanitize variables

	$editadminname    = mysqli_real_escape_string($conn, $_REQUEST['editadminname']);
	$editadminphone   = mysqli_real_escape_string($conn, $_REQUEST['editadminphone']);

	// validate empty fields

	if (empty($editadminname) OR empty($editadminphone))
	{
		die('<font style="color:red">Please fill all fields.</font>');
    }
    
    if (strpos($editadminphone, '+') === false) 
	{
		die('<font style="color:red">The phone number should be in international format starting with +</font>');
	}

	$updateAdmin = "UPDATE `admins` SET `name`='$editadminname',`phone_number`='$editadminphone' WHERE `email`='{$_SESSION['loggedin']}'";
           
	if (mysqli_query($conn, $updateAdmin))
	{
		echo "1";
	}else
	{
		die('<font style="color:red">Oops! Something went wrong.</font>');
	}
}

?>
