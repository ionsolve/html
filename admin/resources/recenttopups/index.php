<?php


/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
 *  
*/


include_once($_SERVER["DOCUMENT_ROOT"] . '/admin/resources/core/index.php');


if(isset($_REQUEST['search_'])){

    $search_         = mysqli_real_escape_string($conn, $_REQUEST['search_']);

    if(!empty($search_)){
        $getTopups  = "SELECT * FROM `top_ups` WHERE `description` != 'System Free SMS' AND DATEDIFF(now(), date_created) < 7 AND `description` like '%$search_%' order by id DESC";
    }else{
        $getTopups  = "SELECT * FROM `top_ups` WHERE `description` != 'System Free SMS' AND DATEDIFF(now(), date_created) < 7 order by id DESC";
    }
               

}else{

    $getTopups = "SELECT * FROM `top_ups` WHERE description != 'System Free SMS' AND DATEDIFF(now(), date_created) < 7 order by id";
}

$run_fetch_query    = mysqli_query($conn, $getTopups);

$number_of_topups = mysqli_num_rows($run_fetch_query);

if($number_of_topups < 1)
{
?>

<div class="">
                                                    
    <div class="list-group m-b">
        <font class="list-group-item text-md text-primary" href="#">No recent Top Ups Available</font> 
        <font class="list-group-item text-success" href="#"><i class="ion-information-circled"></i> 
            Please check later
        </font> 
        <font class="list-group-item text-muted" href="#">
            The recent top ups from the clients will be displayed here.
        </font>
    </div>

</div>

<?php
}else{

?>

<div class="table-responsive">
<table class="table table-bordered m-a-0">
    <thead>
        <tr class="text-primary">
            <th>Client Name</th>
            <th>Amount</th>
            <th>Reference Number</th>
            <th>Type</th>
            <th>Status</th>
            <!-- <th>Groups</th> -->
            <th>Date of Transaction</th>
            <!-- <th>Action</th> -->
            <th></th>
        </tr>
    </thead>
    <tbody>
<?php

while($listtrans = mysqli_fetch_array($run_fetch_query)){

    $tranid            = $listtrans['id'];
    $userid            = $listtrans['parent'];
    $users_fetch_args  = array('id'=>$userid);
    $trans_fetch_args  = array('id'=>$tranid);
?>
        <tr>
            <td><?php echo getByValue('users', 'name', $users_fetch_args); ?></td>
            <td><?php echo getByValue('top_ups', 'static_amount', $trans_fetch_args); ?></td>
            <td><?php echo substr(getByValue('top_ups', 'description', $trans_fetch_args), -10); ?></td>
            <td><?php echo substr(getByValue('top_ups', 'type', $trans_fetch_args), -10); ?></td>
            <?php 
            if(getByValue('users', 'status', $users_fetch_args) == 'pending'){

                echo "<td class='text-warning'><span class='fa fa-exclamation-circle'></span>"." ".ucfirst(getByValue('users', 'status', $users_fetch_args))."</td>";
            }
            elseif (getByValue('users', 'status', $users_fetch_args) == 'active') {
                echo "<td class='text-success'><span class='fa fa-check-circle'></span>"." ".ucfirst(getByValue('users', 'status', $users_fetch_args))."</td>";
            }
            ?>
            <td><?php echo getByValue('top_ups', 'date_created', $trans_fetch_args); ?></td>
            <!-- <td>
                <div class="btn-group dropdown">
                   
                    <button aria-expanded="false" class="btn btn-sm white dropdown-toggle" data-toggle="dropdown"></button>
                    <div class="dropdown-menu"> 
                        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#message<?php echo $userid; ?>" data-ui-toggle-class="fade-left-big" data-ui-target="#animate">Send Message</a> 
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item text-danger" data-toggle="modal" data-target="#delete<?php echo $userid; ?>" data-ui-toggle-class="fade-left-big" data-ui-target="#animate">Suspend Account</a>
                    </div>
                </div>
            </td> -->
            <td></td>
        </tr>

        <!-- Send Message-->

        <div aria-hidden="true" class="modal fade animate" data-backdrop="true" id="message<?php echo $contactid; ?>" style="display: none;">
			<div class="modal-dialog fade-left-big" data-ui-class="fade-left-big" id="animate">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title text-primary">SMS <?php echo getByValue('contacts', 'contact_name', $contact_fetch_args); ?></h5>			
					</div>
					<div class="modal-body p-lg" id="textsingle">
                        <div class="md-form-group">
                            <br>
                            <textarea class="md-input" data="<?php echo $contactid; ?>" id="messagemessage<?php echo $contactid; ?>" placeholder="Type Message Here" rows="4"></textarea>
                            <label>Message</label>
                            <select id="alongsender<?php echo $contactid; ?>" class="form-control c-select" style="border:none; border-bottom:1px solid #e8e8e8;">
                                <option value="">--Select Sender ID--</option>
                                <option value="Ionsolve">System Default</option>
                                <?php
                                    $sender_args_msg  = array('parent'=> $account_id, 'status'=>'active');
                                    $sender_id_values = returnArrayOfRequest('sender_ids','sender_id',$sender_args_msg);

                                    if(!empty($sender_id_values))
                                    {
                                        $ex_sender = explode(",",$sender_id_values);
                                    }
                                    foreach($ex_sender as $senderid)
                                    {
                                        echo '<option value="'.$senderid.'">'.$senderid.'</option>';
                                    }
                                ?>
                            </select><br>
                            <p id="countfeed<?php echo $contactid; ?>"></p>
                        </div>
   
						<p id="messagessresponse<?php echo $contactid; ?>"></p>
					</div>
					<div class="modal-footer" id="messagess">
						<button class="btn dark-white p-x-md" id="forceclosemessage" data-dismiss="modal" type="button">Cancel</button> 
						<button class="btn primary p-x-md" value="<?php echo $contactid; ?>" id="btnmessage" type="button">Send <i class="ion-ios-arrow-thin-right">&nbsp;</i></button>
					</div>
				</div>
			</div>
		</div>
<?php

}

?>
    </tbody>

    
</table><br><br>

</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script>   
    $(document.body).on("keyup","#textsingle textarea",function()
    {
        var datatag             = $(this).attr('data');
        var text_length         = $(this).val().length;
        $('#countfeed'+datatag).html('<font class="text-muted">'+text_length + ' characters</font><br><small class="text-info">Note: 160 characters make up one message</small>');
    })
</script>

<script>
    $('#recenttranspag button').click(function(){

        var pagenumber = $(this).attr("value");
        
        $.post('../../resources/recenttopups/',
        {
            pagenumber:pagenumber
        }, 
        function(nextpage)
        {
            $('#recentsalesdiv').html(nextpage);
        });
    })

    function loadcontacts()
    {
        $.post('../../resources/loadcontacts/',
        function(contresp)
        {
            $('#usersdiv').html(contresp);
        });
    }

    // edit contact

    $('#editcontacttopwrap #editbtn').click(function()
    {
        var editcontactid = $(this).attr('value');

        $('#editcontacttopwrap #editbtn').html('<i class="fa ion-load-c fa-spin"></i>');
        $('#editcontacttopwrap #editbtn').css('disabled','1');
        
        var editedname       =  $('#e_contact_'+editcontactid).val();
        var editednumber     =  $('#e_phone_'+editcontactid).val();
        var editedtag        =  $('#e_tags_'+editcontactid).val();

        $.post("../../system/editcontact/",
		{
            editedname:editedname,
            editednumber:editednumber,
            editedtag:editedtag,
            editingid:editcontactid
		},
		function(editresponse)
		{
			if (editresponse == 1)
			{
                $("#editcontacttopwrap #forcecloseedit").trigger("click");
                $('#editcontacttopwrap #editbtn').html('Update <i class="ion-ios-arrow-thin-right">&nbsp;</i>');
                loadcontacts();

			}else{
				$('#editresponse'+editcontactid).html(editresponse);
				$('#editcontacttopwrap #editbtn').css('disabled','0');
				$('#editcontacttopwrap #editbtn').html('Update <i class="ion-ios-arrow-thin-right">&nbsp;</i>');
			}
		})
        
    })


    // send message

    $('#messagess #btnmessage').click(function()
    {
        var messageid     = $(this).attr('value');
        var messagetosend = $('#messagemessage'+messageid).val();
        var alongsender   = $('#alongsender'+messageid).val();
        var single        = 'single';

        $('#messagess #btnmessage').html('<i class="fa ion-load-c fa-spin"></i>');
        $('#messagess #btnmessage').css('disabled','1');

        $.post("../../system/preparenums/",
		{
            messageid:messageid,
            messagetosend:messagetosend,
            alongsender:alongsender,
            single:single
		},
		function(messageresponse)
		{
			$('#messagessresponse'+messageid).html(messageresponse);
			$('#messagess #btnmessage').css('disabled','0');
			$('#messagess #btnmessage').html('Send <i class="ion-ios-arrow-thin-right">&nbsp;</i>');
		})
        
    })

    // delete contact

    $('#deletewrapper #deletecontactbtn').click(function()
    {
        var deleteid = $(this).attr('value');

        $('#deletewrapper #deletecontactbtn').html('<i class="fa ion-load-c fa-spin"></i>');
        $('#deletewrapper #deletecontactbtn').css('disabled','1');

        $.post("../../system/deletecontact/",
		{
            deleteid:deleteid
		},
		function(deleteresponse)
		{
			if (deleteresponse == 1)
			{
                $("#deletewrapper #forceclosedelete").trigger("click");
                $('#deletewrapper #deletecontactbtn').html('Yes, Delete <i class="ion-ios-arrow-thin-right">&nbsp;</i>');
                loadcontacts();
			}else{
				$('#deleteresponse'+deleteid).html(deleteresponse);
				$('#deletewrapper #deletecontactbtn').css('disabled','0');
				$('#deletewrapper #deletecontactbtn').html('Yes, Delete <i class="ion-ios-arrow-thin-right">&nbsp;</i>');
			}
		})
        
    })
  
</script>

