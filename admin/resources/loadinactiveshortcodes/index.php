<?php


/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
 *  
*/


include_once($_SERVER["DOCUMENT_ROOT"] . '/admin/resources/core/index.php');

if(isset($_REQUEST['search_'])){

    $search_         = mysqli_real_escape_string($conn, $_REQUEST['search_']);

    if(!empty($search_)){
        $fetch_shortcodes  = "SELECT * FROM `short_codes` WHERE `status` = 'pending' AND `code` like '%$search_%' order by id DESC LIMIT 20";
    }else{
        $fetch_shortcodes  = "SELECT * FROM `short_codes` WHERE `status` = 'pending' order by id DESC LIMIT 20";
    }
               

}else{

    if(isset($_REQUEST['pagenumber'])){
        $pagenumber  = mysqli_real_escape_string($conn, $_REQUEST['pagenumber']);
    }else{
        $pagenumber  = 0;
    }

    $limit           = 20 * $pagenumber;
    $limitup         = $limit + 20;

    $fetch_shortcodes = "SELECT * FROM `short_codes` WHERE `status` = 'pending' order by id LIMIT $limit, 20";
}

$run_fetch_query  = mysqli_query($conn, $fetch_shortcodes);

$number_of_shortcodes = mysqli_num_rows($run_fetch_query);


if($number_of_shortcodes < 1)
{
?>

<div class="">
                                                    
    <div class="list-group m-b">
        <font class="list-group-item text-md text-primary" href="#">No Inactive Shortcodes found</font> 
        <span class="list-group-item text-success"><i class="ion-android-information"></i> Everything is upto date.</span> 
        <font class="list-group-item text-muted" href="#">
            The Shortcodes that have not been activated will be displayed here.
        </font>

    </div>

</div>

<?php
}else{

?>

<div class="table-responsive">
<table id="pendingshortcodestable" class="table table-bordered m-a-0">

    <thead>
        <tr class="text-primary">
            <th>Client Name</th>
            <th>Shortcode</th>
            <th>Type</th>
            <th>Status</th>
            <th>Description</th>
            <th>Date Created</th>
            <th>Action</th>
            <th></th>
        </tr>
	</thead>
    <tbody>
<?php

while($listshortcodes = mysqli_fetch_assoc($run_fetch_query)){

    $shortcode           = $listshortcodes['id'];
    $shortcode_fetch  = array('id'=>$shortcode, 'status'=>'pending');
    $user_details_fetch = array('id'=>$listshortcodes['parent']);

?>

    <tr>
        <td><?php echo getByValue('users', 'name', $user_details_fetch); ?></td>
        <td><?php echo getByValue('short_codes', 'code', $shortcode_fetch); ?></td>
        <td><?php echo ucfirst(getByValue('short_codes', 'type', $shortcode_fetch)); ?></td>
        <td class="text-warning"><span class="fa fa-exclamation-circle"></span> <?php echo ucfirst(getByValue('short_codes', 'status', $shortcode_fetch)); ?></td>
        <td><?php echo ucfirst(getByValue('short_codes', 'subscriptionOndemand', $shortcode_fetch)); ?></td>
        <td><?php echo explode(" ", getByValue('short_codes', 'date_created', $shortcode_fetch))[0]; ?></td>
        <td><div class="btn-group dropdown">
               
            <button aria-expanded="false" class="btn btn-sm white dropdown-toggle" data-toggle="dropdown"></button>
            <div class="dropdown-menu"> 
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#activate_shortcode<?php echo $shortcode; ?>" data-ui-toggle-class="fade-left-big" data-ui-target="#animate">Activate Shortcode</a> 
            </div>
        </div></td>
        <td></td>
    </tr>


    <!-- Modal for activating the shortcode -->

    <div aria-hidden="true" class="modal fade animate" data-backdrop="true" id="activate_shortcode<?php echo $shortcode; ?>" style="display: none;">
        <div class="modal-dialog fade-left-big" data-ui-class="fade-left-big" id="animate">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title text-primary">Activate <?php echo getByValue('short_codes', 'code', $shortcode_fetch); ?></h5>         
                </div>
                <div class="modal-body p-lg">
                <div class="p-a padding">
                    <p class="text-md m-t block text-muted">Do you want to proceed?</p><br>
                    <p class="text-muted"><small>The user will be able to use the Shortcode after this.</small></p><br>
                    
                </div>
                    <p id="deleteresponse<?php echo $contactid; ?>"></p>
                </div>

                <div class="modal-footer" id="ad_activate_code_footer">
                    <button class="btn dark-white p-x-md" id="forceclose_act_code" data-dismiss="modal" type="button">Cancel</button> 
                    <button class="btn primary p-x-md" value="<?php echo $shortcode; ?>" id="activatecodebtn" type="button">Yes, Activate <i class="ion-ios-arrow-thin-right">&nbsp;</i></button>
                </div>
            </div>
        </div>
    </div>


<?php

} }

?>
    </tbody>

    
</table><br><br><br><br>

</div>



 <!-- message all pending shortcode users modal-->

<div aria-hidden="true" class="modal fade animate" data-backdrop="true" id="messagependingshortcodes" style="display: none;">
    <div class="modal-dialog fade-left-big" data-ui-class="fade-left-big" id="animate">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-primary">Message Inactive Shortcode Users</h5>            
            </div>
            <div class="modal-body p-lg">
                <div class="md-form-group">
                    <textarea rows="4" class="md-input" id="pending_acc" placeholder="Write Message Here...">Greetings from Ionsolve. We have noted that you have not activated your shortcode.</textarea>
                    <label>Message</label><br>
                </div>
                <p id="editresponse<?php echo $contactid; ?>"></p>
            </div>
            <div class="modal-footer" id="editcontacttopwrap">
                <button class="btn dark-white p-x-md" id="forcecloseedit" data-dismiss="modal" type="button">Cancel</button> 
                <button class="btn primary p-x-md" id="editbtn" value="<?php echo $contactid; ?>" type="button">Send Message <i class="ion-ios-arrow-thin-right">&nbsp;</i></button>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready( function () {
        $('#pendingshortcodestable').DataTable( {
            // paging:false
        })

        // Disable search and ordering by default
        $.extend( $.fn.dataTable.defaults, {
            searching: false,
            ordering:  false
        } );

        function loadAllShortCodes() {
            $.post('../../resources/loadinactiveshortcodes/',
            function(data, textStatus, xhr) {
                $("#pendingshortcodesdiv").html(data);
            });
        }


        $("#ad_activate_code_footer #activatecodebtn").click(function(event) {
            var shortcode_id = $(this).val();

            $.post('../../resources/activateshortcode/', 
            {
                shortcode_id:shortcode_id
            }, 
            function(data, textStatus, xhr) {
                if (data == '1') {
                    $("#activateshortcode_footer #forceclose_act_code").trigger('click');
                    swal({
                      title: "Success!",
                      text: "The Shortcode has been activated.",
                      icon: "success",
                    })
                    .then((success) => {
                      if (success) {
                        loadAllShortCodes();
                      }
                    });
                }
                else{
                    alert(data);
                }
            });
        });
    });
</script>

