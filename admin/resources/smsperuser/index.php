<?php


/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
 *  
*/


include_once($_SERVER["DOCUMENT_ROOT"] . '/admin/resources/core/index.php');

if (isset($_POST['search_'])) {
    if (!empty($_POST['search_'])) {
        $search_ = mysqli_real_escape_string($conn, $_POST['search_']);
        $sql = "SELECT `users`.`name`, `users`.`phone`, `messages`.`parent`, COUNT(`messages`.`parent`) AS `number_of_messages` FROM `users` JOIN `messages` ON `users`.`id` = `messages`.`parent` WHERE `users`.`name` LIKE '%$search_%' GROUP BY `users`.`id` ORDER BY `number_of_messages`";
    }
    else{
        $sql = "SELECT `users`.`name`, `users`.`phone`, `messages`.`parent`, COUNT(`messages`.`parent`) AS number_of_messages FROM `users` JOIN `messages` ON `users`.`id` = `messages`.`parent` GROUP BY `users`.`id` ORDER BY `number_of_messages`";
    }
}

elseif (isset($_POST['startdate']) && isset($_POST['enddate'])) {
    if (!empty($_POST['startdate']) && !empty($_POST['enddate'])) {
        $startdate = mysqli_real_escape_string($conn, $_POST['startdate']);
        $enddate = mysqli_real_escape_string($conn, $_POST['enddate']);

        $sql = "SELECT `users`.`name`, `users`.`phone`, `messages`.`parent`, COUNT(`messages`.`parent`) AS `number_of_messages`, `messages`.`date_sent` FROM `users` JOIN `messages` ON `users`.`id` = `messages`.`parent` WHERE `messages`.`date_sent` BETWEEN '$startdate' AND '$enddate' GROUP BY `users`.`id` ORDER BY `number_of_messages` DESC";
    }

}
else{
    $sql = "SELECT `users`.`name`, `users`.`phone`, `messages`.`parent`, COUNT(`messages`.`parent`) AS `number_of_messages` FROM `users` JOIN `messages` ON `users`.`id` = `messages`.`parent` GROUP BY `users`.`id` ORDER BY `number_of_messages` DESC";
}

$query2 = mysqli_query($conn, $sql);

if (mysqli_num_rows($query2) > 0) {

?>
<div class="table-responsive">
    <table id="smsperusertable" class="table table-bordered m-a-0">

        <thead>
            <tr class="text-primary">
                <th>User</th>
                <th>Number of Messages</th>
                <th>Number of Successful Messages</th>
                <th>Number of Failed Messages</th>
            </tr>
        </thead>
        <tbody>

<?php

while ($row = mysqli_fetch_assoc($query2)) {

    $failed_condition = array('status'=>'Failed', 'parent'=>$row['parent']);

    $success_condition = array('status'=>'Success', 'parent'=>$row['parent']);
?>
        <tr>
            <td><?php echo $row['name']; ?></td>
            <td><?php echo $row['number_of_messages']; ?></td>
            <?php
            if (!empty($startdate) && !empty($enddate)) {
?>
            <td><?php echo returnMessagesTwoDates('messages',$success_condition, $startdate, $enddate); ?></td>
            <td><?php echo returnMessagesTwoDates('messages',$failed_condition, $startdate, $enddate) ?></td>
<?php
            }else{
?>
            <td><?php echo returnCountWithCondition('messages', $success_condition) ?></td>
            <td><?php echo returnCountWithCondition('messages', $failed_condition) ?></td>
<?php
            }
            ?>
            
        </tr>

<?php

}

?>

</tbody>

        
</table><br><br><br><br>

</div>

<?php
}
else{
?>

<br><br><div class="">
                                                        
    <div class="list-group m-b">
        <font class="list-group-item text-md text-primary" href="#">No Results Found</font> 
        <span class="list-group-item text-success"><i class="ion-android-information"></i>Please check later</span> 
        <font class="list-group-item text-muted" href="#">
            Users who have sent messages and their usage will be displayed here.
        </font>

    </div>

</div>

<?php
}
?>

<script type="text/javascript">
    $(document).ready( function () {
        $('#smsperusertable').DataTable( {
            // paging:false
        })
    });
</script>