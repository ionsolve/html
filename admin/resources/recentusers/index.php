<?php


/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
 *  
*/

include_once($_SERVER["DOCUMENT_ROOT"] . '/admin/resources/core/index.php');

if(isset($_POST['search_'])){

    $search_         = mysqli_real_escape_string($conn, $_REQUEST['search_']);

    if(!empty($search_)){
        $getUsers  = "SELECT * FROM `users` WHERE DATEDIFF(now(), date_created) < 7 AND name like '%$search_%' order by name DESC LIMIT 50";
    }else{
        $getUsers  = "SELECT * FROM `users` WHERE DATEDIFF(now(), date_created) < 7 order by name DESC LIMIT 50";
    }
               

}

elseif (isset($_POST['date_range'])) {
    $date_range = mysqli_real_escape_string($conn, $_POST['date_range']);

    if ($date_range == '0') {
        $getUsers = "SELECT * FROM `users` WHERE DATEDIFF(NOW(), `date_created`) < 7 ORDER BY `name`";
    }
    elseif ($date_range == '1') {
        $getUsers = "SELECT * FROM `users` WHERE DATEDIFF(NOW(), `date_created`) < 7 ORDER BY `name`";
    }
    elseif ($date_range == '2') {
        $getUsers = "SELECT * FROM `users` WHERE DATEDIFF(NOW(), `date_created`) < 14 ORDER BY `name`";
    }
    elseif ($date_range == '3') {
        $getUsers = "SELECT * FROM `users` WHERE DATEDIFF(NOW(), `date_created`) < 30 ORDER BY `name`";
    }
    // elseif ($date_range == '4') {
    //     $getUsers = "SELECT * FROM `users` WHERE DATEDIFF(NOW(), `date_created`) > 30 ORDER BY `name` DESC";
    // }
}


else{

    if(isset($_REQUEST['pagenumber'])){
        $pagenumber  = mysqli_real_escape_string($conn, $_REQUEST['pagenumber']);
    }else{
        $pagenumber  = 0;
    }

    $limit           = 50 * $pagenumber;
    $limitup         = $limit + 50;

    $getUsers = "SELECT * FROM `users` WHERE DATEDIFF(now(), date_created) < 7";
}

$run_fetch_query    = mysqli_query($conn, $getUsers);

$date_today = date('m/d/Y H:i:s');

$number_of_users = mysqli_num_rows($run_fetch_query);

if($number_of_users < 1)
{
?>

<br><br><div class="">
                                                    
    <div class="list-group m-b">
        <font class="list-group-item text-md text-primary" href="#">No Results Found.</font> 
        <font class="list-group-item text-success" href="#"><i class="ion-information-circled"></i> 
            Please check later
        </font> 
        <font class="list-group-item text-muted" href="#">
            Users that created accounts in the past one week will be displayed here.
        </font>
    </div>

</div>
<?php
}else{

?>

<div class="table-responsive">
<table id="recentusers_tbl" class="table table-bordered m-a-0">
    <thead>
        <tr class="text-primary">
            
            <th>Name</th>
            <th>Status</th>
            <th>Country</th>
            <th>Phone Number</th>
            <th>Date Created</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
<?php

while($listusers = mysqli_fetch_array($run_fetch_query)){

    $userid           = $listusers['id'];
    $users_fetch_args  = array('id'=>$userid);
    $user_currency = $listusers['account_currency'];

?>
        <tr>
            <td><?php echo getByValue('users', 'name', $users_fetch_args); ?></td>
            <td><?php echo ucfirst(getByValue('users', 'status', $users_fetch_args)); ?></td>
            <td><?php echo getCountryWithCurrencyCode($user_currency) ?></td>
            <td><?php echo getByValue('users', 'phone', $users_fetch_args); ?></td>
            <td><?php echo explode(" ",getByValue('users', 'date_created', $users_fetch_args))[0]; ?></td>
            <td></td>
        </tr>


        <!-- Send Message-->

        <div aria-hidden="true" class="modal fade animate" data-backdrop="true" id="message<?php echo $userid; ?>" style="display: none;">
			<div class="modal-dialog fade-left-big" data-ui-class="fade-left-big" id="animate">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title text-primary">SMS <?php echo getByValue('users', 'name', $users_fetch_args); ?></h5>			
					</div>
					<div class="modal-body p-lg" id="textsingle">
                        <div class="md-form-group">
                            <br>
                            <textarea class="md-input" data="<?php echo $contactid; ?>" id="messagemessage<?php echo $contactid; ?>" placeholder="Type Message Here" rows="4"></textarea>
                            <label>Message</label>
                            <select id="alongsender<?php echo $contactid; ?>" class="form-control c-select" style="border:none; border-bottom:1px solid #e8e8e8;">
                                <option value="">--Select Sender ID--</option>
                                <option value="Ionsolve">System Default</option>
                                <?php
                                    $sender_args_msg  = array('parent'=> $account_id, 'status'=>'active');
                                    $sender_id_values = returnArrayOfRequest('sender_ids','sender_id',$sender_args_msg);

                                    if(!empty($sender_id_values))
                                    {
                                        $ex_sender = explode(",",$sender_id_values);
                                    }
                                    foreach($ex_sender as $senderid)
                                    {
                                        echo '<option value="'.$senderid.'">'.$senderid.'</option>';
                                    }
                                ?>
                            </select><br>
                            <p id="countfeed<?php echo $contactid; ?>"></p>
                        </div>
   
						<p id="messagessresponse<?php echo $contactid; ?>"></p>
					</div>
					<div class="modal-footer" id="messagess">
						<button class="btn dark-white p-x-md" id="forceclosemessage" data-dismiss="modal" type="button">Cancel</button> 
						<button class="btn primary p-x-md" value="<?php echo $contactid; ?>" id="btnmessage" type="button">Send <i class="ion-ios-arrow-thin-right">&nbsp;</i></button>
					</div>
				</div>
			</div>
		</div>
<?php

}

?>
    </tbody>

    
</table><br><br>

</div>
<?php
}
?>

<script type="text/javascript">
    $(document).ready( function () {
        $('#recentusers_tbl').DataTable( {
            // paging:false
        })
    });
</script>