<?php


/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
 *  
*/


include_once($_SERVER["DOCUMENT_ROOT"] . '/admin/resources/core/index.php');

$checkExists = array('email'=>$_POST['adminemail']);

if (empty($_POST['adminname'])) {
	die('<font style="color:red;">Name is required.</font>');
}

elseif (empty($_POST['adminemail'])) {
	die('<font style="color:red;">Email is required.</font>');
}

elseif (returnExists('admins', $checkExists) > 0) {
	die('<font style="color: red;">Email Address is already registered.</font>');
}

elseif (empty($_POST['adminpassword'])) {
	die('<font style="color:red;">Password is required.</font>');
}

elseif (strlen($_POST['adminpassword']) < 8) {
	die("<font style='color: red;'>Password must be at least 8 characters.</font>");
}

elseif (empty($_POST['permission_level'])) {
	die('<font style="color:red;">Please select the permission level.</font>');
}

else{
	$adminname = mysqli_real_escape_string($conn, $_POST['adminname']);
	$adminemail = mysqli_real_escape_string($conn, $_POST['adminemail']);
	$adminpassword = md5(mysqli_real_escape_string($conn, $_POST['adminpassword']));
	$can_topup = mysqli_real_escape_string($conn, $_POST['can_topup']);
	$can_suspend = mysqli_real_escape_string($conn, $_POST['can_suspend']);
	$can_change_price = mysqli_real_escape_string($conn, $_POST['can_change_price']);
	$can_map = mysqli_real_escape_string($conn, $_POST['can_map']);
	$permission_level = mysqli_real_escape_string($conn, $_POST['permission_level']);
	$datetime = date("Y-m-d H:i:s");

	$query_insert = mysqli_query($conn, "INSERT INTO `admins` (`name`,`email`,`password`,`can_topup_accounts`,`can_suspend_accounts`,`can_map_senderids`,`can_change_pricing`,`permission_level`, `date_created`) VALUES ('$adminname','$adminemail','$adminpassword','$can_topup','$can_suspend','$can_map','$can_change_price','$permission_level','$datetime')");

	if ($query_insert) {
		echo "1";
	}
	else{
		echo "An error occurred while processing request.";
	}
}


?>