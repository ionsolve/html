<?php


/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
 *  
*/


include_once($_SERVER["DOCUMENT_ROOT"] . '/admin/resources/core/index.php');

if (isset($_POST['sender_id'])) {
	$senderid = mysqli_real_escape_string($conn, $_POST['sender_id']);

	$sender_fetch_args = array('id'=>$senderid);

	$parent = getByValue('sender_ids', 'parent', $sender_fetch_args);

	$sender = getByValue('sender_ids', 'sender_id', $sender_fetch_args);

	$message = 'Test';

	$phonenumbers = returnArrayOfAllTable('admins', 'phone_number');

	$jsonresponse = testSender($phonenumbers, $message, $parent, $sender);

	if ($jsonresponse['status'] == 200) {
		echo "1";
	}
	else{
		echo "An error occurred while processing your request.".$jsonresponse['message'];
	}
}

?>