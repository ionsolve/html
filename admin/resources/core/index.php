<?php
session_start();
require_once $_SERVER["DOCUMENT_ROOT"] .'/admin/includes/mother.php';

if (!isset($_SESSION['loggedin'])) {
	header('location:../../admin/account/login/');
}

date_default_timezone_set("Africa/Nairobi");


$loggedin = array(
				'email'=>$_SESSION['loggedin'],
			);

// base path for the project

$base_path = "/";

$paybill_number = "509325";

$account_name  = getByValue('admins', 'name', $loggedin);
$account_phone = getByValue('admins', 'phone_number', $loggedin);
$account_id    = getByValue('admins', 'id', $loggedin);

$can_topup_account = getByValue('admins', 'can_topup_accounts', $loggedin);
$can_suspend_accounts = getByValue('admins', 'can_suspend_accounts', $loggedin);
$can_change_pricing   = getByValue('admins', 'can_change_pricing', $loggedin);
$can_map_senderid     = getByValue('admins', 'can_map_senderids', $loggedin);
$permission_level     = getByValue('admins', 'permission_level', $loggedin);


// 

?>