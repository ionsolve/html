<?php


/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
 *  
*/


include_once($_SERVER["DOCUMENT_ROOT"] . '/admin/resources/core/index.php');

if (isset($_POST['search_category'])) {
    $append = array();
    foreach($_POST as $key => $value) {
        if ($value != '0') {
            $append[] = "`".$key."`".' = '."'".$value."'";
        }
    }
    $appendquery = substr(implode(' AND ',$append), 0, -27);

    if (!empty($appendquery)) {
        $fetch_transactions = "SELECT * FROM `top_ups` WHERE ".$appendquery." AND `description` != 'System Free SMS' ORDER BY `id`";
    }
    else{
        $fetch_transactions = "SELECT * FROM `top_ups` WHERE `description` != 'System Free SMS' ORDER BY `id`";
    }
}

elseif(isset($_REQUEST['search_'])){

    $search_         = mysqli_real_escape_string($conn, $_REQUEST['search_']);

    if(!empty($search_)){
        $fetch_transactions  = "SELECT * FROM `top_ups` WHERE `description` != 'System Free SMS' AND `description` like '%$search_%' order by id DESC";
    }else{
        $fetch_transactions  = "SELECT * FROM `top_ups` WHERE `description` != 'System Free SMS' order by id DESC";
    }
               

}else{

    $fetch_transactions = "SELECT * FROM `top_ups` WHERE `description` != 'System Free SMS' order by id";
}

$run_fetch_query  = mysqli_query($conn, $fetch_transactions);

$number_of_transactions = mysqli_num_rows($run_fetch_query);

if($number_of_transactions < 1)
{
?>

<br><br><div class="">
                                                    
    <div class="list-group m-b">
        <font class="list-group-item text-md text-primary" href="#">No User Transactions were found</font> 
        <span class="list-group-item text-success"><i class="ion-android-information"></i> Please check later.</span> 
        <font class="list-group-item text-muted" href="#">
            All the transactions of top ups from the clients will be displayed here.
        </font>

    </div>

</div>

<?php
}else{

?>

<div class="table-responsive">
<table id="userpaymentstable" class="table table-bordered m-a-0">

    <thead>
        <tr class="text-primary">
            <th>Client Name</th>
            <th>Reference Number</th>
            <th>Amount</th>
            <th>Type</th>
            <th>Status</th>
            <th>Date of Transaction</th>
            <!-- <th>Action</th> -->
            <th></th>
        </tr>
	</thead>
    <tbody>
<?php

while($listtransactions = mysqli_fetch_assoc($run_fetch_query)){

    $transactionids           = $listtransactions['id'];
    $tran_fetch_args  = array('id'=>$transactionids);
    $user_details_fetch = array('id'=>$listtransactions['parent']);

?>

    <tr>
        <td><?php echo getByValue('users', 'name', $user_details_fetch); ?></td>
        <td><?php echo substr(getByValue('top_ups', 'description', $tran_fetch_args), -10); ?></td>
        <td><?php echo getByValue('top_ups', 'static_amount', $tran_fetch_args); ?></td>
        <td><?php echo getByValue('top_ups', 'type', $tran_fetch_args); ?></td>
        
        <?php
            if(getByValue('top_ups', 'status', $tran_fetch_args) == "active"){
        ?>
        <td class="text-success"><span class="fa fa-check-circle"></span> Active</td>
            <?php }else{ ?>
        <td class="text-warning"><span class="fa fa-exclamation-circle"></span> Depleted</td>
            <?php } ?>
        <td><?php echo explode(" ", getByValue('top_ups', 'date_created', $tran_fetch_args))[0]; ?></td>
        <td></td>
    </tr>


<?php

}
}


?>
    </tbody>

    
</table><br><br><br><br>

</div>

<script type="text/javascript">
    // do all sender ids pagination

    $('#alltranspag button').click(function(){

        var pagenumber = $(this).attr("value");
        
        $.post('../../resources/loadusertopups/',
        {
            pagenumber:pagenumber
        }, 
        function(nextpage)
        {
            $('#allusertopups').html(nextpage);
        });
    })

    $(document).ready( function () {
        $('#userpaymentstable').DataTable( {
            // paging:false
        })
    });
</script>



