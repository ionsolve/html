<?php


/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
 *  
*/


include_once($_SERVER["DOCUMENT_ROOT"] . '/admin/resources/core/index.php');


if(isset($_REQUEST['search_'])){

    $search_         = mysqli_real_escape_string($conn, $_REQUEST['search_']);

    if(!empty($search_)){
        $fetch_ids  = "SELECT * FROM `sender_ids` WHERE `status` = 'suspended' AND `sender_id` like '%$search_%' order by id DESC LIMIT 20";
    }else{
        $fetch_ids  = "SELECT * FROM `sender_ids` WHERE `status` = 'suspended' order by id DESC LIMIT 20";
    }
               

}else{

    if(isset($_REQUEST['pagenumber'])){
        $pagenumber  = mysqli_real_escape_string($conn, $_REQUEST['pagenumber']);
    }else{
        $pagenumber  = 0;
    }

    $limit           = 20 * $pagenumber;
    $limitup         = $limit + 20;

    $fetch_ids = "SELECT * FROM `sender_ids` WHERE `status` = 'suspended' ORDER BY id DESC LIMIT $limit, 20";
}

$run_fetch_query  = mysqli_query($conn, $fetch_ids);

$number_of_senders = mysqli_num_rows($run_fetch_query);


if($number_of_senders < 1)
{
?>

<div class="">
                                                    
    <div class="list-group m-b">
        <font class="list-group-item text-md text-primary" href="#">No Results found</font> 
        <span class="list-group-item text-success"><i class="ion-android-information"></i> Please check later.</span> 
        <font class="list-group-item text-muted" href="#">
            The Sender IDs that are under risk hold will be displayed here.
        </font>

    </div>

</div>

<?php
}else{

?>

<div class="table-responsive">
<table id="suspendedidstable" class="table table-bordered m-a-0">

    <thead>
        <tr class="text-primary">
            <th>Client Name</th>
            <th>Country</th>
            <th>Sender ID</th>
            <th>Status</th>
            <th>Action</th>
            <th></th>
        </tr>
	</thead>
    <tbody>
<?php

while($listsenders = mysqli_fetch_assoc($run_fetch_query)){

    $senderid           = $listsenders['id'];
    $sender_fetch_args  = array('id'=>$senderid);
    $user_details_fetch = array('id'=>$listsenders['parent']);

?>

    <tr>
        <td><?php echo getByValue('users', 'name', $user_details_fetch); ?></td>
        <td><?php echo ucfirst(getByValue('sender_ids', 'country', $sender_fetch_args)); ?></td>
        <td><?php echo getByValue('sender_ids', 'sender_id', $sender_fetch_args); ?></td>
        
        <?php
            if(getByValue('sender_ids', 'status', $sender_fetch_args) == "active"){
        ?>
        <td class="text-success"><span class="fa fa-check-circle"></span> Active</td>
            <?php }else{ ?>
        <td class="text-warning"><span class="fa fa-exclamation-circle"></span> Inactive</td>
            <?php } ?>
        <td>
            <div class="btn-group dropdown">
                   
                <button aria-expanded="false" class="btn btn-sm white dropdown-toggle" data-toggle="dropdown"></button>
                <div class="dropdown-menu"> 
                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#activate_id_<?php echo $senderid; ?>" data-ui-toggle-class="fade-left-big" data-ui-target="#animate">Activate Sender ID</a>
                </div>
            </div>
        </td>
        <td></td>
    </tr>



    <!-- start of modal for resending the activation code -->

    <div aria-hidden="true" class="modal fade animate" data-backdrop="true" id="activate_id_<?php echo $senderid; ?>" style="display: none;">
        <div class="modal-dialog fade-left-big" data-ui-class="fade-left-big" id="animate">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title text-primary">Release <?php echo getByValue('sender_ids', 'sender_id', $sender_fetch_args); ?></h5>         
                </div>
                <div class="modal-body p-lg">
                <div class="p-a padding">
                    <p class="text-md m-t block text-muted">Do you want to proceed?</p><br>
                    <p class="text-muted"><small>After this, the user will be able to use the Sender ID again.</small></p><br>
                    
                </div>
                    <p id="deleteresponse<?php echo $contactid; ?>"></p>
                </div>

                <div class="modal-footer" id="ad_release_id_footer">
                    <button class="btn dark-white p-x-md" id="forcecloserelease" data-dismiss="modal" type="button">Cancel</button> 
                    <button class="btn primary p-x-md" value="<?php echo $senderid; ?>" id="ad_release_idbtn" type="button">Yes, Release <i class="ion-ios-arrow-thin-right">&nbsp;</i></button>
                </div>
            </div>
        </div>
    </div>

    <!-- end of modal for resending activation code -->


<?php

}

}

?>
    </tbody>

    
</table><br><br><br><br>

</div>



<!-- message all pending sender IDs users modal-->

<div aria-hidden="true" class="modal fade animate" data-backdrop="true" id="messagependingsenderids" style="display: none;">
    <div class="modal-dialog fade-left-big" data-ui-class="fade-left-big" id="animate">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-primary">Message Inactive Sender IDs Users</h5>            
            </div>
            <div class="modal-body p-lg">
                <div class="md-form-group">
                    <textarea rows="4" class="md-input" id="pending_acc" placeholder="Write Message Here...">Greetings from Ionsolve. We have noted that you have not activated your Sender ID.</textarea>
                    <label>Message</label><br>
                </div>
                <p id="editresponse<?php echo $contactid; ?>"></p>
            </div>
            <div class="modal-footer" id="editcontacttopwrap">
                <button class="btn dark-white p-x-md" id="forcecloseedit" data-dismiss="modal" type="button">Cancel</button> 
                <button class="btn primary p-x-md" id="editbtn" value="<?php echo $contactid; ?>" type="button">Send Message <i class="ion-ios-arrow-thin-right">&nbsp;</i></button>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready( function () {
        $('#suspendedidstable').DataTable( {
            // paging:false
        })

        function loadSenderIDs() {
            $.post('../../resources/loadinactiveids/',
            function(data, textStatus, xhr) {
                $("#inactivesenderids").html(data);
            });
        }


        $("#ad_release_id_footer #ad_release_idbtn").click(function(event) {
            var senderid = $(this).val();

            $.post('../../resources/activateid/',
            {
                senderid:senderid
            }, 
            function(data, textStatus, xhr) {
                if (data == '1') {
                    $("#ad_release_id_footer #forcecloserelease").trigger('click');
                    swal({
                      title: "Success!",
                      text: "The Sender ID has been Activated.",
                      icon: "success",
                    })
                    .then((success) => {
                      if (success) {
                        loadSenderIDs();
                      }
                    });
                    }
                else{
                    alert(data);
                }
            });
        });
    });
</script>

