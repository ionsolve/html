<?php


/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
 *  
*/


include_once($_SERVER["DOCUMENT_ROOT"] . '/admin/resources/core/index.php');

if (isset($_POST['search_'])) {
    if (!empty($_POST['search_'])) {
        $search_ = mysqli_real_escape_string($conn, $_POST['search_']);
        $sql = "SELECT `users`.`name`, `users`.`phone`, COUNT(`groups`.`parent`) AS `number_of_groups` FROM `users` JOIN `groups` ON `users`.`id` = `groups`.`parent` WHERE `users`.`name` LIKE '%$search_%' GROUP BY `users`.`id` ORDER BY `number_of_groups` DESC";
    }
    else{
        $sql = "SELECT `users`.`name`, `users`.`phone`, COUNT(`groups`.`parent`) AS `number_of_groups` FROM `users` JOIN `groups` ON `users`.`id` = `groups`.`parent` GROUP BY `users`.`id` ORDER BY `number_of_groups` DESC";
    }
}

else{
    $sql = "SELECT `users`.`name`, `users`.`phone`, COUNT(`groups`.`parent`) AS `number_of_groups` FROM `users` JOIN `groups` ON `users`.`id` = `groups`.`parent` GROUP BY `users`.`id` ORDER BY `number_of_groups` DESC";
}

$query2 = mysqli_query($conn, $sql);

$number_of_users = mysqli_num_rows($query2);

if (mysqli_num_rows($query2) > 0) {

?>

<div class="table-responsive">
    <table id="groupsperusertable" class="table table-bordered m-a-0">

        <thead>
            <tr class="text-primary">
                <th>User</th>
                <th>Phone Number</th>
                <th>Number of Groups</th>
            </tr>
        </thead>
        <tbody>

<?php

while ($row = mysqli_fetch_assoc($query2)) {
?>
        <tr>
            <td><?php echo $row['name']; ?></td>
            <td><?php echo $row['phone'] ?></td>
            <td><?php echo $row['number_of_groups']; ?></td>
        </tr>

<?php
  
}

?>

</tbody>

        
</table><br><br><br><br>

</div>

<?php
}
else{
?>

<div class="">
                                                        
    <div class="list-group m-b">
        <font class="list-group-item text-md text-primary" href="#">No Results found</font> 
        <span class="list-group-item text-success"><i class="ion-android-information"></i>Please check later</span> 
        <font class="list-group-item text-muted" href="#">
            The users of our system and the number of contacts for each will be displayed here.
        </font>

    </div>

</div>

<?php
}
?>

<script type="text/javascript">
    $(document).ready( function () {
        $('#groupsperusertable').DataTable( {
            // paging:false
        })
    });
</script>