<?php
/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
 *
 */
include_once ($_SERVER["DOCUMENT_ROOT"] . '/admin/resources/core/index.php');

// sanitize variables

$oldpass    = md5(mysqli_real_escape_string($conn, $_REQUEST['oldpass']));
$newpass   = md5(mysqli_real_escape_string($conn, $_REQUEST['newpass']));
$confirmpass   = md5(mysqli_real_escape_string($conn, $_REQUEST['newpassagain']));

$fetchargs = array('id'=>$account_id);

$acccount_pass = getByValue('admins', 'password', $fetchargs);

// check if password field is empty

if (empty($_POST['oldpass'])) {
	die("<font style='color:red'>Please enter your current password.</font>");
}

if ($acccount_pass != $oldpass) {
	die("<font style='color:red'>Password does not match the current password.</font>");
}

if (strlen($_POST['newpass']) < 8) {
	die("<font style='color:red'>New password must be at least 8 characters.</font>");
}

if ($newpass != $confirmpass) {
	die("<font style='color:red'>New passwords do not match.</font>");
}


$updatePassword = mysqli_query($conn, "UPDATE `admins` SET `password` = '$newpass' WHERE `email` = '{$_SESSION['loggedin']}'");

if ($updatePassword) {
	echo "1";
}
else{
	die("<font style='color:red'>Oops! An error occurred while processing your request.</font>");
}



?>
