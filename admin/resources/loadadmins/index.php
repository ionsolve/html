<?php


/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
 *  
*/


include_once($_SERVER["DOCUMENT_ROOT"] . '/admin/resources/core/index.php');

$loggedin = $_SESSION['loggedin'];

$fetch_admins = "SELECT * FROM `admins` WHERE `email` != '$loggedin' order by id";

$run_fetch_query  = mysqli_query($conn, $fetch_admins);

$number_of_admins = mysqli_num_rows($run_fetch_query);


if($number_of_admins < 1)
{
?>

<br><br><div class="">
                                                    
    <div class="list-group m-b">
        <font class="list-group-item text-md text-primary" href="#">No Admins found</font> 
        <span class="list-group-item text-success"><i class="ion-android-information"></i> Please check later.</span> 
        <font class="list-group-item text-muted" href="#">
            Admins help in the administrative functions.
        </font>

    </div>

</div>

<?php
}else{

?>

<br><br><div style="padding-bottom: 50px;" class="table-responsive">
<table id="adminstable" class="table table-bordered m-a-0">

    <thead>
        <tr class="text-primary">
            <th>Name</th>
            <th>Can Top Ups Account</th>
            <th>Can Suspend Account</th>
            <th>Can Map Sender ID</th>
            <th>Permission Level</th>
            <th>Action</th>
            <th></th>
        </tr>
	</thead>
    <tbody>
<?php

while($listadmins = mysqli_fetch_assoc($run_fetch_query)){

    $adminid           = $listadmins['id'];
    $admin_fetch_args  = array('id'=>$adminid);

?>

    <tr>
        <td><?php echo getByValue('admins', 'name', $admin_fetch_args); ?></td>
        <td><?php if(getByValue('admins', 'can_topup_accounts', $admin_fetch_args) == 1){echo "Yes";}else{echo "No";} ?></td>
        <td><?php if(getByValue('admins', 'can_suspend_accounts', $admin_fetch_args) == 1){echo "Yes";}else{echo "No";} ?></td>
        <td><?php if(getByValue('admins', 'can_map_senderids', $admin_fetch_args) == 1){echo "Yes";}else{echo "No";} ?></td>
        <td><?php if(getByValue('admins', 'permission_level', $admin_fetch_args) == 1){echo "Admin";}elseif (getByValue('admins', 'permission_level', $admin_fetch_args) == 2) {echo "Super Admin";} ?></td>
        <td>
            <?php
            if(getByValue('admins', 'permission_level', $admin_fetch_args) == 1){
            ?>
            <div class="btn-group dropdown">
                   
                <button aria-expanded="false" class="btn btn-sm white dropdown-toggle" data-toggle="dropdown"></button>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#change_priv<?php echo $adminid; ?>" data-ui-toggle-class="fade-left-big" data-ui-target="#animate">Change Priviledges</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item text-danger" data-toggle="modal" data-target="#remove_admin_<?php echo $adminid; ?>" data-ui-toggle-class="fade-left-big" data-ui-target="#animate">Remove admin</a>
                </div>
            </div>
            <?php
            }
            ?>
        </td>
        <td></td>
    </tr>


    <!-- start of modal for removing an admin -->


    <div aria-hidden="true" class="modal fade animate" data-backdrop="true" id="remove_admin_<?php echo $adminid; ?>" style="display: none;">
        <div class="modal-dialog fade-left-big" data-ui-class="fade-left-big" id="animate">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title text-primary">Remove Admin <?php echo getByValue('admins', 'name', $admin_fetch_args); ?> </h5>         
                </div>
                <div class="modal-body p-lg">
                <div class="p-a padding">
                    <p class="text-md m-t block text-muted">Do you want to proceed?</p><br>
                    <p class="text-muted"><small>Once started, you cannot undo this.</small></p><br>
                    
                </div>
                    <p id="deleteresponse<?php echo $adminid; ?>"></p>
                </div>

                <div class="modal-footer" id="admin_remove_footer">
                    <button class="btn dark-white p-x-md" id="forceclose_remove_admin" data-dismiss="modal" type="button">Cancel</button> 
                    <button class="btn danger p-x-md" value="<?php echo $adminid; ?>" id="admin_removebtn" type="button">Yes, Remove <i class="ion-ios-arrow-thin-right">&nbsp;</i></button>
                </div>
            </div>
        </div>
    </div>

    <!-- end of modal for removing an admin -->


     <!-- start of modal for changing admin priviledges -->


    <div aria-hidden="true" class="modal fade animate" data-backdrop="true" id="change_priv<?php echo $adminid; ?>" style="display: none;">
        <div class="modal-dialog fade-left-big" data-ui-class="fade-left-big" id="animate">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title text-primary">Change Priviledges </h5>         
                </div>
                <div class="modal-body p-lg">
                            
                        </div>
                            <div class="md-form-group">
                                <label class="text-primary"><strong>Can Topup Account</strong></label><br>
                                <select id="admin<?php echo $adminid; ?>" class="form-control">
                                    <option><?php if(getByValue('admins', 'can_topup_accounts', $admin_fetch_args) == 1){
                                        echo "Yes";
                                    }else{
                                        echo "No";
                                    } ?></option>
                                    <option value="1">Yes</option>
                                    <option value="0">No</option>
                                </select>

                                <br><label class="text-primary"><strong>Can suspend account</strong></label><br>
                                <select id="admin<?php echo $adminid; ?>" class="form-control">
                                    <option><?php if(getByValue('admins', 'can_suspend_accounts', $admin_fetch_args) == 1){
                                        echo "Yes";
                                    }else{
                                        echo "No";
                                    } ?></option>
                                    <option value="1">Yes</option>
                                    <option value="0">No</option>
                                </select>

                                <br><label class="text-primary"><strong>Can Map Sender ID</strong></label><br>
                                <select id="admin<?php echo $adminid; ?>" class="form-control">
                                    <option><?php if(getByValue('admins', 'can_map_senderids', $admin_fetch_args) == 1){
                                        echo "Yes";
                                    }else{
                                        echo "No";
                                    } ?></option>
                                    <option value="1">Yes</option>
                                    <option value="0">No</option>
                                </select>

                                <br><label class="text-primary"><strong>Can Change Pricing</strong></label><br>
                                <select id="admin<?php echo $adminid; ?>" class="form-control">
                                    <option><?php if(getByValue('admins', 'can_change_pricing', $admin_fetch_args) == 1){
                                        echo "Yes";
                                    }else{
                                        echo "No";
                                    } ?></option>
                                    <option value="1">Yes</option>
                                    <option value="0">No</option>
                                </select>

                                <br><label class="text-primary"><strong>Permission Level</strong></label><br>
                                <select id="admin<?php echo $adminid; ?>" class="form-control">
                                    <option><?php if(getByValue('admins', 'permission_level', $admin_fetch_args) == 1){
                                        echo "Admin";
                                    }elseif(getByValue('admins', 'permission_level', $admin_fetch_args) == 2){
                                        echo "Super Admin";
                                    } ?></option>
                                    <option value="1">Admin</option>
                                    <option value="2">Super Admin</option>
                                </select>
                            </div>
                        </div>

                <div class="modal-footer" id="ad_activate_id_footer">
                    <button class="btn dark-white p-x-md" id="forcecloseact_id_" data-dismiss="modal" type="button">Cancel</button> 
                    <button class="btn primary p-x-md" value="" id="ad_activate_idbtn" type="button">Update <i class="ion-ios-arrow-thin-right">&nbsp;</i></button>
                </div>
            </div>
        </div>
    </div>

    <!-- end of modal for changing admin priviledges -->


<?php

}
}

?>
    </tbody>

    
</table><br><br><br><br>

</div>

<script type="text/javascript">
    
    function loadAdmin() {
        $.post('../../resources/loadadmins/',
        function(data, textStatus, xhr) {
            $("#alladmins").html(data);
        });
    }


    // data tables

    $(document).ready( function () {

        $('#adminstable').DataTable( {
            paging:true
        })


        $("#admin_remove_footer #admin_removebtn").click(function(event) {
            var adminid = $(this).val();


            $.post('../../resources/removeadmin/', 
            {
                adminid:adminid
            }, 
            function(data, textStatus, xhr) {
                if (data == 1) {
                    $("#admin_remove_footer #forceclose_remove_admin").trigger('click');
                    swal('Success!', 'Admin was removed successfully.', 'success');
                    loadAdmin();
                }
                else{
                    $("#deleteresponse"+adminid).html(data);
                }
            });
        });
    });

</script>



