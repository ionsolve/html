<?php


/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
 *  
*/


include_once($_SERVER["DOCUMENT_ROOT"] . '/admin/resources/core/index.php');


if (isset($_POST['search_category'])) {
    $append = array();
    foreach($_POST as $key => $value) {
        if ($value != '0') {
            $append[] = "`".$key."`".' = '."'".$value."'";
        }
    }
    $appendquery = substr(implode(' AND ',$append), 0, -27);

    if (!empty($appendquery)) {
        $fetch_shortcodes = "SELECT * FROM `short_codes` WHERE ".$appendquery." ORDER BY `code`";
    }
    else{
        $fetch_shortcodes = "SELECT * FROM `short_codes` ORDER BY `code`";
    }
}


elseif(isset($_REQUEST['search_'])){

    $search_         = mysqli_real_escape_string($conn, $_REQUEST['search_']);

    if(!empty($search_)){
        $fetch_shortcodes  = "SELECT * FROM `short_codes` WHERE `code` like '%$search_%' order by id DESC";
    }else{
        $fetch_shortcodes  = "SELECT * FROM `short_codes` order by `id` DESC";
    }
               

}else{

    $fetch_shortcodes = "SELECT * FROM `short_codes` order by id";
}

$run_fetch_query  = mysqli_query($conn, $fetch_shortcodes);

$number_of_shortcodes = mysqli_num_rows($run_fetch_query);


if($number_of_shortcodes < 1)
{
?>

<br><br><div class="">
                                                    
    <div class="list-group m-b">
        <font class="list-group-item text-md text-primary" href="#">No Shortcodes found</font> 
        <span class="list-group-item text-success"><i class="ion-android-information"></i> Please check later.</span> 
        <font class="list-group-item text-muted" href="#">
            The Shortcodes mapped to respective clients will be displayed here.
        </font>

    </div>

</div>

<?php
}else{

?>

<div class="table-responsive">
<table id="shortcodestable" class="table table-bordered m-a-0">

    <thead>
        <tr class="text-primary">
            <th>Client Name</th>
            <th>Code</th>
            <th>Type</th>
            <th>Description</th>
            <th>Status</th>
            <th>Action</th>
            <th></th>
        </tr>
	</thead>
    <tbody>
<?php

while($listshortcodes = mysqli_fetch_assoc($run_fetch_query)){

    $shortcodes           = $listshortcodes['id'];
    $shortcodes_fetch_args  = array('id'=>$shortcodes);
    $user_details_fetch = array('id'=>$listshortcodes['parent']);

?>

    <tr>
        <td><?php echo getByValue('users', 'name', $user_details_fetch); ?></td>
        <td><?php echo getByValue('short_codes', 'code', $shortcodes_fetch_args); ?></td>
        <td><?php echo ucfirst(getByValue('short_codes', 'type', $shortcodes_fetch_args)); ?></td>
        <td><?php echo ucfirst(getByValue('short_codes', 'subscriptionOndemand', $shortcodes_fetch_args)); ?></td>
        
        <?php
            if(getByValue('short_codes', 'status', $shortcodes_fetch_args) == "active"){
        ?>
        <td class="text-success"><span class="fa fa-check-circle"></span> Active</td>
            <?php }elseif(getByValue('short_codes', 'status', $shortcodes_fetch_args) == "pending"){ ?>
        <td class="text-warning"><span class="fa fa-exclamation-circle"></span> Inactive</td>
            <?php }elseif (getByValue('short_codes', 'status', $shortcodes_fetch_args) == "suspended") {
            ?>
            <td class="text-warning"><span class="fa fa-exclamation-circle"></span> Suspended</td>
            <?php
            }
             ?>
        <td>
            <div class="btn-group dropdown">
                   
                <button aria-expanded="false" class="btn btn-sm white dropdown-toggle" data-toggle="dropdown"></button>
                <div class="dropdown-menu">
                <?php
                if(getByValue('short_codes', 'status', $shortcodes_fetch_args) == 'pending'){
                ?>
                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#activate_shortcode<?php echo $shortcodes; ?>" data-ui-toggle-class="fade-left-big" data-ui-target="#animate">Activate Shortcode</a> 
                    <div class="dropdown-divider"></div>
                <?php
                }
                if(getByValue('short_codes', 'status', $shortcodes_fetch_args) == 'active')
                {
                ?>
                    <a class="dropdown-item text-danger" data-toggle="modal" data-target="#suspend_shortcode<?php echo $shortcodes; ?>" data-ui-toggle-class="fade-left-big" data-ui-target="#animate">Suspend Shortcode</a>
                <?php
                }
                if(getByValue('short_codes', 'status', $shortcodes_fetch_args) == 'suspended')
                {
                ?>
                    <a class="dropdown-item text-primary" data-toggle="modal" data-target="#release_shortcode<?php echo $shortcodes; ?>" data-ui-toggle-class="fade-left-big" data-ui-target="#animate">Release Shortcode</a>
                <?php
                }
                ?>
                </div>
            </div>
        </td>
        <td></td>
    </tr>


    <!-- start of modal to suspend shortcode -->

    <div aria-hidden="true" class="modal fade animate" data-backdrop="true" id="suspend_shortcode<?php echo $shortcodes; ?>" style="display: none;">
        <div class="modal-dialog fade-left-big" data-ui-class="fade-left-big" id="animate">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title text-primary">Suspend <?php echo getByValue('short_codes', 'code', $shortcodes_fetch_args) ?></h5>           
                </div>

                <div class="modal-body p-lg">
                    <div class="p-a padding">
                        <p class="text-md m-t block text-muted">Do you want to proceed?</p><br>
                        <p class="text-muted"><small>The user will not be able to use this shortcode after this.</small></p><br>

                        <input type="hidden" value="<?php echo $userid; ?>" id="usertosuspend_<?php echo $userid; ?>">
                        
                    </div>
                    <p id="suspendresponse<?php echo $userid; ?>"></p>
                </div>

                    <div class="modal-footer" id="suspendshortcode_footer">
                        <button class="btn dark-white p-x-md" id="forceclose_sus_code" data-dismiss="modal" type="button">Cancel</button> 
                        <button class="btn danger p-x-md" value="<?php echo $shortcodes; ?>" id="suspendcodebtn">Yes, Suspend <i class="ion-ios-arrow-thin-right">&nbsp;</i></button>
                    </div>
            </div>
        </div>
    </div>

    <!-- End of modal to suspend shortcode -->


    <!-- start of modal to activate shortcode -->

    <div aria-hidden="true" class="modal fade animate" data-backdrop="true" id="activate_shortcode<?php echo $shortcodes; ?>" style="display: none;">
        <div class="modal-dialog fade-left-big" data-ui-class="fade-left-big" id="animate">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title text-primary">Activate <?php echo getByValue('short_codes', 'code', $shortcodes_fetch_args) ?></h5>           
                </div>

                <div class="modal-body p-lg">
                    <div class="p-a padding">
                        <p class="text-md m-t block text-muted">Do you want to proceed?</p><br>
                        <p class="text-muted"><small>After this, the user can start using the shortcode.</small></p><br>

                        <input type="hidden" value="<?php echo $userid; ?>" id="usertosuspend_<?php echo $userid; ?>">
                        
                    </div>
                    <p id="suspendresponse<?php echo $userid; ?>"></p>
                </div>

                    <div class="modal-footer" id="activateshortcode_footer">
                        <button class="btn dark-white p-x-md" id="forceclose_act_code" data-dismiss="modal" type="button">Cancel</button> 
                        <button class="btn primary p-x-md" value="<?php echo $shortcodes; ?>" id="activatecodebtn">Yes, Activate <i class="ion-ios-arrow-thin-right">&nbsp;</i></button>
                    </div>
            </div>
        </div>
    </div>

    <!-- End of modal to activate shortcode -->


    <!-- start of modal to release shortcode -->

    <div aria-hidden="true" class="modal fade animate" data-backdrop="true" id="release_shortcode<?php echo $shortcodes; ?>" style="display: none;">
        <div class="modal-dialog fade-left-big" data-ui-class="fade-left-big" id="animate">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title text-primary">Release <?php echo getByValue('short_codes', 'code', $shortcodes_fetch_args) ?></h5>           
                </div>

                <div class="modal-body p-lg">
                    <div class="p-a padding">
                        <p class="text-md m-t block text-muted">Do you want to proceed?</p><br>
                        <p class="text-muted"><small>After this, the user will be able to use the shortcode again</small></p><br>

                        <input type="hidden" value="<?php echo $userid; ?>" id="usertosuspend_<?php echo $userid; ?>">
                        
                    </div>
                    <p id="suspendresponse<?php echo $userid; ?>"></p>
                </div>

                    <div class="modal-footer" id="releaseshortcode_footer">
                        <button class="btn dark-white p-x-md" id="forceclose_rel_code" data-dismiss="modal" type="button">Cancel</button> 
                        <button class="btn primary p-x-md" value="<?php echo $shortcodes; ?>" id="releasecodebtn">Yes, Activate <i class="ion-ios-arrow-thin-right">&nbsp;</i></button>
                    </div>
            </div>
        </div>
    </div>

    <!-- End of modal to release shortcode -->


<?php

}
}

?>
    </tbody>

    
</table><br><br><br><br>

</div>

<script type="text/javascript">
    $(document).ready( function () {
        $('#shortcodestable').DataTable( {
            // paging:false
        })

        function loadAllShortCodes(){
            $.post('../../resources/loadallshortcodes/',
            function(data, textStatus, xhr) {
                $("#allshortcodesdiv").html(data);
            });
        }

        // Disable search and ordering by default
        $.extend( $.fn.dataTable.defaults, {
            searching: false,
            ordering:  false
        } );

        $("#suspendshortcode_footer #suspendcodebtn").click(function(event) {
            var shortcode_id = $(this).val();

            $.post('../../resources/suspendshortcode/', 
            {
                shortcode_id: shortcode_id
            }, 
            function(data, textStatus, xhr) {
                if (data == '1') {
                    $("#suspendshortcode_footer #forceclose_sus_code").trigger('click');
                    swal({
                      title: "Success!",
                      text: "The Shortcode has been suspended.",
                      icon: "success",
                    })
                    .then((success) => {
                      if (success) {
                        loadAllShortCodes();
                      }
                    });
                }
                else{
                    alert(data);
                }
            });
        });

        // activate shortcode

        $("#activateshortcode_footer #activatecodebtn").click(function(event) {
            var shortcode_id = $(this).val();

            $.post('../../resources/activateshortcode/', 
            {
                shortcode_id:shortcode_id
            }, 
            function(data, textStatus, xhr) {
                if (data == '1') {
                    $("#activateshortcode_footer #forceclose_act_code").trigger('click');
                    swal({
                      title: "Success!",
                      text: "The Shortcode has been activated.",
                      icon: "success",
                    })
                    .then((success) => {
                      if (success) {
                        loadAllShortCodes();
                      }
                    });
                }
                else{
                    alert(data);
                }
            });
        });


        // release a suspended shortcode

        $("#releaseshortcode_footer #releasecodebtn").click(function(event) {
            var shortcode_id = $(this).val();

            $.post('../../resources/releaseshortcode/', 
            {
                shortcode_id:shortcode_id
            }, 
            function(data, textStatus, xhr) {
                if (data == '1') {
                    $("#releasehortcode_footer #forceclose_rel_code").trigger('click');
                    swal({
                      title: "Success!",
                      text: "The Shortcode has been reactivated.",
                      icon: "success",
                    })
                    .then((success) => {
                      if (success) {
                        loadAllShortCodes();
                      }
                    });
                }
                else{
                    alert(data);
                }
            });
        });

    });
</script>



