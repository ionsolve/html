<?php


/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
 *  
*/


include_once($_SERVER["DOCUMENT_ROOT"] . '/admin/resources/core/index.php');

if (isset($_POST['search_category'])) {
    $append = array();
    foreach($_POST as $key => $value) {
        if ($value != '0') {
            $append[] = "`".$key."`".' = '."'".$value."'";
        }
    }
    $appendquery = substr(implode(' AND ',$append), 0, -27);

    if (!empty($appendquery)) {
        $fetch_ids = "SELECT * FROM `sender_ids` WHERE ".$appendquery." ORDER BY `sender_id`";
    }
    else{
        $fetch_ids = "SELECT * FROM `sender_ids` ORDER BY `sender_id`";
    }
}

elseif(isset($_REQUEST['search_'])){

    $search_         = mysqli_real_escape_string($conn, $_REQUEST['search_']);

    if(!empty($search_)){
        $fetch_ids  = "SELECT * FROM `sender_ids` WHERE `sender_id` like '%$search_%' order by id DESC";
    }else{
        $fetch_ids  = "SELECT * FROM `sender_ids` order by id DESC";
    }
               

}else{

    $fetch_ids = "SELECT * FROM `sender_ids` order by id DESC";
}

$run_fetch_query  = mysqli_query($conn, $fetch_ids);

$number_of_senders = mysqli_num_rows($run_fetch_query);


if($number_of_senders < 1)
{
?>

<br><br><div class="">
                                                    
    <div class="list-group m-b">
        <font class="list-group-item text-md text-primary" href="#">No Sender IDs found</font> 
        <span class="list-group-item text-success"><i class="ion-android-information"></i> Please check later.</span> 
        <font class="list-group-item text-muted" href="#">
            The Sender IDs mapped to respective clients will be displayed here.
        </font>

    </div>

</div>

<?php
}else{

?>

<div class="table-responsive">
<table id="senderidsstable" class="table table-bordered m-a-0">

    <thead>
        <tr class="text-primary">
            <th>Client Name</th>
            <th>Country</th>
            <th>Sender ID</th>
            <th>Status</th>
            <th>Action</th>
            <th></th>
        </tr>
	</thead>
    <tbody>
<?php

while($listsenders = mysqli_fetch_assoc($run_fetch_query)){

    $senderid           = $listsenders['id'];
    $sender_fetch_args  = array('id'=>$senderid);
    $user_details_fetch = array('id'=>$listsenders['parent']);
    $userid = $listsenders['parent'];

?>

    <tr>
        <td><?php echo getByValue('users', 'name', $user_details_fetch); ?></td>
        <td><?php echo ucfirst(getByValue('sender_ids', 'country', $sender_fetch_args)); ?></td>
        <td><?php echo getByValue('sender_ids', 'sender_id', $sender_fetch_args); ?></td>
        
        <?php
            if(getByValue('sender_ids', 'status', $sender_fetch_args) == "active"){
        ?>
        <td class="text-success"><span class="fa fa-check-circle"></span> Active</td>
            <?php }else{ ?>
        <td class="text-warning"><span class="fa fa-exclamation-circle"></span> Suspended</td>
            <?php } ?>
        <td>
            <div class="btn-group dropdown">
                   
                <button aria-expanded="false" class="btn btn-sm white dropdown-toggle" data-toggle="dropdown"></button>
                <div class="dropdown-menu">
                <?php
                if (getByValue('sender_ids', 'status', $sender_fetch_args) == "suspended") {
                ?>
                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#activateid_<?php echo $senderid; ?>" data-ui-toggle-class="fade-left-big" data-ui-target="#animate">Activate ID</a>
                    <div class="dropdown-divider"></div>
                <?php
                }
                if(getByValue('sender_ids', 'status', $sender_fetch_args) == "active")
                {
                ?>
                	<button class="dropdown-item testsender" data-id="<?php echo $senderid; ?>">Test Sender ID</button>
                    <a class="dropdown-item text-danger" data-toggle="modal" data-target="#suspendid_<?php echo $senderid; ?>" data-ui-toggle-class="fade-left-big" data-ui-target="#animate">Suspend Sender ID</a>
                <?php
                }
                ?>
                </div>
            </div>
        </td>
        <td></td>
    </tr>


    <!-- start of modal for suspending a sender id -->


    <div aria-hidden="true" class="modal fade animate" data-backdrop="true" id="suspendid_<?php echo $senderid; ?>" style="display: none;">
        <div class="modal-dialog fade-left-big" data-ui-class="fade-left-big" id="animate">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title text-primary">Suspend ID <?php echo getByValue('sender_ids', 'sender_id', $sender_fetch_args); ?></h5>         
                </div>
                <div class="modal-body p-lg">
                <div class="p-a padding">
                    <p class="text-md m-t block text-muted">Do you want to proceed?</p><br>
                    <p class="text-muted"><small>After suspending the Sender ID, the user will not be able to use it.</small></p><br>
                    
                </div>
                    <p id="deleteresponse<?php echo $contactid; ?>"></p>
                </div>

                <div class="modal-footer" id="ad_suspend_id_footer">
                    <button class="btn dark-white p-x-md" id="forceclosesus_id_" data-dismiss="modal" type="button">Cancel</button> 
                    <button class="btn danger p-x-md" value="<?php echo $senderid; ?>" id="ad_suspend_idbtn" type="button">Yes, Suspend <i class="ion-ios-arrow-thin-right">&nbsp;</i></button>
                </div>
            </div>
        </div>
    </div>

    <!-- end of modal for suspending sender id -->


     <!-- start of modal for activating a sender id -->


    <div aria-hidden="true" class="modal fade animate" data-backdrop="true" id="activateid_<?php echo $senderid; ?>" style="display: none;">
        <div class="modal-dialog fade-left-big" data-ui-class="fade-left-big" id="animate">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title text-primary">Release ID <?php echo getByValue('sender_ids', 'sender_id', $sender_fetch_args); ?></h5>         
                </div>
                <div class="modal-body p-lg">
                <div class="p-a padding">
                    <p class="text-md m-t block text-muted">Do you want to proceed?</p><br>
                    <p class="text-muted"><small>After this, the user will be able to use the Sender ID again.</small></p><br>
                    
                </div>
                    <p id="deleteresponse<?php echo $contactid; ?>"></p>
                </div>

                <div class="modal-footer" id="ad_activate_id_footer">
                    <button class="btn dark-white p-x-md" id="forcecloseact_id_" data-dismiss="modal" type="button">Cancel</button> 
                    <button class="btn primary p-x-md" value="<?php echo $senderid; ?>" id="ad_activate_idbtn" type="button">Yes, Release <i class="ion-ios-arrow-thin-right">&nbsp;</i></button>
                </div>
            </div>
        </div>
    </div>

    <!-- end of modal for activation sender id -->


<?php

}
}

?>
    </tbody>

    
</table><br><br><br><br>

</div>

<script type="text/javascript">
    $("button.testsender").click(function(event) {
        var sender_id = $(this).data('id')
        
        $.post('../../resources/testsender/', 
        {
            sender_id:sender_id
        }, 
        function(data, textStatus, xhr) {
            if (data == 1) {
                swal('Success!', 'Message has been sent to admin phone numbers', 'success');
            }
            else{
                swal('Oops!', data, 'error');
            }
        });
    });
</script>

<script type="text/javascript">
    // data tables

    $(document).ready( function () {
        $('#senderidsstable').DataTable( {
            // paging:false
        })

        function loadSenderIDs() {
            $.post('../../resources/loadsenderids/', 
            function(data, textStatus, xhr) {
                $("#allsenderiddiv").html(data);
            });
        }


    // suspend sender id

    $("#ad_suspend_id_footer #ad_suspend_idbtn").click(function(event) {
        var senderid = $(this).val();
        
        $.post('../../resources/suspendid/', 
        {
            senderid:senderid
        }, 
        function(data, textStatus, xhr) {
            if (data == '1') {
                $("#ad_suspend_id_footer #forceclosesus_id_").trigger('click');
                swal({
                  title: "Success!",
                  text: "The Sender ID has been placed under risk hold.",
                  icon: "success",
                })
                .then((success) => {
                  if (success) {
                    loadSenderIDs();
                  }
                });
            }
            else{
                alert(data);
            }
        });
    });


    // activate a sender id
    $("#ad_activate_id_footer #ad_activate_idbtn").click(function(event) {
        var senderid = $(this).val();
        
        $.post('../../resources/activateid/', 
        {
            senderid:senderid
        }, 
        function(data, textStatus, xhr) {
            if (data == '1') {
                $("#ad_activate_id_footer #forcecloseact_id_").trigger('click');
                swal({
                  title: "Success!",
                  text: "The Sender ID has been Activated.",
                  icon: "success",
                })
                .then((success) => {
                  if (success) {
                    loadSenderIDs();
                  }
                });
            }
            else{
                alert(data);
            }
        });
    });

    });

</script>



