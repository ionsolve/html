<?php


/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
 *  
*/


include_once($_SERVER["DOCUMENT_ROOT"] . '/admin/resources/core/index.php');


if (isset($_POST['search_category'])) {
    $append = array();
    foreach($_POST as $key => $value) {
        if ($value != '0') {
            $append[] = "`".$key."`".' = '."'".$value."'";
        }
    }
    $appendquery = substr(implode(' AND ',$append), 0, -27);

    if (!empty($appendquery)) {
        $getUsers = "SELECT * FROM `users` WHERE ".$appendquery." ORDER BY name";
    }
    else{
        $getUsers = "SELECT * FROM `users` ORDER BY `name`";
    }
}



elseif(isset($_REQUEST['search_'])){

    $search_         = mysqli_real_escape_string($conn, $_REQUEST['search_']);

    if(!empty($search_)){
        $getUsers  = "SELECT * FROM `users` WHERE `name` like '%$search_%' order by `name`";
    }else{
        $getUsers  = "SELECT * FROM `users` order by `name`";
    }
               

}else{

    $getUsers = "SELECT * FROM `users` order by name";
}

$run_fetch_query    = mysqli_query($conn, $getUsers);

$number_of_users = mysqli_num_rows($run_fetch_query);

if($number_of_users < 1)
{
?>

<br><br><div class="">
                                                    
    <div class="list-group m-b">
        <font class="list-group-item text-md text-primary" href="#">No users to display</font> 
        <font class="list-group-item text-success" href="#"><i class="ion-information-circled"></i> 
            Please check later
        </font> 
        <font class="list-group-item text-muted" href="#">
            All the users that are using our system will be displayed here.
        </font>
    </div>

</div>

<?php
}else{

?>

<div style="padding-bottom: 80px;" class="table-responsive">
<table id="userstable" class="table table-bordered m-a-0">
    <thead>
        <tr class="text-primary">
            
            <th>Name</th>
            <th>Status</th>
            <th>Country</th>
            <!-- <th>Groups</th> -->
            <th>Phone Number</th>
            <th>Last Active</th>
            <th>Action</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
<?php

while($listusers = mysqli_fetch_array($run_fetch_query)){

    $userid            = $listusers['id'];
    $users_fetch_args  = array('id'=>$userid);
    $user_currency     = $listusers['account_currency'];

    $username = getByValue('users', 'username', $users_fetch_args);
?>
        <tr>
            <td><?php echo getByValue('users', 'name', $users_fetch_args); ?></td>
            <?php 
            if(getByValue('users', 'status', $users_fetch_args) == 'pending'){

                echo "<td class='text-warning'><span class='fa fa-exclamation-circle'></span>"." ".ucfirst(getByValue('users', 'status', $users_fetch_args))."</td>";
            }
            elseif (getByValue('users', 'status', $users_fetch_args) == 'active') {
                echo "<td class='text-success'><span class='fa fa-check-circle'></span>"." ".ucfirst(getByValue('users', 'status', $users_fetch_args))."</td>";
            }
            elseif (getByValue('users', 'status', $users_fetch_args) == 'loadcontacts') {
                echo "<td>Loading Contacts</td>";
            }
            elseif (getByValue('users', 'status', $users_fetch_args) == 'suspended') {
                echo "<td class='text-danger'><span class='fa fa-exclamation-circle'></span>"." ".ucfirst(getByValue('users', 'status', $users_fetch_args))."</td>";
            }
            ?>
            <td><?php echo getCountryWithCurrencyCode($user_currency); ?></td>
            <td><?php echo getByValue('users', 'phone', $users_fetch_args); ?></td>
            <td><?php echo getByValue('users', 'last_used', $users_fetch_args); ?></td>
            <td>
                <div class="btn-group dropdown">
                   
                    <button aria-expanded="false" class="btn btn-sm white dropdown-toggle" data-toggle="dropdown"></button>
                    <div class="dropdown-menu"> 
                        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#message<?php echo $userid; ?>" data-ui-toggle-class="fade-left-big" data-ui-target="#animate">Send Message</a>
                        <?php
                        if(getByValue('users', 'status', $users_fetch_args) == 'pending'){
                        ?>
                        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#admin_resend_code<?php echo $userid; ?>" data-ui-toggle-class="fade-left-big" data-ui-target="#animate">Resend Code</a> 
                        <?php
                        }
                        if($can_map_senderid == '1'){
                        ?>
                        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#map_id<?php echo $userid; ?>" data-ui-toggle-class="fade-left-big" data-ui-target="#animate">Map Sender ID</a>
                        <?php
                        }
                        ?>
                        <a class="dropdown-item" href="../../dashboard/followups/?user=<?php echo $username; ?>">View Follow Ups</a>
                        <?php
                        if(getByValue('users', 'status', $users_fetch_args) != 'pending'){
                        ?>
                        <button class="dropdown-item" id="loginasuserlink<?php echo $userid; ?>" value="<?php echo $userid; ?>" >Login as User</button>
                        <?php
                        }
                        if($can_suspend_accounts == '1'){
                        ?>
                        <div class="dropdown-divider"></div>
                        <?php if($listusers['status'] == 'active'){ ?>
                        <a class="dropdown-item text-danger" data-toggle="modal" data-target="#suspend_account_<?php echo $userid; ?>" data-ui-toggle-class="fade-left-big" data-ui-target="#animate">Suspend Account</a>
                        <?php
                        }
                        elseif ($listusers['status'] == 'suspended') {
                        ?>
                        <a class="dropdown-item text-primary" data-toggle="modal" data-target="#release_account_<?php echo $userid; ?>" data-ui-toggle-class="fade-left-big" data-ui-target="#animate">Release Account</a>
                        <?php
                        }
                        }
                        ?>
                    </div>
                </div>
            </td>
            <td></td>
        </tr>

        
        <!-- Modal to map sender ids -->

        <div aria-hidden="true" class="modal fade animate" data-backdrop="true" id="map_id<?php echo $userid; ?>" style="display: none;">
            <div class="modal-dialog fade-left-big" data-ui-class="fade-left-big" id="animate">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title text-primary">Map ID for <?php echo getByValue('users', 'name', $users_fetch_args) ?></h5>           
                    </div>

                        <div class="modal-body p-lg">

                        <div style="color: red;" id="mapid_error_<?php echo $userid; ?>">
                            
                        </div>
                            <div class="md-form-group">
                                <select id="country<?php echo $userid; ?>" class="form-control">
                                    <option value="0">Select Country</option>
                                    <option>Kenya</option>
                                    <option>Uganda</option>
                                    <option>Malawi</option>
                                    <option>Nigeria</option>
                                    <option>Others</option>
                                </select>
                            </div>

                            <div class="md-form-group">
                                <input class="md-input" id="senderinput_<?php echo $userid; ?>" placeholder="Enter the Sender ID"><label></label>
                            </div>
                        </div>

                        <div class="modal-footer" id="mapfooter">
                            <button class="btn dark-white p-x-md" id="forcecloseedit" data-dismiss="modal" type="button">Cancel</button> 
                            <button class="btn primary p-x-md" value="<?php echo $userid; ?>" id="mapbtn">Map <i class="ion-ios-arrow-thin-right">&nbsp;</i></button>
                        </div>
                </div>
            </div>
        </div>


        <!-- End of modal to map sender ids -->


        <!-- start of modal for resending the activation code -->


         <div aria-hidden="true" class="modal fade animate" data-backdrop="true" id="admin_resend_code<?php echo $userid; ?>" style="display: none;">
            <div class="modal-dialog fade-left-big" data-ui-class="fade-left-big" id="animate">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title text-primary">Resend Code To <?php echo getByValue('users', 'name', $users_fetch_args); ?></h5>         
                    </div>
                    <div class="modal-body p-lg">
                    <div class="p-a padding">
                        <p class="text-md m-t block text-muted">Do you want to proceed?</p><br>
                        <p class="text-muted"><small>A confirmation code will be sent to the user.</small></p><br>
                        
                    </div>
                        <p id="deleteresponse<?php echo $userid; ?>"></p>
                    </div>

                    <div class="modal-footer" id="ad_resendcode_footer">
                        <button class="btn dark-white p-x-md" id="forcecloseresend" data-dismiss="modal" type="button">Cancel</button> 
                        <button class="btn primary p-x-md" value="<?php echo $userid; ?>" id="ad_resendcodebtn" type="button">Yes, Resend <i class="ion-ios-arrow-thin-right">&nbsp;</i></button>
                    </div>
                </div>
            </div>
        </div>

        <!-- end of modal for resending activation code -->


        <!-- start of modal to release an account -->

        <div aria-hidden="true" class="modal fade animate" data-backdrop="true" id="release_account_<?php echo $userid; ?>" style="display: none;">
            <div class="modal-dialog fade-left-big" data-ui-class="fade-left-big" id="animate">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title text-primary">Release Account <?php echo getByValue('users', 'name', $users_fetch_args); ?></h5>         
                    </div>
                    <div class="modal-body p-lg">
                    <div class="p-a padding">
                        <p class="text-md m-t block text-muted">Do you want to proceed?</p><br>
                        <p class="text-muted"><small>After this operation, the user will be able to log in to the account, again.</small></p><br>
                        
                    </div>
                        <p id="activateresponse<?php echo $userid; ?>"></p>
                    </div>

                    <div class="modal-footer" id="ad_activate_acc_footer">
                        <button class="btn dark-white p-x-md" id="forcecloseactivate_acc" data-dismiss="modal" type="button">Cancel</button> 
                        <button class="btn primary p-x-md" value="<?php echo $userid; ?>" id="ad_activ_accbtn" type="button">Yes, Activate <i class="ion-ios-arrow-thin-right">&nbsp;</i></button>
                    </div>
                </div>
            </div>
        </div>

        <!-- end of modal to release an account -->


        <!-- start of modal to suspend user account -->

        <div aria-hidden="true" class="modal fade animate" data-backdrop="true" id="suspend_account_<?php echo $userid; ?>" style="display: none;">
            <div class="modal-dialog fade-left-big" data-ui-class="fade-left-big" id="animate">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title text-primary">Suspend <?php echo getByValue('users', 'name', $users_fetch_args) ?></h5>           
                    </div>

                    <div class="modal-body p-lg">
                        <div class="p-a padding">
                            <p class="text-md m-t block text-muted">Do you want to proceed?</p><br>
                            <p class="text-muted"><small>This user will not be able to login to his dashboard.</small></p><br>

                            <input type="hidden" value="<?php echo $userid; ?>" id="usertosuspend_<?php echo $userid; ?>">
                            
                        </div>
                        <p id="suspendresponse<?php echo $userid; ?>"></p>
                    </div>

                        <div class="modal-footer" id="suspendfooter">
                            <button class="btn dark-white p-x-md" id="forceclosesus" data-dismiss="modal" type="button">Cancel</button> 
                            <button class="btn primary p-x-md" value="<?php echo $userid; ?>" id="suspendbtn">Yes, Suspend <i class="ion-ios-arrow-thin-right">&nbsp;</i></button>
                        </div>
                </div>
            </div>
        </div>

        <!-- create the session so as to login as a user -->

        <script type="text/javascript">
            $(document).ready(function() {
                $("#loginasuserlink<?php echo $userid; ?>").click(function(event) {
                    var userid = $(this).val();
                    

                    $.post('../../resources/mimicsession/', 
                    {
                        userid:userid
                    }, 
                    function(data, textStatus, xhr) {
                        if (data == 1) {
                            window.location.href = '../../../dashboard/';
                        }
                        else{
                            window.reload();
                        }
                    });
                });
            });
        </script>

        <!-- End of modal to suspend user account -->
<?php

}

?>
    </tbody>

    
</table><br><br>

</div>

<?php  
} 
?>
<script type="text/javascript">
    $(document).ready( function () {
        $('#userstable').DataTable( {
            // paging:false
        })
    });
</script>

<script>   
    $(document.body).on("keyup","#textsingle textarea",function()
    {
        var datatag             = $(this).attr('data');
        var text_length         = $(this).val().length;
        $('#countfeed'+datatag).html('<font class="text-muted">'+text_length + ' characters</font><br><small class="text-info">Note: 160 characters make up one message</small>');
    })
</script>

<script>

    function loadcontacts()
    {
        $.post('../../resources/loadcontacts/',
        function(contresp)
        {
            $('#usersdiv').html(contresp);
        });
    }

    // map sender id

    $("#mapfooter #mapbtn").click(function() {
        $(this).html('<i class="fa ion-load-c fa-spin"></i>');
        var user_id = $(this).attr("value");
        var senderid = $("#senderinput_"+user_id).val();
        var country  = $("#country"+user_id).val();
        $.post('../../resources/mapsender/',
        {
            senderid:senderid,
            user_id:user_id,
            country:country
        }, 
        function(data, textStatus, xhr) {
            if (data == '1') {
                $("#mapfooter #mapbtn").html('Map <i class="ion-ios-arrow-thin-right">&nbsp;</i>');
                $("#mapfooter #forceclosesus").trigger('click');
                swal({
                  title: "Success!",
                  text: "The Sender ID has been mapped.",
                  icon: "success",
                })
                .then((success) => {
                  if (success) {
                    window.location.href = "../../dashboard/senderids/";
                  }
                });
            }
            else{
                $("#mapfooter #mapbtn").html('Map <i class="ion-ios-arrow-thin-right">&nbsp;</i>');
                $("div#mapid_error_"+user_id).html(data);
            }
        });
    });


    // load users function

    function loadAllUsers() {
        $.post('../../resources/loadusers/',
        function(data, textStatus, xhr) {
            $('#usersdiv').html(data);
        });
    }


    // suspend user account

    $("#suspendfooter #suspendbtn").click(function(event) {
        $(this).html('<i class="fa ion-load-c fa-spin"></i>');
        var userid = $(this).val();
        

        $.post('../../resources/suspendaccount/',
        {
            userid: userid
        }, 
        function(data, textStatus, xhr) {
        if (data == 1) {
            $("#suspendfooter #suspendbtn").html('Suspend <i class="ion-ios-arrow-thin-right">&nbsp;</i>');
            $("#suspendfooter #forceclosesus").trigger('click');
            swal({
              title: "Success!",
              text: "The account has been suspended.",
              icon: "success",
            })
            .then((success) => {
              if (success) {
                loadAllUsers();
              }
            });
        }
        else{
            $("#suspendfooter #suspendbtn").html('Suspend <i class="ion-ios-arrow-thin-right">&nbsp;</i>');
            $("#suspendresponse"+userid).html(data);
        }
        });
    });


    $("#ad_activate_acc_footer #ad_activ_accbtn").click(function(event) {
        $(this).html('<i class="fa ion-load-c fa-spin"></i>');
        var userid = $(this).val();

        $.post('../../resources/reactivateaccount/',
        {
            userid: userid
        }, 
        function(data, textStatus, xhr) {
        if (data == 1) {
            $("#ad_activate_acc_footer #ad_activ_accbtn").html('Yes, Release <i class="ion-ios-arrow-thin-right">&nbsp;</i>');
            $("#ad_activate_acc_footer #forcecloseactivate_acc").trigger('click');
            swal({
              title: "Success!",
              text: "The account has been reactivated.",
              icon: "success",
            })
            .then((success) => {
              if (success) {
                loadAllUsers();
                // window.location.href = "../../dashboard/users/"
              }
            });
        }
        else{
            $("#ad_activate_acc_footer #ad_activ_accbtn").html('Yes, Release <i class="ion-ios-arrow-thin-right">&nbsp;</i>');
            $("#activateresponse"+userid).html(data);
        }
        });
    });



    // resend confirmation code

    $("#ad_resendcode_footer #ad_resendcodebtn").click(function(event) {
        $(this).html("Please Wait <i class='fa ion-load-c fa-spin'></i>");
        $(this).css('disabled', 'disabled');

        var userid = $(this).val();

        $.post('../../resources/resendcode/',
        {
            userid:userid
        },
        function(data, textStatus, xhr) {
            if (data == '1') {
                $("#ad_resendcode_footer #ad_resendcodebtn").html("Resend Code <i class='ion-ios-arrow-thin-right'>&nbsp;</i>");
                $("#ad_resendcode_footer #ad_resendcodebtn").css('disabled', '');
                $("#ad_resendcode_footer #forcecloseresend").trigger("click");
                swal({
                  title: "Success!",
                  text: "The verification code has been resent.",
                  icon: "success",
                })
                .then((success) => {
                  if (success) {
                    loadAllUsers();
                  }
                });
            }
            else{
                $("#ad_resendcode_footer #ad_resendcodebtn").html("Resend Code <i class='ion-ios-arrow-thin-right'>&nbsp;</i>");
                $("#ad_resendcode_footer #ad_resendcodebtn").css('disabled', '');
                alert(data);
            }
        });
    });


    // send message

    $('#messagess #btnmessage').click(function()
    {
        var messageid     = $(this).attr('value');
        var messagetosend = $('#messagemessage'+messageid).val();
        var alongsender   = $('#alongsender'+messageid).val();
        var single        = 'single';

        $('#messagess #btnmessage').html('<i class="fa ion-load-c fa-spin"></i>');
        $('#messagess #btnmessage').css('disabled','1');

        $.post("../../system/preparenums/",
		{
            messageid:messageid,
            messagetosend:messagetosend,
            alongsender:alongsender,
            single:single
		},
		function(messageresponse)
		{
			$('#messagessresponse'+messageid).html(messageresponse);
			$('#messagess #btnmessage').css('disabled','0');
			$('#messagess #btnmessage').html('Send <i class="ion-ios-arrow-thin-right">&nbsp;</i>');
		})
        
    })
  
</script>

