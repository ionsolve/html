<?php


/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
 *  
*/


include_once($_SERVER["DOCUMENT_ROOT"] . '/admin/resources/core/index.php');


if (isset($_POST['search_'])) {
	if (!empty($_POST['search_'])) {
		$search_ = mysqli_real_escape_string($conn, $_POST['search_']);
		$sql = "SELECT * FROM users WHERE `name` LIKE '%$search_%'";
	}
	else{
		$sql = "SELECT * FROM `users` WHERE last_used >= DATE_ADD(NOW(), INTERVAL -1 WEEK)";
	}
}

else{

    $sql = "SELECT * FROM `users` ORDER BY name";
}

if (isset($_POST['last_used'])) {
	// do fun stuff
	if ($_POST['last_used'] == '0') {
		$sql = "SELECT * FROM `users` ORDER BY name";
	}
	elseif ($_POST['last_used'] == '1') {
		$sql = "SELECT * FROM `users` WHERE `last_used` BETWEEN DATE_ADD(NOW(), INTERVAL -1 WEEK) AND CURDATE() ORDER BY name";
	}
	elseif ($_POST['last_used'] == '2') {
		$sql = "SELECT * FROM `users` WHERE `last_used` BETWEEN DATE_ADD(NOW(), INTERVAL -2 WEEK) AND CURDATE() ORDER BY name";
	}
	elseif ($_POST['last_used'] == '3') {
		$sql = "SELECT * FROM `users` WHERE `last_used` BETWEEN DATE_ADD(NOW(), INTERVAL -1 MONTH) AND CURDATE() ORDER BY name";
	}
	elseif ($_POST['last_used'] == '4') {
		$sql = "SELECT * FROM `users` WHERE `last_used` <= DATE_SUB(CURDATE(), INTERVAL 1 MONTH) ORDER BY `name`";
	}
}

$run_fetch_query    = mysqli_query($conn, $sql);

$number_of_dormant = mysqli_num_rows($run_fetch_query);

if ($number_of_dormant < 1) {
?>

<br><br><div class="">
                                                    
    <div class="list-group m-b">
        <font class="list-group-item text-md text-primary" href="#">No Results Found</font> 
        <font class="list-group-item text-success" href="#"><i class="ion-information-circled"></i> 
            Please check later.
        </font> 
        <font class="list-group-item text-muted" href="#">
            All the users that have not been active for a while will be displayed here.
        </font>
    </div>

</div>

<?php
}

else{

?>

<div class="table-responsive">
<table id="useractivity_tbl" class="table table-bordered m-a-0">
    <thead>
        <tr class="text-primary">
            
            <th>Name</th>
            <th>Stage</th>
            <th>Country</th>
            <!-- <th>Groups</th> -->
            <th>Phone Number</th>
            <th>Number of messages</th>
            <th>Last Active</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
<?php

while($listdormant = mysqli_fetch_array($run_fetch_query)){

    $user_currency     = $listdormant['account_currency'];

    $message_args = array('parent'=>$listdormant['id']);

?>
        <tr>
            <td><?php echo $listdormant['name']; ?></td>
            <?php 
            if($listdormant['status'] == 'pending'){

                echo "<td class='text-warning'><span class='fa fa-exclamation-circle'></span> Pending</td>";
            }
            elseif ($listdormant['status'] == 'active') {
                echo "<td class='text-success'><span class='fa fa-check-circle'></span> Active</td>";
            }
            elseif ($listdormant['status'] == 'loadcontacts') {
                echo "<td>Loading Contacts</td>";
            }
            elseif ($listdormant['status'] == 'sendmessage') {
                echo "<td>Sending Message</td>";
            }
            ?>
            <td><?php echo getCountryWithCurrencyCode($user_currency); ?></td>
            <td><?php echo $listdormant['phone']; ?></td>
            <td><?php echo returnCountWithCondition('messages', $message_args); ?></td>
            <td><?php echo $listdormant['last_used']; ?></td>
            <td></td>
        </tr>

<?php

}

?>
    </tbody>

    
</table><br><br>

</div>

<?php 

}

?>