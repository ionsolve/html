<?php
/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
 *  
*/

/**
* class to login the admin
*/
session_start();
class LoginAdmin
{
	private $ad_email_, $ad_password_;
	
	function __construct()
	{
		require_once $_SERVER["DOCUMENT_ROOT"] . '/configs/database.php';

		if(isset($_POST['ad_email_']) && isset($_POST['ad_password_'])){
			$this->ad_email_ = $_POST['ad_email_'];
			$this->ad_password_ = $_POST['ad_password_'];

			// hash the password
			$enc_password = md5($this->ad_password_);

			if (empty($this->ad_email_) OR empty($this->ad_password_)) {
				die("<span style='color: red;'>Please fill all fields</span>");
			}

			// validate admin email
	        elseif(!filter_var($this->ad_email_, FILTER_VALIDATE_EMAIL)) 
			{
			    die('<span style="color:red">Please enter a valid email address.</span>');
			}

			// check the length of the password

			elseif (strlen($this->ad_password_) < 8) {
				die('<span style="color:red">Password must be at least 8 characters.</span>');
			}

			// check existence

			$query = mysqli_query($conn, "SELECT * FROM admins WHERE email = '$this->ad_email_' AND password = '$enc_password'");

			if (mysqli_num_rows($query) > 0) {
				$_SESSION['loggedin'] = $this->ad_email_;
				echo "1";
			}
			else{
				die("<span style='color:red;'>Incorrect username or password.</span>");
			}

		}
	}
}


// create an object of the login class
$newUser = new LoginAdmin();

?>