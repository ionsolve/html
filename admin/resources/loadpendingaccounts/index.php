<?php


/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
 *  
*/


include_once($_SERVER["DOCUMENT_ROOT"] . '/admin/resources/core/index.php');


if(isset($_REQUEST['search_'])){

    $search_         = mysqli_real_escape_string($conn, $_REQUEST['search_']);

    if(!empty($search_)){
        $getUsers  = "SELECT * FROM `users` WHERE `status` = 'pending' AND name like '%$search_%' order by name";
    }else{
        $getUsers  = "SELECT * FROM `users` WHERE `status` = 'pending' order by name";
    }
               

}else{

    $getUsers = "SELECT * FROM `users` WHERE `status` = 'pending' order by name";
}


if (isset($_POST['date_filter'])) {
    // do fun stuff
    if ($_POST['date_filter'] == '0') {
        $getUsers = "SELECT * FROM `users` ORDER BY name";
    }
    elseif ($_POST['date_filter'] == '1') {
        $getUsers = "SELECT * FROM `users` WHERE `status` = 'pending' AND `date_created` BETWEEN DATE_ADD(NOW(), INTERVAL -1 WEEK) AND NOW() ORDER BY name";
    }
    elseif ($_POST['date_filter'] == '2') {
        $getUsers = "SELECT * FROM `users` WHERE `status` = 'pending' AND `date_created` BETWEEN DATE_ADD(NOW(), INTERVAL -2 WEEK) AND NOW() ORDER BY name";
    }
    elseif ($_POST['date_filter'] == '3') {
        $getUsers = "SELECT * FROM `users` WHERE `status` = 'pending' AND `date_created` BETWEEN DATE_ADD(NOW(), INTERVAL -1 MONTH) AND NOW() ORDER BY name";
    }
    elseif ($_POST['date_filter'] == '4') {
        $getUsers = "SELECT * FROM `users` WHERE `status` = 'pending' AND `date_created` <= DATE_SUB(CURDATE(), INTERVAL 1 MONTH) ORDER BY `name`";
    }
}

$run_fetch_query    = mysqli_query($conn, $getUsers);

$number_of_users = mysqli_num_rows($run_fetch_query);

if($number_of_users < 1)
{
?>

<br><br><div class="">
                                                    
    <div class="list-group m-b">
        <font class="list-group-item text-md text-primary" href="#">No Results Found</font> 
        <font class="list-group-item text-success" href="#"><i class="ion-information-circled"></i> 
            Please check later
        </font> 
        <font class="list-group-item text-muted" href="#">
            Users who have not confirmed their accounts will appear here.
        </font>
    </div>

</div>

<?php
}else{

?>

<div class="table-responsive">
<table id="pendingaccountstable" class="table table-bordered m-a-0">
    <thead>
        <tr class="text-primary">
            
            <th>Name</th>
            <th>Status</th>
            <th>Country</th>
            <!-- <th>Groups</th> -->
            <th>Phone Number</th>
            <th>Date Created</th>
            <th>Action</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
<?php

while($listusers = mysqli_fetch_array($run_fetch_query)){

    $userid            = $listusers['id'];
    $users_fetch_args  = array('id'=>$userid, 'status'=>'pending');
    $user_currency     = $listusers['account_currency'];

?>
        <tr>
            <td><?php echo getByValue('users', 'name', $users_fetch_args); ?></td>
            <td class="text-danger"><span class="fa fa-exclamation-circle"></span><?php echo " ".ucfirst(getByValue('users', 'status', $users_fetch_args)); ?></td>
            <td><?php echo getCountryWithCurrencyCode($user_currency); ?></td>
            <td><?php echo getByValue('users', 'phone', $users_fetch_args); ?></td>
            <td><?php echo explode(" ",$listusers['date_created'])[0]; ?></td>
            <td>
                <div class="btn-group dropdown">
                   
                    <button aria-expanded="false" class="btn btn-sm white dropdown-toggle" data-toggle="dropdown"></button>
                    <div class="dropdown-menu"> 
                        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#resendcode<?php echo $userid; ?>" data-ui-toggle-class="fade-left-big" data-ui-target="#animate">Resend Code</a>
                    </div>
                </div>
            </td>
            <td></td>
        </tr>

        <!-- Resend code to this user-->

        <div aria-hidden="true" class="modal fade animate" data-backdrop="true" id="resendcode<?php echo $userid; ?>" style="display: none;">
			<div class="modal-dialog fade-left-big" data-ui-class="fade-left-big" id="animate">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title text-primary">Resend Code to <?php echo getByValue('users', 'name', $users_fetch_args); ?></h5>			
					</div>
					<div class="modal-body p-lg">
                    <div class="p-a padding">
                        <p class="text-md m-t block text-muted">Do you want to proceed?</p><br>
                        <p class="text-muted"><small>A confirmation code will be sent to the user.</small></p><br>
                        
                    </div>
                        <p id="resendresponse<?php echo $userid; ?>"></p>
                    </div>

                    <div class="modal-footer" id="ad_resendcode_footer">
                        <button class="btn dark-white p-x-md" id="forcecloseresend" data-dismiss="modal" type="button">Cancel</button> 
                        <button class="btn primary p-x-md" value="<?php echo $userid; ?>" id="ad_resendcodebtn" type="button">Yes, Resend <i class="ion-ios-arrow-thin-right">&nbsp;</i></button>
                    </div>
				</div>
			</div>
		</div>
<?php

}

?>
    </tbody>

    
</table><br><br>

</div>

<?php  
} 

?>

<script>   
    $(document.body).on("keyup","#textsingle textarea",function()
    {
        var datatag             = $(this).attr('data');
        var text_length         = $(this).val().length;
        $('#countfeed'+datatag).html('<font class="text-muted">'+text_length + ' characters</font><br><small class="text-info">Note: 160 characters make up one message</small>');
    })
</script>

<script>
    function loadPending()
    {
        $.post('../../resources/loadpendingaccounts/',
        function(userresp)
        {
            $('#pendingusersdiv').html(userresp);
        });
    }

    $("#ad_resendcode_footer #ad_resendcodebtn").click(function(event) {
        $(this).html('<i class="fa ion-load-c fa-spin"></i>');
        $(this).attr('disabled', 'disabled');

        var userid = $(this).val();


        $.post('../../resources/resendcode/', 
        {
            userid:userid
        }, 
        function(data, textStatus, xhr) {
            if (data == 1) {
                $("#ad_resendcode_footer #ad_resendcodebtn").html('Resend Code <i class="ion-ios-arrow-thin-right">&nbsp;</i>');
                $("#ad_resendcode_footer #ad_resendcodebtn").css('disabled', false);
                $("#ad_resendcode_footer #forcecloseresend").trigger('click');
                loadPending();
                swal('Success!', 'Activation code has been resent to the user.', 'success');
            }else{
                $("#ad_resendcode_footer #ad_resendcodebtn").html('Resend Code <i class="ion-ios-arrow-thin-right">&nbsp;</i>');
                $("#ad_resendcode_footer #ad_resendcodebtn").prop('disabled', false);
                $("#resendresponse"+userid).html('<font style="color:red;">'+data+'</font>');
            }
        });
    });


    $(document).ready( function () {
        $('#pendingaccountstable').DataTable( {
            // paging:false
        })
    });
  
</script>

