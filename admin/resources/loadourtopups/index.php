<?php


/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
 *  
*/


include_once($_SERVER["DOCUMENT_ROOT"] . '/admin/resources/core/index.php');

if (isset($_POST['search_category'])) {
    $append = array();
    foreach($_POST as $key => $value) {
        if ($value != '0') {
            $append[] = "`".$key."`".' = '."'".$value."'";
        }
    }
    $appendquery = substr(implode(' AND ',$append), 0, -27);

    if (!empty($appendquery)) {
        $fetch_payments = "SELECT * FROM `allpayments` WHERE ".$appendquery." ORDER BY `id`";
    }
    else{
        $fetch_payments = "SELECT * FROM `allpayments` ORDER BY `id`";
    }
}

elseif(isset($_REQUEST['id_search_'])){

    $search_         = mysqli_real_escape_string($conn, $_REQUEST['id_search_']);

    if(!empty($search_)){
        $fetch_payments  = "SELECT * FROM `allpayments` WHERE `code` like '%$search_%' order by id";
    }else{
        $fetch_payments  = "SELECT * FROM `allpayments` order by id DESC";
    }
               

}else{

    $fetch_payments = "SELECT * FROM `allpayments` order by id";
}

$run_fetch_query  = mysqli_query($conn, $fetch_payments);

$number_of_transactions = mysqli_num_rows($run_fetch_query);

if($number_of_transactions < 1)
{
?>

<br><br><div class="">
                                                    
    <div class="list-group m-b">
        <font class="list-group-item text-md text-primary" href="#">No Account Top Ups have been made yet.</font> 
        <span class="list-group-item text-success"><i class="ion-android-information"></i> Please top up the account and check again.</span> 
        <font class="list-group-item text-muted" href="#">
            All the account top ups will be displayed here.
        </font>

    </div>

</div>

<?php
}else{

?>

<div class="table-responsive">
<table class="table table-bordered m-a-0">

    <thead>
        <tr class="text-primary">
            <th>Amount</th>
            <th>MPESA Code</th>
            <th>Phone Number</th>
            <th>Date of Transaction</th>
            <th>Status</th>
            <!-- <th>Action</th> -->
            <th></th>
        </tr>
	</thead>
    <tbody>
<?php

while($listtransactions = mysqli_fetch_assoc($run_fetch_query)){

    $transactionids           = $listtransactions['id'];
    $tran_fetch_args  = array('id'=>$transactionids);

?>

    <tr>
        <td><?php echo getByValue('allpayments', 'amount', $tran_fetch_args); ?></td>
        <td><?php echo getByValue('allpayments', 'code', $tran_fetch_args); ?></td>
        <td><?php echo getByValue('allpayments', 'phonenumber', $tran_fetch_args); ?></td>
        <td><?php echo explode(" ", getByValue('allpayments', 'humandate', $tran_fetch_args))[0]; ?></td>
        
        <?php
            if(getByValue('allpayments', 'status', $tran_fetch_args) == "Success"){
        ?>
        <td class="text-success"><span class="fa fa-check-circle"></span> Success</td>
            <?php }else{ ?>
        <td class="text-warning"><span class="fa fa-exclamation-circle"></span> Failed</td>
            <?php } ?>
        <!-- <td>
            <div class="btn-group dropdown">
                   
                <button aria-expanded="false" class="btn btn-sm white dropdown-toggle" data-toggle="dropdown"></button>
                <div class="dropdown-menu"> 
                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#message<?php echo $userid; ?>" data-ui-toggle-class="fade-left-big" data-ui-target="#animate">Activate Sender ID</a> 
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item text-danger" data-toggle="modal" data-target="#delete<?php echo $userid; ?>" data-ui-toggle-class="fade-left-big" data-ui-target="#animate">Suspend Sender ID</a>
                </div>
            </div>
        </td> -->
        <td></td>
    </tr>


<?php

}
}


?>
    </tbody>

    
</table><br><br><br><br>

</div>

<?php
$AllTransactions      = returnAllNumRows('allpayments');
echo $AllTransactions;
// do pagination
$no_of_pages = ceil($AllTransactions/20);

if($no_of_pages >= 1){

?>

<div class="padding">
        <div class="btn-group" id="ourtopupspag">
            <button type="button" value="<?php echo $i; ?>" class="btn-sm btn primary">Pages</button>
            <?php
            for($i=0;$i<$no_of_pages;$i++){
            ?>
            <button type="button" value="<?php echo $i; ?>" class="btn-sm btn white"><?php echo $i + 1; ?></button> 
            <?php } ?>
        </div>
</div>

<br><br><br>

<?php } ?> 


<script type="text/javascript">
    // do all sender ids pagination

    $('#ourtopupspag button').click(function(){

        var pagenumber = $(this).attr("value");
        
        $.post('../../resources/loadourtopups/',
        {
            pagenumber:pagenumber
        }, 
        function(nextpage)
        {
            $('#statementloader').html(nextpage);
        });
    })
</script>



