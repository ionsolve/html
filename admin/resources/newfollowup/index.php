<?php


/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
 *  
*/


include_once($_SERVER["DOCUMENT_ROOT"] . '/admin/resources/core/index.php');

$userid = $_SESSION['addingfollowup'];

if (isset($_POST['search_category'])) {
    $append = array();
    foreach($_POST as $key => $value) {
        if ($value != '0') {
            $append[] = "`".$key."`".' = '."'".$value."'";
        }
    }
    $appendquery = substr(implode(' AND ',$append), 0, -27);

    if (!empty($appendquery)) {
        $getUsers = "SELECT * FROM `users` WHERE ".$appendquery." ORDER BY name";
    }
    else{
        $getUsers = "SELECT * FROM `users` ORDER BY `name`";
    }
}

else{

    $getFollowups = "SELECT * FROM `followups` WHERE `user_id`='$userid'";
}

$run_fetch_query    = mysqli_query($conn, $getFollowups);

$number_of_followups = mysqli_num_rows($run_fetch_query);

if($number_of_followups < 1)
{
?>

<br><br><div class="">
                                                    
    <div class="list-group m-b">
        <font class="list-group-item text-md text-primary" href="#">No Followups to display</font> 
        <font class="list-group-item text-success" href="#"><i class="ion-information-circled"></i> 
            Please add a new followup.
        </font> 
        <font class="list-group-item text-muted" href="#">
            All the followups for this user will be displayed here.
        </font>
    </div>

</div>

<?php
}else{

?>

<div class="table-responsive">
<table id="mytaskstable" class="table table-bordered m-a-0">
    <thead>
        <tr class="text-primary">
            
            <th>Followup Title</th>
            <th>Comment</th>
            <th>Status</th>
            <th>Date Created</th>
            <th>Action</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
<?php

while($listfollowups = mysqli_fetch_array($run_fetch_query)){

?>
        <tr>
            <td><?php echo $listfollowups['title']; ?></td>
            <td><?php echo $listfollowups['comment'];?></td>
            <td><?php echo ucfirst($listfollowups['status']);?></td>
            <td><?php echo explode(" ",$listfollowups['date_created'])[0]; ?></td>
            <td>
                <div class="btn-group dropdown">
                   
                    <button aria-expanded="false" class="btn btn-sm white dropdown-toggle" data-toggle="dropdown"></button>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#map_id<?php echo $userid; ?>" data-ui-toggle-class="fade-left-big" data-ui-target="#animate">Mark as Closed</a>
                        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#map_id<?php echo $userid; ?>" data-ui-toggle-class="fade-left-big" data-ui-target="#animate">Edit</a>
                    </div>
                </div>
            </td>
            <td></td>
        </tr>
<?php

}

?>
    </tbody>

</table><br><br>

</div>

<?php  
} 
?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script>   
    $(document.body).on("keyup","#textsingle textarea",function()
    {
        var datatag             = $(this).attr('data');
        var text_length         = $(this).val().length;
        $('#countfeed'+datatag).html('<font class="text-muted">'+text_length + ' characters</font><br><small class="text-info">Note: 160 characters make up one message</small>');
    })
</script>

<script>

    $(document).ready( function () {
        $('#mytaskstable').DataTable( {
            // paging:false
        })
    });
  
</script>

