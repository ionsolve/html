<?php

/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0

 **/

require_once $_SERVER["DOCUMENT_ROOT"] .'/configs/database.php';

function getCountryWithCurrencyCode($currency)
{
	switch ($currency) {
		case 'KES':
			return "Kenya";
			break;

		case 'UGX':
			return "Uganda";
			break;

		case 'TZS':
            # code..
			return "Tanzania";
        break;

        case 'MWK':
            # code...
            return "Malawi";
        break;

        case 'NGN':
            # code...
            return "Nigeria";
        break;
		
		default:
			return "Others";
			break;
	}
}


function returnAllNumRows($table)
{
    global $conn;
    $result       = mysqli_query($conn, "SELECT COUNT(*) AS `id` FROM $table");
    $row          = mysqli_fetch_assoc($result);
    $count        = $row['id'];
    return $count;
}

function returnDaysDifference($date1, $date2){
    $firstdate = strtotime($date1);

    $seconddate = strtotime($date2);

    return ceil(($firstdate - $seconddate)/(60*60*24));
}

function getAccountBalance()
{
    // Be sure to include the file you've just downloaded
    require_once('../../configs/AfricasTalkingGateway.php');
    // Specify your login credentials
    $username   = "ionsms";
    $apikey     = "e11579457530057781f4d7afbbd16406e064f3bf4583c94e852ffe41763c5804";
    // Create a new instance of our awesome gateway class
    $gateway    = new AfricasTalkingGateway($username, $apikey);
    // Any gateway errors will be captured by our custom Exception class below, 
    // so wrap the call in a try-catch block
    try
    { 
      // Fetch the data from our USER resource and read the balance
      $data = $gateway->getUserData();
      $accountbalance = substr($data->balance, 4);

      $unitstosell = $accountbalance/0.4;

      echo number_format(floor($unitstosell));
      // The result will have the format=> KES XXX
    }
    catch ( AfricasTalkingGatewayException $e )
    {
      echo "Encountered an error while fetching user data: ".$e->getMessage()."\n";
    }
    // DONE!!! 
}


// breadcrumps

function resourceBreadCrumbs() {
    $baseurl="";
    $urlArray = parse_url( $baseurl.$_SERVER["REQUEST_URI"], PHP_URL_PATH ); //appends the http://.../ to /folder1/subfolder/file.php
    $crumbs = array_slice(explode( "/", $urlArray ), 3); //transforms /folder/folder/file.php -> array['','folder','subfolder','file.php']
    $REMOVE = array( "", "index.php" ); //an array that will hold strings to be removed -> array['folder','subfolder','file.php']
    
    // remove the elements who's values are stored in REMOVE array
    $crumbs = array_diff( $crumbs, $REMOVE );
    //used later to avoid adding a trailing >
    $totalCrumbs = count( $crumbs ); //counts the total number of items in the array
    $count = 0;
    //this is the subdirectory we are on the folder...
    $uri = "http://localhost/admin/";
?>
<ol style="width: 90%;color: #27AAFF;" class="breadcrumb">
<?php
    foreach ( $crumbs as $crumb ) {
        $count++;
        if ( $count!=$totalCrumbs ) { //checks if we are at the end of the urlArray
            //not at end so, lets append an > to the url, and then for the href lets add the additonal crumb to the uri string...
            echo '<li class="breadcrumb-item"><a href="'.$uri.$crumb.'">'. ucwords( str_replace( array( "-", ".php", "_" ), array( " ", "", " " ), $crumb ) . '</a> <span class="text-muted">/ </span></li> ' );
        } else {
            //at the end of the uri string, we don't need >, and for href lets use the requesting URI (works well when pass arguments and queries ?,# in URI)
            echo '<a href="'.$_SERVER["REQUEST_URI"].'">'.ucwords( str_replace( array( "-", ".php", "_" ), array( " ", "", " " ), $crumb ) . '</a>' );
        }
        //add the string to the uri and store it.
        $uri = $uri.$crumb.'/';
    }
}

?>