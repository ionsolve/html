<?php


/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
 *  
*/


include_once($_SERVER["DOCUMENT_ROOT"] . '/system/core/index.php');


if(isset($_REQUEST['search_'])){

    $search_         = mysqli_real_escape_string($conn, $_REQUEST['search_']);

    if(!empty($search_)){
        $fetch_contacts  = "SELECT * FROM `contacts` 
        WHERE `parent`='$account_id' 
        AND (`contact_name` LIKE '%$search_%' 
        OR `tags` LIKE '%$search_%' 
        OR `phone_number` LIKE '%$search_%') 
        order by contact_name DESC LIMIT 50";
    }else{
        $fetch_contacts  = "SELECT * FROM `contacts` 
        WHERE `parent`='$account_id' 
        order by contact_name DESC LIMIT 50";
    }
               

}else{

    if(isset($_REQUEST['pagenumber'])){
        $pagenumber  = mysqli_real_escape_string($conn, $_REQUEST['pagenumber']);
    }else{
        $pagenumber  = 0;
    }

    $limit           = 50 * $pagenumber;
    $limitup         = $limit + 50;

    $fetch_contacts  = "SELECT * FROM `contacts` 
    WHERE `parent`='$account_id' 
    order by contact_name
    LIMIT $limit, $limitup";
}

$run_fetch_query    = mysqli_query($conn, $fetch_contacts);

$number_of_contacts = mysqli_num_rows($run_fetch_query);

if($number_of_contacts < 1)
{
?>

<div class="">
                                                    
    <div class="list-group m-b">
        <font class="list-group-item text-md text-primary" href="#">No contacts found</font> 
        <font class="list-group-item text-success" href="#"><i class="ion-information-circled"></i> 
            Please add contact
        </font> 
        <font class="list-group-item text-muted" href="#">
            Contacts can be added and grouped according to the unique organizations or fomations they fall in.
        </font>
    </div>

</div>

<?php
}else{

?>

<div style="padding-bottom: 50px;" class="table-responsive">
<table class="table table-bordered m-a-0">
    <thead>
        <tr class="text-primary">
            
            <th>Date Created</th>
            <th>Name</th>
            <th>Phone Number</th>
            <th>Tags</th>
            <th>Action</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
<?php

while($listcontacts = mysqli_fetch_array($run_fetch_query)){

    $contactid           = $listcontacts['id'];
    $contact_fetch_args  = array('id'=>$contactid);

?>
        <tr>
            <td><?php echo getByValue('contacts', 'date_created', $contact_fetch_args); ?></td>
            <td><?php echo getByValue('contacts', 'contact_name', $contact_fetch_args); ?></td>
            <td><?php echo getByValue('contacts', 'phone_number', $contact_fetch_args); ?></td>
            <!-- <td>
                <a class="text-primary" href="#">Ionsolve</a>, <a class="text-primary" href="#">Africa's Talking</a>
            </td> -->
            <td><?php echo getByValue('contacts', 'tags', $contact_fetch_args); ?></td>
            <td>
                <div class="btn-group dropdown">
                   
                    <button aria-expanded="false" class="btn btn-sm white dropdown-toggle" data-toggle="dropdown"></button>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#editcontact<?php echo $contactid; ?>" data-ui-toggle-class="fade-left-big" data-ui-target="#animate">Edit</a> 
                        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#message<?php echo $contactid; ?>" data-ui-toggle-class="fade-left-big" data-ui-target="#animate">Send Message</a> 
                        <!-- <a class="dropdown-item" href="#" data-toggle="modal" data-target="#addtogroups<?php echo $contactid; ?>" data-ui-toggle-class="fade-left-big" data-ui-target="#animate">Add to Groups</a> -->
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item text-danger" data-toggle="modal" data-target="#delete<?php echo $contactid; ?>" data-ui-toggle-class="fade-left-big" data-ui-target="#animate">Delete Contact</a>
                    </div>
                </div>
            </td>
            <td></td>
        </tr>

        <!-- Edit Contact-->

        <div aria-hidden="true" class="modal fade animate" data-backdrop="true" id="editcontact<?php echo $contactid; ?>" style="display: none;">
			<div class="modal-dialog fade-left-big" data-ui-class="fade-left-big" id="animate">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title text-primary">Edit <?php echo getByValue('contacts', 'contact_name', $contact_fetch_args); ?></h5>			
					</div>
					<div class="modal-body p-lg">
						<div class="md-form-group">
							<input class="md-input" value="<?php echo getByValue('contacts', 'contact_name', $contact_fetch_args); ?>" id="e_contact_<?php echo $contactid; ?>" placeholder="Alaine Wafula"><label></label>
						</div>
						<div class="md-form-group">
							<input class="md-input" value="<?php echo getByValue('contacts', 'phone_number', $contact_fetch_args); ?>" id="e_phone_<?php echo $contactid; ?>" placeholder ="+2547*** *** ***"><label></label>
						</div>
						<div class="md-form-group">
							<input class="md-input" value="<?php echo getByValue('contacts', 'tags', $contact_fetch_args); ?>" id="e_tags_<?php echo $contactid; ?>" placeholder="marketing, bitcoin"><label></label>
						</div>
						<p id="editresponse<?php echo $contactid; ?>"></p>
					</div>
					<div class="modal-footer" id="editcontacttopwrap">
						<button class="btn dark-white p-x-md" id="forcecloseedit" data-dismiss="modal" type="button">Cancel</button> 
						<button class="btn primary p-x-md" id="editbtn" value="<?php echo $contactid; ?>" type="button">Update <i class="ion-ios-arrow-thin-right">&nbsp;</i></button>
					</div>
				</div>
			</div>
		</div>

        <!-- Send Message-->

        <div aria-hidden="true" class="modal fade animate" data-backdrop="true" id="message<?php echo $contactid; ?>" style="display: none;">
			<div class="modal-dialog fade-left-big" data-ui-class="fade-left-big" id="animate">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title text-primary">SMS <?php echo getByValue('contacts', 'contact_name', $contact_fetch_args); ?></h5>			
					</div>
					<div class="modal-body p-lg" id="textsingle">
                        <div class="md-form-group">
                            <br>
                            <textarea class="md-input" data="<?php echo $contactid; ?>" id="messagemessage<?php echo $contactid; ?>" placeholder="Type Message Here" rows="4"></textarea>
                            <label>Message</label>
                            <select id="alongsender<?php echo $contactid; ?>" class="form-control c-select" style="border:none; border-bottom:1px solid #e8e8e8;">
                                <option value="">--Select Sender ID--</option>
                                <option value="Ionsolve">System Default</option>
                                <?php
                                    $sender_args_msg  = array('parent'=> $account_id, 'status'=>'active');
                                    $sender_id_values = returnArrayOfRequest('sender_ids','sender_id',$sender_args_msg);

                                    if(!empty($sender_id_values))
                                    {
                                        $ex_sender = explode(",",$sender_id_values);
                                    }
                                    foreach($ex_sender as $senderid)
                                    {
                                        echo '<option value="'.$senderid.'">'.$senderid.'</option>';
                                    }
                                ?>
                            </select><br>
                            <p id="countfeed<?php echo $contactid; ?>"></p>
                        </div>
   
						<p id="messagessresponse<?php echo $contactid; ?>"></p>
					</div>
					<div class="modal-footer" id="messagess">
						<button class="btn dark-white p-x-md" id="forceclosemessage" data-dismiss="modal" type="button">Cancel</button> 
						<button class="btn primary p-x-md" value="<?php echo $contactid; ?>" id="btnmessage" type="button">Send <i class="ion-ios-arrow-thin-right">&nbsp;</i></button>
					</div>
				</div>
			</div>
		</div>

         <!-- Delete Contact-->

         <div aria-hidden="true" class="modal fade animate" data-backdrop="true" id="delete<?php echo $contactid; ?>" style="display: none;">
			<div class="modal-dialog fade-left-big" data-ui-class="fade-left-big" id="animate">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title text-primary">Delete <?php echo getByValue('contacts', 'contact_name', $contact_fetch_args); ?></h5>			
					</div>
					<div class="modal-body p-lg">
                    <div class="p-a padding">
                        <p class="text-md m-t block text-muted">Do you want to proceed?</p><br>
                        <p class="text-muted"><small>This will remove the contact from all groups.</small></p><br>
                        
                    </div>
    					<p id="deleteresponse<?php echo $contactid; ?>"></p>
					</div>

					<div class="modal-footer" id="deletewrapper">
						<button class="btn dark-white p-x-md" id="forceclosedelete" data-dismiss="modal" type="button">Cancel</button> 
						<button class="btn danger p-x-md" value="<?php echo $contactid; ?>" id="deletecontactbtn" type="button">Yes, Delete <i class="ion-ios-arrow-thin-right">&nbsp;</i></button>
					</div>
				</div>
			</div>
		</div>

<?php

}

?>
    </tbody>

    
</table><br><br>

</div>

<?php  
} 

$contargs           = array('parent' => $account_id);
$totalContacts      = returnCountWithCondition('contacts', $contargs);
// do pagination
$no_of_pages = ceil($totalContacts/49);

if($no_of_pages >= 1){

?>

<div class="padding">
        <div class="btn-group" id="pagination">
            <button type="button" value="0" class="btn-sm btn primary">Pages</button> 
            <?php
            for($i=0;$i<$no_of_pages;$i++){
            ?>
            <button type="button" value="<?php echo $i; ?>" class="btn-sm btn white"><?php echo $i + 1; ?></button> 
            <?php } ?>
        </div>
</div>

<br><br><br>

<?php } ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script>   
    $(document.body).on("keyup","#textsingle textarea",function()
    {
        var datatag             = $(this).attr('data');
        var text_length         = $(this).val().length;
        $('#countfeed'+datatag).html('<font class="text-muted">'+text_length + ' characters</font><br><small class="text-info">Note: 160 characters make up one message</small>');
    })
</script>

<script>
    $('#pagination button').click(function(){

        var pagenumber = $(this).attr("value");
        
        $.post('../../system/loadcontacts/',
        {
            pagenumber:pagenumber
        }, 
        function(returnsearch)
        {
            $('#contactspace').html(returnsearch);
        });
    })

    function loadcontacts()
    {
        $.post('../../system/loadcontacts/',
        function(contresp)
        {
            $('#contactspace').html(contresp);
        });
    }

    // edit contact

    $('#editcontacttopwrap #editbtn').click(function()
    {
        var editcontactid = $(this).attr('value');

        $('#editcontacttopwrap #editbtn').html('<i class="fa ion-load-c fa-spin"></i>');
        $('#editcontacttopwrap #editbtn').css('disabled','1');
        
        var editedname       =  $('#e_contact_'+editcontactid).val();
        var editednumber     =  $('#e_phone_'+editcontactid).val();
        var editedtag        =  $('#e_tags_'+editcontactid).val();

        $.post("../../system/editcontact/",
		{
            editedname:editedname,
            editednumber:editednumber,
            editedtag:editedtag,
            editingid:editcontactid
		},
		function(editresponse)
		{
			if (editresponse == 1)
			{
                $("#editcontacttopwrap #forcecloseedit").trigger("click");
                $('#editcontacttopwrap #editbtn').html('Update <i class="ion-ios-arrow-thin-right">&nbsp;</i>');
                loadcontacts();

			}else{
				$('#editresponse'+editcontactid).html(editresponse);
				$('#editcontacttopwrap #editbtn').css('disabled','0');
				$('#editcontacttopwrap #editbtn').html('Update <i class="ion-ios-arrow-thin-right">&nbsp;</i>');
			}
		})
        
    })


    // send message

    $('#messagess #btnmessage').click(function()
    {
        var messageid     = $(this).attr('value');
        var messagetosend = $('#messagemessage'+messageid).val();
        var alongsender   = $('#alongsender'+messageid).val();
        var single        = 'single';

        $('#messagess #btnmessage').html('<i class="fa ion-load-c fa-spin"></i>');
        $('#messagess #btnmessage').css('disabled','1');

        $.post("../../system/preparenums/",
		{
            messageid:messageid,
            messagetosend:messagetosend,
            alongsender:alongsender,
            single:single
		},
		function(messageresponse)
		{
			$('#messagessresponse'+messageid).html(messageresponse);
			$('#messagess #btnmessage').css('disabled','0');
			$('#messagess #btnmessage').html('Send <i class="ion-ios-arrow-thin-right">&nbsp;</i>');
		})
        
    })

    // delete contact

    $('#deletewrapper #deletecontactbtn').click(function()
    {
        var deleteid = $(this).attr('value');

        $('#deletewrapper #deletecontactbtn').html('<i class="fa ion-load-c fa-spin"></i>');
        $('#deletewrapper #deletecontactbtn').css('disabled','1');

        $.post("../../system/deletecontact/",
		{
            deleteid:deleteid
		},
		function(deleteresponse)
		{
			if (deleteresponse == 1)
			{
                $("#deletewrapper #forceclosedelete").trigger("click");
                $('#deletewrapper #deletecontactbtn').html('Yes, Delete <i class="ion-ios-arrow-thin-right">&nbsp;</i>');
                loadcontacts();
			}else{
				$('#deleteresponse'+deleteid).html(deleteresponse);
				$('#deletewrapper #deletecontactbtn').css('disabled','0');
				$('#deletewrapper #deletecontactbtn').html('Yes, Delete <i class="ion-ios-arrow-thin-right">&nbsp;</i>');
			}
		})
        
    })
  
</script>

