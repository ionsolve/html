<?php


/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
 *  
*/

include_once($_SERVER["DOCUMENT_ROOT"] . '/system/core/index.php');
include_once($_SERVER["DOCUMENT_ROOT"] . '/loader.php');


if (empty($_POST['scheduledate'])) {

    // read single contact

    if(isset($_REQUEST['single']) AND isset($_REQUEST['messagetosend']) AND isset($_REQUEST['alongsender']))
    {
        $messageid     = mysqli_real_escape_string($conn, $_REQUEST['messageid']);
        $messagetosend = mysqli_real_escape_string($conn, $_REQUEST['messagetosend']);
        $alongsender   = mysqli_real_escape_string($conn, $_REQUEST['alongsender']);

        if(empty($messagetosend) OR empty($alongsender)){
            die('<font style="color:red"><br>Please provide a message and a sender ID</font>');
        }

        $ValidateSingleContact = array('id'=>$messageid, 'parent'=>$account_id);

        if(returnExists('contacts',$ValidateSingleContact) > 0)
        {
            $phoneNumberToSend = getByValue('contacts', 'phone_number', $ValidateSingleContact);
            $refid              = uniqid();
            
            $trace_request = sendMessage($phoneNumberToSend, $messagetosend, $account_id, '', $refid, '', $alongsender);

            $query = mysqli_query($conn, "UPDATE `users` SET `stage` = 'sendmessage' WHERE `email` = '{$_SESSION['alphaion']}'");

            $update_last_sent = mysqli_query($conn, "UPDATE `users` SET `last_used` = CURDATE() WHERE `email` = '{$_SESSION['alphaion']}'");

            echo $trace_request;
        }
    }

    // read group contacts

    if(isset($_REQUEST['group']) AND isset($_REQUEST['groupmessagetosend']) AND isset($_REQUEST['alongsender']))
    {
        $groupmsgid         = mysqli_real_escape_string($conn, $_REQUEST['groupmsgid']);
        $groupmessagetosend = mysqli_real_escape_string($conn, $_REQUEST['groupmessagetosend']);
        $alongsender        = mysqli_real_escape_string($conn, $_REQUEST['alongsender']);

        if(empty($groupmessagetosend) OR empty($alongsender)){
            die('<font style="color:red"><br>Please provide a message and a sender ID</font>');
        }

        $ValidateGroup = array('id'=>$groupmsgid, 'parent'=>$account_id);

        if(returnExists('groups',$ValidateGroup) > 0)
        {
            $checkGroupContacts = array('group_id' => $groupmsgid, 'parent' => $account_id);

            if(returnExists('group_contacts', $checkGroupContacts) == 0){
                die('<font style="color:red"><br>The group does not have members</font>');
            }else{
                
                $allContactsInGroup = returnArrayOfRequest('group_contacts','contact_id', $checkGroupContacts);
                $phoneNumbersToSend  = returnArrayOfRequestIN('contacts','phone_number',$allContactsInGroup);
                $refid              = uniqid();
                 
                $trace_request = sendMessage($phoneNumbersToSend, $groupmessagetosend, $account_id, '2', $refid, $groupmsgid, $alongsender);

                $query = mysqli_query($conn, "UPDATE `users` SET `stage` = 'sendmessage' WHERE `email` = '{$_SESSION['alphaion']}'");

                $update_last_sent = mysqli_query($conn, "UPDATE `users` SET `last_used` = CURDATE() WHERE `email` = '{$_SESSION['alphaion']}'");

                echo $trace_request;
            }
        }
    }



    // send to all contacts

    if(isset($_REQUEST['all']) AND isset($_REQUEST['textmessageall']) AND isset($_REQUEST['alongsender']))
    {
        $textmessageall         = mysqli_real_escape_string($conn, $_REQUEST['textmessageall']);
        $alongsender            = mysqli_real_escape_string($conn, $_REQUEST['alongsender']);

        if(empty($textmessageall) OR empty($alongsender)){
            die('<font style="color:red"><br>Please provide a message and a sender ID</font>');
        }
        
        $ValidateAll = array('parent'=>$account_id);

        if(returnExists('contacts',$ValidateAll) > 0)
        {
            $phoneNumbersToSend  = returnArrayOfRequest('contacts','phone_number',$ValidateAll);
            $refid              = uniqid();
                 
            $trace_request = sendMessage($phoneNumbersToSend, $textmessageall, $account_id, '3', $refid, '', $alongsender);

            $query = mysqli_query($conn, "UPDATE `users` SET `stage` = 'sendmessage' WHERE `email` = '{$_SESSION['alphaion']}'");

            $update_last_sent = mysqli_query($conn, "UPDATE `users` SET `last_used` = CURDATE() WHERE `email` = '{$_SESSION['alphaion']}'");

            echo $trace_request;
          
        }else{
            die('<font style="color:red"><br>You do not have any contacts. Please add contacts.</font>');
        }
    }

    // resend failed messages

    if(isset($_REQUEST['resend']) AND isset($_REQUEST['resendmessageref']))
    {
        $resendmessageref    = mysqli_real_escape_string($conn, $_REQUEST['resendmessageref']);

        if(empty($resendmessageref)){
            die('<font style="color:red"><br>We are unable to locate the message reference</font>');
        }

        $ValidateResend = array('refid'=>$resendmessageref, 'parent'=>$account_id, 'status' => 'Failed');

        if(returnExists('messages',$ValidateResend) > 0)
        {
            // fetch unique
            $select_distinct     = mysqli_query($conn, "SELECT * FROM `messages` WHERE `refid`='$resendmessageref' ORDER BY id DESC LIMIT 1");
            
            $distinctdata        = mysqli_fetch_array($select_distinct);

            $themessage          = $distinctdata['message'];
            $sender_id           = $distinctdata['sender_id'];
            $link                = $distinctdata['link'];
            $sender              = $distinctdata['sender_id'];

            $phoneNumbersToSend  = returnArrayOfRequest('messages','sent_to',$ValidateResend);

            $refid               = uniqid();
                 
            $trace_request = sendMessage($phoneNumbersToSend, $themessage, $account_id, '4', $refid, $link, $sender);

            mysqli_query($conn, "UPDATE `messages` SET `status` = 'Success' WHERE `refid`='$resendmessageref'");

            $query = mysqli_query($conn, "UPDATE `users` SET `stage` = 'sendmessage' WHERE `email` = '{$_SESSION['alphaion']}'");

            $update_last_sent = mysqli_query($conn, "UPDATE `users` SET `last_used` = CURDATE() WHERE `email` = '{$_SESSION['alphaion']}'");

            echo $trace_request;
            
        }else{
            die('<font style="color:red"><br>There are no failed messages in this bunch</font>');
        }
    }

}

else{
   // schedule to all contacts

    if(isset($_REQUEST['all']) AND isset($_REQUEST['textmessageall']) AND isset($_REQUEST['alongsender']) AND isset($_POST['scheduledate']))
    {
        $textmessageall         = mysqli_real_escape_string($conn, $_REQUEST['textmessageall']);
        $alongsender            = mysqli_real_escape_string($conn, $_REQUEST['alongsender']);
        $scheduledate           = mysqli_real_escape_string($conn, $_POST['scheduledate']);

        $ValidateAll = array('parent'=>$account_id);

        $datetime = date("Y-m-d H:i:s");

        $refid = uniqid();

        $str_time_now = strtotime($datetime);

        $str_scheduledate = strtotime($scheduledate);

        if(empty($textmessageall) OR empty($alongsender) OR empty($scheduledate)){
            die('<font style="color:red"><br>Please provide a sender ID, message and the schedule date.</font>');
        }

        elseif ($str_scheduledate < $str_time_now) {
            die("<font style='color:red;'>Schedule date cannot be a past date and time.</font>");
        }
        else{
            if(returnExists('contacts',$ValidateAll) > 0)
            {
                $cont_phone = returnArrayOfRequest('contacts','phone_number',$ValidateAll);

                $query_insert = mysqli_query($conn, "INSERT INTO `scheduled_messages` (`senderid`,`message`,`ref_id`,`status`,`parent`,`type`, `link`,`phone`,`datetosend`,`date_created`) VALUES ('$alongsender','$textmessageall','$refid','pending','$account_id','3', '','$cont_phone','$scheduledate','$datetime')");

                if ($query_insert) {
                    echo '1';
                }
                else{
                    echo "An error occurred while processing your request.";
                }
              
            }else{
                die('<font style="color:red"><br>You do not have any contacts. Please add contacts.</font>');
            }
        }
    }



    // schedule to all contacts in a group

    if(isset($_REQUEST['group']) AND isset($_REQUEST['groupmessagetosend']) AND isset($_REQUEST['alongsender']) AND isset($_POST['scheduledate']))
    {
        $groupmessagetosend         = mysqli_real_escape_string($conn, $_REQUEST['groupmessagetosend']);
        $alongsender                = mysqli_real_escape_string($conn, $_REQUEST['alongsender']);
        $scheduledate               = mysqli_real_escape_string($conn, $_POST['scheduledate']);
        $groupid                    = mysqli_real_escape_string($conn, $_POST['groupmsgid']);
        $phone                = "";

        $checkExist = array('group_id'=>$groupid,'parent'=>$account_id);

        $datetime = date("Y-m-d H:i:s");

        $refid = uniqid();

        $str_time_now = strtotime($datetime);

        $str_scheduledate = strtotime($scheduledate);

        if(empty($groupmessagetosend) OR empty($alongsender) OR empty($scheduledate)){
            die('<font style="color:red"><br>Please provide a sender ID, message and the schedule date.</font>');
        }

        elseif ($str_scheduledate < $str_time_now) {
            echo "<font style='color:red;'>Schedule date cannot be a past date and time.</font>";
        }

        else{
            if(returnExists('group_contacts',$checkExist) > 0)
            {
            $contactids = returnArrayOfRequest('group_contacts','contact_id',$checkExist);

            $contacts_array = explode(',', $contactids);

            foreach ($contacts_array as $eachid) {
                $phone .= getByValue('contacts', 'phone_number', array('id'=>$eachid)).",";
            }

            $phonenumber = substr($phone, 0,-1);

                
                $query_insert = mysqli_query($conn, "INSERT INTO `scheduled_messages` (`senderid`,`message`, `ref_id`,`status`,`parent`,`type`, `link`,`phone`,`datetosend`,`date_created`) VALUES ('$alongsender','$groupmessagetosend', '$refid','pending','$account_id','2', '$groupid','$phonenumber','$scheduledate','$datetime')");

                if ($query_insert) {
                    echo '1';
                }
                else{
                    echo "<br><font style='color:red;'>An error occurred while processing your request.</font>";
                }
              
            }else{
                die('<font style="color:red"><br>Sorry, you do not have contacts in this group.</font>');
            }
        }
    }
}


