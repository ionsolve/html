<?php
/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
 *
 */
include_once ($_SERVER["DOCUMENT_ROOT"] . '/system/core/index.php');

if (isset($_POST['temptoedit']) AND isset($_POST['temp_input']))
{
	// sanitize variables

	$temptoedit     = mysqli_real_escape_string($conn, $_POST['temptoedit']);
    $temp_input       = mysqli_real_escape_string($conn, $_POST['temp_input']);
	// validate empty fields

	if (empty($temp_input))
	{
		die('<font style="color:red">Please enter a message template.</font>');
	}

	$datetime = date('Y-m-d H:i:s');

	// insert into database

	$updateTemplate = "UPDATE `templates` SET `message`='$temp_input',`updated_date`='$datetime' WHERE `id`='$temptoedit' AND `parent`='$account_id'";
           
	if (mysqli_query($conn, $updateTemplate))
	{
		echo "1";
	}
	else
	{
		die('<font style="color:red">Oops! Something went wrong.</font>');
	}
}
