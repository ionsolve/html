<?php


/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
 *  
*/


include_once($_SERVER["DOCUMENT_ROOT"] . '/system/core/index.php');


if(isset($_REQUEST['statsearch_'])){

    $statsearch_         = mysqli_real_escape_string($conn, $_REQUEST['statsearch_']);

    if(!empty($statsearch_)){
        $fetch_stat  = "SELECT * FROM `top_ups` 
		WHERE `parent`='$account_id' 
		AND (`type` LIKE '%$statsearch_%' 
        OR `static_amount` LIKE '%$statsearch_%' 
        OR `description` LIKE '%$statsearch_%') 
		order by id DESC LIMIT 20";
    }else{
        $fetch_stat  = "SELECT * FROM `top_ups` 
		WHERE `parent`='$account_id' 
		order by id DESC LIMIT 20";
    }
               

}else{

    if(isset($_REQUEST['grouppagenumber'])){
        $grouppagenumber        = mysqli_real_escape_string($conn, $_REQUEST['grouppagenumber']);
    }else{
        $grouppagenumber        = 0;
    }

    $limit           = (20 * $grouppagenumber);
    $limitup         = $limit + 20;

    $fetch_stat  = "SELECT * FROM `top_ups` 
	WHERE `parent`='$account_id' 
	order by id DESC
	LIMIT $limit, $limitup";
}

$run_fetch_query  = mysqli_query($conn, $fetch_stat);

$number_of_statements = mysqli_num_rows($run_fetch_query);


if($number_of_statements < 1)
{
?>

<div class="">
                                                    
    <div class="list-group m-b">
        <font class="list-group-item text-md text-primary" href="#">No payments found</font> 
        <font class="list-group-item text-success" href="https://ionsolve.com/dashboard/topup/">
            <i class="ion-information-circled"></i> Make a top up 
        <font> 
        <font class="list-group-item text-muted" href="#">
            View your previous payments to Ionsolve
        </font>
    </div>

</div>

<?php
}else{

?>

<div class="table-responsive">
<table class="table table-bordered m-a-0">

    <thead>
		<tr class="text-primary">
			<th>Date</th>
			<th>Amount</th>
            <th>Method</th>
			<th>Description</th>
			<th></th>
		</tr>
	</thead>
    <tbody>
<?php

while($liststat = mysqli_fetch_array($run_fetch_query)){

    $statid           = $liststat['id'];
    $stat_fetch_args  = array('id'=>$statid);

?>

        <tr>
			<td><?php echo getByValue('top_ups', 'date_created', $stat_fetch_args); ?></td>
			<td><?php echo getByValue('top_ups', 'static_amount', $stat_fetch_args); ?></td>
			<td><?php echo getByValue('top_ups', 'type', $stat_fetch_args); ?></td>
            <td><?php echo getByValue('top_ups', 'description', $stat_fetch_args); ?></td>
			<td></td>
		</tr>

<?php

}

?>
    </tbody>

    
</table><br><br><br><br><br><br>

</div>

<?php  
} 

// do pagination
$no_of_pages = ceil($number_of_statements/20);

if($no_of_pages > 1){
?>

<div class="padding">
        <div class="btn-group" id="pagination">
			<button type="button" value="<?php echo $i; ?>" class="btn-sm btn primary">Pages</button> 
            <?php
            for($i=0;$i<$no_of_pages;$i++){
            ?>
            <button type="button" value="<?php echo $i; ?>" class="btn-sm btn white"><?php echo $i + 1; ?></button> 
            <?php } ?>
        </div>
</div>

<?php } ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script>
    $('#pagination button').click(function(){

        var grouppagenumber = $(this).attr("value");
        
        $.post('../../system/viewstatement/',
        {
            grouppagenumber:grouppagenumber
        }, 
        function(returnsearch)
        {
            $('#contactspace').html(returnsearch);
        });
	});
  
</script>


