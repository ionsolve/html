<?php


/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
 *  
*/


include_once($_SERVER["DOCUMENT_ROOT"] . '/system/core/index.php');


if(isset($_REQUEST['codesearch_'])){

    $codesearch_         = mysqli_real_escape_string($conn, $_REQUEST['codesearch_']);

    if(!empty($codesearch_)){
        $fetch_codes  = "SELECT * FROM `short_codes` 
        WHERE `parent`='$account_id' 
        AND (`code` LIKE '%$codesearch_%' 
        OR `type` LIKE '%$codesearch_%' 
		OR `subscriptionOndemand` LIKE '%$codesearch_%' 
        OR `keyword` LIKE '%$codesearch_%') 
        order by code DESC LIMIT 50";
    }else{
        $fetch_codes  = "SELECT * FROM `short_codes` 
        WHERE `parent`='$account_id' 
        order by code DESC LIMIT 50";
    }
               

}else{

    if(isset($_REQUEST['pagenumber'])){
        $pagenumber  = mysqli_real_escape_string($conn, $_REQUEST['pagenumber']);
    }else{
        $pagenumber  = 0;
    }

    $limit           = 50 * $pagenumber;
    $limitup         = $limit + 50;

    $fetch_codes  = "SELECT * FROM `short_codes` 
    WHERE `parent`='$account_id' 
    order by code
    LIMIT $limit, $limitup";
}

$run_fetch_query    = mysqli_query($conn, $fetch_codes);

$number_of_codes = mysqli_num_rows($run_fetch_query);

if($number_of_codes < 1)
{
?>

<div class="">
                                                    
    <div class="list-group m-b">
        <font class="list-group-item text-md text-primary" href="#">No shortcodes/keywords found</font> 
        <font class="list-group-item text-success" href="#"><i class="ion-information-circled"></i> 
            Please add a shortcode/keyword
        </font> 
        <font class="list-group-item text-muted" href="#">
            Shortcodes help you manage your two way communication
        </font>
    </div>

</div>

<?php
}else{

?>

<div class="table-responsive">
<table class="table table-bordered m-a-0">
    <thead>
        <tr class="text-primary">
            
            <th>Date Created</th>
            <th>Code</th>
			<th>Keyword</th>
            <th>Type</th>
            <th>MT/MO</th>
			<th>Price</th>
			<th>Category</th>
			<th>Entries</th>
			<th>Automessage</th>
			<th>Status</th>
            <th>Action</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
<?php

while($listcodes = mysqli_fetch_array($run_fetch_query)){

    $codeid              = $listcodes['id'];
	$code_fetch_args     = array('id'=>$codeid);
	$countID             = getByValue('short_codes', 'id', $code_fetch_args);
	$countGetUsers       = array('parent' => $countID);

?>
        <tr id="codecode">
            <td> <?php echo getByValue('short_codes', 'date_created', $code_fetch_args); ?></td>
            <td>
				<?php if(getByValue('short_codes', 'subscriptionOndemand', $code_fetch_args) == "ondemand") { ?>
                    <a href="#" id="selectondemand" value="<?php echo $codeid; ?>" class="text-primary"><?php echo getByValue('short_codes', 'code', $code_fetch_args); ?></a>
                <?php }else{ ?>
                    <a href="#" id="selectcode" value="<?php echo $codeid; ?>" class="text-primary"><?php echo getByValue('short_codes', 'code', $code_fetch_args); ?></a>
				<?php } ?> 
			</td>
            <td><?php echo getByValue('short_codes', 'keyword', $code_fetch_args); ?></td>
			<td><?php echo getByValue('short_codes', 'type', $code_fetch_args); ?></td>
            <td><?php echo strtoupper(getByValue('short_codes', 'mtmo', $code_fetch_args)); ?></td>
            <td><?php echo "KES ".getByValue('short_codes', 'price', $code_fetch_args); ?></td>
            <td><?php echo getByValue('short_codes', 'subscriptionOndemand', $code_fetch_args); ?></td>
			<td>
				<?php
					if(getByValue('short_codes', 'subscriptionOndemand', $code_fetch_args) == "ondemand")
					{
						echo returnExists('on_demand_users', $countGetUsers);
					}else{
						echo returnExists('subscription_users', $countGetUsers);
					} 
				?>
			</td>
			<td><?php echo getByValue('short_codes', 'automessage', $code_fetch_args); ?></td>
            <td>
                <?php
                    if(getByValue('short_codes', 'status', $code_fetch_args)=='active'){
                        echo "<font class='text-success'>active</font>";
                    }else{
                        echo "<font class='text-warning'>".getByValue('short_codes', 'status', $code_fetch_args)."</font>";
                    }
                ?>
            </td>
            <td>
                <div class="btn-group dropdown">
                   
                    <button aria-expanded="false" class="btn btn-sm white dropdown-toggle" data-toggle="dropdown"></button>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#editcontact<?php echo $codeid; ?>" data-ui-toggle-class="fade-left-big" data-ui-target="#animate">Edit</a> 
                        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#callback<?php echo $codeid; ?>" data-ui-toggle-class="fade-left-big" data-ui-target="#animate">View CallBack URL</a> 
                        <!-- <a class="dropdown-item" href="#" data-toggle="modal" data-target="#addtogroups<?php echo $codeid; ?>" data-ui-toggle-class="fade-left-big" data-ui-target="#animate">Add to Groups</a> -->
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item text-danger" data-toggle="modal" data-target="#delete<?php echo $codeid; ?>" data-ui-toggle-class="fade-left-big" data-ui-target="#animate">Delete Contact</a>
                    </div>
                </div>
            </td>
            <td></td>
        </tr>

        <!-- Edit Contact-->

        <div aria-hidden="true" class="modal fade animate" data-backdrop="true" id="editcontact<?php echo $codeid; ?>" style="display: none;">
			<div class="modal-dialog fade-left-big" data-ui-class="fade-left-big" id="animate">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title text-primary">Edit <?php echo getByValue('contacts', 'contact_name', $contact_fetch_args); ?></h5>			
					</div>
					<div class="modal-body p-lg">
						<div class="md-form-group">
							<input class="md-input" value="<?php echo getByValue('contacts', 'contact_name', $contact_fetch_args); ?>" id="e_contact_<?php echo $codeid; ?>" placeholder="Alaine Wafula"><label></label>
						</div>
						<div class="md-form-group">
							<input class="md-input" value="<?php echo getByValue('contacts', 'phone_number', $contact_fetch_args); ?>" id="e_phone_<?php echo $codeid; ?>" placeholder ="+2547*** *** ***"><label></label>
						</div>
						<div class="md-form-group">
							<input class="md-input" value="<?php echo getByValue('contacts', 'tags', $contact_fetch_args); ?>" id="e_tags_<?php echo $codeid; ?>" placeholder="marketing, bitcoin"><label></label>
						</div>
						<p id="editresponse<?php echo $codeid; ?>"></p>
					</div>
					<div class="modal-footer" id="editcontacttopwrap">
						<button class="btn dark-white p-x-md" id="forcecloseedit" data-dismiss="modal" type="button">Cancel</button> 
						<button class="btn primary p-x-md" id="editbtn" value="<?php echo $codeid; ?>" type="button">Update <i class="ion-ios-arrow-thin-right">&nbsp;</i></button>
					</div>
				</div>
			</div>
		</div>

        <!-- Call Back View-->

        <div aria-hidden="true" class="modal fade animate" data-backdrop="true" id="callback<?php echo $codeid; ?>" style="display: none;">
			<div class="modal-dialog fade-left-big" data-ui-class="fade-left-big" id="animate">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title text-primary">Callback URL </h5>			
					</div>
					<div class="modal-body p-lg" id="">
                        <div class="md-form-group">
						   <?php
							if(getByValue('short_codes', 'subscriptionOndemand', $code_fetch_args) == "ondemand"){
						   ?>
                           <h5>Please set the following callback URL at Africa's Talking for <a class="text-primary" target="_blank" href="https://account.africastalking.com/apps/paxneujdeo/sms/inbox/callback">On Demand</a> </h5>
							<br><br>
						   <p class="text-primary">https://ionsolve.com/callbacks/shortcodes/ondemand/?token=<?php echo getByValue('short_codes', 'token', $code_fetch_args); ?></p>
						   <?php }else{ ?>
							<h5>Please set the following callback URL at Africa's Talking for <a class="text-primary" target="_blank" href="https://account.africastalking.com/apps/paxneujdeo/sms/premium/callback">Subscription</a></h5>
						    <br><br>
							<p class="text-primary">https://ionsolve.com/callbacks/shortcodes/subscription/?token=<?php echo getByValue('short_codes', 'token', $code_fetch_args); ?></p>
						   <?php } ?>
                        </div>
					</div>
					<div class="modal-footer" id="">
						<button class="btn dark-white p-x-md" data-dismiss="modal" type="button">Close</button> 
					</div>
				</div>
			</div>
		</div>

         <!-- Delete Contact-->

         <div aria-hidden="true" class="modal fade animate" data-backdrop="true" id="delete<?php echo $codeid; ?>" style="display: none;">
			<div class="modal-dialog fade-left-big" data-ui-class="fade-left-big" id="animate">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title text-primary">Delete <?php echo getByValue('contacts', 'contact_name', $contact_fetch_args); ?></h5>			
					</div>
					<div class="modal-body p-lg">
                    <div class="p-a padding">
                        <p class="text-md m-t block text-muted">Do you want to proceed?</p><br>
                        <p class="text-muted"><small>This will remove the contact from all groups.</small></p><br>
                        
                    </div>
    					<p id="deleteresponse<?php echo $codeid; ?>"></p>
					</div>

					<div class="modal-footer" id="deletewrapper">
						<button class="btn dark-white p-x-md" id="forceclosedelete" data-dismiss="modal" type="button">Cancel</button> 
						<button class="btn danger p-x-md" value="<?php echo $codeid; ?>" id="deletecontactbtn" type="button">Yes, Delete <i class="ion-ios-arrow-thin-right">&nbsp;</i></button>
					</div>
				</div>
			</div>
		</div>

<?php

}

?>
    </tbody>

    
</table><br><br>

</div>

<?php  
} 

$contargs           = array('parent' => $account_id);
$totalCodes         = returnCountWithCondition('short_codes', $contargs);
// do pagination
$no_of_pages = ceil($totalCodes/49);

if($no_of_pages >= 1){

?>

<div class="padding">
        <div class="btn-group" id="pagination">
            <button type="button" value="<?php echo $i; ?>" class="btn-sm btn primary">Pages</button> 
            <?php
            for($i=0;$i<$no_of_pages;$i++){
            ?>
            <button type="button" value="<?php echo $i; ?>" class="btn-sm btn white"><?php echo $i + 1; ?></button> 
            <?php } ?>
        </div>
</div>

<br><br><br>

<?php } ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>


<script>
    $('#pagination button').click(function(){

        var pagenumber = $(this).attr("value");
        
        $.post('../../system/loadcodes/',
        {
            pagenumber:pagenumber
        }, 
        function(returnsearchcode)
        {
            $('#codespace').html(returnsearchcode);
        });
    })

    function loadcodes()
    {
        $.post('../../system/loadcodes/',
        function(coderesp)
        {
            $('#codespace').html(coderesp);
        });
    }
</script>


<script>

    $('#codecode #selectcode').click(function()
    {
		var codeid = $(this).attr('value');

		$.post("../../system/setcode/",
		{
            codeid:codeid
		},
		function(loadcode)
		{
			if(loadcode == 1){
				window.location.href = "https://ionsolve.com/dashboard/subscriptionusers";
			}else{
				location.reload();
			}
		});
	});

    $('#codecode #selectondemand').click(function()
    {
		var codeid = $(this).attr('value');

		$.post("../../system/setcode/",
		{
            codeid:codeid
		},
		function(loadcode)
		{
			if(loadcode == 1){
				window.location.href = "https://ionsolve.com/dashboard/ondemandusers";
			}else{
				location.reload();
			}
		});
	});

</script>

