<?php

/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
 *  
*/

        include_once($_SERVER["DOCUMENT_ROOT"] . '/system/core/index.php');

        if(isset($_REQUEST['_groupname_']))
        {
            
            // sanitize variables
            $_groupname_        = mysqli_real_escape_string($conn, $_REQUEST['_groupname_']);

            // validate empty fields
            if(empty($_groupname_))
			{
				die('<font style="color:red">Please fill the group name.</font>');
            }  
            
            # check duplicate
			$verify_group = mysqli_query($conn, "SELECT * FROM `groups` WHERE `group_name`='$_groupname_' AND `parent`='$account_id'");

			if(mysqli_num_rows($verify_group) > 0)
			{
				die('<font style="color:red">Duplicate entry for group name.</font>');
            }

			$date         = date('m/d/Y H:i:s');
            
            # insert into database
			$saveGroup     =  "INSERT INTO `groups`(`group_name`,`created_on`,`parent`) 
                            VALUES('$_groupname_','$date','$account_id')";

            if(mysqli_query($conn,$saveGroup))
            {
                echo "1";
            }else{
                die('<font style="color:red">Oops! Something went wrong.</font>');
            }
        }



?>