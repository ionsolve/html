<?php
/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
 *
 */
include_once ($_SERVER["DOCUMENT_ROOT"] . '/system/core/index.php');

if (isset($_REQUEST['editinfoname']) AND isset($_REQUEST['editinfophone']) AND isset($_REQUEST['editinfocity']))
{

	// sanitize variables

	$editinfoname    = mysqli_real_escape_string($conn, $_REQUEST['editinfoname']);
	$editinfophone   = mysqli_real_escape_string($conn, $_REQUEST['editinfophone']);
	$editinfocity    = mysqli_real_escape_string($conn, $_REQUEST['editinfocity']);

	// validate empty fields

	if (empty($editinfoname) OR empty($editinfophone) OR empty($editinfocity))
	{
		die('<font style="color:red">Please fill all fields.</font>');
    }
    
    if (strpos($editinfophone, '+') === false) 
	{
		die('<font style="color:red">The phone number should be in international format starting with +</font>');
	}

	$updateUser = "UPDATE `users` SET `name`='$editinfoname',`phone`='$editinfophone',`city`='$editinfocity' WHERE `email`='{$_SESSION['alphaion']}'";
           
	if (mysqli_query($conn, $updateUser))
	{
		echo "1";
	}else
	{
		die('<font style="color:red">Oops! Something went wrong.</font>');
	}
}
