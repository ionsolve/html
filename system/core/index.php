<?php

/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
*/


session_start();

if(!isset($_SESSION['alphaion'])){
    header('location:../../account/login');
}

include_once($_SERVER["DOCUMENT_ROOT"] . '/loader.php');

date_default_timezone_set('Africa/Nairobi');

// account data
$account_args         =    array('email' => $_SESSION['alphaion']);
$account_status       =    getByValue('users', 'status', $account_args);
$account_name         =    getByValue('users', 'name', $account_args);
$account_code         =    getByValue('users', 'code', $account_args);
$account_id           =    getByValue('users', 'id', $account_args);
$account_currency     =    getByValue('users', 'account_currency', $account_args);
$account_phone        =    getByValue('users', 'phone', $account_args);
$account_income       =    getByValue('users', 'income', $account_args);
$getApiKey            =    getByValue('users', 'api_key', $account_args);
$account_city         =    getByValue('users', 'city', $account_args);
$account_ref          =    getByValue('users', 'account_lead', $account_args);
$date                 =    date('Y-m-d');
####### get account balance

// account currency
$account_currency_args = array(
    'id' => $account_id,
    'status' => 'active'
);

$samaritan_top_up_args = array(
    'parent' => $account_id,
    'type' => 'system'
);

if(returnExists('top_ups', $samaritan_top_up_args) == 0)
{
    switch ($account_currency) {
        case 'KES':
            # code...
            $entry_amount    = 5;
            $static_amount   = "KES 5";
        break;

        case 'TZS':
            # code...
            $entry_amount    = 100;
            $static_amount   = "TZS 100";
        break;

        case 'UGX':
            # code...
            $entry_amount    = 150;
            $static_amount   = "UGX 150";
        break;

        case 'MWK':
            # code...
            $entry_amount    = 35;
            $static_amount   = "MWK 35";
        break;

        case 'NGN':
            # code...
            $entry_amount    = 15;
            $static_amount   = "NGN 15";
        break;
        
        default:
            # code...
            $entry_amount    = 0.05;
            $static_amount   = "USD 0.05";
        break;
    }

    mysqli_query($conn, 
    "INSERT INTO `top_ups`
    (`amount`,`package`,`status`,`description`,`parent`,`static_amount`,`type`,`date_created`)
    VALUES('$entry_amount','1','active','System Free SMS','$account_id','$static_amount','system','$date')");
}

$active_top_up_args = array(
    'parent' => $account_id,
    'status' => 'active'
);
$pending_top_up_args = array(
    'parent' => $account_id,
    'status' => 'pending'
);
$depleted_top_up_args = array(
    'parent' => $account_id,
    'status' => 'depleted'
);

// get active in account
$total_active_in_accounts    = returnSumWithCondition('top_ups', $active_top_up_args);

// get pending in account
$total_pending_in_accounts   = returnSumWithCondition('top_ups', $pending_top_up_args);

// get depleted in account
$total_depleted_in_accounts   = returnSumWithCondition('top_ups', $depleted_top_up_args);

// sum accounts
$total_in_accounts           = $total_active_in_accounts + $total_pending_in_accounts + $total_depleted_in_accounts;