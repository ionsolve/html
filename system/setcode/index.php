<?php


/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
 *  
*/

include_once($_SERVER["DOCUMENT_ROOT"] . '/system/core/index.php');


if(isset($_REQUEST['codeid'])){

    $codeid         = mysqli_real_escape_string($conn, $_REQUEST['codeid']);

    $verify_code = mysqli_query($conn, "SELECT * FROM `short_codes` 
    WHERE `id`='$codeid' 
    AND `parent`='$account_id'");
    
    if (mysqli_num_rows($verify_code) > 0)
	{
        $_SESSION['activecode'] = $codeid;
        
        echo "1";
	}else{
        echo "Access to shortcode denied";
    }

}