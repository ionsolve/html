<?php

/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
 *  
*/


include_once($_SERVER["DOCUMENT_ROOT"] . '/system/core/index.php');

$arguments = array(
    'group_id' => $_SESSION['activegroup'],
    'parent' => $account_id
);

$membersin = returnArrayOfRequest('group_contacts','contact_id',$arguments);

if(isset($_REQUEST['forgroup'])){

    $forgroup         = mysqli_real_escape_string($conn, $_REQUEST['forgroup']);

    if(!empty($forgroup)){

        $fetch_contacts  = "SELECT * FROM `contacts` 
        WHERE `parent`='$account_id' 
        AND `id` NOT IN ($membersin)
        AND (`contact_name` LIKE '%$forgroup%'
        OR  `tags` LIKE '%$forgroup%' 
        OR `phone_number` LIKE '%$forgroup%') 
        order by contact_name ASC LIMIT 50";

    }else{
        $fetch_contacts  = "SELECT * FROM `contacts` 
        WHERE `parent`='$account_id' 
        AND `id` NOT IN ($membersin)
        order by contact_name ASC LIMIT 50";
    }
               

}else{

    if(isset($_REQUEST['pagenumber'])){
        $pagenumber  = mysqli_real_escape_string($conn, $_REQUEST['pagenumber']);
    }else{
        $pagenumber  = 0;
    }

    $limit           = (50 * $pagenumber);
    $limitup         = $limit + 50;

    $fetch_contacts  = "SELECT * FROM `contacts` 
    WHERE `parent`='$account_id' 
    AND `id` NOT IN ($membersin)
    ORDER BY contact_name ASC
    LIMIT $limit, $limitup";
}

$run_fetch_query    = mysqli_query($conn, $fetch_contacts);

$number_of_contacts = mysqli_num_rows($run_fetch_query);

if($number_of_contacts < 1)
{
?>

<div class="">
                                                    
    <div class="list-group m-b">
        <a class="list-group-item text-md text-primary" href="#">No contacts found</a> 
        <a class="list-group-item text-success" href="#"><i class="ion-information-circled"></i> Please add a contact</a> 
        <a class="list-group-item text-muted" href="#">
            Groups help you manage specific people or groups of people and send them messages by a click of a buttton.
        </a>
    </div>

</div>

<?php
}else{

?>

<div class="table-responsive">
<table class="table table-bordered m-a-0">
    <thead>
        <tr class="text-primary">
            <th class="text-muted">
                <label class="md-check">
                    
                </label>
            </th>
            <th>Date Created</th>
            <th>Name</th>
            <th>Phone Number</th>
            <!-- <th>Groups</th> -->
            <th>Tags</th>
            
        </tr>
    </thead>
    <tbody>


<?php

while($listcontacts = mysqli_fetch_array($run_fetch_query)){

    $contactid           = $listcontacts['id'];
    $contact_fetch_args  = array('id'=>$contactid);

?>
        <tr>
            <td class="text-muted">
                <label class="md-check">
                    <input class="has-value" value="<?php echo $contactid; ?>" name="checkset[]" type="checkbox"> 
                    <i class="primary"></i>
                </label>
            </td>
            <td><?php echo getByValue('contacts', 'date_created', $contact_fetch_args); ?></td>
            <td><?php echo getByValue('contacts', 'contact_name', $contact_fetch_args); ?></td>
            <td><?php echo getByValue('contacts', 'phone_number', $contact_fetch_args); ?></td>
            <!-- <td>
                <a class="text-primary" href="#">Ionsolve</a>, <a class="text-primary" href="#">Africa's Talking</a>
            </td> -->
            <td><?php echo getByValue('contacts', 'tags', $contact_fetch_args); ?></td>

        </tr>
 

<?php

}

?>


    </tbody>

    
</table><br><br><br><br><br>

</div>

<?php  
} 

// do pagination
$no_of_pages = ceil($number_of_contacts/50);

if($no_of_pages > 1){

?>

<div class="padding">
        <div class="btn-group" id="pagination">
            <button type="button" value="<?php echo $i; ?>" class="btn-sm btn primary">Pages</button> 
            <?php
            for($i=0;$i<$no_of_pages;$i++){
            ?>
            <button type="button" value="<?php echo $i; ?>" class="btn-sm btn white"><?php echo $i + 1; ?></button> 
            <?php } ?>
        </div>
</div>

            <?php } ?>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script>
    $('#paginatiif($no_of_pages > 1){on button').click(function(){

        var pagenumber = $(this).attr("value");
        
        $.post('../system/searchforgroup/',
        {
            pagenumber:pagenumber
        }, 
        function(returnsearch)
        {
            $('#contactspace').html(returnsearch);
        });
    })

    function loadcontacts()
    {
        $.post('../system/loadcontacts/',
        function(contresp)
        {
            $('#contactspace').html(contresp);
        });
    }

    $().click(function(){

    });
  
</script>

