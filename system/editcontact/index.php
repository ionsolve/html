<?php
/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
 *
 */
include_once ($_SERVER["DOCUMENT_ROOT"] . '/system/core/index.php');

if (isset($_REQUEST['editedname']) AND isset($_REQUEST['editednumber']) AND isset($_REQUEST['editedtag']) AND isset($_REQUEST['editingid']))
{

	// sanitize variables

	$editedname     = mysqli_real_escape_string($conn, $_REQUEST['editedname']);
	$editednumber   = str_replace(' ', '', mysqli_real_escape_string($conn, $_REQUEST['editednumber']));
	$editedtag      = mysqli_real_escape_string($conn, $_REQUEST['editedtag']);
    $editingid      = mysqli_real_escape_string($conn, $_REQUEST['editingid']);

	// validate empty fields

	if (empty($editedname) OR empty($editednumber))
	{
		die('<font style="color:red">Please fill all fields.</font>');
	}

	if (strpos($editednumber, '+') === false) 
	{
		die('<font style="color:red">The phone number should be in international format starting with +</font>');
	}

	// check duplicate
	$verify_contact = mysqli_query($conn, "SELECT * FROM `contacts` WHERE `phone_number`='$editednumber' AND `parent`='$account_id' AND id != '$editingid'");
	if (mysqli_num_rows($verify_contact) > 0)
	{
		die('<font style="color:red">The phone you gave already exists.</font>');
	}

	if(!empty($editedtag))
    {
        if (preg_match('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', $editedtag))
        {
	        die('<font style="color:red">Tags should not contain special characters</font>');
        }
    }

	$date = date('m/d/Y H:i:s');

	// insert into database

	$updateContact = "UPDATE `contacts` SET `contact_name`='$editedname',`phone_number`='$editednumber',`tags`='$editedtag',`date_edited`='$date' WHERE `id`='$editingid' AND `parent`='$account_id'";
           
	if (mysqli_query($conn, $updateContact))
	{
		echo "1";
	}
	else
	{
		die('<font style="color:red">Oops! Something went wrong.</font>');
	}
}
