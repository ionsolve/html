<?php

/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
 *  
*/

        include_once($_SERVER["DOCUMENT_ROOT"] . '/system/core/index.php');

        if(isset($_REQUEST['stextmessage']) and isset($_REQUEST['sdatetime']))
        {
            
            // sanitize variables
            $stextmessage        = mysqli_real_escape_string($conn, $_REQUEST['stextmessage']);
            $sdatetime           = mysqli_real_escape_string($conn, $_REQUEST['sdatetime']);

            // validate empty fields
            if(empty($stextmessage) OR empty($sdatetime))
			{
				die('<font style="color:red">Please fill all fields.</font>');
            }

            $convertedDate = strtotime($sdatetime);
            $refid         = "ionsm_".uniqid();
            
            # insert into database
			$saveSchedule     =  "INSERT INTO `scheduled_smessages`(`message`,`refid`,`status`,`parent`,`sendtime`) 
                            VALUES('$stextmessage','$refid','pending','{$_SESSION['activecode']}','$convertedDate')";

            if(mysqli_query($conn,$saveSchedule))
            {
                echo "1";
            }else{
                echo mysqli_error($conn);
            }
        }



?>