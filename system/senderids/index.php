<?php


/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
 *  
*/


include_once($_SERVER["DOCUMENT_ROOT"] . '/system/core/index.php');


$fetch_senders  = "SELECT * FROM `sender_ids` 
WHERE `parent`='$account_id' ORDER BY id DESC ";

$run_fetch_query  = mysqli_query($conn, $fetch_senders);

$number_of_senders = mysqli_num_rows($run_fetch_query);


if($number_of_senders < 1)
{
?>

<div class="">
                                                    
    <div class="list-group m-b">
        <font class="list-group-item text-md text-primary" href="#">No Sender IDs found</font> 
        <a class="list-group-item text-success" href="https://ionsolve.com/dashboard/senderids/SenderIdApllication.odt"><i class="ion-android-download"></i> Download Application Form</a> 
        <font class="list-group-item text-muted" href="#">
            A Sender ID is a message band for your organization. It is an alphanumeric with a maximum of 11 characters
        </font>

    </div>

</div>

<?php
}else{

?>

<div class="table-responsive">
<table class="table table-bordered m-a-0">

    <thead>
        <tr class="text-primary">  
            <th>Country</th>
            <th>Sender ID</th>
            <th>Status</th>
            <th></th>
        </tr>
	</thead>
    <tbody>
<?php

while($listsenders = mysqli_fetch_array($run_fetch_query)){

    $senderid           = $listsenders['id'];
    $sender_fetch_args  = array('id'=>$senderid,'parent' => $account_id);

?>

    <tr>
        <td><?php echo ucfirst(getByValue('sender_ids', 'country', $sender_fetch_args)); ?></td>
        <td><?php echo getByValue('sender_ids', 'sender_id', $sender_fetch_args); ?></td>
        
        <?php
            if(getByValue('sender_ids', 'status', $sender_fetch_args) == "active"){
        ?>
        <td class="text-success">Active</td>
            <?php }else{ ?>
        <td class="text-warning">Inactive</td>
            <?php } ?>
        <td></td>
    </tr>


<?php

} }

?>
    </tbody>

    
</table><br><br><br><br>

</div>





