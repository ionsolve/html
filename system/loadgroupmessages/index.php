<?php


/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
 *  
*/


include_once($_SERVER["DOCUMENT_ROOT"] . '/system/core/index.php');
include_once($_SERVER["DOCUMENT_ROOT"] . '/loader.php');

$fetch_args  =  array('link' => $_SESSION['activegroup'],'parent' => $account_id);

$totalsenthere = returnExists('messages',$fetch_args);

if($totalsenthere == 0){
?>

<div class="">
                                                    
    <div class="list-group m-b">
        <font class="list-group-item text-md text-primary" href="#">No messages found</font> 
        <font class="list-group-item text-success" href="#">
            <i class="ion-information-circled"></i> Please send message
        </font> 
        <font class="list-group-item text-muted" href="#">
            Keep in touch with this group by sending a message
        </font>
    </div>

</div>
<?php
}else{

?>

<div class="streamline" style="">           
    <div class="p-a-md">
															
<?php

    if(isset($_GET['page'])){
        $pagenumber  = mysqli_real_escape_string($conn, $_GET['page']);
    }else{
        $pagenumber  = 0;
    }

    $limit           = (10 * $pagenumber);
    $limitup         = $limit + 10;

    $allMessages = "SELECT DISTINCT `refid`
    FROM `messages` 
    WHERE `parent`= '$account_id' AND `link`='{$_SESSION['activegroup']}'
    ORDER BY id DESC LIMIT $limit, $limitup";

    $run_query = mysqli_query($conn, $allMessages);

    while($loadMessages = mysqli_fetch_array($run_query))
    {
        $mref           = $loadMessages['refid'];
        $typeof         = $loadMessages['type'];
        $fargs          = array('refid' => $mref);

        $successargs = array('refid' => $mref, 'status' => 'Success');
        $failedargs  = array('refid' => $mref, 'status' => 'Failed');
        
?>

<div class="m-b">
    <a class="pull-left w-40 m-r-sm" href="#">
        <span class="w-40 avatar circle b-primary text-primary b-a">
            <?php echo substr(getByValue('messages', 'message', $fargs),0,1); ?>
        </span>
    </a>
    <div class="clear">
        <div class="p-a p-y-sm b-a b-primary text-primary inline r">
            <?php echo getByValue('messages', 'message', $fargs); ?>
        </div>
        <div class="text-muted text-xs m-t-xs">
            <?php echo getByValue('messages', 'date_sent', $fargs); ?>   
            
            <a href="#" class="text-success"><?php echo returnExists('messages', $successargs) ?> Successful</a> . 
            <a href="#" class="text-warning"><?php echo returnExists('messages', $failedargs) ?> Failed</a>
            <?php
                if(getByValue('messages', 'type', $fargs)==4){
                    echo '<span class="label cyan">Sending Retry</span>';
                }
            ?>
            <font class="text-primary" id="retrywrapper">
                <?php  
                    if(returnExists('messages', $failedargs) > 0)
                    {
                        echo '<a href="#" value="'.$mref.'" class="text-primary"><i class="ion-refresh"></i> Retry Sending Failed</a>';
                    } 
                ?>
            </font>
        </div>
    </div>
</div>

<?php 
}
?>
</div>
</div>
<?php

}
// do pagination
$gquery = mysqli_query($conn, "SELECT DISTINCT `refid` FROM `messages` WHERE `link`='{$_SESSION['activegroup']}' AND `parent`='$account_id'");
$thispages = mysqli_num_rows($gquery);
$no_of_pages = ceil($thispages/10);

if($no_of_pages > 1){

?>

<div class="padding" style="position:relative">
        <div class="btn-group" id="pagination">
            <button type="button" value="<?php echo $i; ?>" class="btn-sm btn primary">Pages</button> 
            <?php
            for($i=0;$i<$no_of_pages;$i++){
            ?>
            <button type="button" value="<?php echo $i; ?>" class="btn-sm btn white"><?php echo $i + 1; ?></button> 
            <?php } ?>
        </div>
</div>

<?php } ?>

<script>
    function loadlogs(){
        $('#loadlogs').load("../../system/loadgroupmessages/");
    }
    
    $('#retrywrapper a').click(function()
    {
        $('#retrywrapper a').html('<i class="fa ion-load-c fa-spin"></i>');

        var resendmessageref = $('#retrywrapper a').attr('value');
        var resend  = "resend";

        $.post("../../system/preparenums/",
        {
            resend:resend,
            resendmessageref:resendmessageref
        },
        function(loadmessages)
        {
            if(loadmessages == 1){
                loadlogs();
            }else{
                loadlogs();
            }
        });
    });

    $('#pagination button').click(function()
    {

        var pagenumber = $(this).attr("value");

        $('#loadlogs').load("../../system/loadgroupmessages/?page="+pagenumber);

    })
</script>

