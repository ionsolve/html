<?php


/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
 *  
*/


include_once($_SERVER["DOCUMENT_ROOT"] . '/system/core/index.php');


if(isset($_REQUEST['searchref_'])){

    $searchref_         = mysqli_real_escape_string($conn, $_REQUEST['searchref_']);

    if(!empty($search_)){
        $fetch_contacts  = "SELECT * FROM `users` 
        WHERE `account_lead`='$account_id' 
        AND (`username` LIKE '%$searchref_%' 
        OR `email` LIKE '%$searchref_%' 
        OR `phone` LIKE '%$searchref_%') 
        order by id DESC LIMIT 20";
    }else{
        $fetch_contacts  = "SELECT * FROM `users` 
        WHERE `account_lead`='$account_id' 
        order by id DESC LIMIT 20";
    }
               

}else{

    if(isset($_REQUEST['pagenumber'])){
        $pagenumber  = mysqli_real_escape_string($conn, $_REQUEST['pagenumber']);
    }else{
        $pagenumber  = 0;
    }

    $limit           = 20 * $pagenumber;
    $limitup         = $limit + 20;

    $fetch_contacts  = "SELECT * FROM `users` 
    WHERE `account_lead`='$account_id' 
    order by id
    LIMIT $limit, $limitup";
}

$run_fetch_query    = mysqli_query($conn, $fetch_contacts);

$number_of_users = mysqli_num_rows($run_fetch_query);

if($number_of_users < 1)
{
?>

<div class="">
                                                    
    <div class="list-group m-b">
        <font class="list-group-item text-md text-primary" href="#">No references</font> 
        <font class="list-group-item text-success" href="#"><i class="ion-information-circled"></i> 
            Please add a reference
        </font> 
        <font class="list-group-item text-muted" href="#">
            References earn you income once they send messages from their accounts.
        </font>
    </div>

</div>

<?php
}else{

?>

<div class="table-responsive">
<table class="table table-bordered m-a-0">
    <thead>
        <tr class="text-primary">
			<th>Date Created</th>
			<th>Name</th>
			<th>Email</th>
			<th>Phone Number</th>
			<th>Account Currency</th>
			<th></th>
		</tr>
    </thead>
    <tbody>
<?php

while($listcontacts = mysqli_fetch_array($run_fetch_query)){

    $contactid           = $listcontacts['id'];
    $contact_fetch_args  = array('id'=>$contactid);

?>
        <tr>
            <td><?php echo getByValue('users', 'date_created', $contact_fetch_args); ?></td>
            <td><?php echo getByValue('users', 'name', $contact_fetch_args); ?></td>
            <td><?php echo getByValue('users', 'email', $contact_fetch_args); ?></td>
            <td><?php echo getByValue('users', 'phone', $contact_fetch_args); ?></td>
            <!-- <td>
                <a class="text-primary" href="#">Ionsolve</a>, <a class="text-primary" href="#">Africa's Talking</a>
            </td> -->
            <td><?php echo getByValue('users', 'account_currency', $contact_fetch_args); ?></td>

            <td></td>
        </tr>

<?php

}

?>
    </tbody>

    
</table><br><br>

</div>

<?php  
} 

// do pagination
$no_of_pages = ceil($number_of_users/20);

if($no_of_pages > 1){

?>

<div class="padding">
        <div class="btn-group" id="pagination">
            <button type="button" value="<?php echo $i; ?>" class="btn-sm btn primary">Pages</button> 
            <?php
            for($i=0;$i<$no_of_pages;$i++){
            ?>
            <button type="button" value="<?php echo $i; ?>" class="btn-sm btn white"><?php echo $i + 1; ?></button> 
            <?php } ?>
        </div>
</div>

<br><br><br>

<?php } ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>


<script>
    $('#pagination button').click(function(){

        var pagenumber = $(this).attr("value");
        
        $.post('../../system/loadrefs/',
        {
            pagenumber:pagenumber
        }, 
        function(returnsearch)
        {
            $('#refspace').html(returnsearch);
        });
    })

    function loadrefs()
    {
        $.post('../../system/loadrefs/',
        function(contresp)
        {
            $('#refspace').html(contresp);
        });
    }
  
</script>

