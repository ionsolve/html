<?php

/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
 *  
*/

session_start();

class LoginUser
{
   
    private $l_email_, $l_password_;
	
	function __construct()
	{
        include_once($_SERVER["DOCUMENT_ROOT"] . '/loader.php');

        if(isset($_REQUEST['l_email_']) AND isset($_REQUEST['l_password_'])){
            
            // sanitize variables

            $this->l_email_          = mysqli_real_escape_string($conn, $_REQUEST['l_email_']);
            $this->l_password_       = mysqli_real_escape_string($conn, $_REQUEST['l_password_']);

            // validate empty fields
            if(empty($this->l_email_) OR empty($this->l_password_))
			{
				die('<font style="color:red">Please fill all fields.</font>');
            }
            
            // validate email format
            if(!filter_var($this->l_email_, FILTER_VALIDATE_EMAIL)) 
			{
			    die('<font style="color:red">Please enter a valid email address.</font>');
			}
            
            // validate password length
            if(strlen($this->l_password_) < 6)
			{
				die('<font style="color:red">The password should be at least 6 characters long.</font>');
            }

            $hashPassword = sha1($this->l_password_);
            
            # verify account
			$verify_email = mysqli_query($conn, "SELECT * FROM `users` WHERE `email`='$this->l_email_' AND `password`='$hashPassword'");

			if(mysqli_num_rows($verify_email) > 0)
			{
				$_SESSION['alphaion']   = $this->l_email_;
                echo "1";
            }else{
                die('<font style="color:red">Incorrect login credentials.</font>');
            }
            
        }

        
	}
}

$loginUser = new LoginUser();

?>