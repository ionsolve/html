<?php


/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
 *  
*/


include_once($_SERVER["DOCUMENT_ROOT"] . '/system/core/index.php');


if(isset($_REQUEST['groupsearch_'])){

    $groupsearch_         = mysqli_real_escape_string($conn, $_REQUEST['groupsearch_']);

    if(!empty($groupsearch_)){
        $fetch_groups  = "SELECT * FROM `groups` 
		WHERE `parent`='$account_id' 
		AND `group_name` LIKE '%$groupsearch_%' 
		order by group_name ASC LIMIT 50";
    }else{
        $fetch_groups  = "SELECT * FROM `groups` 
		WHERE `parent`='$account_id' 
		order by group_name ASC LIMIT 50";
    }
               

}else{

    if(isset($_REQUEST['grouppagenumber'])){
        $grouppagenumber        = mysqli_real_escape_string($conn, $_REQUEST['grouppagenumber']);
    }else{
        $grouppagenumber        = 0;
    }

    $limit           = (50 * $grouppagenumber);
    $limitup         = $limit + 50;

    $fetch_groups  = "SELECT * FROM `groups` 
	WHERE `parent`='$account_id' 
	order by group_name
	LIMIT $limit, $limitup";
}

$run_fetch_query  = mysqli_query($conn, $fetch_groups);

$number_of_groups = mysqli_num_rows($run_fetch_query);


if($number_of_groups < 1)
{
?>

<div class="">
                                                    
    <div class="list-group m-b">
        <font class="list-group-item text-md text-primary" href="#">No groups found</font> 
        <font class="list-group-item text-success" href="#"><i class="ion-information-circled"></i> 
			Please create a group
		</font> 
        <font class="list-group-item text-muted" href="#">
            Groups help you manage specific people or groups of people and send them messages by a click of a buttton.
        </font>
    </div>

</div>

<?php
}else{

?>

<div class="table-responsive">
<table class="table table-bordered m-a-0">

    <thead>
		<tr class="text-primary">
			<th>Date Created</th>
			<th>Group</th>
			<th>Members</th>
			<th>Action</th>
			<th></th>
		</tr>
	</thead>
    <tbody>
<?php

while($listgroups = mysqli_fetch_array($run_fetch_query)){

    $groupid           = $listgroups['id'];
    $group_fetch_args  = array('id'=>$groupid);

?>

        <tr>
			<td><?php echo getByValue('groups', 'created_on', $group_fetch_args); ?></td>
			<td id="sgr"><a href="#" id="selectgroup" value="<?php echo $groupid; ?>" class="text-primary"><?php echo getByValue('groups', 'group_name', $group_fetch_args); ?></a></td>
			<td>
				<?php
					$groupargs = array('group_id' => $groupid,'parent' => $account_id);
					echo returnCountWithCondition('group_contacts', $groupargs);
				?>
			</td>
			<td>
				<div class="btn-group dropdown">
                   
                    <button aria-expanded="false" class="btn btn-sm white dropdown-toggle" data-toggle="dropdown"></button>
                    <div class="dropdown-menu">
                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#editgroup<?php echo $groupid; ?>" data-ui-toggle-class="fade-left-big" data-ui-target="#animate">Edit</a> 
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item text-danger" data-toggle="modal" data-target="#deletegroup<?php echo $groupid; ?>" data-ui-toggle-class="fade-left-big" data-ui-target="#animate">Delete Group</a>
				</div>
				
			</div>
			</td>
			<td></td>
		</tr>

        <!-- Edit Groups-->

        <div aria-hidden="true" class="modal fade animate" data-backdrop="true" id="editgroup<?php echo $groupid; ?>" style="display: none;">
			<div class="modal-dialog fade-left-big" data-ui-class="fade-left-big" id="animate">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title text-primary">Edit <?php echo getByValue('groups', 'group_name', $group_fetch_args); ?></h5>			
					</div>
					<div class="modal-body p-lg">
						<div class="md-form-group">
							<input class="md-input" value="<?php echo getByValue('groups', 'group_name', $group_fetch_args); ?>" id="e_group<?php echo $groupid; ?>" placeholder="Group Name"><label></label>
						</div>
						<p id="editgroupresponse<?php echo $groupid; ?>"></p>
					</div>
					<div class="modal-footer" id="editgrouppwrap">
						<button class="btn dark-white p-x-md" id="forceclose" data-dismiss="modal" type="button">Cancel</button> 
						<button class="btn primary p-x-md" value="<?php echo $groupid; ?>" id="editgroup" type="button">Update <i class="ion-ios-arrow-thin-right">&nbsp;</i></button>
					</div>
				</div>
			</div>
		</div>


         <!-- Delete Contact-->

         <div aria-hidden="true" class="modal fade animate" data-backdrop="true" id="deletegroup<?php echo $groupid; ?>" style="display: none;">
			<div class="modal-dialog fade-left-big" data-ui-class="fade-left-big" id="animate">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title text-primary">Delete Group <?php echo getByValue('groups', 'group_name', $group_fetch_args); ?></h5>			
					</div>
					<div class="modal-body p-lg">
                    <div class="p-a padding">
                        <a class="text-md m-t block text-muted" href="#">Do you want to proceed?</a><br>
                        <p class="text-muted"><small>This process cannot be rolled back.</small></p><br>
                        
                    </div>
    					<p id="deleteresponse<?php echo $groupid; ?>"></p>
					</div>

					<div class="modal-footer" id="deletewrapper">
						<button class="btn dark-white p-x-md" id="forceclosedelete" data-dismiss="modal" type="button">Cancel</button> 
						<button class="btn danger p-x-md" value="<?php echo $groupid; ?>" id="deletegroupbtn" type="button">Yes, Delete <i class="ion-ios-arrow-thin-right">&nbsp;</i></button>
					</div>
				</div>
			</div>
		</div>

<?php

}

?>
    </tbody>

    
</table><br><br><br><br><br><br>

</div>

<?php  
} 
$groupargs           = array('parent' => $account_id);
$totalGroups        = returnCountWithCondition('groups', $groupargs);
// do pagination
$no_of_pages = ceil($totalGroups/49);

if($no_of_pages > 1){
?>

<div class="padding">
        <div class="btn-group" id="pagination">
			<button type="button" value="<?php echo $i; ?>" class="btn-sm btn primary">Pages</button> 
            <?php
            for($i=0;$i<$no_of_pages;$i++){
            ?>
            <button type="button" value="<?php echo $i; ?>" class="btn-sm btn white"><?php echo $i + 1; ?></button> 
            <?php } ?>
        </div>
</div>

<?php } ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script>
    $('#pagination button').click(function(){

        var grouppagenumber = $(this).attr("value");
        
        $.post('../../system/loadgroups/',
        {
            grouppagenumber:grouppagenumber
        }, 
        function(returnsearch)
        {
            $('#contactspace').html(returnsearch);
        });
	});
	
	function loadgroups(){
        $.post('../../system/loadgroups/',

        function(groupresp)
        {
            $('#groupspace').html(groupresp);
        });
    }

	$('#editgrouppwrap #editgroup').click(function()
    {
        var editgroupid = $(this).attr('value');

        $('#editgrouppwrap #editgroup').html('<i class="fa ion-load-c fa-spin"></i>');
        $('#editgrouppwrap #editgroup').css('disabled','1');
        
        var e_group       =  $('#e_group'+editgroupid).val();

        $.post("../../system/editgroup/",
		{
            e_group:e_group,
			e_gid:editgroupid
		},
		function(editgroupresponse)
		{
			if (editgroupresponse == 1)
			{
                $('#editgroupresponse'+editgroupid).html(editgroupresponse);
				$("#editgrouppwrap #forceclose").trigger("click");
				loadgroups();
                $('#editgrouppwrap #editgroup').html('Update <i class="ion-ios-arrow-thin-right">&nbsp;</i>');

			}else{
				$('#editgroupresponse'+editgroupid).html(editgroupresponse);
				$('#editgrouppwrap #editgroup').css('disabled','0');
				$('#editgrouppwrap #editgroup').html('Update <i class="ion-ios-arrow-thin-right">&nbsp;</i>');
			}
		});
	});

	// delete contact

    $('#deletewrapper #deletegroupbtn').click(function()
    {
        var deleteid = $(this).attr('value');

        $('#deletewrapper #deletegroupbtn').html('<i class="fa ion-load-c fa-spin"></i>');
        $('#deletewrapper #deletegroupbtn').css('disabled','1');

        $.post("../../system/deletegroup/",
		{
            deleteid:deleteid
		},
		function(deleteresponse)
		{
			if (deleteresponse == 1)
			{
                $('#deleteresponse'+deleteid).html(deleteresponse);
                $("#deletewrapper #forceclosedelete").trigger("click");
                $('#deletewrapper #deletegroupbtn').html('Yes, Delete <i class="ion-ios-arrow-thin-right">&nbsp;</i>');
                loadgroups();
			}else{
				$('#deleteresponse'+deleteid).html(deleteresponse);
				$('#deletewrapper #deletegroupbtn').css('disabled','0');
				$('#deletewrapper #deletegroupbtn').html('Yes, Delete <i class="ion-ios-arrow-thin-right">&nbsp;</i>');
			}
		}); 
	});
	

	$('#sgr #selectgroup').click(function(){

		var groupid = $(this).attr('value');

		$.post("../../system/setgroup/",
		{
            groupid:groupid
		},
		function(loadgroup)
		{
			if(loadgroup == 1){
				window.location.href = "../../dashboard/viewgroup";
			}else{
				location.reload();
			}
		});
	});
  
</script>


