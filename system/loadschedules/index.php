<?php


/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
 *  
*/


include_once($_SERVER["DOCUMENT_ROOT"] . '/system/core/index.php');
include_once($_SERVER["DOCUMENT_ROOT"] . '/loader.php');

$fetch_args  =  array('parent' => $account_id);

$totalsenthere = returnExists('scheduled_messages',$fetch_args);

if($totalsenthere  == 0){
?>

<div class="">
                                                    
    <div class="list-group m-b">
        <font class="list-group-item text-md text-primary" href="#">No scheduled messages found</font> 
        <font class="list-group-item text-success" href="#"><i class="ion-information-circled"></i> Please schedule a message</font> 
        <font class="list-group-item text-muted" href="#">
            Schedule messages and we will send them when the time reaches.
        </font>
    </div>

</div>

<?php
}else{
?>
<div style="padding-bottom: 80px;" class="table-responsive">
<table class="table table-bordered m-a-0">

    <thead>
        <tr class="text-primary">
            <th>Message</th>
            <th>Status</th>
            <th>Scheduled Date</th>
            <th>Action</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
<?php

    if(isset($_POST['page'])){
        $pagenumber  = mysqli_real_escape_string($conn, $_POST['page']);
    }else{
        $pagenumber  = 0;
    }

    $limit           = (10 * $pagenumber);
    $limitup         = $limit + 10;

    $allSchedules = "SELECT DISTINCT `ref_id`
    FROM `scheduled_messages` 
    WHERE `parent`= '$account_id'
    ORDER BY `id` DESC LIMIT $limit, $limitup";

    $run_query = mysqli_query($conn, $allSchedules);

    while($loadSchedules = mysqli_fetch_array($run_query)){
        $mref           = $loadSchedules['ref_id'];
        $message_args = array('ref_id'=>$mref);

        // $sent_fetch_args = array('ref_id' => $mref, 'status' => 'sent');
        // $pending_fetch_args = array('ref_id' => $mref, 'status' => 'pending');
?>

 <tr>
    <td><?php echo substr(getByValue('scheduled_messages', 'message', $message_args), 0, 50)."..."; ?></td>
    <td><?php echo ucfirst(getByValue('scheduled_messages', 'status', $message_args)); ?></td>
    <td><?php echo getByValue('scheduled_messages', 'datetosend', $message_args); ?></td>
    <td>
        <div class="btn-group dropdown">
           
            <button aria-expanded="false" class="btn btn-sm white dropdown-toggle" data-toggle="dropdown"></button>
            <div class="dropdown-menu">
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#reschedule<?php echo $mref; ?>" data-ui-toggle-class="fade-left-big" data-ui-target="#animate">Reschedule</a>
                <?php
                if(getByValue('scheduled_messages', 'status', $message_args) == 'pending'){
                ?> 
                <div class="dropdown-divider"></div>
                <a class="dropdown-item text-danger" id="delete_schedule<?php echo $mref; ?>" data-toggle="modal" data-target="#delete_schedule<?php echo $mref; ?>" data-ui-toggle-class="fade-left-big" data-ui-target="#animate">Remove Schedule</a>
                <?php
                }
                ?>
            </div>
        
        </div>
    </td>
    <td></td>
</tr>


<!-- delete scheduled messages -->

<div aria-hidden="true" class="modal fade animate" data-backdrop="true" id="delete_schedule<?php echo $mref; ?>" style="display: none;">
    <div class="modal-dialog fade-left-big" data-ui-class="fade-left-big" id="animate">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-primary">Delete Message</h5>         
            </div>
            <div class="modal-body p-lg">
            <div class="p-a padding">
                <a class="text-md m-t block text-muted" href="#">Do you want to proceed?</a><br>
                <p class="text-muted"><small>After this, the message will not be sent to the recipients.</small></p><br>
                
            </div>
                <p id="deleteresponse<?php echo $mref; ?>"></p>
            </div>

            <div class="modal-footer" id="delete_sched_footer">
                <button class="btn dark-white p-x-md" id="forceclosedelete" data-dismiss="modal" type="button">Cancel</button> 
                <button class="btn danger p-x-md" value="<?php echo $mref; ?>" id="deleteschedulebtn" type="button">Yes, Delete <i class="ion-ios-arrow-thin-right">&nbsp;</i></button>
            </div>
        </div>
    </div>
</div>



<!-- reschedule messages -->


<div aria-hidden="true" class="modal fade animate" data-backdrop="true" id="reschedule<?php echo $mref; ?>" style="display: none;">
    <div class="modal-dialog fade-left-big" data-ui-class="fade-left-big" id="animate">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-primary">Reschedule Message</h5>         
            </div>
            <div class="modal-body p-lg">
                <div class="md-form-group">
                    <textarea rows="4" class="md-input" id="schedulemessage<?php echo $mref; ?>" placeholder="Type your message..."><?php echo getByValue('scheduled_messages', 'message', $message_args) ?></textarea><label>Message Body</label>
                </div>

                <div class="form-group">
                        <label style="color: #27AAFF;" for="default"><b>Schedule Date</b></label>
                        <div class="input-group date" id="datetimepicker<?php echo $mref; ?>">
                        <input id="rescheduledate<?php echo $mref; ?>" value="<?php echo getByValue('scheduled_messages', 'datetosend', $message_args); ?>" type="text" class="form-control" placeholder="Enter Date">
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>

                <p id="reschedule_resp<?php echo $mref; ?>"></p>
            </div>
            <div class="modal-footer" id="reschedule_footer">
                <button class="btn dark-white p-x-md" id="rescheduleforceclose" data-dismiss="modal" type="button">Cancel</button> 
                <button class="btn primary p-x-md" value="<?php echo $mref; ?>" id="reschedulebtn" type="button">Update <i class="ion-ios-arrow-thin-right">&nbsp;</i></button>
            </div>
        </div>
    </div>
</div>

<!-- date and time picker -->
<script type="text/javascript">
    $(function () {
        $('#datetimepicker<?php echo $mref; ?>').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss'
        });
    });
</script>


<?php

}
?>
</div>
</div>
<?php

}
// do pagination
$smquery = mysqli_query($conn, "SELECT DISTINCT `ref_id` FROM `scheduled_messages` WHERE `parent`='$account_id'");
$thispages = mysqli_num_rows($smquery);
$no_of_pages = ceil($thispages/10);

if($no_of_pages > 1){

?>

<div class="padding" style="position:relative">
        <div class="btn-group" id="pagination">
            <button type="button" value="<?php echo $i; ?>" class="btn-sm btn primary">Pages</button> 
            <?php
            for($i=0;$i<$no_of_pages;$i++){
            ?>
            <button type="button" value="<?php echo $i; ?>" class="btn-sm btn white"><?php echo $i + 1; ?></button> 
            <?php } ?>
        </div>
</div>

<?php } ?>

<script>

    $('#pagination button').click(function()
    {

        var pagenumber = $(this).attr("value");

        $.post('', 
        {
            pagenumber: pagenumber
        }, 
        function(data, textStatus, xhr) {
            $("#scheduledmsgspace").html(data);
        });

    })

    function loadSchedules() {
        $.post('../../system/loadschedules/',
        function(data, textStatus, xhr) {
            $("#scheduledmsgspace").html(data);
        });
    }

    // update schedule

    $("#reschedule_footer #reschedulebtn").click(function(event) {
        var mref = $(this).val();
        var rescheduledate = $("#rescheduledate"+mref).val();
        var message = $("#schedulemessage"+mref).val();


        $.post('../../system/updateschedule/', 
        {
            mref:mref,
            rescheduledate:rescheduledate,
            message:message
        }, 
        function(data, textStatus, xhr) {
            if (data == '1') {
                $("#reschedule_footer #rescheduleforceclose").trigger('click');
                swal('Done', 'Message has been rescheduled.', 'success');
                loadSchedules();
            }
            else{
                $("#reschedule_resp"+mref).html(data);
            }
        });
    });


    $("#delete_sched_footer #deleteschedulebtn").click(function(event) {
        var refid = $(this).val();

        $.post('../../system/deleteschedules/', 
        {
            refid:refid
        }, 
        function(data, textStatus, xhr) {
            if (data == 1) {
                $("#delete_sched_footer #forceclosedelete").trigger('click');
                swal('Success!', 'Scheduled message has been removed.', 'success');
                loadSchedules();
            }
            else{
                $("#deleteresponse"+refid).html(data);
            }
        });
    });

</script>