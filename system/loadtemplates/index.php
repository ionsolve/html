<?php


/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
 *  
*/


include_once($_SERVER["DOCUMENT_ROOT"] . '/system/core/index.php');


if(isset($_REQUEST['searchtemplate'])){

    $searchtemplate         = mysqli_real_escape_string($conn, $_REQUEST['searchtemplate']);

    if(!empty($searchtemplate)){
        $fetchtemplates  = "SELECT * FROM `templates` 
		WHERE `parent`='$account_id' 
		AND `message` LIKE '%$searchtemplate%' 
		order by `message` ASC LIMIT 20";
    }else{
        $fetchtemplates  = "SELECT * FROM `templates` 
		WHERE `parent`='$account_id' 
		order by `message` ASC LIMIT 50";
    }
               

}else{

    if(isset($_POST['pagenumber'])){
        $pagenumber        = mysqli_real_escape_string($conn, $_REQUEST['pagenumber']);
    }else{
        $pagenumber        = 0;
    }

    $limit           = (20 * $pagenumber);
    $limitup         = $limit + 20;

    $fetchtemplates  = "SELECT * FROM `templates` 
	WHERE `parent`='$account_id' 
	order by message
	LIMIT $limit, $limitup";
}

$run_fetch_query  = mysqli_query($conn, $fetchtemplates);

$number_of_templates = mysqli_num_rows($run_fetch_query);


if($number_of_templates < 1)
{
?>

<div class="">
                                                    
    <div class="list-group m-b">
        <font class="list-group-item text-md text-primary" href="#">No templates found</font> 
        <font class="list-group-item text-success" href="#"><i class="ion-information-circled"></i> 
			Please add a template.
		</font> 
        <font class="list-group-item text-muted" href="#">
            Templates are messages that you are likely to send often. Create a template and copy from it every time you need the message.
        </font>
    </div>

</div>

<?php
}else{

?>

<div class="table-responsive">
<table class="table table-bordered m-a-0">

    <thead>
		<tr class="text-primary">
			<th>Message Body</th>
			<th>Date Created</th>
			<th>Action</th>
			<th></th>
		</tr>
	</thead>
    <tbody>
<?php

while($listtemplates = mysqli_fetch_array($run_fetch_query)){

    $templateid           = $listtemplates['id'];
    $temp_fetch_args  = array('id'=>$listtemplates['id'], 'parent'=>$account_id);

?>

        <tr>
			<td style="width: 50%;"><?php echo substr(getByValue('templates', 'message', $temp_fetch_args), 0, 130)."..."; ?></td>
			<td><?php echo getByValue('templates', 'date_created', $temp_fetch_args); ?></td>
			<td>
				<div class="btn-group dropdown">
                   
                    <button aria-expanded="false" class="btn btn-sm white dropdown-toggle" data-toggle="dropdown"></button>
                    <div class="dropdown-menu">
                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#edittemplate<?php echo $templateid; ?>" data-ui-toggle-class="fade-left-big" data-ui-target="#animate">Edit</a> 
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item text-danger" data-toggle="modal" data-target="#deletetemplate<?php echo $templateid; ?>" data-ui-toggle-class="fade-left-big" data-ui-target="#animate">Delete Template</a>
				</div>
				
			</div>
			</td>
			<td></td>
		</tr>

        <!-- Edit Template-->

        <div aria-hidden="true" class="modal fade animate" data-backdrop="true" id="edittemplate<?php echo $templateid; ?>" style="display: none;">
			<div class="modal-dialog fade-left-big" data-ui-class="fade-left-big" id="animate">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title text-primary">Edit Template</h5>			
					</div>
					<div class="modal-body p-lg">
						<div class="md-form-group">
							<textarea rows="4" class="md-input" id="temp_input<?php echo $templateid; ?>" placeholder="Type your template..."><?php echo getByValue('templates', 'message', $temp_fetch_args) ?></textarea><label>Message Body</label>
						</div>
						<p id="edittemp_resp<?php echo $templateid; ?>"></p>
					</div>
					<div class="modal-footer" id="edit_temp_footer">
						<button class="btn dark-white p-x-md" id="tempeditforceclose" data-dismiss="modal" type="button">Cancel</button> 
						<button class="btn primary p-x-md" value="<?php echo $templateid; ?>" id="edittemplatebtn" type="button">Update <i class="ion-ios-arrow-thin-right">&nbsp;</i></button>
					</div>
				</div>
			</div>
		</div>


         <!-- Delete Template-->

         <div aria-hidden="true" class="modal fade animate" data-backdrop="true" id="deletetemplate<?php echo $templateid; ?>" style="display: none;">
			<div class="modal-dialog fade-left-big" data-ui-class="fade-left-big" id="animate">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title text-primary">Delete Template</h5>			
					</div>
					<div class="modal-body p-lg">
                    <div class="p-a padding">
                        <a class="text-md m-t block text-muted" href="#">Do you want to proceed?</a><br>
                        <p class="text-muted"><small>Once started, you cannot undo the changes.</small></p><br>
                        
                    </div>
    					<p id="deletetemp_response<?php echo $templateid; ?>"></p>
					</div>

					<div class="modal-footer" id="del_temp_footer">
						<button class="btn dark-white p-x-md" id="forceclosedeletetemp_" data-dismiss="modal" type="button">Cancel</button> 
						<button class="btn danger p-x-md" value="<?php echo $templateid; ?>" id="delete_tempbtn" type="button">Yes, Delete <i class="ion-ios-arrow-thin-right">&nbsp;</i></button>
					</div>
				</div>
			</div>
		</div>

<?php

}

?>
    </tbody>

    
</table><br><br><br><br><br><br>

</div>

<?php  
} 
$temp_fetch_args = array('parent'=>$account_id);
$allTemplates        = returnCountWithCondition('templates', $temp_fetch_args);
// do pagination
$no_of_pages = ceil($allTemplates/20);

if($no_of_pages > 1){
?>

<div class="padding">
        <div class="btn-group" id="temp_pagination">
			<button type="button" value="<?php echo $i; ?>" class="btn-sm btn primary">Pages</button> 
            <?php
            for($i=0;$i<$no_of_pages;$i++){
            ?>
            <button type="button" value="<?php echo $i; ?>" class="btn-sm btn white"><?php echo $i + 1; ?></button> 
            <?php } ?>
        </div>
</div>

<?php } ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script>
    $('#temp_pagination button').click(function(){

        var pagenumber = $(this).val()
        
        $.post('../../system/loadtemplates/',
        {
            pagenumber:pagenumber
        }, 
        function(data)
        {
            $('#templatespace').html(data);
        });
	});
	
	function loadTemplates(){
        $.post('../../system/loadtemplates/',

        function(tempresp)
        {
            $('#templatespace').html(tempresp);
        });
    }

	$('#edit_temp_footer #edittemplatebtn').click(function()
    {
    	$('#edit_temp_footer #edittemplatebtn').html('<i class="fa ion-load-c fa-spin"></i>');
        $('#edit_temp_footer #edittemplatebtn').css('disabled','1');

        var temptoedit = $(this).val();
        var temp_input       =  $('#temp_input'+temptoedit).val();

        $.post("../../system/edittemplate/",
		{
            temptoedit:temptoedit,
            temp_input:temp_input
		},
		function(edittempresp)
		{
			if (edittempresp == 1)
			{
				$("#edit_temp_footer #tempeditforceclose").trigger("click");
				loadTemplates();
                $('#edit_temp_footer #edittemplatebtn').html('Update <i class="ion-ios-arrow-thin-right">&nbsp;</i>');

			}else{
				$('#edittemp_resp'+temptoedit).html(edittempresp);
				$('#edit_temp_footer #edittemplatebtn').css('disabled','0');
				$('#edit_temp_footer #edittemplatebtn').html('Update <i class="ion-ios-arrow-thin-right">&nbsp;</i>');
			}
		});
	});

	// delete template

    $('#del_temp_footer #delete_tempbtn').click(function()
    {
        var deleteid = $(this).attr('value');

        $('#del_temp_footer #delete_tempbtn').html('<i class="fa ion-load-c fa-spin"></i>');
        $('#del_temp_footer #delete_tempbtn').css('disabled','1');

        $.post("../../system/deletetemplate/",
		{
            deleteid:deleteid
		},
		function(deleteresponse)
		{
			if (deleteresponse == 1)
			{
                $('#deletetemp_response'+deleteid).html(deleteresponse);
                $("#del_temp_footer #forceclosedeletetemp_").trigger("click");
                $('#del_temp_footer #delete_tempbtn').html('Yes, Delete <i class="ion-ios-arrow-thin-right">&nbsp;</i>');
                loadTemplates();
			}else{
				$('#deletetemp_response'+deleteid).html(deleteresponse);
				$('#del_temp_footer #delete_tempbtn').css('disabled','0');
				$('#del_temp_footer #delete_tempbtn').html('Yes, Delete <i class="ion-ios-arrow-thin-right">&nbsp;</i>');
			}
		}); 
	});


	// search for template

	$("#templatesearch_").keyup(function(event) {
		var searchtemplate = $("#templatesearch_").val();

		$.post('../../system/loadtemplates/', 
		{
			searchtemplate:searchtemplate
		}, 
		function(data, textStatus, xhr) {
		    $("#templatespace").html(data);
		});
	});
  
</script>


