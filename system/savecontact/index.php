<?php

/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
 *  
*/

        include_once($_SERVER["DOCUMENT_ROOT"] . '/system/core/index.php');

        if (isset($_REQUEST['_contact_']) AND isset($_REQUEST['_phone_']) AND isset($_REQUEST['_tags_']))
        {     
            // sanitize variables
            $contact_        = mysqli_real_escape_string($conn, $_REQUEST['_contact_']);
            $phone_          = str_replace(' ', '', mysqli_real_escape_string($conn, $_REQUEST['_phone_']));
            $tags_           = mysqli_real_escape_string($conn, $_REQUEST['_tags_']);

            // validate empty fields
            if (empty($contact_) OR empty($phone_))
			{
				die('<font style="color:red">Please fill all fields.</font>');
            }

            if (strpos($phone_, '+') === false) {
                die('<font style="color:red">The phone number should be in international format starting with +</font>');
            }

            if(!empty($tags_))
            {
                if (strpos($tags_, ',') !== false) 
                {
                    die('<font style="color:red">Tags should not contain commas</font>');
                }
            }
            else{
                $tags_ = 'notags';
            }
            
            # check duplicate
			$verify_contact = mysqli_query($conn, "SELECT * FROM `contacts` WHERE `phone_number`='$phone_' AND `parent`='$account_id'");

			if (mysqli_num_rows($verify_contact) > 0)
			{
				die('<font style="color:red">The phone you gave already exists.</font>');
            }

			$date         = date('Y-m-d');
            
            # insert into database
			$saveContact     =  "
            INSERT INTO `contacts`(`contact_name`,`phone_number`,`date_created`,`tags`,`parent`) 
                            VALUES('$contact_','$phone_','$date','$tags_','$account_id')";

            if (mysqli_query($conn,$saveContact))
            {
                $update_stage = mysqli_query($conn, "UPDATE `users` SET `stage` = 'loadcontact' WHERE `email` = '{$_SESSION['alphaion']}'");

                if ($update_stage) {
                    echo "1";
                }
            }else{
                die('<font style="color:red">Oops! Something went wrong.</font>');
            }
        }



?>