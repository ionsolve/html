<?php
/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
 *
 */
include_once ($_SERVER["DOCUMENT_ROOT"] . '/system/core/index.php');
include_once ($_SERVER["DOCUMENT_ROOT"] . '/config/system.php');


if(isset($_REQUEST['rfgid']))
{
    $contactsToRemove = $_POST['rfgid'];

    $removeContact = mysqli_query($conn, "DELETE FROM `group_contacts` 
    WHERE `contact_id`='$contactsToRemove' 
    AND `group_id`='{$_SESSION['activegroup']}' 
    AND `parent` ='$account_id'");

    if($removeContact === TRUE){
        echo "1";
    }else{
        die($conn->error);
    }   
}else{
    die($conn->error);
}