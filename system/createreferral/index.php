<?php

/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
 *  
*/

        include_once($_SERVER["DOCUMENT_ROOT"] . '/system/core/index.php');

        if(isset($_REQUEST['a_name_']) AND isset($_REQUEST['a_email_']) AND isset($_REQUEST['a_phone_']) AND isset($_REQUEST['a_username_']) AND isset($_REQUEST['a_country_']) AND isset($_REQUEST['a_password_']))
        {
            // sanitize variables
            $_name_           = ucwords(mysqli_real_escape_string($conn, $_REQUEST['a_name_']));
            $_email_          = mysqli_real_escape_string($conn, $_REQUEST['a_email_']);
            $_phone_          = mysqli_real_escape_string($conn, $_REQUEST['a_phone_']);
            $_username_       = strtolower(mysqli_real_escape_string($conn, $_REQUEST['a_username_']));
            $_country_        = ucfirst(mysqli_real_escape_string($conn, $_REQUEST['a_country_']));
            $_password_       = mysqli_real_escape_string($conn, $_REQUEST['a_password_']);

            // validate empty fields
            if(empty($_name_) OR empty($_email_) OR empty($_phone_) OR empty($_username_) OR empty($_country_) OR empty($_password_))
			{
                die('<font style="color:red">Please fill all fields</font>');
            }

            // validate name
            if(!preg_match("/^[a-zA-Z ]*$/",$_name_)) 
			{
			  die('<font style="color:red">The name should not contain special characters or numbers.</font>');
            }
            
            // validate email format
            if(!filter_var($_email_, FILTER_VALIDATE_EMAIL)) 
			{
			    die('<font style="color:red">Please enter a valid email address.</font>');
            }
            
            if (strpos($_phone_, '+') === false) {
                die('<font style="color:red">The phone number should be in international format starting with +</font>');
            }
            
            // validate password length
            if(strlen($_password_) < 6)
			{
				die('<font style="color:red">The password should be at least 6 characters long.</font>');
            }
            
            # verify email
			$verify_email = mysqli_query($conn, "SELECT * FROM `users` WHERE `email`='$_email_'");

			if(mysqli_num_rows($verify_email) > 0)
			{
				die('<font style="color:red">The email address you gave is already registered.</font>');
            }
            
            # verify username
			$verify_username = mysqli_query($conn, "SELECT * FROM `users` WHERE `username`='$_username_'");

			if(mysqli_num_rows($verify_username) > 0)
			{
				die('<font style="color:red">The username you gave is already registered.</font>');
			}

			$hashPassword = sha1($_password_);
			$date         = date('Y-m-d H:i:s');
            $code         = rand(1000,999999);
            $apikey       = sha1(md5($hashPassword.time().$_email_));
            
            # insert into database
			$saveUser     =  "
            INSERT INTO `users`(`name`,`username`,`email`,`phone`,`password`,`status`,`date_created`,`account_currency`,`code`,`api_key`,`account_lead`) 
                            VALUES('$_name_','$_username_','$_email_','$_phone_','$hashPassword','pending','$date','$_country_','$code','$apikey','$account_id')";

            if(mysqli_query($conn,$saveUser))
            {
                // send welcome email
                opensendSMS($_phone_,$code,$_name_);
                welcomeEmail($_name_, $_email_);
                echo "1";

            }else{
                die('<font style="color:red">Oops! Something went wrong.</font>');
            }
        }

?>