<?php
/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
 *
 */
include_once ($_SERVER["DOCUMENT_ROOT"] . '/system/core/index.php');
include_once ($_SERVER["DOCUMENT_ROOT"] . '/config/system.php');


if(isset($_POST['checkset'])){
    $contactsArray = $_POST['checkset'];
    $date         = date('m/d/Y H:i:s');
    foreach($contactsArray as $singleContact){
        mysqli_query($conn, "INSERT INTO `group_contacts` (`group_id`,`contact_id`,`date_created`,`parent`) 
        VALUES('{$_SESSION['activegroup']}','$singleContact','$date','$account_id')");
    }
    header('location:'.$base_path.'dashboard/groupmembers/');
}else{
    header('location:'.$base_path.'dashboard/groupmembers/');
}