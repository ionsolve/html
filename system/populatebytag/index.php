<?php

/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
**/

include_once($_SERVER["DOCUMENT_ROOT"] . '/system/core/index.php');

if(isset($_REQUEST['datefrom']) AND isset($_REQUEST['dateto']) AND isset($_REQUEST['greentags']) AND isset($_REQUEST['redtags']))
{
    $datefrom          = mysqli_real_escape_string($conn,$_REQUEST['datefrom']);
    $dateto            = mysqli_real_escape_string($conn,$_REQUEST['dateto']);
    $green             = mysqli_real_escape_string($conn,$_REQUEST['greentags']);
    $redzone           = mysqli_real_escape_string($conn,$_REQUEST['redtags']);

    $search_this    = explode(" ",$redzone);
    $search_green   = explode(" ",$green);

    $date           = date('Y-m-d');

    if(empty($datefrom) AND empty($dateto) AND empty($green) AND empty($redzone)){
        die('<font style="color:red">Please fill in your tags.</font>');
    }

    if(empty($datefrom) OR empty($dateto)){
        $query  = mysqli_query($conn,"SELECT * FROM `contacts` WHERE `parent`='$account_id'");
    }else{
        $query  = mysqli_query($conn,
            "SELECT * FROM `contacts` 
            WHERE DATE_FORMAT( `date_created` , '%Y-%m-%d' ) <= '$dateto'
            AND DATE_FORMAT( `date_created` , '%Y-%m-%d' ) >= '$datefrom'
            AND `parent` = '$account_id'");
    }

    while($run_query     = mysqli_fetch_array($query))
    {
        $all = explode(" ",$run_query['tags']); 

        if(!empty($green) && !empty($redzone))
        {
            if(!array_intersect($all, $search_this)) 
            {
                if(array_intersect($all, $search_green)) 
                {
                    mysqli_query($conn,"INSERT INTO `group_contacts`(`group_id`,`contact_id`,`parent`,`date_created`)
                    VALUES('{$_SESSION['activegroup']}','{$run_query['id']}','$account_id','$date')");
                }
            }
        }

        else if(empty($green) && empty($redzone))
        {
            mysqli_query($conn,"INSERT INTO `group_contacts`(`group_id`,`contact_id`,`parent`,`date_created`)
            VALUES('{$_SESSION['activegroup']}','{$run_query['id']}','$account_id','$date')");
        }

        else if(empty($green) AND !empty($redzone))
        {
            if(!array_intersect($all, $search_this)) 
            {
                mysqli_query($conn,"INSERT INTO `group_contacts`(`group_id`,`contact_id`,`parent`,`date_created`)
                    VALUES('{$_SESSION['activegroup']}','{$run_query['id']}','$account_id','$date')");
            }
        }  
        
        else if(empty($redzone) AND !empty($green))
        {
            if(array_intersect($all, $search_green)) 
            {
                mysqli_query($conn,"INSERT INTO `group_contacts`(`group_id`,`contact_id`,`parent`,`date_created`)
                    VALUES('{$_SESSION['activegroup']}','{$run_query['id']}','$account_id','$date')");
            }
        }   

        $process = $process + 1;
    }

    echo "1";
}

?>