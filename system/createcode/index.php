<?php

/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
 *  
*/

        include_once($_SERVER["DOCUMENT_ROOT"] . '/system/core/index.php');

        if(isset($_REQUEST['scode']) AND isset($_REQUEST['ds']) AND isset($_REQUEST['sds']) AND isset($_REQUEST['smtmo']) AND isset($_REQUEST['skeyword']) AND isset($_REQUEST['sprice'])  AND isset($_REQUEST['sautomessage']) AND isset($_REQUEST['susername']) AND isset($_REQUEST['sapikey']))
        {
            // sanitize variables
            $scode            = mysqli_real_escape_string($conn, $_REQUEST['scode']);
            $ds               = mysqli_real_escape_string($conn, $_REQUEST['ds']);
            $sds              = mysqli_real_escape_string($conn, $_REQUEST['sds']);
            $smtmo            = mysqli_real_escape_string($conn, $_REQUEST['smtmo']);
            $skeyword         = mysqli_real_escape_string($conn, $_REQUEST['skeyword']);
            $sprice           = mysqli_real_escape_string($conn, $_REQUEST['sprice']);
            $sautomessage     = mysqli_real_escape_string($conn, $_REQUEST['sautomessage']);
            $susername        = mysqli_real_escape_string($conn, $_REQUEST['susername']);
            $sapikey          = mysqli_real_escape_string($conn, $_REQUEST['sapikey']);

            // validate empty fields
            if(empty($scode) OR empty($ds) OR empty($sds) OR empty($smtmo) OR empty($sprice) OR empty($susername) OR empty($sapikey))
			{
                die('<font style="color:red">Please fill all fields</font>');
            }
            
            if (!ctype_digit($scode)) 
            {
                die('<font style="color:red">The code should be a digit</font>');
            }

            if (strlen($scode) !== 5) 
            {
                die('<font style="color:red">The Shortcode should be 5 digits long</font>');
            }

            if (!ctype_digit($sprice)) 
            {
                die('<font style="color:red">The price should be a digit</font>');
            }

            if($scode == "22384" OR $scode == "20880")
            {
                if(empty($skeyword))
                {
                    die('<font style="color:red">You need to provide a keyword for the given shortcode</font>');
                }
            }

            // encrypt AT Data
            $atencuser   = encdec($susername, $action = 'e');
            $atencapi    = encdec($sapikey, $action = 'e');

            $date        = date('Y-m-d H:i:s');
            $token       = "ion_".md5(uniqid()); 
                        
            # insert into database
			$saveCode     =  "INSERT INTO `short_codes`(`code`,`type`,`mtmo`,`price`,`keyword`,`subscriptionOndemand`,`automessage`,`token`,`date_created`,`parent`,`at_username`,`at_apikey`,`status`) 
            VALUES('$scode','$ds','$smtmo','$sprice','$skeyword','$sds','$sautomessage','$token','$date','$account_id','$atencuser','$atencapi','pending')";

            if(mysqli_query($conn,$saveCode))
            {
                // send message
                $message  = "Hello ".$account_name.", please activate the shortcode created by paying KES 10,000 to Paybill ".$paybill_number." Account Number SC for you to start receiving and sending messages.";
                opensendSMS($account_phone,$message);
                echo "1";
            }else{
                die(mysqli_error($conn));
            }
        }

?>