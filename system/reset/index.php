<?php

/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
 *  
*/

include_once($_SERVER["DOCUMENT_ROOT"] . '/configs/database.php');
include_once($_SERVER["DOCUMENT_ROOT"] . '/configs/functions.php');

    $_usermail_    = mysqli_real_escape_string($conn, $_REQUEST['r_email_']);

    if(empty($_usermail_)){
        die('<font style="color:red">Please fill your email address.</font>');
    }

    if(!filter_var($_usermail_, FILTER_VALIDATE_EMAIL)) {
        die('<font style="color:red">Please enter a valid email address.</font>');
    }

    # verify email
    $verify_email = mysqli_query($conn, "SELECT * FROM `users` WHERE `email`='$_usermail_'");

    if(mysqli_num_rows($verify_email) > 0){
        $newpass  =  "i0n".rand(10000,99999)."Lusr";
        $newhash  = sha1($newpass);

        $reset_account    = "UPDATE `users` SET  `password`='$newhash' WHERE `email`='$_usermail_'";
        $run_reset_query  = mysqli_query($conn, $reset_account);

        $account_args     = array('email' => $_usermail_);

        $account_phone    = getByValue('users', 'phone', $account_args);
        $account_name     = getByValue('users', 'name', $account_args);

        if($run_reset_query === TRUE)
        {
            $message  = '<html>
            <head>
                <title>Password Reset </title>
            </head>
                <body>
                    <div style="margin:0 auto;  background-color:#fff; font-family:font-family: "Varela Round", sans-serif; border-radius:5px; padding:10px;">
            
                        <h5 style="color:#000; font-size:20px;">Hello From Ionsolve</b>,</h5> <br>
                        You requested for a password change and we did just that!<br>
                        If you did not request for this please contact info@ionsolve.com.
                    </div>
                </body>
            </html>';

            $message      = "Hello ".$account_name.", your new account password is ".$newpass;
            opensendSMS($account_phone,$message);
            openEmail('Password Reset', $_usermail_, $message);
            echo "1";

        }else{
            die('<font style="color:red">Something went wrong</font>');
        }

    }else{
        die('<font style="color:red">The email address you gave is not registered with us.</font>');
    }

?>