<?php


/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
 *  
*/

include_once($_SERVER["DOCUMENT_ROOT"] . '/system/core/index.php');
include_once($_SERVER["DOCUMENT_ROOT"] . '/loader.php');

if (empty($_POST['scheduledate'])) {
	$senderid = mysqli_real_escape_string($conn, $_POST['senderid']);
	$groupid    = mysqli_real_escape_string($conn, $_POST['group']);
	$message    = mysqli_real_escape_string($conn, $_POST['message']);
	$phone = "";
	$refid = uniqid();

	$all_fetch_args = array('parent'=>$account_id);

	if (empty($groupid) || empty($senderid) || empty($message)) {
		die("<font style='color:red;'>Please enter Sender ID, Group and Message</font>");
	}

	if ($groupid == 'allcontacts') {
		$phonenumbers = returnArrayOfRequest('contacts','phone_number',$all_fetch_args);

		$trace_request = sendMessage($phonenumbers, $message, $account_id, '', $refid, '', $senderid);

		echo $trace_request;

		$update_stage = mysqli_query($conn, "UPDATE `users` SET `stage` = 'sendmessage' WHERE `email` = '{$_SESSION['alphaion']}'");

        $update_last_sent = mysqli_query($conn, "UPDATE `users` SET `last_used` = CURDATE() WHERE `email` = '{$_SESSION['alphaion']}'");
	}
	else{
		$group_fetch_args = array('group_id'=>$groupid, 'parent'=>$account_id);
		if (returnExists('group_contacts', $group_fetch_args) > 0) {
			$contact_ids = explode(',',returnArrayOfRequest('group_contacts','contact_id',$group_fetch_args));

			foreach ($contact_ids as $eachid) {
				$cont_fetch_args = array('id'=>$eachid);
				$query = mysqli_query($conn, "SELECT phone_number FROM `contacts` WHERE `id` = '$eachid'");

				while ($row = mysqli_fetch_assoc($query)) {
					$phone .= $row['phone_number'].",";
				}
			}

			$phonenumbers = substr($phone, 0, -1);

			$trace_request = sendMessage($phonenumbers, $message, $account_id, '2', $refid, $groupid, $senderid);

			echo $trace_request;

			$update_stage = mysqli_query($conn, "UPDATE `users` SET `stage` = 'sendmessage' WHERE `email` = '{$_SESSION['alphaion']}'");

            $update_last_sent = mysqli_query($conn, "UPDATE `users` SET `last_used` = CURDATE() WHERE `email` = '{$_SESSION['alphaion']}'");

		}
		else{
			die('There are no contacts in this group. Please add some.');
		}
	}
}

else{
	$senderid = mysqli_real_escape_string($conn, $_POST['senderid']);
	$groupid    = mysqli_real_escape_string($conn, $_POST['group']);
	$message    = mysqli_real_escape_string($conn, $_POST['message']);
	$scheduledate = mysqli_real_escape_string($conn, $_POST['scheduledate']);
	$datetime = date("Y-m-d H:i:s");
	$phone = "";
	$refid = uniqid();

	// convert date and time now and scheduled date to string

	$datetostr_now = strtotime($datetime);
	$scheduled_time = strtotime($scheduledate);


	$all_fetch_args = array('parent'=>$account_id);

	if (empty($groupid) || empty($senderid) || empty($message)) {
		die("<font style='color:red;'>Please enter Sender ID, Group and Message</font>");
	}

	if ($scheduled_time < $datetostr_now) {
		die('<font style="color:red;">Schedule date cannot be a past date and time.</font>');
	}

	if ($groupid == 'allcontacts') {
		$phonenumbers = returnArrayOfRequest('contacts','phone_number',$all_fetch_args);
		$type         = '1';
		$link         = '';
	}
	else{
		$group_fetch_args = array('group_id'=>$groupid, 'parent'=>$account_id);
		if (returnExists('group_contacts', $group_fetch_args) > 0) {
			$contact_ids = explode(',',returnArrayOfRequest('group_contacts','contact_id',$group_fetch_args));

			foreach ($contact_ids as $eachid) {
				$cont_fetch_args = array('id'=>$eachid);
				$query = mysqli_query($conn, "SELECT phone_number FROM `contacts` WHERE `id` = '$eachid'");

				while ($row = mysqli_fetch_assoc($query)) {
					$phone .= $row['phone_number'].",";
				}
			}

			$phonenumbers = substr($phone, 0, -1);
			$type         = '2';
			$link         = $groupid;

		}
		else{
			die('There are no contacts in this group. Please add some.');
		}
	}


	$query_insert = mysqli_query($conn, "INSERT INTO `scheduled_messages` (`senderid`,`message`,`ref_id`,`status`,`parent`,`type`, `link`,`phone`,`datetosend`,`date_created`) VALUES ('$senderid','$message','$refid','pending','$account_id','$type', '$link','$phonenumbers','$scheduledate','$datetime')");

    if ($query_insert) {
        echo '1';
    }
    else{
        echo "An error occurred while processing your request.";
    }

}

?>