<?php
/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
 *  
*/

include_once($_SERVER["DOCUMENT_ROOT"] . '/configs/database.php');
include_once($_SERVER["DOCUMENT_ROOT"] . '/configs/functions.php');

date_default_timezone_set('Africa/Nairobi');

if (isset($_POST['phone'])) {
	if (!empty($_POST['phone'])) {

		if (strpos($_POST['phone'], '+') === false) {
	        die('<font style="color:red">The phone number should be in international format.</font>');
	    }
	    else{
	    	$check_args = array('phone'=>$_POST['phone']);
	    	if(returnExists('test_clients', $check_args) <= 5){
				$phonenumber = mysqli_real_escape_string($conn, $_POST['phone']);

				$msgarray = array("How did the farmer find his wife? He tractor down.", "Did you know? The first programmer was a woman. Now you know.", "I know the voices in my head arent real... but sometimes their ideas are just absolutely awesome.", "If we shouldnt eat at night, then why is there a light in the fridge?", "Be strong! I whispered to my WiFi.", "What do you call a computer that sings? A-Dell.", "Life always offers you a second chance. Its called tomorrow.");

				$randommsg = $msgarray[array_rand($msgarray)];

				$datetime = date('Y-m-d H:i:s');

				$query = mysqli_query($conn, "INSERT INTO `test_clients` (`phone`, `msg_sent`, `date_created`) VALUES ('$phonenumber', '$randommsg', '$datetime')");

				if ($query) {
					$arraydata = testUs($phonenumber, $randommsg);

					if ($arraydata['status'] == '200') {
						opensendSMS('+254782810000', "Hello, ".$phonenumber." just tested our system. Please call back.");
						echo '1';
					}
					else{
						echo "<font style='color:red;'>Oopps! An error was encountered while processing request.</font>";
					}
				}
				else{
					echo "<font style='color:red;'>Oops! An error was encountered while processing your request.</font>";
				}
			}
			else{
				echo "<font style='color:red;'>Sorry, you have exhausted your 5 test messages.</font>";
			}
		}
	}

	else{
		die("<font style='color:red;'>Please provide your phone number.</font>");
	}
}


?>
