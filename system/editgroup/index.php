<?php
/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
 *
 */
include_once ($_SERVER["DOCUMENT_ROOT"] . '/system/core/index.php');

if (isset($_REQUEST['e_group']) AND isset($_REQUEST['e_gid']))
{
	// sanitize variables

	$e_group     = mysqli_real_escape_string($conn, $_REQUEST['e_group']);
    $e_gid       = mysqli_real_escape_string($conn, $_REQUEST['e_gid']);
	// validate empty fields

	if (empty($e_group))
	{
		die('<font style="color:red">Please fill all fields.</font>');
	}

	// check duplicate

	$verify_group = mysqli_query($conn, "SELECT * FROM `groups` WHERE `group_name`='$e_group' AND `parent`='$account_id' AND id != '$e_gid'");
	if (mysqli_num_rows($verify_group) > 0)
	{
		die('<font style="color:red">The group name already exists.</font>');
	}

	$date = date('m/d/Y H:i:s');

	// insert into database

	$updateGroup = "UPDATE `groups` SET `group_name`='$e_group',`updated_date`='$date' WHERE `id`='$e_gid' AND `parent`='$account_id'";
           
	if (mysqli_query($conn, $updateGroup))
	{
		echo "1";
	}
	else
	{
		die('<font style="color:red">Oops! Something went wrong.</font>');
	}
}
