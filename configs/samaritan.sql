-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 05, 2018 at 07:18 AM
-- Server version: 5.7.21-0ubuntu0.16.04.1
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `samaritan`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(11) NOT NULL,
  `email` varchar(250) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `password` varchar(32) DEFAULT NULL,
  `date_created` varchar(30) DEFAULT NULL,
  `can_topup_accounts` int(1) DEFAULT NULL,
  `can_suspend_accounts` int(1) DEFAULT NULL,
  `can_change_pricing` int(1) DEFAULT NULL,
  `phone_number` varchar(20) DEFAULT NULL,
  `permission_level` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(12) NOT NULL,
  `contact_name` varchar(200) NOT NULL,
  `phone_number` varchar(30) NOT NULL,
  `date_created` date NOT NULL,
  `date_edited` date NOT NULL,
  `tags` varchar(200) NOT NULL,
  `parent` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` int(11) NOT NULL,
  `group_name` varchar(50) NOT NULL,
  `created_on` varchar(30) NOT NULL,
  `parent` int(11) NOT NULL,
  `updated_date` varchar(30) NOT NULL,
  `dates` text NOT NULL,
  `green` text NOT NULL,
  `red` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Table structure for table `group_contacts`
--

CREATE TABLE `group_contacts` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `contact_id` int(12) NOT NULL,
  `date_created` varchar(30) NOT NULL,
  `parent` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `message` text NOT NULL,
  `sent_to` varchar(20) NOT NULL,
  `cost` varchar(10) NOT NULL,
  `status` varchar(30) NOT NULL,
  `message_id` varchar(50) NOT NULL,
  `sender_id` varchar(20) NOT NULL,
  `date_sent` varchar(30) NOT NULL,
  `type` int(11) NOT NULL,
  `parent` int(11) NOT NULL,
  `metadata` varchar(250) NOT NULL,
  `refid` varchar(100) NOT NULL,
  `link` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `on_demand_users`
--

CREATE TABLE `on_demand_users` (
  `id` int(11) NOT NULL,
  `phonenumber` varchar(30) NOT NULL,
  `text` text NOT NULL,
  `linkid` varchar(50) NOT NULL,
  `date` date NOT NULL,
  `status` varchar(10) NOT NULL,
  `reply` text NOT NULL,
  `parent` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE `packages` (
  `id` int(11) NOT NULL,
  `kenya` double NOT NULL,
  `uganda` double NOT NULL,
  `rwanda` double NOT NULL,
  `malawi` double NOT NULL,
  `tanzania` double NOT NULL,
  `nigeria` double NOT NULL,
  `international` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Table structure for table `scheduled_messages`
--

CREATE TABLE `scheduled_messages` (
  `id` int(11) NOT NULL,
  `message` text NOT NULL,
  `phone_numbers` text NOT NULL,
  `date_to_send` varchar(20) NOT NULL,
  `time_to_send` varchar(10) NOT NULL,
  `status` int(1) NOT NULL,
  `parent` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `scheduled_smessages`
--

CREATE TABLE `scheduled_smessages` (
  `id` int(11) NOT NULL,
  `sendtime` date NOT NULL,
  `message` text NOT NULL,
  `refid` varchar(50) NOT NULL,
  `status` varchar(10) NOT NULL,
  `parent` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sender_ids`
--

CREATE TABLE `sender_ids` (
  `id` int(11) NOT NULL,
  `sender_id` varchar(15) NOT NULL,
  `parent` int(11) NOT NULL,
  `country` varchar(15) NOT NULL,
  `status` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `short_codes`
--

CREATE TABLE `short_codes` (
  `id` int(11) NOT NULL,
  `code` int(5) NOT NULL,
  `type` varchar(10) NOT NULL,
  `mtmo` varchar(5) NOT NULL,
  `price` int(3) NOT NULL,
  `keyword` varchar(30) NOT NULL,
  `subscriptionOndemand` varchar(30) NOT NULL,
  `automessage` text NOT NULL,
  `token` varchar(50) NOT NULL,
  `date_created` varchar(30) NOT NULL,
  `parent` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `subscription_outbox`
--

CREATE TABLE `subscription_outbox` (
  `id` int(11) NOT NULL,
  `message` text NOT NULL,
  `phonenumber` varchar(30) NOT NULL,
  `code` int(5) NOT NULL,
  `keyword` varchar(30) NOT NULL,
  `refid` varchar(100) NOT NULL,
  `status` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `subscription_users`
--

CREATE TABLE `subscription_users` (
  `id` int(11) NOT NULL,
  `phonenumber` varchar(30) NOT NULL,
  `date_joined` varchar(30) NOT NULL,
  `parent` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `system`
--

CREATE TABLE `system` (
  `id` int(11) NOT NULL,
  `at_username` varchar(100) NOT NULL,
  `at_api_key` varchar(250) NOT NULL,
  `default_sender_id_kenya` varchar(11) NOT NULL,
  `default_sender_id_uganda` varchar(11) NOT NULL,
  `default_sender_id_tanzania` varchar(11) NOT NULL,
  `default_sender_id_rwanda` varchar(11) NOT NULL,
  `default_sender_id_nigeria` varchar(11) NOT NULL,
  `default_sender_id_malawi` varchar(11) NOT NULL,
  `default_sender_id_international` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `top_ups`
--

CREATE TABLE `top_ups` (
  `id` int(11) NOT NULL,
  `amount` double NOT NULL,
  `package` int(1) NOT NULL,
  `status` varchar(10) NOT NULL,
  `description` varchar(50) NOT NULL,
  `parent` int(11) NOT NULL,
  `static_amount` varchar(30) NOT NULL,
  `type` varchar(30) NOT NULL,
  `date_created` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(200) NOT NULL,
  `email` varchar(50) NOT NULL,
  `name` varchar(200) NOT NULL,
  `password` varchar(100) NOT NULL,
  `status` varchar(50) NOT NULL,
  `date_created` varchar(30) NOT NULL,
  `api_key` varchar(250) NOT NULL,
  `account_currency` varchar(5) NOT NULL,
  `account_lead` int(11) NOT NULL,
  `code` int(10) NOT NULL,
  `income` double NOT NULL DEFAULT '0',
  `phone` varchar(30) NOT NULL,
  `city` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent` (`parent`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent` (`parent`);

--
-- Indexes for table `group_contacts`
--
ALTER TABLE `group_contacts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent` (`parent`),
  ADD KEY `contact` (`contact_id`),
  ADD KEY `group_id` (`group_id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent` (`parent`);

--
-- Indexes for table `on_demand_users`
--
ALTER TABLE `on_demand_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent` (`parent`);

--
-- Indexes for table `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kenya` (`kenya`),
  ADD KEY `uganda` (`uganda`),
  ADD KEY `rwanda` (`rwanda`),
  ADD KEY `malawi` (`malawi`),
  ADD KEY `tanzania_vodacome` (`tanzania`),
  ADD KEY `nigeria_other` (`nigeria`),
  ADD KEY `international` (`international`);

--
-- Indexes for table `scheduled_messages`
--
ALTER TABLE `scheduled_messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent` (`parent`);

--
-- Indexes for table `scheduled_smessages`
--
ALTER TABLE `scheduled_smessages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent` (`parent`);

--
-- Indexes for table `sender_ids`
--
ALTER TABLE `sender_ids`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent` (`parent`);

--
-- Indexes for table `short_codes`
--
ALTER TABLE `short_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent` (`parent`);

--
-- Indexes for table `subscription_outbox`
--
ALTER TABLE `subscription_outbox`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscription_users`
--
ALTER TABLE `subscription_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent` (`parent`);

--
-- Indexes for table `system`
--
ALTER TABLE `system`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `top_ups`
--
ALTER TABLE `top_ups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent` (`parent`),
  ADD KEY `package` (`package`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_currency` (`account_currency`),
  ADD KEY `account_currency_2` (`account_currency`),
  ADD KEY `account_lead` (`account_lead`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=675;
--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1158;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT for table `group_contacts`
--
ALTER TABLE `group_contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34768;
--
-- AUTO_INCREMENT for table `on_demand_users`
--
ALTER TABLE `on_demand_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `packages`
--
ALTER TABLE `packages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `scheduled_messages`
--
ALTER TABLE `scheduled_messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `scheduled_smessages`
--
ALTER TABLE `scheduled_smessages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sender_ids`
--
ALTER TABLE `sender_ids`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `short_codes`
--
ALTER TABLE `short_codes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `subscription_outbox`
--
ALTER TABLE `subscription_outbox`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `subscription_users`
--
ALTER TABLE `subscription_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `system`
--
ALTER TABLE `system`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `top_ups`
--
ALTER TABLE `top_ups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=112;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `contacts`
--
ALTER TABLE `contacts`
  ADD CONSTRAINT `contacts_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `groups`
--
ALTER TABLE `groups`
  ADD CONSTRAINT `groups_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `group_contacts`
--
ALTER TABLE `group_contacts`
  ADD CONSTRAINT `group_contacts_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `group_contacts_ibfk_2` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `group_contacts_ibfk_3` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `messages`
--
ALTER TABLE `messages`
  ADD CONSTRAINT `messages_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `on_demand_users`
--
ALTER TABLE `on_demand_users`
  ADD CONSTRAINT `on_demand_users_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `short_codes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `scheduled_messages`
--
ALTER TABLE `scheduled_messages`
  ADD CONSTRAINT `scheduled_messages_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sender_ids`
--
ALTER TABLE `sender_ids`
  ADD CONSTRAINT `sender_ids_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `short_codes`
--
ALTER TABLE `short_codes`
  ADD CONSTRAINT `short_codes_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `group_contacts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `subscription_users`
--
ALTER TABLE `subscription_users`
  ADD CONSTRAINT `subscription_users_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `short_codes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `top_ups`
--
ALTER TABLE `top_ups`
  ADD CONSTRAINT `top_ups_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `top_ups_ibfk_2` FOREIGN KEY (`package`) REFERENCES `packages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
