<?php

/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
*/

include_once($_SERVER["DOCUMENT_ROOT"] . '/configs/database.php');
include_once($_SERVER["DOCUMENT_ROOT"] . '/configs/functions.php');

$data  = json_decode(file_get_contents('php://input'), true);

$category            = $data["category"];
$phoneNumber         = $data["source"];
$value               = $data["value"];
$acc                 = strtolower($data["clientAccount"]);
$account             = trim($acc);
$providerRefId       = $data["providerRefId"];
$transactionDate     = $data["transactionDate"];
$aitd                = $data["transactionId"];
$status              = $data["status"];
$senderName          = $data["providerMetadata"]["Personal Details"]["First Name"]." ".$data["providerMetadata"]["Personal Details"]["Last Name"];
$date                = date('Y-m-d H:i:s');
// log whole transaction
mysqli_query($conn,
"INSERT INTO `allpayments`(`amount`,`code`,`phonenumber`,`timein`,`account`,`atid`,`name`,`status`,`humandate`)
VALUES('$value','$providerRefId ','$phoneNumber','$transactionDate','$acc','$aitd','$senderName','$status','$date')");


$editamount          = explode(".", $value);
$amountvalue         = $editamount[0];
$removespaces        = str_replace(" ", "", $amountvalue);
$intamount           = (int)str_replace("KES", "", $removespaces);



if ($category == "MobileC2B") 
{
    if($status == "Success")
    {
        // check user account if exists
        $existsArray = array('username'=>$account);
        $userExists  = returnExists('users',$existsArray);
        
        if($userExists > 0)
        {
            if($intamount <=10000)
            {
                $package = 1;
            }else{
                $package = 2;
            }
            // get user id now
            $userId = getByValue('users', 'id', $existsArray);
            $userAC = getByValue('users', 'account_currency', $existsArray);
            $userNm = getByValue('users', 'name', $existsArray);

            switch ($userAC) {
                case 'KES':
                    # code...
                    $entry_amount    = 1 * $intamount;
                    $static_amount   = "KES ".$entry_amount;
                break;
        
                case 'TZS':
                    # code...
                    $entry_amount    = 21 * $intamount;
                    $static_amount   = "TZS ".$entry_amount;
                break;
        
                case 'UGX':
                    # code...
                    $entry_amount    = 35 * $intamount;
                    $static_amount   = "UGX ".$entry_amount;
                break;
        
                case 'MWK':
                    # code...
                    $entry_amount    = 7 * $intamount;
                    $static_amount   = "MWK ".$entry_amount;
                break;
        
                case 'NGN':
                    # code...
                    $entry_amount    = 3.5 * $intamount;
                    $static_amount   = "NGN ".$entry_amount;
                break;
                
                default:
                    # code...
                    $entry_amount    = 0.0098 * $intamount;
                    $static_amount   = "USD ".$entry_amount;
                break;
            }

            // check if user has an active balance, create array
            $userBalanceArray = array('parent' => $userId, 'status' => 'active');

            if(returnExists('top_ups', $userBalanceArray) > 0)
            {
                $packagestatus = "pending";
            }else{
                $packagestatus = "active";
            }

            $type = "Ref: ".$providerRefId;

            mysqli_query($conn, "INSERT INTO 
            `top_ups` (`amount`,`package`,`status`,`description`,`parent`,`static_amount`,`type`,`date_created`)
            VALUES('$entry_amount','$package','$packagestatus','$type','$userId','$static_amount','MPESA','$date')");

            // send sms from ionsolve
            $message      = "Hello ".$userNm.", we have received your account top up of ".$static_amount.". Thank you for choosing Ionsolve.";
            opensendSMS($phoneNumber,$message);
            reconcile($intamount);

        }else{
            // call snitch here
        }
    }
} 
