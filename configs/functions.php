<?php
/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
 */

// Create Search String From Array

require_once 'database.php';

function formSearchString($arguments)
{
    $string = "";
    foreach ($arguments as $key => $value) {
        $string .= "`" . $key . "`=" . "'" . $value . "' && ";
    }
    
    $conditions = substr($string, 0, -3);
    return $conditions;
}

function returnExists($table, $arguments)
{
    global $conn;
    $appendSearch = formSearchString($arguments);
    $formedQuery  = "SELECT * FROM $table WHERE " . $appendSearch . " ORDER BY id DESC";
    $getValues    = mysqli_num_rows(mysqli_query($conn, $formedQuery));
    return $getValues;
}

function getByValue($table, $column, $arguments)
{
    global $conn;
    $appendSearch = formSearchString($arguments);
    $formedQuery  = "SELECT * FROM $table WHERE " . $appendSearch . " ORDER BY id DESC";
    $executeQuery = mysqli_query($conn, $formedQuery);
    if (mysqli_num_rows($executeQuery) > 0) 
    {
        $getValues = mysqli_fetch_array($executeQuery);
        return $getValues[$column];
    } else {
        return false;
    }
}

function returnArrayOfRequest($table,$column,$arguments)
{
    global $conn;
    $appendSearch    = formSearchString($arguments);
    $formedQuery     = "SELECT $column FROM $table WHERE " . $appendSearch;
    $run_Array_fetch = mysqli_query($conn, $formedQuery);
    $getValues       = mysqli_num_rows($run_Array_fetch);

    if($getValues > 0)
    {
        $feedback = "";
        while($array_results = mysqli_fetch_array($run_Array_fetch))
        {
            $feedback .= $array_results[$column].",";
        }
        return substr($feedback,0, -1);
    }else{
        return "0";
    }
    
}


function returnArrayOfAllTable($table,$column)
{
    global $conn;
    $formedQuery     = "SELECT $column FROM $table";
    $run_Array_fetch = mysqli_query($conn, $formedQuery);
    $getValues       = mysqli_num_rows($run_Array_fetch);

    if($getValues > 0)
    {
        $feedback = "";
        while($array_results = mysqli_fetch_array($run_Array_fetch))
        {
            $feedback .= $array_results[$column].",";
        }
        return substr($feedback,0, -1);
    }else{
        return "0";
    }
    
}

function returnArrayOfRequestIN($table,$column,$invalues)
{
    global $conn;
    $formedQuery     = "SELECT $column FROM $table WHERE `id` IN ($invalues)";
    $run_Array_fetch = mysqli_query($conn, $formedQuery);
    $getValues       = mysqli_num_rows($run_Array_fetch);

    if($getValues > 0)
    {
        $feedback = "";
        while($array_results = mysqli_fetch_array($run_Array_fetch))
        {
            $feedback .= $array_results[$column].",";
        }
        return substr($feedback,0, -1);
    }else{
        return "0";
    }
    
}

function returnCountWithCondition($table, $arguments)
{
    global $conn;
    $appendSearch = formSearchString($arguments);
    $result       = mysqli_query($conn, "SELECT COUNT(*) AS `id` FROM $table WHERE " . $appendSearch);
    $row          = mysqli_fetch_assoc($result);
    $count        = $row['id'];
    return $count;
}

function returnMessagesTwoDates($table,$arguments, $startdate, $enddate)
{
    global $conn;
    $appendSearch = formSearchString($arguments);
    $result       = mysqli_query($conn, "SELECT COUNT(*) AS `id` FROM $table WHERE " . $appendSearch." && `date_sent` BETWEEN '$startdate' AND '$enddate'");
    $row = mysqli_fetch_assoc($result);
    $count = $row['id'];
    return $count;
}

function returnSumWithCondition($table, $arguments)
{
    global $conn;
    $appendSearch = formSearchString($arguments);
    $sql          = mysqli_query($conn, "SELECT SUM(amount) as total FROM $table WHERE " . $appendSearch);
    $row          = mysqli_fetch_array($sql);
    $sum          = $row['total'];
    return $sum;
}

function returnCountNoCondition($table)
{
    global $conn;
    $result       = mysqli_query($conn, "SELECT COUNT(*) AS `id` FROM $table");
    $row          = mysqli_fetch_assoc($result);
    $count        = $row['id'];
    return $count;
}

function getManyByValue($table, $column, $arguments)
{
    global $conn;
    $databack     = "";
    $appendSearch = formSearchString($arguments);
    $formedQuery  = "SELECT * FROM $table WHERE " . $appendSearch . " ORDER BY id DESC";
    $executeQuery = mysqli_query($conn, "$formedQuery");
    if (mysqli_num_rows($executeQuery) > 0) 
    {
        while ($getValues = mysqli_fetch_array($executeQuery)) 
        {
            $databack .= $getValues[$column] . ",";
        }
        
        $columnArray = substr($databack, 0, -1);
        return explode(",", $columnArray);
    } else {
        return "No Data Found";
    }
}

function getManyFromTable($table, $column)
{
    global $conn;
    $databack     = "";
    $formedQuery  = "SELECT * FROM $table ORDER BY $column DESC";
    $executeQuery = mysqli_query($conn, "$formedQuery");
    if (mysqli_num_rows($executeQuery) > 0) 
    {
        while ($getValues = mysqli_fetch_array($executeQuery)) 
        {
            $databack .= $getValues[$column] . ",";
        }
        
        $columnArray = substr($databack, 0, -1);
        return explode(",", $columnArray);
    } else {
        return "No Data Found";
    }
}

function sendMessage($numbers, $message, $user_id, $type, $refid, $link, $passedSender)
{
    global $conn;
    // get user credentials
    $get_account_details         = array('id' => $user_id);
    // EDIT ACCOUNT DETAILS
    $x_username      = getByValue('users', 'username', $get_account_details);
    $x_apikey        = getByValue('users', 'api_key', $get_account_details);

    // split job
    $numbers_to_array            = explode(",",$numbers);
    $chuck_array                 = array_chunk($numbers_to_array,4000);

    // track request
    $traceRequest              = 0;
    $endOfRequest              = count($chuck_array);
    // message endpoint
    $messageHost     = "https://ionsolve.com/api/sms/send";

    for($i=0;$i<count($chuck_array);$i++ )
    {
        $send_request  =  implode(",",$chuck_array[$i]);

        $createMessage   = array(
            "phoneNumbers" => $send_request, // multiple contacts comma separated
            "message"      => $message, 
            "senderId"     => $passedSender, // default value is Ionsolve
            "refId"        => $refid,
            "link"         => $link,
            "type"         => $type
        );
    
        // encoding params
        $params          = json_encode($createMessage);
        $req             = curl_init($messageHost);
    
        curl_setopt($req, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($req, CURLOPT_TIMEOUT, 60);
        curl_setopt($req, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($req, CURLOPT_POSTFIELDS, $params);
        curl_setopt($req, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: '.strlen($params),
            'x-api-user: '.$x_username, 
            'x-api-key: '.$x_apikey
        ));
    
        // read api response
        $res              = curl_exec($req);

        $decodeme         = json_decode($res);
    
        curl_close($req);

        if($decodeme->status == 200)
        {
            $traceRequest  = $traceRequest + 1;
            if($traceRequest === $endOfRequest)
            {
                return "<font class='text-success'><br><i class='ion-checkmark-circled'></i> Messages queued. Please view status in <a class='text-primary' href='https://ionsolve.com/dashboard/outbox'>outbox</a></font>";
            }
        }else{
            return "<font class='text-danger'><br>".$decodeme->message."</font>";
        }
    }

}


function sendMessageCron($number, $message, $user_id, $type, $refid, $link, $passedSender)
{
    global $conn;
    $fetch_user_dets = array('id'=>$user_id);


    $x_username      = getByValue('users', 'username', $fetch_user_dets);
    $x_apikey        = getByValue('users', 'api_key', $fetch_user_dets);

    // EDIT MESSAGE PARAMS
    $createMessage   = array(
        "phoneNumbers" => $number, // multiple contacts comma separated
        "message"      => $message, 
        "senderId"     => $passedSender, // default value is Ionsolve
        "refId"        => $refid,
        "link"         => $link,
        "type"         => $type
    );

    // message endoint
    $messageHost     = "https://ionsolve.com/api/sms/send";

    // encoding params
    $params          = json_encode($createMessage);
    $req             = curl_init($messageHost);

    curl_setopt($req, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($req, CURLOPT_TIMEOUT, 60);
    curl_setopt($req, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($req, CURLOPT_POSTFIELDS, $params);
    curl_setopt($req, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: '.strlen($params),
        'x-api-user: '.$x_username, 
        'x-api-key: '.$x_apikey
    ));

    // read api response
    $jsonresponse              = json_decode(curl_exec($req));

    if ($jsonresponse->status == 200) {
        echo "<font class='text-success'><br><i class='ion-checkmark-circled'></i> Messages queued. Please view status in <a class='text-primary' href='https://ionsolve.com/dashboard/outbox'>outbox</a></font>";
    }
    else{
        echo "<font class='text-danger'><br>".$jsonresponse->message."</font>";
    }

    // close curl
    curl_close($req);
}

// Send Ondemand messages

function sendOnDemand($recipients, $shortCodeId, $shortCodeParent, $message, $refid, $linkId, $rowid)
{
    global $conn;
    // get shortcode credentials

    require_once "AfricasTalkingGateway.php";

    $get_shortcode_details         = array('id' => $shortCodeId, 'parent' => $shortCodeParent);
    // get account details
    $savedUser        = getByValue('short_codes', 'at_username', $get_shortcode_details);
    $savedKey         = getByValue('short_codes', 'at_apikey', $get_shortcode_details);
    $keyword          = getByValue('short_codes', 'keyword', $get_shortcode_details);
    $code             = getByValue('short_codes', 'code', $get_shortcode_details);
    $date             = date('Y-m-d');

    $at_username      = encdec($savedUser, $action = 'd');
    $at_apikey        = encdec($savedKey, $action = 'd');
    
    $bulkSMSMode = 1;

    $options = array(
        'keyword'             => $keyword,
        'linkId'              => $linkId,
        'retryDurationInHours' => "5"
    );
            
    $gateway    = new AfricasTalkingGateway($at_username, $at_apikey);
    try 
    { 
        $results = $gateway->sendMessage($recipients, $message, $code, $bulkSMSMode, $options);
        foreach($results as $result) 
        {
            if($result->status == "Success"){
                $status = "replied";
                $reason = "";
            }else{
                $status = "pending";
                $reason = $result->status;
            }

            $date         = date('m/d/Y H:i:s');
            
            # insert into database
            $saveReply     =  "UPDATE `on_demand_users` SET `reply`='$message',`status`='$status',`replied_date`='$date',`reason`='$reason' WHERE `id`='$rowid'";

            if(mysqli_query($conn,$saveReply))
            {
                echo "1";
            }else{
                echo '<font style="color:red"> Something went wrong.</font>';
            }

        }
    }
    catch ( AfricasTalkingGatewayException $e )
    {
        echo $e->getMessage();
        // log on snitch
    }

}

// Send subscription messages

function sendSubscription($recipients, $shortCodeId, $shortCodeParent, $message, $refid)
{
    global $conn;
    // get shortcode credentials

    require_once "AfricasTalkingGateway.php";

    $get_shortcode_details         = array('id' => $shortCodeId, 'parent' => $shortCodeParent);
    // get account details
    $savedUser        = getByValue('short_codes', 'at_username', $get_shortcode_details);
    $savedKey         = getByValue('short_codes', 'at_apikey', $get_shortcode_details);
    $keyword          = getByValue('short_codes', 'keyword', $get_shortcode_details);
    $code             = getByValue('short_codes', 'code', $get_shortcode_details);
    $date             = date('Y-m-d');

    $at_username      = encdec($savedUser, $action = 'd');
    $at_apikey        = encdec($savedKey, $action = 'd');
    
    $bulkSMSMode = 0;
    
    $options = array(
                'keyword'              => $keyword,
                'retryDurationInHours' => "5"
    );
            
    $gateway    = new AfricasTalkingGateway($at_username, $at_apikey);
    try 
    { 
        $results = $gateway->sendMessage($recipients, $message, $code, $bulkSMSMode, $options);
        foreach($results as $result) 
        {
            if($result->status == "Success"){
                $status = "Success";
                $reason = "";
            }else{
                $status = "Failed";
                $reason = $result->status;
            }
            // save in database
            mysqli_query($conn, "INSERT INTO `subscription_outbox`
            (`message`,`phonenumber`,`code`,`keyword`,`refid`,`status`,`reason`,`atrefid`,`date`)
            VALUES('$message','$result->number','$code','$keyword','$refid','$status','$reason','$result->messageId','$date')");
        }
    }
    catch ( AfricasTalkingGatewayException $e )
    {
        echo $e->getMessage();
        // log on snitch

    }
}

function getLastNDays($days, $format = 'd/m')
{
    $m = date("m"); $de= date("d"); $y= date("Y");
    $dateArray = array();
    for($i=0; $i<=$days-1; $i++)
    {
        $dateArray[] = date($format, mktime(0,0,0,$m,($de-$i),$y)); 
    }
    return array_reverse($dateArray);
}

function returnValuesOf($statusmessage,$datethen,$account_id)
{
    global $conn;
    $result       = mysqli_query($conn, "SELECT * FROM `messages` WHERE `date_sent` LIKE '$datethen%' AND `parent`='$account_id' AND `status`='$statusmessage'");
    $getValues    = mysqli_num_rows($result);

    return $getValues;
}

function snitch($report_to, $message, $send_slack, $send_sms, $send_email, $write_to_file)
{
    include_once($_SERVER["DOCUMENT_ROOT"] . '/heartbeat/heartbeat.php');
    
    if ($send_slack == 1) 
    {
        // send to slack api
    }
    
    if ($send_sms == 1) 
    {
        $snitch_to_phone = $reporting[$report_to]['phone'];
        sendMessage($snitch_to_phone, $message, 'TaylorMover');
        opensendSMS($phone,$code,$name);
    }
    
    if ($send_email == 1) 
    {
        // send email
    }
    
    if (!empty($write_to_file)) 
    {
        // handle writing to error files
    }
}

function welcomeEmail($name, $email)
{
    $subject = 'Welcome Message';
    $message = '<html>
        <head>
            <title>Ionsolve </title>
        </head>
            <body>
                <div style="margin:0 auto;  background-color:#fff; font-family:font-family: "Varela Round", sans-serif; border-radius:5px; padding:10px;">

                    <h5 style="color:#000; font-size:20px;">Hello <b>' . $name . '</b>, </h5> <br>
                    Thank you for creating an account with Ionsolve.<br>
                    You got 5 free SMS to test the system.
                </div>
            </body>
    </html>';

  $headers = 'MIME-Version: 1.0' . "\r\n";
  $headers = "Reply-To: Ionsolve <info@ionsolve.com>\r\n"; 
  $headers .= "Return-Path: Ionsolve <info@ionsolve.com>\r\n"; 
  $headers .= "From: Ionsolve <info@ionsolve.com>\r\n";  
  $headers .= "Organization: Ionsolve\r\n";
  $headers .= "MIME-Version: 1.0\r\n";
  $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
  $headers .= "X-Priority: 3\r\n";
  $headers .= "X-Mailer: PHP". phpversion() ."\r\n" ;

  $send = mail($email, $subject, $message, $headers);
}

function openEmail($subject, $email, $message)
{
  $headers = 'MIME-Version: 1.0' . "\r\n";
  $headers .= "Reply-To: Ionsolve <info@ionsolve.com>\r\n"; 
  $headers .= "Return-Path: Ionsolve <info@ionsolve.com>\r\n"; 
  $headers .= "From: Ionsolve <info@ionsolve.com>\r\n";  
  $headers .= "Organization: Ionsolve\r\n";
  $headers .= "MIME-Version: 1.0\r\n";
  $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
  $headers .= "X-Priority: 3\r\n";
  $headers .= "X-Mailer: PHP". phpversion() ."\r\n" ;
  
  $send = mail($email, $subject, $message, $headers);
}

function opensendSMS($phone,$message)
{
    // EDIT ACCOUNT DETAILS
    $x_username      = "root";
    $x_apikey        = "0506b3bbdcb3aaeb5c8e3fa2181efa487df64ede";

    // EDIT MESSAGE PARAMS
    $createMessage   = array(
        "phoneNumbers" => $phone, // multiple contacts comma separated
        "message"      => $message, 
        "senderId"     => "Ionsolve" // default value is Ionsolve
    );

    // message endoint
    $messageHost     = "https://ionsolve.com/api/sms/send";

    // encoding params
    $params          = json_encode($createMessage);
    $req             = curl_init($messageHost);

    curl_setopt($req, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($req, CURLOPT_TIMEOUT, 60);
    curl_setopt($req, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($req, CURLOPT_POSTFIELDS, $params);
    curl_setopt($req, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: '.strlen($params),
        'x-api-user: '.$x_username, 
        'x-api-key: '.$x_apikey
    ));

    // read api response
    $res              = curl_exec($req);

    // close curl
    curl_close($req);
}


// function for testing our system

function testUs($phone,$message)
{
    // EDIT ACCOUNT DETAILS
    $x_username      = "root";
    $x_apikey        = "0506b3bbdcb3aaeb5c8e3fa2181efa487df64ede";

    // EDIT MESSAGE PARAMS
    $createMessage   = array(
        "phoneNumbers" => $phone, // multiple contacts comma separated
        "message"      => $message, 
        "senderId"     => "Ionsolve" // default value is Ionsolve
    );

    // message endoint
    $messageHost     = "https://ionsolve.com/api/sms/send";

    // encoding params
    $params          = json_encode($createMessage);
    $req             = curl_init($messageHost);

    curl_setopt($req, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($req, CURLOPT_TIMEOUT, 60);
    curl_setopt($req, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($req, CURLOPT_POSTFIELDS, $params);
    curl_setopt($req, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: '.strlen($params),
        'x-api-user: '.$x_username, 
        'x-api-key: '.$x_apikey
    ));

    // read api response
    $res              = json_decode(curl_exec($req), true);

    return $res;

    // close curl
    curl_close($req);
}


// admin test a sender id

function testSender($phone, $message, $owner, $senderid)
{
    $user_fetch = array('id'=>$owner);
    // EDIT ACCOUNT DETAILS
    $x_username      = getByValue('users', 'username', $user_fetch);
    $x_apikey        = getByValue('users', 'api_key', $user_fetch);

    // EDIT MESSAGE PARAMS
    $createMessage   = array(
        "phoneNumbers" => $phone, // multiple contacts comma separated
        "message"      => $message, 
        "senderId"     => $senderid
    );

    // message endoint
    $messageHost     = "https://ionsolve.com/api/sms/send";

    // encoding params
    $params          = json_encode($createMessage);
    $req             = curl_init($messageHost);

    curl_setopt($req, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($req, CURLOPT_TIMEOUT, 60);
    curl_setopt($req, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($req, CURLOPT_POSTFIELDS, $params);
    curl_setopt($req, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: '.strlen($params),
        'x-api-user: '.$x_username, 
        'x-api-key: '.$x_apikey
    ));

    // read api response
    $res              = json_decode(curl_exec($req), true);

    return $res;

    // close curl
    curl_close($req);
}


function sendMessageAdmin($phone,$message)
{
    // EDIT ACCOUNT DETAILS
    $x_username      = "root";
    $x_apikey        = "0506b3bbdcb3aaeb5c8e3fa2181efa487df64ede";

    // EDIT MESSAGE PARAMS
    $createMessage   = array(
        "phoneNumbers" => $phone, // multiple contacts comma separated
        "message"      => $message, 
        "senderId"     => "Ionsolve" // default value is Ionsolve
    );

    // message endoint
    $messageHost     = "https://ionsolve.com/api/sms/send";

    // encoding params
    $params          = json_encode($createMessage);
    $req             = curl_init($messageHost);

    curl_setopt($req, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($req, CURLOPT_TIMEOUT, 60);
    curl_setopt($req, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($req, CURLOPT_POSTFIELDS, $params);
    curl_setopt($req, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: '.strlen($params),
        'x-api-user: '.$x_username, 
        'x-api-key: '.$x_apikey
    ));

    // read api response
    $res              = curl_exec($req);

    $jsondecode = json_decode($res);

    if($jsondecode->status == 200)
    {
        $traceRequest  = $traceRequest + 1;
        if($traceRequest === $endOfRequest)
        {
            return "<font class='text-success'><br><i class='ion-checkmark-circled'></i> Messages queued. Please view status in <a class='text-primary' href='../admin/dashboard/sendall/'>outbox</a></font>";
        }
    }else{
        return "<font class='text-danger'><br>".$decodeme->message."</font>";
    }

    // close curl
    curl_close($req);
}


function insertRecord($contact,$phone,$tags,$account_id)
{
    global $conn;
     // sanitize variables
     $contact_        = mysqli_real_escape_string($conn, $contact);
     $phone_          = mysqli_real_escape_string($conn, $phone);
     $tags_           = mysqli_real_escape_string($conn, $tags);

     // validate empty fields
     if (empty($contact_) OR empty($phone_))
     {
         echo '<font style="color:red">Please fill all fields.</font>';
     }

     if (strpos($phone_, '+') === false) 
     {
         echo '<font style="color:red">The phone number "'.$phone_.'" should be in international format starting with +</font>';
     }

     if(!empty($tags_))
    {
        if (strpos($tags_, ',') !== false) 
        {
            echo '<font style="color:red">Tags should not contain commas</font>';
        }
    }
     
     # check duplicate
     $verify_contact = mysqli_query($conn, "SELECT * FROM `contacts` WHERE `phone_number`='$phone_' AND `parent`='$account_id'");
     $date         = date('Y-m-d');

     if (mysqli_num_rows($verify_contact) == 0)
     {
        # insert into database
        $saveContact     =  "
        INSERT INTO `contacts`(`contact_name`,`phone_number`,`date_created`,`tags`,`parent`) 
                        VALUES('$contact_','$phone_','$date','$tags_','$account_id')";

        mysqli_query($conn,$saveContact);

     } 
 }

function encdec($string, $action = 'e') 
{
    $secret_key = 'natashanjokinyambura';
    $secret_iv  = 'njokikanyimaria';
 
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash( 'sha256', $secret_key );
    $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
 
    if( $action == 'e' ) {
        $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
    }
    else if( $action == 'd' ){
        $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
    }
 
    return $output;
}


function reconcile($amount)
{
    require_once('AfricasTalkingGateway.php');
    $use = array(
        "ionsms" => array(
            "username" => "ionsms",
            "apikey" => "e11579457530057781f4d7afbbd16406e064f3bf4583c94e852ffe41763c5804",
            "productname" => "ionsms"
        )
    );
    $provider = "MPESA";
    $baseroot     = "ionsms";
    $destinationChannel = "525900";
    $destinationAccount = "ionsms.api";
    $transferType = "BusinessPayBill";

    $providerData = array('provider' => $provider,
                        'destinationChannel' => $destinationChannel,
                        'destinationAccount' => $destinationAccount,
                        'transferType' => $transferType);

    // Specify the metadata options. These data will be sent to you in a notification when payment has been made
    $metadata = array('account' => "RC",
                    'uid' => "acc_uid");

    $gateway      = new AfricasTalkingGateway($use[$baseroot]['username'], $use[$baseroot]['apikey']);
    $productName  = $use[$baseroot]['productname'];
    $currencyCode = "KES";

    try 
    { 
        $results = $gateway->mobilePaymentB2BRequest($productName, $providerData, $currencyCode, $amount, $metadata);
    }
    catch ( AfricasTalkingGatewayException $e )
    {
        // echo "Encountered an error while sending: ".$e->getMessage();
    }
  
}