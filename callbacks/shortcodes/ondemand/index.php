<?php
/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
*/

include_once($_SERVER["DOCUMENT_ROOT"] . '/configs/database.php');
include_once($_SERVER["DOCUMENT_ROOT"] . '/configs/functions.php');

$passedToken = $_GET['token'];

$tokenExists = array('token' => $passedToken, 'status'=> 'active');

if(returnExists('short_codes', $tokenExists) > 0)
{
    // get account details
    $date                                = date('Y-m-d');
    $shortcode_id                        = getByValue('short_codes', 'id', $tokenExists);
    $shortcode_subscriptionOndemand      = getByValue('short_codes', 'subscriptionOndemand', $tokenExists);

    $phoneNumber                         = $_POST['from'];
    $shortCode                           = $_POST['to'];
    $text                                = $_POST['text'];
    $date                                = $_POST['date'];
    $id                                  = $_POST['id'];

    $linkId                              = $_POST['linkId']; 

    if($shortcode_subscriptionOndemand == 'ondemand') 
    {
        // insert record to database
        mysqli_query($conn,
        "INSERT INTO `on_demand_users`(`phonenumber`,`text`,`linkid`,`date`,`status`,`reply`,`at_referenceid`,`parent`)
        VALUES('$phoneNumber','$text','$linkId','$date','pending','','$id','$shortcode_id')");
    }

}else{
    // report to snitch
}
    
    
