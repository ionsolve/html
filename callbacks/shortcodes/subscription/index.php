<?php

/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
*/

include_once($_SERVER["DOCUMENT_ROOT"] . '/configs/database.php');
include_once($_SERVER["DOCUMENT_ROOT"] . '/configs/functions.php');
date_default_timezone_set('Africa/Nairobi');

$passedToken = $_GET['token'];

$tokenExists = array('token' => $passedToken, 'status'=> 'active');

if(returnExists('short_codes', $tokenExists) > 0)
{
    // get account details
    $date                        = date('Y-m-d H:i:s');
    $shortcode_id                = getByValue('short_codes', 'id', $tokenExists);
    $shortcode_parent            = getByValue('short_codes', 'parent', $tokenExists);
    $shortcode_keyword           = getByValue('short_codes', 'keyword', $tokenExists);
    $shortcode_automessage       = getByValue('short_codes', 'automessage', $tokenExists);

    $phoneNumber                 = "+254".substr($_POST['phoneNumber'],-9);
    $shortCode                   = $_POST['shortCode'];
    $keyword                     = $_POST['keyword'];
    $updateType                  = $_POST['updateType'];


    if($updateType == "Addition" OR $updateType == "addition") 
    {
        // insert record to database
        mysqli_query($conn,
        "INSERT INTO `subscription_users`(`phonenumber`,`date_joined`,`parent`)
        VALUES('$phoneNumber','$date','$shortcode_id')");

        $refid = "isc_".uniqid();

        // send welcome message
        sendSubscription($phoneNumber, $shortcode_id, $shortcode_parent, $shortcode_automessage, $refid);
    }

    elseif($updateType == "Deletion" OR $updateType == "deletion") 
    {
        // remove record from database
        mysqli_query($conn, 
        "DELETE FROM `subscription_users` WHERE `phonenumber`='$phoneNumber' and `parent`='$shortcode_id'");
    }
}else{
    // report to snitch
}
    
    
