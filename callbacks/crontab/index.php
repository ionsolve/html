<?php
/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
*/

include_once($_SERVER["DOCUMENT_ROOT"] . '/configs/database.php');
include_once($_SERVER["DOCUMENT_ROOT"] . '/configs/functions.php');

date_default_timezone_set('Africa/Nairobi');

$datenow   = date('Y-m-d H:i');

$now       = strtotime($datenow);

$arguments = array('status' => 'pending');

$allPendingSubscription = returnArrayOfRequest('scheduled_smessages','id',$arguments);
$explodeSubscriptions   = explode(",",$allPendingSubscription);

foreach($explodeSubscriptions as $individualSend)
{   
    // fetch from subscriptions
    $firstArgs       = array('id' => $individualSend);

    $sendtime        = getByValue('scheduled_smessages', 'sendtime', $firstArgs);

    if($sendtime < $now)
    {
        $messageToSend   = getByValue('scheduled_smessages', 'message', $firstArgs);
        $refId           = getByValue('scheduled_smessages', 'refid', $firstArgs);
        $parentShortcode = getByValue('scheduled_smessages', 'parent', $firstArgs);
        
        // fetch from shortcode table
        $secondArgs      = array('id' => $parentShortcode);

        $theShortId      = getByValue('short_codes', 'id', $secondArgs);
        $theShortParent  = getByValue('short_codes', 'parent', $secondArgs);

        // fetch from subscribers
        $thirdArgs       = array('parent' => $theShortId);

        $subscribers     = returnArrayOfRequest('subscription_users','phonenumber',$thirdArgs);

        sendSubscription($subscribers, $theShortId, $theShortParent, $messageToSend, $refId);

        mysqli_query($conn,"UPDATE `scheduled_smessages` SET `status`='sent' WHERE `id`='$individualSend'");
    }
}


