<?php

/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
*/

session_start();

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport">
	<meta content="" name="description">
	<meta content="" name="Ionsolve">
	<link href="../assets/img/logo_small.png" rel="icon" type="image/x-icon">
	<title>IonSolve : About Us</title>
	<link href="../assets/css/app.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
</head>
<body class="loaded">
	
	<div class="paper-loading" id="app">
		
		<?php include_once '../com_header_light.php'; ?>

		<main>
			<section class="testimonials animatedParent animateOnce">
				<div class="container animated fadeInUpShort">
                    <header class="section-heading p-t-20">
						<h3 class="font-weight-bolder">About Us</h3>
						<h5 style="line-height:30px;" class="stylemuted">Ionsolve is the best sms dashboard that gives you amazing features to 
                            <br>help you manage communication with your clients.</h5>
					</header>
					
				</div>
				<img src="../assets/img/dummy/userss3.png" style="width:100%;"/>
				
            </section>
            
            <section class="p-t-b-80 indigo text-white animatedParent animateOnce">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="b-r-o">
                                <label class="badge light-blue s-18 thin no-m">WHO WE ARE</label>
                                <h2 class="s-36 thin">Building the best communication channels is the core of a good business.</h2>
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <p class="s-18 p-t-40 thin">Our dashboard and APIs give you a new look into how you should communicate with your clients. 
							We believe that enabling you get a reliable communication channel helps you build a better relationship with your existing and potential clients, that is why we do it;for you!</p>
							<a class="s-18" href="https://ionsolve.com/account"><i class="ion-ios-arrow-thin-right"></i> Visit our dashboard</a>
                        </div>
                    </div>
                </div>
            </section>
            
		</main>
		
		<?php include_once '../com_footer.php'; ?>

	</div>
	
	<script src="../assets/js/app.js"></script>
	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-114404103-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-114404103-1');
</script>
</body>
</html>