<?php

/**
 * 2017 Ionsolve Limited
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  @author    Ionsolve Limited.
 *  @copyright 2017 Ionsolve Limited.
 *  @license   http://www.apache.org/licenses/LICENSE-2.0
*/

include_once($_SERVER["DOCUMENT_ROOT"] . '/system/core/index.php');

if($account_status == 'active'){
    header('../../dashboard');
}

?>




<!DOCTYPE html>
<html class="" lang="en">
<head>
	<meta charset="utf-8">
	<title>Verify</title>
	<meta content="" name="description">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport">
	<link href="../css/app.v1.css" rel="stylesheet" type="text/css">
	<link href="../images/logo_small.png" rel="icon" type="image/x-icon">
    <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"/>
    <!--[if lt IE 9]> <script src="js/ie/html5shiv.js"></script> <script src="js/ie/respond.min.js"></script> <script src="js/ie/excanvas.js"></script> <![endif]-->
</head>
<body class="">
	<section class="m-t-lg wrapper-md animated fadeInUp" id="content">
		<div class="container aside-xl">
			<a class="block text-center" href="https://ionsolve.com"><img src="../images/logo.png" style="width:150px;"/></a>
			<section class="m-b-lg">
				<header class="wrapper text-center">
					<strong id="verifyresponse">Verify Account</strong>
                    
				</header>
				
					<div class="list-group">
						<div class="list-group-item">
							<input class="form-control no-border" id="_verifycode_" placeholder="Verification Code" type="number">
						</div>
					</div>

                    <button id="verifybtn" class="btn btn-lg btn-primary btn-block" type="submit">Verify</button>

					<div class="line line-dashed"></div>
                    <p class="text-center">
                    <?php
                    	if(isset($_SESSION['alphaphone'])){
                    ?>
                        Confirmation code was sent to <b><?php echo $_SESSION['alphaphone']; ?></b><br><br>
                    <?php
                    	}
                    ?>
                        <a href="#" class="text-primary" id="resendcode">
                            Resend Code <i class="ion-ios-arrow-thin-right">&nbsp;</i>
                        </a>
						<a href="/dashboard/signout" class="text-primary">
                            Sign Out <i class="ion-ios-arrow-thin-right">&nbsp;</i>
                        </a>
                    </p><br>
                    <a class="btn btn-lg btn-default btn-block" href="../login/">Login</a>
				
			</section>
		</div>
	</section><!-- footer -->
	<footer id="footer">
		<div class="text-center padder">
			<p><small>Ionsolve<br>
			&copy; 2018</small></p>
		</div>
	</footer><!-- / footer --><!-- Bootstrap --><!-- App -->
	<script src="../js/app.v1.js"></script> 
	<script src="../js/app.plugin.js"></script>
    <script src="../js/main.js"></script>
</body>
</html>