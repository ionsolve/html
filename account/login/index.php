<?php

session_start();

if(isset($_SESSION['alphaion']))
{
    header('location:../../dashboard/');
}

?>
<!DOCTYPE html>
<html class="" lang="en">
<head>
	<meta charset="utf-8">
	<title>Login</title>
	<meta content="" name="description">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport">
	<link href="../css/app.v1.css" rel="stylesheet" type="text/css">
	<link href="../images/logo_small.png" rel="icon" type="image/x-icon">
    <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"/>
    <!--[if lt IE 9]> <script src="js/ie/html5shiv.js"></script> <script src="js/ie/respond.min.js"></script> <script src="js/ie/excanvas.js"></script> <![endif]-->
</head>
<body class="">
	<section class="m-t-lg wrapper-md animated fadeInUp" id="content">
		<div class="container aside-xl">
			<a class="block text-center" href="https://ionsolve.com"><img src="../images/logo.png" style="width:150px;"/></a>
			<section class="m-b-lg">
				<header class="wrapper text-center">
					<strong id="loginresponse">Login to your account</strong>
				</header>
				
					<div class="list-group">
						<div class="list-group-item">
							<input class="form-control no-border" id="l_email_" placeholder="Email" type="email">
						</div>
						<div class="list-group-item">
							<input class="form-control no-border" id="l_password_" placeholder="Password" type="password">
						</div>
					</div>

					<button class="btn btn-lg btn-primary btn-block" id="loginbtn">Sign in</button>

					<div class="text-center m-t m-b">
						<a  class="text-primary" href="../reset/">Reset password  <i class="ion-ios-arrow-thin-right">&nbsp;</i></a>
					</div>

					<div class="line line-dashed"></div>

					<p class="text-muted text-center"><small>Do not have an account?</small></p>
					<a class="btn btn-lg btn-default btn-block" href="../signup/">Create an account</a>
				
			</section>
		</div>
	</section><!-- footer -->
	<footer id="footer">
		<div class="text-center padder">
			<p><small>Ionsolve<br>
			&copy; 2018</small></p>
		</div>
	</footer><!-- / footer --><!-- Bootstrap --><!-- App -->
	<script src="../js/app.v1.js"></script> 
	<script src="../js/app.plugin.js"></script>
	<script src="../js/main.js"></script>
	<script>
		$(document).ready(function(){
			$('#l_email_').val("");
			$('#l_password_').val("");
		});
	</script>
</body>
</html>