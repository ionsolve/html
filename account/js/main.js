$(document).ready(function()
{
	$('#_new_account_').click(function()
	{
		$('#_new_account_').html('<i class="fa ion-load-c fa-spin"></i>');
		$('#_new_account_').css('disabled','1');

		var _name_        = $('#_name_').val();
		var _email_       = $('#_email_').val();
		var _phone_       = $('#_phone_').val();
		var _username_    = $('#_username_').val();
		var _country_     = $('#_country_').val();
		var _password_    = $('#_password_').val();
		
		$.post("../../system/signup/",
			{
				_name_:_name_,
				_email_:_email_,
				_phone_:_phone_,
				_username_:_username_,
				_country_:_country_,
				_password_:_password_
			},
			function(ajaxresponse)
			{
				if(ajaxresponse == 1)
				{
					window.location.href = '../verify/';
				}else{
					$('#signresp').html(ajaxresponse);
					$('#_new_account_').html('Sign Up');
					$('#_new_account_').css('disabled','0');
				}
			});
	});

	// Resend Code

	$('#resendcode').click(function()
	{
		$('#resendcode').html('<i class="fa ion-load-c fa-spin"></i>');

		$.post("../../system/resend/",

		function(resendresponse)
		{
			$('#resendcode').html('<p class="text-success"><i class="ion-checkmark-circled"></i> Activation Code Resent</p>');
		});
	});

	// Confirm Account

	$('#verifybtn').click(function()
	{
		$('#verifybtn').html('<i class="fa ion-load-c fa-spin"></i>');
		$('#verifybtn').css('disabled','1');

		var _verifycode_   = $('#_verifycode_').val();

		$.post("../../system/confirm/",
		{
			_verifycode_:_verifycode_
		},
		function(confirmresponse)
		{
			if (confirmresponse == 1)
			{
				window.location.href = "../../dashboard/";
			}else{
				$('#verifyresponse').html(confirmresponse);
				$('#verifybtn').css('disabled','0');
				$('#verifybtn').html('Verify');
			}
		});
	});


	// Login

	$('#loginbtn').click(function()
	{
		$('#loginbtn').html('<i class="fa ion-load-c fa-spin"></i>');
		$('#loginbtn').css('disabled','1');

		var l_email_      = $('#l_email_').val();
		var l_password_   = $('#l_password_').val();

		$.post("../../system/login/",
		{
			l_email_:l_email_,
			l_password_:l_password_
		},
		function(loginresponse)
		{
			if (loginresponse == 1)
			{
				window.location.href = "../../dashboard/";
			}else{
				$('#loginresponse').html(loginresponse);
				$('#loginbtn').css('disabled','0');
				$('#loginbtn').html('Verify');
			}
		});
	});

	// Reset Account

	$('#resetbtn').click(function()
	{
		$('#resetbtn').html('<i class="fa ion-load-c fa-spin"></i>');
		$('#resetbtn').css('disabled','1');

		var r_email_      = $('#r_email_').val();

		$.post("../../system/reset/",
		{
			r_email_:r_email_
		},
		function(resetresponse)
		{
			if (resetresponse == 1)
			{
				$('#resetbtn').css('disabled','0');
				$('#resetbtn').html('<i class="ion-checkmark-circled"></i> Password Reset');
				$('#resetbtn').attr('class','btn btn-lg btn-success btn-block');
			}else{
				$('#resetresponse').html(resetresponse);
				$('#resetbtn').css('disabled','0');
				$('#resetbtn').html('Verify');
			}
		});
	});

	

});
